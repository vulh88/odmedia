<?php

namespace Modules;

use Illuminate\Routing\Controller;

/**
 * @SWG\Swagger(
 *     basePath="/api/v1",
 *     schemes={"http", "https"},
 *     host=L5_SWAGGER_CONST_HOST,
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="ODMedia API Documentation",
 *         description="ODMedia Documentation",
 *         @SWG\Contact(
 *             email="vulh@elinext.com"
 *         ),
 *     )
 * )
 */

class ApiController extends Controller
{
	// Uniform the response
	public function apiRes($status, $message, $data, $error, $code, $datatype = 1)
	{
		$apiResp = [];
		$apiResp['code_token'] = 1;
		$apiResp['datatype'] = $datatype;
		$apiResp['status'] = $status ? 200 : 404;
		$apiResp['data'] = $data;
		$apiResp['msg'] = $message;
		$apiResp['errors'] = $error;
		
		return response($apiResp, $code ? $code : 200);
	}
	
	// Simple response success with data
	public function apiOk($data, $datatype = 1)
	{
		return $this->apiRes(true, 'success', $data, null, null, $datatype);
	}
	
	// Some of common errors resposnse
	public function apiError($msg)
	{
		return $this->apiRes(false, $msg, null, null, null);
	}
	
	public function apiErrorWithCode($msg, $code)
	{
		return $this->apiRes(false, $msg, null, null, $code);
	}
	
	public function apiErrorDetails($msg, $errors, $code)
	{
		return $this->apiRes(false, $msg, null, $errors, $code);
	}
}
