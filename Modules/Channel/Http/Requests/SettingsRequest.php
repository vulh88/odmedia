<?php

namespace Modules\Channel\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           // 'upload_extensions' => 'required|string|max:255',
            'maximum_upload' => 'integer|min:0|max:'. config('filesystems.odmedia.upload_max_size'),
            'content_type_extensions' => 'required|string|max:255',
            'overlay_type_extensions' => 'required|string|max:255',
        ];
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge(
            array_fill_keys(
                [
                    'maximum_upload.min',
                    'maximum_upload.max',
                ],
                __('Maximum upload only be 0 to ' . config('filesystems.odmedia.upload_max_size') . ' MB')
            )
        );
    }
}
