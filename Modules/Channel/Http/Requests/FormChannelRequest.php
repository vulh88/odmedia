<?php

namespace Modules\Channel\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormChannelRequest extends FormRequest
{

    private $rules = [
        'name' => 'required|max:255',
        'playout_control_port' => 'integer',
        'needs_content_download' => 'integer|min:0|max:1',
        'resource_path' => 'string|max:255',
        'thumbnail_storage' => 'string|max:255',
        'resource_folder_id' => 'integer|min:0',
        'wipe_id' => 'integer|min:1|nullable',
        'enable_timeline' => 'integer|min:0|max:1',
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->rules;
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge(
            [
                'id.exists' => 'Can not find data with id ' . $this->channel
            ],
            array_fill_keys(
                [
                    'needs_content_download.min',
                    'needs_content_download.max',
                    'enable_timeline.min',
                    'enable_timeline.max',
                ],
                __('Can only be 0 or 1. If empty, default 0')
            )
        );
    }
}
