<?php
namespace Modules\Channel\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;
use SebastianBergmann\Exporter\Exporter;

class ReportController extends Controller
{
    protected $csv;

    protected $fileName;

    public function __construct(Exporter $csv)
    {
        $this->csv = $csv;
        $this->fileName = 'played_resource_' . date('M');
    }

    /**
     * Monthly report
     * @param string $format
     * @return mixed
     */
    public function monthlyReport($format = 'csv')
    {
        $mockData = [
            [
                'ID'    => 1,
                'resource_name' => 'ONS_000051_S01E01_Lone_Ranger_ENG_169PB_PAL-tim.mp4',
                'played_times'  => 10,
                'air_date'  => '2018-06-10 15:00:00'
            ],
            [
                'ID'    => 2,
                'resource_name' => 'ONS_000055_John_Wayne_Desert_Trail_ENG_169PB_PAL-tim.mp4',
                'played_times'  => 11,
                'air_date'  => '2018-06-10 15:00:00'
            ],
            [
                'ID'    => 1,
                'resource_name' => 'ONS_gary_jenkins_afl09.mp4',
                'played_times'  => 10,
                'air_date'  => '2018-06-10 15:00:00'
            ],
            [
                'ID'    => 1,
                'resource_name' => 'PassieTV Bug',
                'played_times'  => 10,
                'air_date'  => '2018-06-10 15:00:00'
            ],
            [
                'ID'    => 1,
                'resource_name' => 'Bang me, doctor!',
                'played_times'  => 10,
                'air_date'  => '2018-06-10 15:00:00'
            ],
            [
                'ID'    => 1,
                'resource_name' => 'Curvy Latina',
                'played_times'  => 10,
                'air_date'  => '2018-06-10 15:00:00'
            ],
            [
                'ID'    => 1,
                'resource_name' => 'Sister-in-law',
                'played_times'  => 10,
                'air_date'  => '2018-06-10 15:00:00'
            ],

        ];

        $excel = Excel::create($this->fileName, function (LaravelExcelWriter $csv) use ($mockData) {
            $csv->sheet($this->fileName, function ($sheet) use ($mockData) {
                $sheet->fromArray($mockData);
            });
        })->store($format);

        if (!file_exists(storage_path('exports/'. $this->fileName . '.csv'))) {
            return "Send mail error!";
        }

        $data = ['month' => date('M')];
        Mail::send('channel::emails.report-monthly', $data, function($message) {
            $message->to('vulh@elidev.info')->subject('Report on ' . date('M'));
            $message->attach(storage_path('exports/'. $this->fileName . '.csv'));
            $message->from('thanh.ldt92@gmail.com','Thanh Le');
        });

        $excel->export($format);
        return "Email Sent with attachment. Check your inbox.";
    }
}