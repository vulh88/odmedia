<?php

namespace Modules\Channel\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Channel\Repositories\Contracts\ChannelRepositoryInterface;
use Modules\Channel\Transformers\ChannelCollection;
use Modules\Channel\Http\Requests\FormChannelRequest;
use Modules\Channel\Transformers\ChannelResource;
use Modules\Channel\Services\DeleteChannelService;
use Modules\Resource\Repositories\Contracts\ResourceFolderRepositoryInterface;
use Modules\Resource\Repositories\Contracts\ResourceRepositoryInterface;
use Modules\User\Repositories\Contracts\ChannelUsersRepositoryInterface;

class ChannelController extends Controller
{
    /**
     * Get list of channel.
     *
     * @param ChannelRepositoryInterface $channelRepository
     * @param Request $request
     * @return ChannelCollection
     *
     * @SWG\Get(
     *     path="/channels",
     *     tags={"Channels"},
     *     operationId="getChannel",
     *     description="Gets channel items",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="keyword", in="query", type="string", description="Search by name"),
     *
     *     @SWG\Parameter(name="sort", in="query", type="string", description="Field to sort by, if prefixed with '-', the sorting direction will be descending"),
     *     @SWG\Parameter(name="page", in="query", type="integer", description="Number of page used to pagination"),
     *     @SWG\Parameter(name="count", in="query", type="integer", description="Number of records to display"),
     *     @SWG\Parameter(name="assigned_channels", in="query", type="integer", description="Only get assigned channel"),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *         @SWG\Header(header="X-Total", type="integer", description="Total count of channel"),
     *         @SWG\Header(header="X-Skipped", type="integer", description="Channel skipped"),
     *         @SWG\Header(header="X-Limit", type="integer", description="Channel limit")
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function index(ChannelRepositoryInterface $channelRepository, Request $request)
    {
        $params = $request->all();
        if (!empty($params['assigned_channels'])) {
            $params['user_id'] = Auth::user()->id;
        }

        $columns = ['channels.id', 'channels.name', 'channels.playout_control_port', 'channels.needs_content_download', 'channels.wipe_id', 'channels.root_resource_folder_id', 'channels.enable_timeline'];
        $results = $channelRepository->search($columns, $params);
        return new ChannelCollection($results->getCollection(), $results->total(), $results->currentPage(), $results->lastPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Create a channel
     *
     * @param ChannelRepositoryInterface $channelRepository
     * @param FormChannelRequest  $request
     * @param ResourceFolderRepositoryInterface $resourceFolderRepository
     * @return ChannelResource
     * @SWG\Post(
     *     path="/channels",
     *     tags={"Channels"},
     *     operationId="postCreateChannel",
     *     description="Creates new (or updates existing) channel.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Resource folder object that needs to be created",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/ChannelRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successfully created resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of created channel"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function store (
        ChannelRepositoryInterface $channelRepository,
        FormChannelRequest $request,
        ResourceFolderRepositoryInterface $resourceFolderRepository
    )
    {
        $data = $request->validated();
        $channel = $channelRepository->createChannel($data, $resourceFolderRepository);
        if ($channel) {
            /**
             * create channel storage
             */
            $resourcePath = createChannelStorage($channel->id, $channel->name);
            $channelRepository->update([
                'resource_path' => $resourcePath['resource_path'],
                'thumbnail_storage' => $resourcePath['thumbnail_storage']
            ], $channel->id);
        }
        return new ChannelResource($channel);
    }

    /**
     * @param $id
     * @param ChannelRepositoryInterface $channelRepository
     * @return ChannelResource
     */
    public function show($id, ChannelRepositoryInterface $channelRepository)
    {
        $data = $channelRepository->findOrFail($id);
        return new ChannelResource($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Updates a channel
     *
     * @param int  $id
     * @param ChannelRepositoryInterface $channelRepository
     * @param FormChannelRequest $request
     * @return \Illuminate\Http\JsonResponse|ChannelResource
     * @SWG\Put(
     *     path="/channels/{id}",
     *     tags={"Channels"},
     *     operationId="putUpdateChannel",
     *     description="Creates new (or updates existing) channel.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     * 			name="id",
     * 			in="path",
     * 			required=true,
     * 			type="integer",
     * 			description="UUID",
     * 		),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Resource folder object that needs to be Updated",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/ChannelRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successfully updated resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of updated"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function update($id, ChannelRepositoryInterface $channelRepository, FormChannelRequest $request)
    {
        $data = $request->validated();
        $data['user_id'] = $request->user()->id;
        /**
         * create channel storage
         */
        $resourcePath = createChannelStorage($id, $data['name']);
        $data['resource_path'] = $resourcePath['resource_path'];
        $data['thumbnail_storage'] = $resourcePath['thumbnail_storage'];
        return new ChannelResource($channelRepository->update($data, $id));
    }

    /**
     * Delete existing channel.
     *
     * @param int $id
     * @param DeleteChannelService $service
     * @return mixed
     * @SWG\Delete(
     *     path="/channels/{id}",
     *     tags={"Channels"},
     *     operationId="deleteResourceType",
     *     description="Delete existing channel",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of channel that should be deleted",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of deleted channel"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Channel not found.",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function destroy($id, DeleteChannelService $service)
    {
        if ( $service->destroy($id) ) {
            return response()->noContent();
        }

        return response()->error(__('This channel has already destroyed permanently or channel was not in trash or channel does not exist in our system. Please check the channel ID.'), 404);
    }


    /**
     * Restore a channel.
     *
     * @param int $id
     * @param DeleteChannelService $service
     * @return mixed
     * @SWG\Put(
     *     path="/channels/{id}/restore",
     *     tags={"Channels"},
     *     operationId="restoreChannel",
     *     description="Restore a channel from the trash",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of channel that should be restored",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of deleted channel"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Channel not found.",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function restore($id, DeleteChannelService $service)
    {
        if ( $service->restore($id) ) {
            return response()->noContent();
        }

        return response()->error(__('This channel has not existed in the trash yet'), 404);
    }

    /**
     * Delete a channel temporarily. (Move to trash)
     *
     * @param int $id
     * @param DeleteChannelService $service
     * @param ChannelUsersRepositoryInterface $channelUsersRepository
     * @param Request $request
     * @return mixed
     * @SWG\Put(
     *     path="/channels/{id}/trash",
     *     tags={"Channels"},
     *     operationId="trashChannel",
     *     description="Delete a channel from the trash",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of channel that should be trashed",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of deleted channel"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Channel not found.",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function trash($id, DeleteChannelService $service, ChannelUsersRepositoryInterface $channelUsersRepository, Request $request)
    {
        if ( $service->softDelete($id) ) {
            /**
             * Remove item in channel user
             */
            $channelUsersRepository->deleteChannel(['user_id' => $request->user()->id, 'channel_id' => $id]);
            return response()->noContent();
        }

        return response()->error(__('This channel has already contained in the trash or it does not exist in our system. Please check the channel ID.'), 404);
    }

    /**
     * Reset premiere
     *
     * @param $id
     * @param ChannelRepositoryInterface $channelRepository
     * @param ResourceRepositoryInterface $resourceRepository
     * @return mixed
     * @SWG\Put(
     *     path="/channels/{id}/reset-premiere",
     *     tags={"Channels"},
     *     operationId="resetPremiereChannel",
     *     description="Reset premiere from channel ID",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of channel",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of channel"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Can not reset the premiere of the resource",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function resetPremiere($id, ChannelRepositoryInterface $channelRepository, ResourceRepositoryInterface $resourceRepository)
    {
        $channelRepository->findOrFail($id);
        if ( $resourceRepository->updatePremiereByChannel($id) ) {
            return response()->noContent();
        }

        return response()->error(__("Can not reset the premiere of the resource. Maybe this channel doesn't have any resources"), 404);
    }
}
