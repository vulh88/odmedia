<?php

namespace Modules\Channel\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Setting;
use Modules\Channel\Http\Requests\SettingsRequest;

class SettingsController extends Controller
{

    /**
     * Get system settings
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSystemSettings()
    {
        $contentExts = explode(',', Setting::get('content_type_extensions', 'mp4'));
        $overlayExts = explode(',', Setting::get('overlay_type_upload', 'mp4'));
        $allExts = array_merge($contentExts, $overlayExts);
        foreach($allExts as $key =>  $ext) {
            $allExts[$key] = '.' . trim($ext);
        }
        $allAllowExts = implode(', ', $allExts);
        return response()->json([
            'upload_extensions' => Setting::get('upload_extensions', 'jpg, gif, jpeg, png, mp3, mp4'),
            'maximum_upload' => Setting::get('maximum_upload', '10'),
            'content_type_extensions' => Setting::get('content_type_extensions', 'mp4'),
            'overlay_type_extensions' => Setting::get('overlay_type_upload', 'mov'),
            'common_allow_extensions' => $allAllowExts,
        ]);
    }

    /**
     * Set system settings
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setSystemSettings(SettingsRequest $request)
    {
        Setting::set('maximum_upload', $request['maximum_upload']);
        Setting::set('content_type_extensions', $request['content_type_extensions']);
        Setting::set('overlay_type_upload', $request['overlay_type_extensions']);
        Setting::save();

        return response()->json(['msg'=>'Success']);
    }
}
