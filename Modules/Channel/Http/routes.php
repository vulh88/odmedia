<?php


Route::get('test/report', 'Modules\Channel\Http\Controllers\ReportController@monthlyReport')->name('channel.report');

Route::group([
    'prefix' => 'api/v1',
    'middleware' => ['auth:api', 'cors', 'authorizedChannel'],
    'namespace' => 'Modules\Channel\Http\Controllers\Api'], function () {
    Route::resource('channels', 'ChannelController');
    Route::put('channels/{id}/trash', 'ChannelController@trash');
    Route::put('channels/{id}/restore', 'ChannelController@restore');
    Route::put('channels/{id}/reset-premiere', 'ChannelController@resetPremiere')->name('channels.reset-premiere');

    Route::put('settings', 'SettingsController@setSystemSettings');
    Route::get('settings', 'SettingsController@getSystemSettings');

});
