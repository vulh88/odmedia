<?php

namespace Modules\Channel\Http\Middleware;

use Closure;
use Modules\Channel\Repositories\Contracts\ChannelRepositoryInterface;
use Modules\User\Repositories\Contracts\ChannelUsersRepositoryInterface;
use Illuminate\Support\Facades\Route;

class checkAuthorizedChannel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->guard('api')->user();

        if ($request->has('channel_id')) {
            if ($channelId = $request->get('channel_id')) {
                $channelRepo = app(ChannelRepositoryInterface::class);
                $channelRepo->findOrFail($channelId);
            }
            else {
                return response()->error(__("Missing channel ID"), 1004, 403);
            }
        }

        if (!$user || !$channelId = $request->get('channel_id')) {
            return $next($request);
        }

        if ($user && $user->hasRole(config('acl.group_with_full_permissions'))) {
            return $next($request);
        }

        $route = Route::getRoutes()->match($request);

        // Route name
        $rName = $route->getName();
        list($moduleName, $action) = explode('.', $rName);

        $mapActions = config('acl.map_actions');
        if(isset($mapActions[$action])) {
            $action = $mapActions[$action];
        }
        $permissionName = $moduleName.'.'. $action;

        /**
         * Verify channel
         */
        $repo = app()->make(ChannelUsersRepositoryInterface::class);
        $channelUser = $repo->findWhere([
            'channel_id'    => $channelId,
            'user_id'   => $user->id
        ])->first();

        // has permission
        if (!$channelUser || !$channelUser->hasPermissionTo($permissionName)) {
            return response()->error(__("You are not authorized on this channel"), 1003, 403);
        }

        return $next($request);
    }
}
