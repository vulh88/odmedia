<?php

namespace Modules\Channel\Repositories\Eloquents;

use Modules\Channel\Entities\Channel;
use Elidev\Repository\Eloquent\BaseRepository;
use Modules\Channel\Repositories\Contracts\ChannelRepositoryInterface;
use Elidev\Repository\Traits\PaginationTrait;


/**
 * Class ChannelRepository
 */
class ChannelRepository extends BaseRepository implements ChannelRepositoryInterface
{
    use PaginationTrait;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Channel::class;
    }

    /**
     * @param array $columns
     * @param array $params
     * @return array
     */
    public function search($columns = array(), $params = [])
    {
        $pagination = $this->extractPagination($params);

        $query = $this->model()
            ::addSelect($columns)
            ->orderBy($pagination['sortColumn'],$pagination['sortOrder']);

        if (!empty($params['user_id'])) {
            $query->join('channel_users', 'channel_users.channel_id', '=', 'channels.id');
            $query->where('channel_users.user_id', $params['user_id']);
        }

        /**
         * Search items by keyword
         */
        if (!empty($params['keyword'])) {
            $query
                ->where('name', 'like', '%'.$params['keyword'].'%');
        }

        /**
         * Search trashed items by trash param
         */
        if (!empty($params['trash']) && $params['trash'] == 1) {
            $query->onlyTrashed();
        }

        return $query->paginate($pagination['count'], $columns);
    }

    /**
     * @param array $data
     * @param $resourceFolderRepository
     * @return mixed
     */
    function createChannel(array $data, $resourceFolderRepository)
    {
        $channel = $this->create($data);
        if ($channel) {
            $resourceFolder = [
                'name' => 'root ' . $channel->name,
                'channel_id' => $channel->id,
            ];
            $resourceFolderData = $resourceFolderRepository->create($resourceFolder);
            if ($resourceFolderData) {
                $dataUpdate = [
                    'root_resource_folder_id' => $resourceFolderData->id
                ];
                $this->update($dataUpdate, $channel->id);
            }
        }

        return $channel;
    }
}
