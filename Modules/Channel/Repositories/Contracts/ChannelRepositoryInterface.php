<?php

namespace Modules\Channel\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface ChannelRepositoryInterface
 */
interface ChannelRepositoryInterface extends RepositoryInterface
{
    /**
     * @param array $columns
     * @param array $params
     * @return mixed
     */
    public function search($columns = array(), $params = []);

    /**
     * @param array $data
     * @param $resourceFolderRepository
     * @return mixed
     */
    public function createChannel(array $data, $resourceFolderRepository);

}
