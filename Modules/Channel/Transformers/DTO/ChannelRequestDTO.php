<?php

namespace Modules\Resource\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="ChannelRequestDTO"
 * )
 */
class ChannelRequestDTO
{
    /**
     * @var string
     *
     * @SWG\Property(
     *    property="name",
     *    type="string",
     *    description="Name *"
     * )
     */
    protected $name;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="playout_control_port",
     *    type="integer",
     *    description="Playout control port"
     * )
     */
    protected $playout_control_port;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="needs_content_download",
     *    type="integer",
     *    description="Needs content download"
     * )
     */
    protected $needs_content_download;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="resource_path",
     *    type="string",
     *    description="Resource path"
     * )
     */
    protected $resource_path;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="thumbnail_storage",
     *    type="string",
     *    description="Thumbnail storage"
     * )
     */
    protected $thumbnail_storage;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="root_resource_folder_id",
     *    type="integer",
     *    description="Root Resource folder ID"
     * )
     */
    protected $root_resource_folder_id;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="wipe_id",
     *    type="integer",
     *    description="wipe ID"
     * )
     */
    protected $wipe_id;

}
