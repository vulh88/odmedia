<?php

namespace Modules\Channel\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class ChannelResource extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'playout_control_port' => $this->playout_control_port,
            'needs_content_download' => $this->needs_content_download,
            'player_directory' => $this->player_directory,
            'wipe_id' => $this->wipe_id,
            'enable_timeline' => $this->enable_timeline,
            'resource_folder_id' => $this->resource_folder_id,
            'resource_path' => config('filesystems.odmedia.resources') . '/' . $this->resource_path,
            'thumbnail_storage' => config('filesystems.odmedia.resources_thumbnail') . '/' .$this->thumbnail_storage,
            'updated_at' => $this->updated_at->format(config('api.format_datetime')),
            'created_at' => $this->created_at->format(config('api.format_datetime')),
        ];
    }
}
