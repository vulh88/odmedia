<?php

namespace Modules\Channel\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Channel extends Model
{
    protected $fillable = [
        'name',
        'playout_control_port',
        'needs_content_download',
        'resource_path',
        'thumbnail_storage',
        'root_resource_folder_id',
        'wipe_id',
        'enable_timeline'
    ];

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
