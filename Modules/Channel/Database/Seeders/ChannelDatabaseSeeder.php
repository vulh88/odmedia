<?php

namespace Modules\Channel\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Channel\Repositories\Contracts\ChannelRepositoryInterface;
use Modules\User\Repositories\Contracts\ChannelUsersRepositoryInterface;
use Modules\Resource\Repositories\Contracts\ResourceFolderRepositoryInterface;

class ChannelDatabaseSeeder extends Seeder
{
    private $repo;
    private $cuRepo;
    private $rFolderRepo;

    public function __construct(ChannelRepositoryInterface $repository, ChannelUsersRepositoryInterface $channelUsersRepository, ResourceFolderRepositoryInterface $resourceFolderRepository)
    {
        $this->repo = $repository;
        $this->cuRepo = $channelUsersRepository;
        $this->rFolderRepo = $resourceFolderRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $channels = [
            [
                "name" => "Passion 2X",
                "playout_control_port" =>  0,
                "needs_content_download" =>  0,
                "resource_path" =>  "string",
                "thumbnail_storage" =>  "string",
                "root_resource_folder_id" =>  0,
                "wipe_id" =>  0
            ],
            [
                "name" => "Passie 3X",
                "playout_control_port" =>  0,
                "needs_content_download" =>  0,
                "resource_path" =>  "string",
                "thumbnail_storage" =>  "string",
                "root_resource_folder_id" =>  0,
                "wipe_id" =>  0
            ]
        ];

        /**
         * Folder data
         */
        foreach ($channels as $channel) {
            $c = $this->repo->create($channel);

            $data = createChannelStorage($c->id, $c->name);
            $c->resource_path = $data['resource_path'];
            $c->thumbnail_storage = $data['thumbnail_storage'];
            $c->save();

            $this->cuRepo->create([
                'user_id'   => 1,
                'channel_id'    => $c->id,
                'api_path'  => 'https://api-passion4tv.imcdev.com/od/content/index',
                'api_auth'  => 'WXKPYJkZZKiBqOkRLqHK70BbFN8-7BZ1',
            ]);

            // Create root folder
            $pRFolder = $this->rFolderRepo->create([
                'name' => $c->name.' Root',
                'channel_id'    => $c->id,
            ]);

            // Create import folder inside created root folder
            $this->rFolderRepo->create([
                'name'  => 'API Import',
                'parent_id' => $pRFolder->id,
                'channel_id'    => $c->id,
            ]);
        }
    }
}
