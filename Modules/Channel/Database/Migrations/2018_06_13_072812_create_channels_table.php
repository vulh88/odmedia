<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('playout_control_port')->nullable();
            $table->tinyInteger('needs_content_download')->nullable();
            $table->string('resource_path')->nullable();
            $table->string('thumbnail_storage')->nullable();
            $table->bigInteger('root_resource_folder_id')->nullable();
            $table->bigInteger('wipe_id')->nullable();
            $table->tinyInteger('enable_timeline')->default(1);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('channels');
    }
}
