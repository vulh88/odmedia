<?php

namespace Modules\Channel\Services;

use Modules\Channel\Repositories\Contracts\ChannelRepositoryInterface;
use App\Services\Abstracts\DeleteEntityServiceAbstract;

use Log;

class DeleteChannelService extends DeleteEntityServiceAbstract
{

    public function __construct(ChannelRepositoryInterface $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Process after destroy
     *
     * @return mixed
     */
    public function afterDestroy()
    {

    }

    /**
     * Process after soft delete
     *
     * @return mixed
     */
    function afterSoftDelete()
    {
        // TODO: Implement afterSoftDelete() method.
    }

    /**
     * Process after restore
     *
     * @return mixed
     */
    function afterRestore()
    {
        // TODO: Implement afterRestore() method.
    }
}
