<?php

return [
    'name' => 'Playlist',

    'format'  => [
        'rules' => [
            'author'    => 'random_by_author',
            'folder'    => 'random_by_folder',
            'tag'   => 'random_by_tag',
            'theme'   => 'random_by_theme',
            'premiere'    => 'random_by_premiere',
            'file'  => 'specific_file',

            'anchor'    => 'anchor',
            'event_block'    => 'event_block',
        ],
        'content_rules' => [
            'author', 'folder', 'tag', 'theme', 'premiere', 'file'
        ],
    ]
];
