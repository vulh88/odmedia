<?php

namespace Modules\Playlist\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class PlaylistResource extends Resource
{

    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
            'status' => $this->status,
            'is_auto_generated' => $this->is_auto_generated,
            'channel_id' => $this->channel_id,
            'user_id' => $this->user_id,
            'change_state_timeline' => !empty($this->changeStateTimeline) ? $this->changeStateTimeline : 0,
            'updated_at' => $this->updated_at->format(config('api.format_datetime')),
            'created_at' => $this->created_at->format(config('api.format_datetime')),
        ];
    }
}
