<?php

namespace Modules\Playlist\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="FormatRequestDTO"
 * )
 */
class FormatRequestDTO
{
    /**
     * @var string
     *
     * @SWG\Property(
     *    property="name",
     *    type="string",
     *    description="Name *"
     * )
     */
    protected $name;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="channel_id",
     *    type="integer",
     *    description="Channel ID *"
     * )
     */
    protected $channel_id;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="length",
     *    type="integer",
     *    description="Length *"
     * )
     */
    protected $length;


}
