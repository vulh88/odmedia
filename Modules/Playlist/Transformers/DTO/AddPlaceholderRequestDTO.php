<?php

namespace Modules\Playlist\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="AddPlaceholderRequestDTO"
 * )
 */
class AddPlaceholderRequestDTO
{
    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="playlist_id",
     *    type="integer",
     *    description="Playlist ID *"
     * )
     */
    protected $playlist_id;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="resource_id",
     *    type="integer",
     *    description="Resource ID *"
     * )
     */
    protected $resource_id;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="title",
     *    type="string",
     *    description="Title *"
     * )
     */
    protected $title;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="duration",
     *    type="integer",
     *    description="Duration *"
     * )
     */
    protected $duration;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="length",
     *    type="string",
     *    description="Length *"
     * )
     */
    protected $length;

}
