<?php

namespace Modules\Playlist\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="EventBlockRequestDTO"
 * )
 */
class EventBlockRequestDTO
{
    /**
     * @var string
     *
     * @SWG\Property(
     *    property="name",
     *    type="string",
     *    description="Name *"
     * )
     */
    protected $name;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="channel_id",
     *    type="integer",
     *    description="Channel ID *"
     * )
     */
    protected $channel_id;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="type",
     *    type="string",
     *    description="Type *"
     * )
     */
    protected $type;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="default_duration",
     *    type="integer",
     *    description="Default duration section *"
     * )
     */
    protected $default_duration;


}
