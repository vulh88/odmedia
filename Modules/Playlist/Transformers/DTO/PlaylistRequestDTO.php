<?php

namespace Modules\Playlist\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="PlaylistRequestDTO"
 * )
 */
class PlaylistRequestDTO
{
    /**
     * @var string
     *
     * @SWG\Property(
     *    property="name",
     *    type="string",
     *    description="Name *"
     * )
     */
    protected $name;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="start_time",
     *    type="string",
     *    description="Start time *"
     * )
     */
    protected $start_time;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="end_time",
     *    type="string",
     *    description="End time *"
     * )
     */
    protected $end_time;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="is_auto_generated",
     *    type="integer",
     *    description="Is auto generated *"
     * )
     */
    protected $is_auto_generated;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="channel_id",
     *    type="integer",
     *    description="Channel ID *"
     * )
     */
    protected $channel_id;


}
