<?php

namespace Modules\Resource\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="PlaylistItemRequestDTO"
 * )
 */
class PlaylistItemRequestDTO
{
    /**
     * @var $playlist_id
     *
     * @SWG\Property(
     *    property="playlist_id",
     *    type="integer",
     *    description="Playlist ID *"
     * )
     */
    protected $playlist_id;

    /**
     * @var $resource_id
     *
     * @SWG\Property(
     *    property="resource_id",
     *    type="integer",
     *    description="Resource ID *"
     * )
     */
    protected $resource_id;

    /**
     * @var $start_time
     *
     * @SWG\Property(
     *    property="start_time",
     *    type="string",
     *    description="Start time *"
     * )
     */
    protected $start_time;

    /**
     * @var $end_time
     *
     * @SWG\Property(
     *    property="end_time",
     *    type="string",
     *    description="End time *"
     * )
     */
    protected $end_time;


    /**
     * @var $item_track
     *
     * @SWG\Property(
     *    property="item_track",
     *    type="integer",
     *    description="Item track * (1 is overlay resource, 2 is content resource)"
     * )
     */
    protected $item_track;

    /**
     * @var $event_block_data
     *
     * @SWG\Property(
     *    property="event_block_data",
     *    type="string",
     *    description="Event block data *"
     * )
     */
    protected $event_block_data;
}
