<?php

namespace Modules\Playlist\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="DuplicatePlaylistRequestDTO"
 * )
 */
class DuplicatePlaylistRequestDTO
{
    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="id",
     *    type="integer",
     *    description="Playlist ID *"
     * )
     */
    protected $playlist_id;

}
