<?php

namespace Modules\Playlist\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class EventBlockResource extends Resource
{

    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'channel_id' => $this->channel_id,
            'type' => $this->type,
            'default_duration' => $this->default_duration,
            'updated_at' => $this->updated_at->format(config('api.format_datetime')),
            'created_at' => $this->created_at->format(config('api.format_datetime')),
        ];
    }
}
