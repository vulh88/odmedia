<?php

namespace Modules\Playlist\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FormatResource extends Resource
{
    /**
     * Load AppHelpers
     */
    use \AppHelpers;

    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        foreach ($this->formatRules as $rule) {
            $rule->data = $rule->data ? unserialize($rule->data) : '';
        }
        $startTime = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
        return [
            'id' => $this->id,
            'name' => $this->name,
            'channel_id' => $this->channel_id,
            'user_id' => $this->user_id,
            'length' => self::parseFormatLength($this->length),
            'lengthOriginal' => $this->length,
            'filling_out_time' => $this->filling_out_time,
            'dynamic_sub_format' => $this->dynamic_sub_format,
            'active' => $this->active,
            'direct_publish' => $this->direct_publish,
            'formatRules' => $this->formatRules,
            'start_time' => $startTime,
            'end_time' =>  self::parseEndTimeFormat($startTime, $this->length),
            'updated_at' => $this->updated_at->format(config('api.format_datetime')),
            'created_at' => $this->created_at->format(config('api.format_datetime')),
        ];
    }
}
