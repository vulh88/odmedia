<?php

namespace Modules\Playlist\Transformers;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Services\ApiTrait\ResourceCollectionTrait;

class PlaylistContentItemsCollection extends ResourceCollection
{
    use ResourceCollectionTrait;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Support\Collection
     */
    public function toArray($request)
    {
        return $this->collection;
    }

    /**
     * Customize the outgoing response for the resource.
     *
     * @param \Illuminate\Http\Request
     * @param \Illuminate\Http\JsonResponse
     * @return void
     */
    public function withResponse($request, $response)
    {
        $count = !empty($request->get('count')) ? $request->get('count') : config('api.default_limit');
        $response->header('X-Total', $this->getTotal());
        $response->header('X-Current-Page', $this->getCurrentPage());
        $response->header('X-Limit', $count);
        $response->header('X-Last-Page', $this->getLastPage());
    }
}
