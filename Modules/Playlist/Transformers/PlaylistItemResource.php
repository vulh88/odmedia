<?php

namespace Modules\Playlist\Transformers;

use App\Services\ApiTrait\ResourceCollectionTrait;
use Illuminate\Http\Resources\Json\Resource;
use Modules\Resource\Repositories\Contracts\ResourceRepositoryInterface;
use Modules\Resource\Transformers\ResourceResource;

class PlaylistItemResource extends Resource
{
    use ResourceCollectionTrait;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        if (!isset($this->resource_id) || !$this->resource_id) {
            return [];
        }

        $rr = app(ResourceRepositoryInterface::class);
        $resource = $rr->find($this->resource_id);

        return [
            'id'    => $this->id,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
            'resource'  => new ResourceResource($resource)
        ];
    }

    /**
     * Customize the outgoing response for the resource.
     *
     * @param \Illuminate\Http\Request
     * @param \Illuminate\Http\JsonResponse
     * @return void
     */
    public function withResponse($request, $response)
    {
        $count = !empty($request->get('count')) ? $request->get('count') : config('api.default_limit');
        $response->header('X-Total', $this->getTotal());
        $response->header('X-Current-Page', $this->getCurrentPage());
        $response->header('X-Limit', $count);
        $response->header('X-Last-Page', $this->getLastPage());
    }
}
