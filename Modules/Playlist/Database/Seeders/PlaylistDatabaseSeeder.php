<?php

namespace Modules\Playlist\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Playlist\Repositories\Contracts\EventBlockRepositoryInterface;

class PlaylistDatabaseSeeder extends Seeder
{
    private $eventBlockRepo;

    function __construct(EventBlockRepositoryInterface $eventBlockRepository)
    {
        $this->eventBlockRepo = $eventBlockRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $eventBlocks = [
            [
                "channel_id" => 1,
                "type" => "1upcoming",
                "default_duration" => 20,
                "name" => "1 upcoming titel",
            ],
            [
                "channel_id" => 1,
                "type" => "2upcoming",
                "default_duration" => 20,
                "name" => "2 upcoming titels",
            ],
            [
                "channel_id" => 1,
                "type" => "4upcoming",
                "default_duration" => 20,
                "name" => "4 upcoming titels",
            ],
            [
                "channel_id" => 1,
                "type" => "ad",
                "default_duration" => 10,
                "name" => "Triade Advertenties",
            ]
        ];
        foreach ($eventBlocks as $event) {
            $this->eventBlockRepo->create($event);
        }

    }
}
