<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormatRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('format_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('format_id');
            $table->string('rule_type', 100);
            $table->text('data')->nullable();
            $table->integer('priority')->default(0)->comment('sort order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('format_rules');
    }
}
