<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaylistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('playlists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->dateTime('start_time')->nullable();
            $table->dateTime('end_time')->nullable();
            $table->string('status')->default('pending')->nullable()->comment('(publish, pending), default pending');
            $table->tinyInteger('is_auto_generated')->default(0)->nullable();
            $table->bigInteger('channel_id')->default(0);
            $table->bigInteger('user_id')->foreign('user_id')->references('id')->on('users')->default(0);
            $table->index(['user_id', 'channel_id']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('playlists');
    }
}
