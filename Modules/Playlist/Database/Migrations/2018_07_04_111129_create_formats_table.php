<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->integer('channel_id');
            $table->integer('user_id');
            $table->integer('length')->default(0);
            $table->tinyInteger('filling_out_time')->default(0)
                ->comment('This checkbox will make sure automatic playlist generation will always fill items until the end of the duration is reached, 
                even if the last item will overlap the next hour. In contrast, the overlap will be avoided and the last item truncated to fit');
            $table->tinyInteger('dynamic_sub_format')->default(0)->comment('Using the nested formats to increase the duration of the playlist 
            by compensating into the parent format duration');

            $table->tinyInteger('active')->default(1);
            $table->tinyInteger('direct_publish')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formats');
    }
}
