<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->integer('channel_id')->nullable();
            $table->string('type', 255);
            $table->integer('default_duration')->default(0)->comment('Default duration value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_blocks');
    }
}
