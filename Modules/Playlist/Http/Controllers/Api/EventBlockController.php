<?php

namespace Modules\Playlist\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Playlist\Repositories\Contracts\EventBlockRepositoryInterface;
use Modules\Playlist\Transformers\EventBlockCollection;
use Modules\Playlist\Http\Requests\FormEventBlockRequest;
use Modules\Playlist\Transformers\EventBlockResource;
use Carbon\Carbon;

class EventBlockController extends Controller
{
    /**
     * Get list of event blocks.
     *
     * @param EventBlockRepositoryInterface $eventBlockRepository
     * @param Request $request
     * @return EventBlockCollection
     * @SWG\Get(
     *     path="/event-blocks",
     *     tags={"Event blocks"},
     *     operationId="getEventBlock",
     *     description="Gets format items",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="keyword", in="query", type="string", description="Search by name"),
     *
     *     @SWG\Parameter(name="sort", in="query", type="string", description="Field to sort by, if prefixed with '-', the sorting direction will be descending"),
     *     @SWG\Parameter(name="page", in="query", type="integer", description="Number of page used to pagination"),
     *     @SWG\Parameter(name="count", in="query", type="integer", description="Number of records to display"),
     *     @SWG\Parameter(name="channel_id", in="query", type="integer", description="Number of records to display"),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *         @SWG\Header(header="X-Total", type="integer", description="Total count of format"),
     *         @SWG\Header(header="X-Skipped", type="integer", description="EventBlock skipped"),
     *         @SWG\Header(header="X-Limit", type="integer", description="EventBlock limit")
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function index(EventBlockRepositoryInterface $eventBlockRepository, Request $request)
    {
        $columns = [
            'id',
            'name',
            'channel_id',
            'type',
            'default_duration',
            'created_at',
            'updated_at'
        ];
        $results = $eventBlockRepository->search($columns, $request->all());
        return new EventBlockCollection(
            $results->getCollection(),
            $results->total(),
            $results->currentPage(),
            $results->lastPage()
        );
    }

    /**
     * Show the form for creating a new event blocks.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Create a event block
     *
     * @param EventBlockRepositoryInterface $eventBlockRepository
     * @param FormEventBlockRequest  $request
     * @return EventBlockResource
     * @SWG\Post(
     *     path="/event-blocks",
     *     tags={"Event blocks"},
     *     operationId="postCreateEventBlock",
     *     description="Creates new (or updates existing) format.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="EventBlock object that needs to be created",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/EventBlockRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successfully created resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of created event block"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function store(EventBlockRepositoryInterface $eventBlockRepository, FormEventBlockRequest $request)
    {
        $data = $request->validated();
        return new EventBlockResource($eventBlockRepository->create($data));
    }

    /**
     * Get detail event block
     *
     * @param $id
     * @param EventBlockRepositoryInterface $eventBlockRepository
     * @return EventBlockResource
     * @SWG\Get(
     *     path="/event-blocks/{id}",
     *     tags={"Event blocks"},
     *     operationId="getDetailEventBlock",
     *     description="Detail event block",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *            name="id",
     *            in="path",
     *            required=true,
     *            type="string",
     *            description="UUID",
     *        ),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function show($id, EventBlockRepositoryInterface $eventBlockRepository)
    {
        $eventBlock = $eventBlockRepository->findOrFail($id);

        return new EventBlockResource($eventBlock);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Updates a event block
     *
     * @param int  $id
     * @param EventBlockRepositoryInterface $eventBlockRepository
     * @param FormEventBlockRequest $request
     * @return \Illuminate\Http\JsonResponse|EventBlockResource
     * @SWG\Put(
     *     path="/event-blocks/{id}",
     *     tags={"Event blocks"},
     *     operationId="putUpdateEventBlock",
     *     description="Creates new (or updates existing) event block.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     * 			name="id",
     * 			in="path",
     * 			required=true,
     * 			type="integer",
     * 			description="UUID",
     * 		),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="EventBlock object that needs to be Updated",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/EventBlockRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successfully updated resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of updated"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function update($id, EventBlockRepositoryInterface $eventBlockRepository, FormEventBlockRequest $request)
    {
        $data = $request->validated();
        return new EventBlockResource($eventBlockRepository->update($data, $id));
    }

    /**
     * Delete existing event block.
     *
     * @param int $id
     * @param EventBlockRepositoryInterface $eventBlockRepository
     * @return mixed
     * @SWG\Delete(
     *     path="/event-blocks/{id}",
     *     tags={"Event blocks"},
     *     operationId="deleteResourceType",
     *     description="Delete existing event block",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of event block that should be deleted",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of deleted event block"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="EventBlock not found.",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function destroy($id, EventBlockRepositoryInterface $eventBlockRepository)
    {
        if ( $eventBlockRepository->delete($id) ) {
            return response()->noContent();
        }

        return response()->error(__('This event block has already destroyed permanently or event block was not in trash or event block does not exist in our system. Please check the event block ID.'), 404);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTypes(Request $request)
    {
        $types = [
            [
                'name' => '1 upcoming',
                'value' => '1upcoming'
            ],
            [
                'name' => '2 upcoming',
                'value' => '2upcoming'
            ],
            [
                'name' => '4 upcoming',
                'value' => '4upcoming'
            ],
            [
                'name' => 'ad',
                'value' => 'ad'
            ]
        ];

        return response()->json($types);
    }


}
