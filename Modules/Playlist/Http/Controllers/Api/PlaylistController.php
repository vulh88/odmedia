<?php

namespace Modules\Playlist\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Channel\Repositories\Contracts\ChannelRepositoryInterface;
use Modules\Playlist\Http\Requests\AddPlaceHolderRequest;
use Modules\Playlist\Jobs\PlaylistQueue;
use Modules\Playlist\Repositories\Contracts\PlaylistItemRepositoryInterface;
use Modules\Playlist\Repositories\Contracts\PlaylistRepositoryInterface;
use Modules\Playlist\Transformers\PlaylistCollection;
use Modules\Playlist\Http\Requests\FormPlaylistRequest;
use Modules\Playlist\Transformers\PlaylistContentItemsCollection;
use Modules\Playlist\Transformers\PlaylistItemResource;
use Modules\Playlist\Transformers\PlaylistResource;
use Modules\Playlist\Services\DeletePlaylistService;
use Modules\Playlist\Http\Requests\FormGeneratePlaylistRequest;
use Modules\Resource\Repositories\Contracts\ResourceRepositoryInterface;
use Modules\Resource\Repositories\Eloquents\ResourceRepository;
use Modules\User\Repositories\Contracts\ChannelUsersRepositoryInterface;
use Modules\Resource\Transformers\ResourceResource;
use Illuminate\Support\Facades\Auth;

class PlaylistController extends Controller
{
    /**
     * Get list of playlist.
     *
     * @param PlaylistRepositoryInterface $playlistRepository
     * @param Request $request
     * @param ChannelRepositoryInterface $channelRepository
     * @return PlaylistCollection
     * @SWG\Get(
     *     path="/playlists",
     *     tags={"Playlist"},
     *     operationId="getPlaylist",
     *     description="Gets playlist items",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="keyword", in="query", type="string", description="Search by name"),
     *
     *     @SWG\Parameter(name="sort", in="query", type="string", description="Field to sort by, if prefixed with '-', the sorting direction will be descending"),
     *     @SWG\Parameter(name="page", in="query", type="integer", description="Number of page used to pagination"),
     *     @SWG\Parameter(name="count", in="query", type="integer", description="Number of records to display"),
     *     @SWG\Parameter(name="channel_id", in="query", type="integer", description="Number of records to display"),
     *     @SWG\Parameter(name="status", in="query", type="string", description="Playlist status = publish|pending"),
     *     @SWG\Parameter(name="trash", in="query", type="integer", description="Only trashed playlists (trash=1)"),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *         @SWG\Header(header="X-Total", type="integer", description="Total count of playlist"),
     *         @SWG\Header(header="X-Skipped", type="integer", description="Playlist skipped"),
     *         @SWG\Header(header="X-Limit", type="integer", description="Playlist limit")
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function index(PlaylistRepositoryInterface $playlistRepository, Request $request, ChannelRepositoryInterface $channelRepository)
    {
        if (!empty($request->get('channel_id'))) {
            $channelRepository->findOrFail($request->get('channel_id'));
        }
        $columns = ['id', 'name', 'start_time', 'end_time', 'status', 'is_auto_generated', 'channel_id', 'user_id'];
        $params = $request->all();
        $params['user_id'] = Auth::user()->id;
        $results = $playlistRepository->search($columns, $params);
        return new PlaylistCollection($results->getCollection(), $results->total(), $results->currentPage(), $results->lastPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Create a playlist
     *
     * @param PlaylistRepositoryInterface $playlistRepository
     * @param FormPlaylistRequest  $request
     * @return PlaylistResource
     * @SWG\Post(
     *     path="/playlists",
     *     tags={"Playlist"},
     *     operationId="postCreatePlaylist",
     *     description="Creates new (or updates existing) playlist.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Playlist object that needs to be created",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/PlaylistRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successfully created resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of created playlist"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function store(PlaylistRepositoryInterface $playlistRepository, FormPlaylistRequest $request)
    {
        $data = $request->validated();
        $data['user_id'] = $request->user()->id;
        return new PlaylistResource($playlistRepository->create($data));
    }

    /**
     * Get detail playlist
     *
     * @param $id
     * @param PlaylistRepositoryInterface $playlistRepository
     * @return PlaylistResource
     * @SWG\Get(
     *     path="/playlists/{id}",
     *     tags={"Playlist"},
     *     operationId="getDetailPlaylist",
     *     description="Detail playlist",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *            name="id",
     *            in="path",
     *            required=true,
     *            type="string",
     *            description="UUID",
     *        ),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function show($id, PlaylistRepositoryInterface $playlistRepository)
    {
        $playlist = $playlistRepository->findOrFail($id);

        return new PlaylistResource($playlist);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Updates a playlist
     *
     * @param int  $id
     * @param PlaylistRepositoryInterface $playlistRepository
     * @param PlaylistItemRepositoryInterface $playlistItemRepository,
     * @param FormPlaylistRequest $request
     * @return \Illuminate\Http\JsonResponse|PlaylistResource
     * @SWG\Put(
     *     path="/playlists/{id}",
     *     tags={"Playlist"},
     *     operationId="putUpdatePlaylist",
     *     description="Creates new (or updates existing) playlist.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     * 			name="id",
     * 			in="path",
     * 			required=true,
     * 			type="integer",
     * 			description="UUID",
     * 		),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Playlist object that needs to be Updated",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/PlaylistRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successfully updated resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of updated"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function update(
        $id,
        PlaylistRepositoryInterface $playlistRepository,
        PlaylistItemRepositoryInterface $playlistItemRepository,
        FormPlaylistRequest $request
    )
    {
        $data = $request->validated();
        $data['user_id'] = $request->user()->id;
        $playlistData = $playlistRepository->findOrFail($id);
        $changeStateTimeline = 0;
        if ($playlistData->status == "pending") {
            if ($playlistData->start_time != $data["start_time"] || $playlistData->end_time != $data["end_time"]) {
                /**
                 * Remove playlist items has start vs end time not match with start vs end time of Playlist
                 */
                $playlistItemRepository->removeItemsNotInPlaylistTime($id, $data["start_time"], $data["end_time"]);
                /**
                 * Update time for playlist items has start time greater than playlist start time vs end time greater than playlist end time
                 */
                $playlistItemRepository->updateTimeItemsToMatchedPlaylist($id, $data["start_time"], $data["end_time"]);
                /**
                 * The variable used to set state then response for client to reload time line in UI
                 */
                $changeStateTimeline = 1;
            }
        }
        $playlist = $playlistRepository->update($data, $id);
        $playlist->changeStateTimeline = $changeStateTimeline;
        return new PlaylistResource($playlist);
    }

    /**
     * Delete existing playlist.
     *
     * @param int $id
     * @param DeletePlaylistService $service
     * @return mixed
     * @SWG\Delete(
     *     path="/playlists/{id}",
     *     tags={"Playlist"},
     *     operationId="deleteResourceType",
     *     description="Delete existing playlist",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of playlist that should be deleted",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of deleted playlist"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Playlist not found.",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function destroy($id, DeletePlaylistService $service)
    {
        if ( $service->destroy($id) ) {
            return response()->noContent();
        }

        return response()->error(__('This playlist has already destroyed permanently or playlist was not in trash or playlist does not exist in our system. Please check the playlist ID.'), 404);
    }


    /**
     * Restore a playlist.
     *
     * @param int $id
     * @param DeletePlaylistService $service
     * @return mixed
     * @SWG\Put(
     *     path="/playlists/{id}/restore",
     *     tags={"Playlist"},
     *     operationId="restorePlaylist",
     *     description="Restore a playlist from the trash",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of playlist that should be restored",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of deleted playlist"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Playlist not found.",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function restore($id, DeletePlaylistService $service)
    {
        if ( $service->restore($id) ) {
            return response()->noContent();
        }

        return response()->error(__('This playlist has not existed in the trash yet'), 404);
    }

    /**
     * Delete a playlist temporarily. (Move to trash)
     *
     * @param int $id
     * @param DeletePlaylistService $service
     * @return mixed
     * @SWG\Put(
     *     path="/playlists/{id}/trash",
     *     tags={"Playlist"},
     *     operationId="trashPlaylist",
     *     description="Delete a playlist from the trash",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of playlist that should be trashed",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of deleted playlist"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Playlist not found.",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function trash($id, DeletePlaylistService $service)
    {
        if ( $service->softDelete($id) ) {
            return response()->noContent();
        }

        return response()->error(__('This playlist has already contained in the trash or it does not exist in our system. Please check the playlist ID.'), 404);
    }

    /**
     * Schedule a resource
     *
     * @param $playlistID
     * @param PlaylistRepositoryInterface $playlistRepository
     * @param Request $request
     * @param ChannelRepositoryInterface $channelRepository
     * @param PlaylistItemRepositoryInterface $playlistItemRepository
     * @param ResourceRepositoryInterface $resourceRepository
     * @return PlaylistItemResource
     * @SWG\Put(
     *     path="/playlists/{playlist}/publish",
     *     tags={"Playlist"},
     *     operationId="publishPlaylist",
     *     description="Publish a playlist",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Playlist ID",
     *         in="path",
     *         type="integer",
     *         name="playlist",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function publish($playlistID, PlaylistRepositoryInterface $playlistRepository,
                            Request $request,
                            ChannelRepositoryInterface $channelRepository,
                            PlaylistItemRepositoryInterface $playlistItemRepository,
                            ResourceRepositoryInterface $resourceRepository)
    {
        $playlist = $playlistRepository->findOrFail($playlistID);
        $channelId = $playlist->channel_id;
        $channel = $channelRepository->findOrFail($channelId);

        /**
         * Check playlist must have at least resource
         */
        $hasResources = $playlistRepository->getPlaylistResources($playlistID, true);
        if ($hasResources->isEmpty()) {
            return response()->error(__("Can not publish the playlist due to there are not any resources"));
        }

        /**
         * There are some playlist are playing on this date range, so can not publish
         */
//        $startDate = $playlistItemRepository->getStartDatePlaylist($playlistID);
//        $endDate = $playlistItemRepository->getEndDatePlaylist($playlistID);
        $startDate = $playlist->start_time;
        $endDate = $playlist->end_time;
        $checkOtherPlaylistOnChannel = $playlistRepository->getPublishedPlaylistsInTheDateRange(
            $startDate,
            $endDate,
            $channelId,
            $playlistID
        );
        if ($checkOtherPlaylistOnChannel->count()) {
            return response()->error(__("Can not publish the playlist due to there are some other playlist are playing on this time"));
        }

        /**
         * Check playlist has published yet
         */
        $playlistPub = $playlistRepository->checkPlaylistPublish($playlistID);
        if ($playlistPub) {
            return response()->error(__("This playlist was already published"));
        }

        // Check channel required content need to be download or not
        if ($channel->needs_content_download) {
            /**
             * resource need to be download to the player directory if api_video_url is not null
             */
            $contentHaveNotDownloadYet = $resourceRepository->getResourcesForDownloadByPlaylist($playlistID, array(
                'only_not_downloaded_yet' => 1,
            ), true);
            if ($contentHaveNotDownloadYet > 0) {
                return response()->error(__("There are some resources have not download yet. 
                However, this channel requires to download resources into the player directory. Please verify your resources"));
            }
        }

        $playlistRepository->update([
            'status'    => 'publish'
        ], $playlistID);

        return response()->noContent();
    }

    /**
     * Get list of playlist.
     *
     * @param $id
     * @param Request $request
     * @param PlaylistRepositoryInterface $playlistRepository
     * @return PlaylistCollection
     * @SWG\Get(
     *     path="/playlists/{playlist}/resources",
     *     tags={"Playlist"},
     *     operationId="getPlaylistResources",
     *     description="Gets playlist resources",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Playlist ID",
     *         in="path",
     *         type="integer",
     *         name="playlist",
     *         required=true,
     *     ),
     *     @SWG\Parameter(name="page", in="query", type="integer", description="Number of page used to pagination"),
     *     @SWG\Parameter(name="count", in="query", type="integer", description="Number of records to display"),
     *     @SWG\Parameter(name="sort", in="query", type="string", description="Field to sort by, if prefixed with '-', the sorting direction will be descending"),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *         @SWG\Header(header="X-Total", type="integer", description="Total count of playlist"),
     *         @SWG\Header(header="X-Skipped", type="integer", description="Playlist skipped"),
     *         @SWG\Header(header="X-Limit", type="integer", description="Playlist limit")
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function getResources($id, Request $request, PlaylistRepositoryInterface $playlistRepository) {
        $playlistResources = $playlistRepository->getPlaylistResources($id, $request->all());
        foreach ($playlistResources as $val) {
            $val->title = $val->event_block_type ? "Eventblock - " . $val->event_block_type : $val->title;
        }
        return new PlaylistContentItemsCollection($playlistResources);
    }


    /**
     * Get resources for downloading by a playlist.
     *
     * @param $id
     * @param Request $request
     * @param ResourceRepositoryInterface $resourceRepository
     * @return PlaylistCollection
     * @SWG\Get(
     *     path="/playlists/{playlist}/resources-for-downloading",
     *     tags={"Playlist"},
     *     operationId="getResourcesForDownloadByPlaylist",
     *     description="Gets playlist resources for downloading",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Playlist ID",
     *         in="path",
     *         type="integer",
     *         name="playlist",
     *         required=true,
     *     ),
     *     @SWG\Parameter(name="page", in="query", type="integer", description="Number of page used to pagination"),
     *     @SWG\Parameter(name="count", in="query", type="integer", description="Number of records to display"),
     *     @SWG\Parameter(name="sort", in="query", type="string", description="Field to sort by, if prefixed with '-', the sorting direction will be descending"),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *         @SWG\Header(header="X-Total", type="integer", description="Total count of playlist"),
     *         @SWG\Header(header="X-Skipped", type="integer", description="Playlist skipped"),
     *         @SWG\Header(header="X-Limit", type="integer", description="Playlist limit")
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function getResourcesForDownloadByPlaylist($id, Request $request, ResourceRepositoryInterface $resourceRepository) {
        $playlistResources = $resourceRepository->getResourcesForDownloadByPlaylist($id, $request->all());
        return new PlaylistContentItemsCollection($playlistResources);
    }

    /**
     * Generate a playlist.
     *
     * @param FormGeneratePlaylistRequest $request
     * @param PlaylistRepositoryInterface $playlistRepository
     * @return mixed
     * @SWG\Post(
     *     path="/playlists/generate",
     *     tags={"Playlist"},
     *     operationId="generatePlaylist",
     *     description="Generate a playlist",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Start time",
     *         in="path",
     *         type="string",
     *         name="start_time",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         description="End time",
     *         in="path",
     *         type="string",
     *         name="end_time",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         description="Channel ID",
     *         in="path",
     *         type="integer",
     *         name="channel_id",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         description="Format ID",
     *         in="path",
     *         type="integer",
     *         name="format_id",
     *         required=true
     *     ),
     *
     *     @SWG\Response(
     *         response=204,
     *         description="Successfully generate playlist",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="No content"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function generatePlaylist(FormGeneratePlaylistRequest $request, PlaylistRepositoryInterface $playlistRepository)
    {
        dispatch((new PlaylistQueue($request->all()))->delay(60 * 1));
        return response()->noContent();
    }


    /**
     * Duplicate a playlist
     *
     * @param PlaylistRepositoryInterface $playlistRepository
     * @param Request  $request
     * @param PlaylistItemRepositoryInterface $playlistItemRepository
     * @return PlaylistResource
     * @SWG\Post(
     *     path="/playlists/duplicate",
     *     tags={"Playlist"},
     *     operationId="postDuplicatePlaylist",
     *     description="Duplicate a playlist already.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Playlist object that needs to be created",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/DuplicatePlaylistRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successfully created resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of created playlist"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function duplicatePlaylist(PlaylistRepositoryInterface $playlistRepository, Request $request, PlaylistItemRepositoryInterface $playlistItemRepository)
    {
        $data = $request->all();
        $oldPlaylist = $playlistRepository->findOrFail($data['id']);
        $record = $oldPlaylist->toArray();
        $record['name'] = $record['name'] . ' - Copy';
        $record['status'] = 'pending';
        /**
         * Clone playlist
         */
        $newPlaylist = $playlistRepository->create($record);
        if ($newPlaylist) {
            /**
             * Clone playlist items
             */
            $playlistItemRepository->duplicatePlaylistItems($oldPlaylist->id, $newPlaylist->id);
        }

        return $newPlaylist;
    }

    /**
     * @param AddPlaceHolderRequest $request
     * @param ResourceRepository $resourceRepository
     * @param ChannelUsersRepositoryInterface $channelUsersRepository
     * @param PlaylistRepositoryInterface $playlistRepository
     * @param PlaylistRepositoryInterface $playlistRepository
     * @return mixed
     */

    /**
     * Add placeholder to create resource then generate playlist items
     *
     * @param AddPlaceHolderRequest $request
     * @param ResourceRepository $resourceRepository
     * @return mixed
     * @SWG\Post(
     *     path="/playlists/add-placeholder",
     *     tags={"Playlist"},
     *     operationId="postAddPlaceHolderPlaylist",
     *     description="Add placeholder a playlist already.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Playlist object that needs to be created",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/AddPlaceholderRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successfully created playlist",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of created playlist"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function addPlaceHolder(
        AddPlaceHolderRequest $request,
        ResourceRepository $resourceRepository
    )
    {
        $data = $request->validated();
        $data["user_id"] = $request->user()->id;

        $resource = $resourceRepository->cloneResourcePlaceholder($data);
        if (!$resource) {
            return response()->error(__("Can not create the resource for placeholder"));
        }

        return new ResourceResource($resource);
    }
}
