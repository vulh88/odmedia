<?php

namespace Modules\Playlist\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Channel\Repositories\Contracts\ChannelRepositoryInterface;
use Modules\Playlist\Http\Requests\FormPlaylistItemRequest;
use Modules\Playlist\Repositories\Contracts\PlaylistItemRepositoryInterface;
use Modules\Playlist\Repositories\Contracts\PlaylistRepositoryInterface;
use Modules\Playlist\Services\Scheduler\ManualScheduleService;
use Modules\Playlist\Transformers\PlaylistCollection;
use Modules\Playlist\Http\Requests\FormPlaylistRequest;
use Modules\Playlist\Transformers\PlaylistContentItemsCollection;
use Modules\Playlist\Transformers\PlaylistItemResource;
use Modules\Playlist\Transformers\PlaylistResource;
use Modules\Playlist\Services\DeletePlaylistService;

class PlaylistItemController extends Controller
{

    /**
     * Creating a resource on the playlist timeline
     *
     * @param FormPlaylistItemRequest $playlistItemRequest
     * @param ManualScheduleService $manualScheduleService
     * @return PlaylistItemResource
     * @SWG\Post(
     *     path="/playlist-items",
     *     tags={"Playlist Items"},
     *     operationId="createScheduleResourcePlaylist",
     *     description="Schedule resource.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Playlist item object that needs to be scheduled",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/PlaylistItemRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function store(FormPlaylistItemRequest $playlistItemRequest, ManualScheduleService $manualScheduleService)
    {
        $playlistItemData = $manualScheduleService->execute($playlistItemRequest->all());
        return new PlaylistItemResource($playlistItemData['playlist_item']);
    }

    /**
     * Updating a resource on the playlist timeline
     *
     * @param $id
     * @param FormPlaylistItemRequest $playlistItemRequest
     * @param ManualScheduleService $manualScheduleService
     * @return PlaylistItemResource
     * @SWG\Put(
     *     path="/playlist-items/{playlist_item}",
     *     tags={"Playlist Items"},
     *     operationId="updateScheduleResourcePlaylist",
     *     description="Update a resource on the playlist timeline.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Playlist Item ID",
     *         in="path",
     *         type="integer",
     *         name="playlist_item",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Playlist item object that needs to be scheduled",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/PlaylistItemRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function update($id, FormPlaylistItemRequest $playlistItemRequest, ManualScheduleService $manualScheduleService)
    {
        $playlistItemData = $manualScheduleService->execute($playlistItemRequest->validated(), $id);
        return new PlaylistItemResource($playlistItemData['playlist_item']);
    }

    /**
     * Delete a playlist item.
     *
     * @param int $id
     * @param PlaylistItemRepositoryInterface $playlistItemRepository
     * @return mixed
     * @SWG\Delete(
     *     path="/playlist-items/{id}",
     *     tags={"Playlist Items"},
     *     operationId="deletePlaylistItem",
     *     description="Delete a playlist item",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of the playlist item",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of deleted playlist item"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Resource not found.",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function destroy($id, PlaylistItemRepositoryInterface $playlistItemRepository)
    {

        if ( $playlistItemRepository->delete($id) ) {
            return response()->noContent();
        }

        return response()->error(__('Playlist item is not found'), 404);
    }
}
