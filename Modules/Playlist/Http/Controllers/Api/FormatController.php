<?php

namespace Modules\Playlist\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Playlist\Repositories\Contracts\FormatRepositoryInterface;
use Modules\Playlist\Repositories\Contracts\FormatRuleRepositoryInterface;
use Modules\Playlist\Transformers\FormatCollection;
use Modules\Playlist\Http\Requests\FormFormatRequest;
use Modules\Playlist\Transformers\FormatResource;


class FormatController extends Controller
{
    /**
     * Load AppHelpers
     */
    use \AppHelpers;

    /**
     * Get list of format.
     *
     * @param FormatRepositoryInterface $formatRepository
     * @param Request $request
     * @return FormatCollection
     * @SWG\Get(
     *     path="/formats",
     *     tags={"Formats"},
     *     operationId="getFormat",
     *     description="Gets format items",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="keyword", in="query", type="string", description="Search by name"),
     *
     *     @SWG\Parameter(name="sort", in="query", type="string", description="Field to sort by, if prefixed with '-', the sorting direction will be descending"),
     *     @SWG\Parameter(name="page", in="query", type="integer", description="Number of page used to pagination"),
     *     @SWG\Parameter(name="count", in="query", type="integer", description="Number of records to display"),
     *     @SWG\Parameter(name="channel_id", in="query", type="integer", description="Number of records to display"),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *         @SWG\Header(header="X-Total", type="integer", description="Total count of format"),
     *         @SWG\Header(header="X-Skipped", type="integer", description="Format skipped"),
     *         @SWG\Header(header="X-Limit", type="integer", description="Format limit")
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function index(FormatRepositoryInterface $formatRepository, Request $request)
    {
        $columns = [
            'id',
            'name',
            'created_at',
            'updated_at'
        ];
        $results = $formatRepository->search($columns, $request->all());
        return new FormatCollection(
            $results->getCollection(),
            $results->total(),
            $results->currentPage(),
            $results->lastPage()
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Create a format
     *
     * @param FormatRepositoryInterface $formatRepository
     * @param FormFormatRequest  $request
     * @param FormatRuleRepositoryInterface $formatRuleRepository
     * @return FormatResource
     * @SWG\Post(
     *     path="/formats",
     *     tags={"Formats"},
     *     operationId="postCreateFormat",
     *     description="Creates new (or updates existing) format.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Format object that needs to be created",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/FormatRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successfully created resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of created format"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function store(FormatRepositoryInterface $formatRepository, FormFormatRequest $request, FormatRuleRepositoryInterface $formatRuleRepository)
    {
        $data = $request->all();
        $data['length'] = self::formatLength($data['length']);
        $data['user_id'] = $request->user()->id;
        $format = new FormatResource($formatRepository->create($data));
        /**
         * Create Format Rules
         */
        if ($format && !empty($data['rules'])) {
            $formatRuleRepository->saveFormatRules($data['rules'], $format->id);
        }

        return $format;
    }

    /**
     * Get detail format
     *
     * @param $id
     * @param FormatRepositoryInterface $formatRepository
     * @return FormatResource
     * @SWG\Get(
     *     path="/formats/{id}",
     *     tags={"Formats"},
     *     operationId="getDetailFormat",
     *     description="Detail format",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *            name="id",
     *            in="path",
     *            required=true,
     *            type="string",
     *            description="UUID",
     *        ),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function show($id, FormatRepositoryInterface $formatRepository)
    {
        $format = $formatRepository->findOrFail($id);
        return new FormatResource($format);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Updates a format
     *
     * @param int  $id
     * @param FormatRepositoryInterface $formatRepository
     * @param FormFormatRequest $request
     * @param FormatRuleRepositoryInterface $formatRuleRepository
     * @return \Illuminate\Http\JsonResponse|FormatResource
     * @SWG\Put(
     *     path="/formats/{id}",
     *     tags={"Formats"},
     *     operationId="putUpdateFormat",
     *     description="Creates new (or updates existing) format.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     * 			name="id",
     * 			in="path",
     * 			required=true,
     * 			type="integer",
     * 			description="UUID",
     * 		),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Format object that needs to be Updated",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/FormatRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successfully updated resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of updated"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function update($id, FormatRepositoryInterface $formatRepository, FormFormatRequest $request, FormatRuleRepositoryInterface $formatRuleRepository)
    {
        $data = $request->all();
        $data['length'] = self::formatLength($data['length']);
        $data['user_id'] = $request->user()->id;
        $format = new FormatResource($formatRepository->update($data, $id));

        /**
         * Update Format Rules
         */
        if ( $format ) {
            $formatRuleRepository->saveFormatRules($data['rules'], $id);
        }

        return $format;
    }

    /**
     * Delete existing format.
     *
     * @param int $id
     * @param FormatRepositoryInterface $formatRepository
     * @param FormatRuleRepositoryInterface $formatRuleRepository
     * @return mixed
     * @SWG\Delete(
     *     path="/formats/{id}",
     *     tags={"Formats"},
     *     operationId="deleteResourceType",
     *     description="Delete existing format",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of format that should be deleted",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of deleted format"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Format not found.",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function destroy($id, FormatRepositoryInterface $formatRepository, FormatRuleRepositoryInterface $formatRuleRepository)
    {
        if ( $formatRepository->delete($id) ) {
            $formatRuleRepository->removeRulesOfFormat($id);
            return response()->noContent();
        }

        return response()->error(__('This format has already destroyed permanently or format was not in trash or format does not exist in our system. Please check the format ID.'), 404);
    }

    /**
     * Duplicate a format
     *
     * @param FormatRepositoryInterface $formatRepository
     * @param Request  $request
     * @param FormatRuleRepositoryInterface $formatRuleRepository
     * @return FormatResource
     * @SWG\Post(
     *     path="/formats/duplicate",
     *     tags={"Formats"},
     *     operationId="postDuplicateFormat",
     *     description="Duplicate a format already.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         type="integer",
     *         description="Format Id that needs to be duplicated",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successfully created resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of created format"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function duplicateFormat(FormatRepositoryInterface $formatRepository, Request $request, FormatRuleRepositoryInterface $formatRuleRepository)
    {
        $data = $request->all();
        $oldFormat = $formatRepository->findOrFail($data['id']);
        $record = $oldFormat->toArray();
        $record['name'] = $record['name'] . ' - Copy';
        /**
         * Duplicate format
         */
        $newFormat = $formatRepository->create($record);
        if ($newFormat) {
            /**
             * Duplicate format rules
             */
            $formatRuleRepository->duplicateFormatRules($oldFormat->id, $newFormat->id);
        }

        return $newFormat;

    }



}
