<?php

// API
Route::group([
    'prefix' => 'api/v1',
    'middleware' => ['auth:api', 'cors', 'authorizedChannel'],
    'namespace' => 'Modules\Playlist\Http\Controllers\Api'], function () {

    Route::post('playlists/generate', 'PlaylistController@generatePlaylist')->name('playlist.generate');
    Route::post('playlists/duplicate', 'PlaylistController@duplicatePlaylist')->name('playlist.duplicate');
    Route::post('playlists/add-placeholder', 'PlaylistController@addPlaceHolder')->name('playlist.add_placeholder');
    Route::put('playlists/{playlist}/trash', 'PlaylistController@trash')->name('playlist.trash');
    Route::put('playlists/{playlist}/restore', 'PlaylistController@restore')->name('playlist.restore');
    Route::put('playlists/{playlist}/publish', 'PlaylistController@publish')->name('playlist.publish');
    Route::get('playlists/{playlist}/resources', 'PlaylistController@getResources')->name('playlist.resources');
    Route::get('playlists/{playlist}/resources-for-downloading', 'PlaylistController@getResourcesForDownloadByPlaylist')->name('playlist.resources-for-downloading');
    Route::resource('playlists', 'PlaylistController');

    /**
     * Routes for playlist items
     */
    Route::resource('playlist-items', 'PlaylistItemController');

    /**
     * Routes for format
     */
    Route::post('formats/duplicate', 'FormatController@duplicateFormat')->name('formats.duplicate_format');
    Route::resource('formats', 'FormatController');

    /**
     * Routes for event blocks
     */
    Route::get('event-blocks/types', 'EventBlockController@getTypes')->name('event-blocks.types');
    Route::resource('event-blocks', 'EventBlockController');

});
