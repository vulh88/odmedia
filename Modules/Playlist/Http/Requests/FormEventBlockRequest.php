<?php

namespace Modules\Playlist\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Channel\Repositories\Contracts\ChannelRepositoryInterface;
use App\Rules\CheckExistsDataRule;

class FormEventBlockRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'channel_id' => ['required', 'integer', new CheckExistsDataRule(app()->make(ChannelRepositoryInterface::class))],
            'type' => 'required|string',
            'default_duration' => 'integer|min:0',
        ];
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge(
            array_fill_keys(
                [
                    'default_duration.min',
                ],
                __('Can only be 0 or 1. If empty, default 0')
            )
        );
    }
}
