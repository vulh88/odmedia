<?php

namespace Modules\Playlist\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Playlist\Repositories\Contracts\EventBlockRepositoryInterface;
use Modules\Resource\Repositories\Contracts\ResourceRepositoryInterface;
use App\Rules\CheckExistsDataRule;
use Modules\Playlist\Repositories\Contracts\PlaylistRepositoryInterface;

class FormPlaylistItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // Only owner of the play can schedule the resource
        $playlistRepo = app()->make(PlaylistRepositoryInterface::class);
        $playlist = $playlistRepo->find($this->playlist_id);

        if (!$this->user()->can('update', $playlist) || !$playlist->channel_id) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $dtFormat = config('api.format_datetime');
        $rules = [
            'resource_id' => ['integer', new CheckExistsDataRule(app()->make(ResourceRepositoryInterface::class))],
            'playlist_id' => ['integer', new CheckExistsDataRule(app()->make(PlaylistRepositoryInterface::class))],
            'start_time' => 'required|date_format:'.$dtFormat.'',
            'end_time' => 'required|date_format:Y-m-d H:i:s|after:start_time',
            'event_block_id' => ['integer', new CheckExistsDataRule(app()->make(EventBlockRepositoryInterface::class))],
            'item_track'    => 'required|between:1,2',
        ];
        if ($this->resource_id == 0) {
            unset($rules["resource_id"]);
        }
        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
