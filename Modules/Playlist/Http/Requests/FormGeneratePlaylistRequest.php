<?php

namespace Modules\Playlist\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Channel\Repositories\Contracts\ChannelRepositoryInterface;
use App\Rules\CheckExistsDataRule;
use Modules\Playlist\Repositories\Contracts\FormatRepositoryInterface;

class FormGeneratePlaylistRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'integer', new CheckExistsDataRule(app()->make(ChannelRepositoryInterface::class))],
            'format_id' => ['required', 'integer', new CheckExistsDataRule(app()->make(FormatRepositoryInterface::class))],
            'start_time' => 'required|date_format:Y-m-d H:i:s',
            'end_time' => 'required|date_format:Y-m-d H:i:s|after:start_time',
        ];
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
