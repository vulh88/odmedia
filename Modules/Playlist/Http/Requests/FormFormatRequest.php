<?php

namespace Modules\Playlist\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Channel\Repositories\Contracts\ChannelRepositoryInterface;
use App\Rules\CheckExistsDataRule;

class FormFormatRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'channel_id' => ['required', 'integer', new CheckExistsDataRule(app()->make(ChannelRepositoryInterface::class))],
            'length' => 'required',
            'filling_out_time' => 'integer|min:0|max:1',
            'dynamic_sub_format' => 'integer|min:0|max:1',
            'direct_publish' => 'integer|min:0|max:1',
            'active' => 'integer|min:0|max:1',
        ];
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge(
            array_fill_keys(
                [
                    'length.min',
                    'filling_out_time.min',
                    'filling_out_time.max',
                    'dynamic_sub_format.min',
                    'dynamic_sub_format.max',
                    'direct_publish.min',
                    'direct_publish.max',
                    'active.min',
                    'active.max',
                ],
                __('Can only be 0 or 1. If empty, default 0')
            )
        );
    }
}
