<?php

namespace Modules\Playlist\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Channel\Repositories\Contracts\ChannelRepositoryInterface;
use App\Rules\CheckExistsDataRule;

class FormPlaylistRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $dFormat = config('api.format_datetime');
        return [
            'name' => 'required|max:255',
            'start_time' => 'required|date_format:'.$dFormat.'',
            'end_time' => 'required|date_format:'.$dFormat.'|after:start_time',
            'is_auto_generated' => 'integer|min:0|max:1|nullable',
            'channel_id' => ['required', 'integer', new CheckExistsDataRule(app()->make(ChannelRepositoryInterface::class))],
        ];
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge(
            array_fill_keys(
                [
                    'is_auto_generated.min',
                    'is_auto_generated.max',
                ],
                __('Can only be 0 or 1. If empty, default 0')
            )
        );
    }
}
