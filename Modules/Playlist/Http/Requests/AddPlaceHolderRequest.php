<?php

namespace Modules\Playlist\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CheckExistsDataRule;
use Modules\Channel\Repositories\Contracts\ChannelRepositoryInterface;
use Modules\Resource\Repositories\Contracts\ResourceRepositoryInterface;

class AddPlaceHolderRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'duration' => 'required|integer|min:0',
            'channel_id' => ['required', 'integer', new CheckExistsDataRule(app()->make(ChannelRepositoryInterface::class))],
            'resource_id' => ['required', 'integer', new CheckExistsDataRule(app()->make(ResourceRepositoryInterface::class))]
        ];
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
