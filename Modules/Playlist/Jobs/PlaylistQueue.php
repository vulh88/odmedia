<?php

namespace Modules\Playlist\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Playlist\Repositories\Contracts\PlaylistRepositoryInterface;
use Log;
use Modules\Playlist\Services\AddGeneratePlaylistQueueService;

class PlaylistQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     * PlaylistQueue constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     * @param PlaylistRepositoryInterface $playlistRepository
     *
     * @return void
     */
    public function handle(PlaylistRepositoryInterface $playlistRepository)
    {
        $generate = new AddGeneratePlaylistQueueService();
        $generate->execute($this->data);
    }
}
