<?php

namespace Modules\Playlist\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface FormatRuleRepositoryInterface
 */
interface FormatRuleRepositoryInterface extends RepositoryInterface
{
    /**
     * Create format rules
     * @param $rules
     * @param $formatID
     * @return mixed
     */
    public function saveFormatRules($rules, $formatID = 0);

    /**
     * Duplicate format rules
     * @param $oldFormatId
     * @param $newFormatId
     * @return mixed
     */
    public function duplicateFormatRules($oldFormatId, $newFormatId);

    /**
     * Remove format rules of the format
     * @param $formatId
     * @return mixed
     */
    public function removeRulesOfFormat($formatId);

    /**
     * Get format rules
     * @param $formatID
     * @return mixed
     */
    public function getFormatRules($formatID);
}
