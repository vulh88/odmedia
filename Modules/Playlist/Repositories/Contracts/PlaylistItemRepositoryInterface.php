<?php

namespace Modules\Playlist\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface PlaylistRepositoryInterface
 */
interface PlaylistItemRepositoryInterface extends RepositoryInterface
{
    /**
     * @param array $columns
     * @param array $params
     * @return mixed
     */
    public function search($columns = array(), $params = []);

    /**
     * Get start date of a playlist
     * @param $playlistId
     * @return array
     */
    public function getStartDatePlaylist($playlistId);

    /**
     * Get end date of a playlist
     * @param $playlistId
     * @return array
     */
    public function getEndDatePlaylist($playlistId);

    /**
     * Duplicate playlist items
     * @param $oldPlaylistId
     * @param $newPlaylistId
     * @return mixed|null
     */
    public function duplicatePlaylistItems($oldPlaylistId, $newPlaylistId);

    /**
     * Delete playlist items after a specific point
     * @param $time
     * @return mixed
    */
    public function deletePlaylistItemAfterAfterTime($time);

    /**
     * Remove playlist items has start vs end time not match with start vs end time of Playlist
     * @param $playlistId
     * @param $playlistStartTime
     * @param $playlistEndTime
     * @return mixed
     */
    public function removeItemsNotInPlaylistTime($playlistId, $playlistStartTime, $playlistEndTime);

    /**
     * Update time for playlist items has start time greater than playlist start time vs end time greater than playlist end time
     * @param $playlistId
     * @param $playlistStartTime
     * @param $playlistEndTime
     * @return mixed
     */
    public function updateTimeItemsToMatchedPlaylist($playlistId, $playlistStartTime, $playlistEndTime);



}
