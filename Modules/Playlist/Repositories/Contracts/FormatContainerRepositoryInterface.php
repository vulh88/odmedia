<?php

namespace Modules\Playlist\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface FormatContainerRepositoryInterface
 */
interface FormatContainerRepositoryInterface extends RepositoryInterface
{

}
