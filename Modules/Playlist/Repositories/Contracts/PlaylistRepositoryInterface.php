<?php

namespace Modules\Playlist\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface PlaylistRepositoryInterface
 */
interface PlaylistRepositoryInterface extends RepositoryInterface
{
    /**
     * @param array $columns
     * @param array $params
     * @return mixed
     */
    public function search($columns = array(), $params = []);

    /**
     * Get playlist items
     * @param $id
     * @param array $args
     * @param array $columns
     * @return collection
     */
    public function getPlaylistResources($id, $args = [], $columns = []);

    /**
     * Get playlists are in the given date range
     * @param $startDate
     * @param $endDate
     * @param $channelId
     * @param bool $notIncludePlaylistId
     * @return mixed
     */
    public function getPublishedPlaylistsInTheDateRange($startDate, $endDate, $channelId, $notIncludePlaylistId = false);

    /**
     * Check the playlist published yet
     * @param $playlistID
     * @return bool
     */
    public function checkPlaylistPublish($playlistID);


    /**
     * @param $channelID
     * @return mixed
     */
    public function getTheLastPlaylist($channelID);

    /**
     * Get resources of playlist has start time less than now
     * @return mixed
     */
    public function getPlaylistResourcesLessThanNow();

}
