<?php

namespace Modules\Playlist\Repositories\Eloquents;

use Modules\Playlist\Entities\EventBlock;
use Elidev\Repository\Eloquent\BaseRepository;
use Modules\Playlist\Repositories\Contracts\EventBlockRepositoryInterface;
use Elidev\Repository\Traits\PaginationTrait;


/**
 * Class EventBlockRepository
 */
class EventBlockRepository extends BaseRepository implements EventBlockRepositoryInterface
{
    use PaginationTrait;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EventBlock::class;
    }

    /**
     * @param array $columns
     * @param array $params
     * @return mixed
     */
    public function search($columns = array(), $params = []) {
        $pagination = $this->extractPagination($params);
        $pagination['count'] = $pagination['page'] == -1 ? 0 : $pagination['count'];
        $query = $this->model()
            ::addSelect($columns)
            ->orderBy($pagination['sortColumn'],$pagination['sortOrder']);

        /**
         * Handle where for request params
         */
        $this->whereConditions($params, $query);

        return $query->paginate($pagination['count'], $columns);
    }

    /**
     * Handle where for request params
     * @param array $params
     * @param $query
     */
    private function whereConditions($params = [], &$query)
    {
        $arrConditions = [
            'keyword',
            'channel_id',
        ];
        foreach ($params as $k => $v) {
            if ( in_array($k, $arrConditions) ) {
                switch ($k) {
                    case 'keyword':
                        /**
                         * Search items by keyword
                         */
                        $query->where('name', 'like', '%' . $v .'%');
                        break;
                    default:
                        $query->where($k, '=', $v);
                        break;
                }
            }
        }
    }
}
