<?php

namespace Modules\Playlist\Repositories\Eloquents;

use Elidev\Repository\Traits\PaginationTrait;
use Illuminate\Support\Facades\DB;
use Modules\Playlist\Entities\Playlist;
use Elidev\Repository\Eloquent\BaseRepository;
use Modules\Playlist\Repositories\Contracts\collection;
use Modules\Playlist\Repositories\Contracts\PlaylistRepositoryInterface;
use Carbon\Carbon;


/**
 * Class PlaylistRepository
 */
class PlaylistRepository extends BaseRepository implements PlaylistRepositoryInterface
{

    use PaginationTrait;

    private $defaultResourcesCols = [
        'playlist_items.id',
        'resources.id as resource_id',
        'resources.filename',
        'resources.title',
        'resources.api_video_url',
        'resources.api_fetched',
        'resources.duration',
        'resources.duration_ms',
        'playlist_items.event_block_type',
        'playlist_items.start_time',
        'playlist_items.end_time',
        'playlist_items.item_track',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Playlist::class;
    }

    /**
     * @param array $columns
     * @param array $params
     * @return array
     */
    public function search($columns = array(), $params = [])
    {
        $pagination = $this->extractPagination($params);
        $pagination['count'] = $pagination['page'] == -1 ? 0 : $pagination['count'];
        $query = $this->model()
            ::addSelect($columns)
            ->orderBy($pagination['sortColumn'],$pagination['sortOrder']);

        /**
         * Handle where for request params
         */
        $this->whereConditions($params, $query);

        return $query->paginate($pagination['count'], $columns);
    }

    /**
     * Handle where for request params
     * @param array $params
     * @param $query
     */
    private function whereConditions($params = [], &$query)
    {
        $arrConditions = [
            'keyword',
            'start_time',
            'end_time',
            'channel_id',
            'user_id',
            'status',
            'trash',
        ];
        foreach ($params as $k => $v) {
            if ( in_array($k, $arrConditions) ) {
                switch ($k) {
                    case 'keyword':
                        /**
                         * Search items by keyword
                         */
                        $query->where('title', 'like', '%' . $v .'%');
                        break;
                    case 'start_time':
                        $startTime = date('Y-m-d 00:00:00', strtotime($params['start_time']));
                        $query->where('start_time', '>=', $startTime);
                        break;
                    case 'end_time':
                        $endTime = date('Y-m-d 23:59:59', strtotime($params['end_time']));
                        $query->where('end_time', '<=', $endTime);
                        break;
                        break;
                    case 'trash':
                        if ($v == 1) {
                            /**
                             * Search trashed items by trash param
                             */
                            $query->onlyTrashed();
                        }
                        break;
                    default:
                        $query->where( $k, '=', $v);
                        break;
                }
            }
        }
    }

    /**
     * Get playlist items
     * @param $id
     * @param array $args
     * @param array $columns
     * @return collection
     */
    public function getPlaylistResources($id, $args = [], $columns = [])
    {
        if (empty($columns)) {
            $columns = $this->defaultResourcesCols;
        }

        $pagination = $this->extractPagination($args);
        $select = implode(',', $columns);

        $query = DB::table('playlist_items')
            ->select(DB::raw($select))
            ->leftjoin('resources', 'resources.id', '=', 'playlist_items.resource_id')
            ->where('playlist_items.playlist_id', $id)
            ->orderBy($pagination['sortColumn'], $pagination['sortOrder']);

        if (!empty($args['sum'])) {
            return $query->count();
        }
        return $query->get();
    }

    /**
     * Get playlists are in the given date range
     * @param $startDate
     * @param $endDate
     * @param $channelId
     * @param bool $notIncludePlaylistId
     * @return mixed
     */
    public function getPublishedPlaylistsInTheDateRange($startDate, $endDate, $channelId, $notIncludePlaylistId = false)
    {
        $query = $this->model()
            ::where('channel_id', $channelId)
            ->where(function ($query) use ($startDate, $endDate) {

                $query->orWhere(function ($query) use ($startDate, $endDate) {
                    $query->Where('start_time', '>=', $startDate)
                        ->where('start_time', '<=' , $endDate);
                })
                ->orWhere(function ($query) use ($startDate, $endDate) {
                    $query->Where('end_time', '>=', $startDate)
                        ->where('end_time', '<=' , $endDate);
                })
                ->orWhere(function ($query) use ($startDate, $endDate) {
                    $query->where('start_time', '<=', $startDate)
                        ->where('end_time', '>=' , $endDate);
                });
            })

            ->where('status', 'publish');
        if ($notIncludePlaylistId) {
            $query->where('id', '!=',  $notIncludePlaylistId);
        }

        return $query;
    }

    /**
     * Check the playlist published yet
     * @param $playlistID
     * @return bool
     */
    public function checkPlaylistPublish($playlistID) {
        $result = $this->findOrFail($playlistID);
        return $result->status == 'publish' ? true : false;
    }

    /**
     * @param $channelID
     * @return mixed
     */
    public function getTheLastPlaylist($channelID)
    {
        $myTime = Carbon::now();
        $query = $this->model()
            ::where('channel_id', $channelID)
            ->where('status', 'publish')
            ->where('end_time', '>=', $myTime)
            ->orderBy('end_time', 'desc')
            ->limit(1);

        return $query->first();
    }

    /**
     * Get resources of playlist has start time less than now
     * @return mixed
     */
    public function getPlaylistResourcesLessThanNow()
    {
        $now = Carbon::now();
        $query = DB::table('playlists')
            ->select("playlist_items.resource_id")
            ->join('playlist_items', 'playlists.id', '=', 'playlist_items.playlist_id')
            ->join('resources', 'resources.id', '=', 'playlist_items.resource_id')
            ->where('playlists.start_time', '<', $now)
            ->where('resources.premiere', 1)
            ->orderBy("playlist_items.id", "ASC");

        return $query->get();
    }
}
