<?php

namespace Modules\Playlist\Repositories\Eloquents;

use Modules\Playlist\Entities\FormatContainer;
use Elidev\Repository\Eloquent\BaseRepository;
use Modules\Playlist\Repositories\Contracts\collection;
use Modules\Playlist\Repositories\Contracts\FormatContainerRepositoryInterface;


/**
 * Class PlaylistRepository
 */
class FormatContainerRepository extends BaseRepository implements FormatContainerRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return FormatContainer::class;
    }
}
