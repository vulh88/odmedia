<?php

namespace Modules\Playlist\Repositories\Eloquents;

use Elidev\Repository\Eloquent\BaseRepository;
use Modules\Playlist\Entities\PlaylistItems;
use Modules\Playlist\Repositories\Contracts\PlaylistItemRepositoryInterface;


/**
 * Class PlaylistItemRepository
 */
class PlaylistItemRepository extends BaseRepository implements PlaylistItemRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PlaylistItems::class;
    }

    /**
     * @param array $columns
     * @param array $params
     * @return mixed
     */
    public function search($columns = array(), $params = [])
    {
        // TODO: Implement search() method.
    }

    /**
     * Get start date of a playlist
     * @param $playlistId
     * @return array
     */
    public function getStartDatePlaylist($playlistId)
    {
        return $this->model()
            ::where('playlist_id', $playlistId)
            ->min('start_time');
    }

    /**
     * Get end date of a playlist
     * @param $playlistId
     * @return array
     */
    public function getEndDatePlaylist($playlistId)
    {
        return $this->model()
            ::where('playlist_id', $playlistId)
            ->max('end_time');
    }

    /**
     * Duplicate playlist items
     * @param $oldPlaylistId
     * @param $newPlaylistId
     * @return mixed|null
     */
    public function duplicatePlaylistItems($oldPlaylistId, $newPlaylistId)
    {
        $playlistItems = $this->findWhere(['playlist_id' => $oldPlaylistId]);
        $playlistItemsData = [];
        foreach ($playlistItems as $rule) {
            $newRule = $rule->toArray();
            unset($newRule['id']);
            $newRule['playlist_id'] = $newPlaylistId;
            $playlistItemsData[] = $newRule;
        }
        if ($playlistItemsData) {
            /**
             * Create multi records for playlist items
             */
            return $this->model()::insert($playlistItemsData);
        }
        return null;
    }

    /**
     * Delete playlist items after a specific point
     * @param $time
     * @return mixed
     */
    public function deletePlaylistItemAfterAfterTime($time)
    {
        return $this->model()
            ::where('end_time', '>=', $time)->delete();
    }

    /**
     * Remove playlist items has start vs end time not match with start vs end time of Playlist
     * @param $playlistId
     * @param $playlistStartTime
     * @param $playlistEndTime
     * @return mixed
     */
    public function removeItemsNotInPlaylistTime($playlistId, $playlistStartTime, $playlistEndTime)
    {
        return $this->model()
            ::where('playlist_id', $playlistId)
            ->where(function($w) use ($playlistStartTime, $playlistEndTime) {
                $w->where('start_time', '>=', $playlistEndTime)
                    ->orWhere('end_time', '<=', $playlistStartTime);
            })
            ->delete();
    }

    /**
     * Update time for playlist items has start time greater than playlist start time vs end time greater than playlist end time
     * @param $playlistId
     * @param $playlistStartTime
     * @param $playlistEndTime
     * @return mixed
     */
    public function updateTimeItemsToMatchedPlaylist($playlistId, $playlistStartTime, $playlistEndTime)
    {
        $data =  $this->model()
            ::where('playlist_id', $playlistId)
            ->where(function($w) use ($playlistStartTime, $playlistEndTime) {
                $w->where('start_time', '<', $playlistStartTime)
                    ->orWhere('end_time', '>', $playlistEndTime);
            })->get();
        $success = 0;

        if ($data) {
            foreach ($data as $val) {
                //dump($val);
                $records = [];
                if ($val->start_time < $playlistStartTime) {
                    $records["start_time"] = $playlistStartTime;
                }
                if ($val->end_time > $playlistEndTime) {
                    $records["end_time"] = $playlistEndTime;
                }
                $success = $this->update($records, $val->id);
            }
        }
        return $success;
    }
}
