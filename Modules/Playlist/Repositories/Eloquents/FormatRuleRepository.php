<?php

namespace Modules\Playlist\Repositories\Eloquents;

use Modules\Playlist\Entities\FormatRule;
use Elidev\Repository\Eloquent\BaseRepository;
use Modules\Playlist\Repositories\Contracts\collection;
use Modules\Playlist\Repositories\Contracts\FormatRuleRepositoryInterface;


/**
 * Class PlaylistRepository
 */
class FormatRuleRepository extends BaseRepository implements FormatRuleRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return FormatRule::class;
    }

    /**
     * @param $rules
     * @param int $formatID
     * @return mixed|void
     */
    public function saveFormatRules($rules, $formatID = 0)
    {
        $formatRulesIds = [];
        $formatRules = $this->findWhere(['format_id' => $formatID]);
        foreach ($formatRules as $rule) {
            $formatRulesIds[] = $rule->id;
        }

        foreach ( $rules as $key => $rule ) {
            $ruleData = [
                'rule_type' => $rule['type'],
                'data' => $rule['data'] ? serialize($rule['data']) : '',
                'priority' => $rule['index'],
            ];
            if ($rule['id'] > 0) {
                $this->update($ruleData, $rule['id']);
                if (($key = array_search($rule['id'], $formatRulesIds)) !== false) {
                    unset($formatRulesIds[$key]);
                }
            } else {
                $ruleData['format_id'] = $formatID;
                $this->create($ruleData);
            }
        }
        /**
         * Delete Records Not Update
         */
        if ($formatRulesIds) {
            $this->model()::whereIn('id',$formatRulesIds)->delete();
        }

        return;
    }

    /**
     * Duplicate format rules
     * @param $oldFormatId
     * @param $newFormatId
     * @return mixed|null
     */
    public function duplicateFormatRules($oldFormatId, $newFormatId)
    {
        $formatRules = $this->findWhere(['format_id' => $oldFormatId]);
        $formatRulesData = [];
        foreach ($formatRules as $rule) {
            $newRule = $rule->toArray();
            unset($newRule['id']);
            $newRule['format_id'] = $newFormatId;
            $formatRulesData[] = $newRule;
        }
        if ($formatRulesData) {
            /**
             * Create multi records for format rules
             */
            return $this->model()::insert($formatRulesData);
        }
        return null;
    }

    /**
     * Remove format rules of the format
     * @param $formatId
     * @return mixed
     */
    public function removeRulesOfFormat($formatId)
    {
        return $this->model()::where('format_id',$formatId)->delete();
    }


    /**
     * Get format rules
     * @param $formatID
     * @return mixed
     */
    public function getFormatRules($formatID)
    {
        return $this->model()::where('format_id', $formatID)
            ->orderBy('priority', 'asc')->get();
    }
}
