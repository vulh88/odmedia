<?php

namespace Modules\Playlist\Policies;

use App\User;
use App\Playlist;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;
use Modules\Playlist\Repositories\Contracts\PlaylistRepositoryInterface;

class PlaylistPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the playlist.
     *
     * @param  \App\User  $user
     * @param  \App\Playlist  $playlist
     * @return mixed
     */
    public function view(User $user, Playlist $playlist)
    {
        return $playlist && $user->id == $playlist->user_id;
    }

    /**
     * Determine whether the user can create playlists.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the playlist.
     *
     * @param $user
     * @param $playlist
     * @return mixed
     */
    public function update($user, $playlist)
    {
        return $playlist && $user->id == $playlist->user_id;
    }

    /**
     * Determine whether the user can delete the playlist.
     *
     * @param  \App\User  $user
     * @param  \App\Playlist  $playlist
     * @return mixed
     */
    public function delete(User $user, Playlist $playlist)
    {
        //
    }
}
