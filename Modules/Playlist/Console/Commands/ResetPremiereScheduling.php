<?php

namespace Modules\Playlist\Console\Commands;

use Illuminate\Console\Command;
use Modules\Playlist\Services\ResetResourcePremiereService;
use Illuminate\Support\Facades\Log;
use App\Events\Cronjob;

class ResetPremiereScheduling extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reset-premiere';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset premiere for resources of playlist has start time less than now';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /**
         * Save logs for cronjob
         */
        event(new Cronjob([
            "type" => "reset-premiere",
            "description" => "Reset premiere for resources of playlist has start time less than now"])
        );

        /**
         * Reset premiere for resources of playlist has start time less than now
         */
        app()->make(ResetResourcePremiereService::class)->execute();
        //Log::info("reset-premiere");
    }

}