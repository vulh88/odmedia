<?php

namespace Modules\Playlist\Tests;

use Modules\Playlist\Repositories\Contracts\PlaylistItemRepositoryInterface;
use Modules\Playlist\Repositories\Contracts\PlaylistRepositoryInterface;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;


class UpdateTimeForPlaylistItemsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRemoveItems()
    {
        $status = app()->make(PlaylistItemRepositoryInterface::class)->removeItemsNotInPlaylistTime(4, "2018-08-23 17:00:00", "2018-08-23 18:00:00");
        $this->assertGreaterThan(0, $status);
    }

    public function testUpdateItems()
    {
        $status = app()->make(PlaylistItemRepositoryInterface::class)->updateTimeItemsToMatchedPlaylist(4, "2018-08-23 17:00:00", "2018-08-23 18:00:00");
        $this->assertGreaterThan(0, $status);
    }

}
