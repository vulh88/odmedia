<?php

namespace Modules\Playlist\Tests;

use Modules\Playlist\Repositories\Contracts\PlaylistRepositoryInterface;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Modules\Playlist\Services\ResetResourcePremiereService;
use App\Events\Cronjob;

class PlaylistSchedulerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testScheduling()
    {
        $service = app()->make(ResetResourcePremiereService::class);
        $service->execute();
        $this->assertTrue(true);
    }

    public function testEventSaveCronLogs()
    {
        event(new Cronjob([
                "type" => "reset-premiere",
                "description" => "Reset premiere for resources of playlist has start time less than now"])
        );
    }
}
