<?php

namespace Modules\Playlist\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\Playlist\Repositories\Contracts\PlaylistItemRepositoryInterface;
use Modules\Playlist\Repositories\Contracts\PlaylistRepositoryInterface;
use Modules\Playlist\Repositories\Eloquents\PlaylistItemRepository;
use Modules\Playlist\Repositories\Eloquents\PlaylistRepository;
use Modules\Playlist\Repositories\Contracts\EventBlockRepositoryInterface;
use Modules\Playlist\Repositories\Eloquents\EventBlockRepository;
use Modules\Playlist\Repositories\Eloquents\FormatRepository;
use Modules\Playlist\Repositories\Contracts\FormatRepositoryInterface;
use Modules\Playlist\Repositories\Eloquents\FormatRuleRepository;
use Modules\Playlist\Repositories\Contracts\FormatRuleRepositoryInterface;
use Modules\Playlist\Repositories\Eloquents\FormatContainerRepository;
use Modules\Playlist\Repositories\Contracts\FormatContainerRepositoryInterface;

class PlaylistServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PlaylistRepositoryInterface::class, PlaylistRepository::class);
        $this->app->bind(PlaylistItemRepositoryInterface::class, PlaylistItemRepository::class);
        $this->app->bind(EventBlockRepositoryInterface::class, EventBlockRepository::class);
        $this->app->bind(FormatRepositoryInterface::class, FormatRepository::class);
        $this->app->bind(FormatRuleRepositoryInterface::class, FormatRuleRepository::class);
        $this->app->bind(FormatContainerRepositoryInterface::class, FormatContainerRepository::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('playlist.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'playlist'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/playlist');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/playlist';
        }, \Config::get('view.paths')), [$sourcePath]), 'playlist');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/playlist');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'playlist');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'playlist');
        }
    }

    /**
     * Register an additional directory of factories.
     * 
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
