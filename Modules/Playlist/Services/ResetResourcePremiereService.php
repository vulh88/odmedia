<?php
namespace Modules\Playlist\Services;

use App\Services\Abstracts\BaseServiceAbstract;
use Modules\Playlist\Repositories\Contracts\PlaylistRepositoryInterface;
use Modules\Resource\Repositories\Contracts\ResourceRepositoryInterface;
use Log;

class ResetResourcePremiereService extends BaseServiceAbstract
{
    /**
     * Playlist repository
     * @var PlaylistRepositoryInterface
     */
    private $playlist;

    /**
     * Resource repository
     * @var ResourceRepositoryInterface
     */
    private $resource;

    function __construct(PlaylistRepositoryInterface $playlist, ResourceRepositoryInterface $resource)
    {
        $this->playlist = $playlist;
        $this->resource = $resource;
    }

    /**
     * Execute service
     * @param array $data
     * @return mixed
     */
    public function execute($data = [])
    {
        $result = $this->playlist->getPlaylistResourcesLessThanNow();
        $record = null;
        if ($result) {
            $resourceIds = array_column($result->toArray(), "resource_id");
            $record = $this->resource->model()::whereIn("id", $resourceIds)->update(['premiere' => 0]);
        }
        return $record;
    }

    /**
     * After execute service
     * @return mixed
     */
    public function afterExecuted()
    {

    }
}