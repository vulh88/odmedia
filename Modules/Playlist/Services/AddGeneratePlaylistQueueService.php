<?php
namespace Modules\Playlist\Services;

use App\Services\Abstracts\BaseServiceAbstract;
use Modules\Playlist\Repositories\Contracts\PlaylistRepositoryInterface;
use Log;

class AddGeneratePlaylistQueueService extends BaseServiceAbstract
{

    function __construct()
    {
    }

    /**
     * Execute service
     * @param array $data
     * @return mixed
     */
    public function execute($data = [])
    {
        //check file laravel.log will be seen queue excute
        Log::info("Add Generate Playlist Queue Service");
    }

    /**
     * After execute service
     * @return mixed
     */
    public function afterExecuted()
    {
        // TODO: Implement afterExecuted() method.
    }
}