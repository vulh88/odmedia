<?php

namespace Modules\Playlist\Services\Generating;

use Modules\Playlist\Services\Generating\ResourcesFactories\PremiereFactory;
use Modules\Playlist\Services\Generating\ResourcesFactories\AuthorFactory;
use Modules\Playlist\Services\Generating\ResourcesFactories\ResourcesRuleInterface;

class ResourcesFactory extends AbstractFactory
{

    private $ruleConfigurationsFactory;

    public function __construct(RuleConfigurationsFactory $ruleConfigurationsFactory)
    {
        $this->ruleConfigurationsFactory = $ruleConfigurationsFactory;
    }

    /**
     * Get resources list
     * @param $formatContentRule
     * @param array $args
     * @return array
     */
    function get($formatContentRule, $args = [])
    {

        $ruleConfigurationModel = $this->ruleConfigurationsFactory->get($formatContentRule, $args);
        $rule = $formatContentRule->rule_type;

        $objectClass = false;
        switch ($rule) {
            case config('playlist.format.rules.author'):
                $objectClass = new AuthorFactory();
                break;
            case config('playlist.format.rules.premiere'):
                $objectClass = new PremiereFactory();
                break;
        }

        if ($objectClass) {
            $objectClass->setConfigModel($ruleConfigurationModel);
            return $objectClass->getResources();
        }
    }
}