<?php

namespace Modules\Playlist\Services\Generating;

abstract class AbstractFactory
{
    /**
     * Get resources list
     * @param $formatContentRule
     * @param array $args
     * @return array
     */
    abstract function get($formatContentRule, $args = []);
}