<?php
namespace Modules\Playlist\Services\Generating\RuleConfigurationModel;


abstract class RuleConfigurationAbstract
{
    /**
     * @var $ruleRowData
     * each row fetched from DB
     */
    private $ruleRowData;

    /**
     * @var $ruleData
     * Data attribute from the $ruleRowData
     */
    private $ruleData;

    /**
     * @var $currentPosition
     * Current position
     */
    private $currentPosition;

    /**
     * @var $maxDuration
     * Max duration will be compared to the resource duration to get the data
     */
    private $maxDuration;

    private $playlistID;
    private $playlistStartTime;

    /**
     * @return mixed
     */
    public function getPlaylistStartTime()
    {
        return $this->playlistStartTime;
    }

    /**
     * @param mixed $playlistStartTime
     */
    public function setPlaylistStartTime($playlistStartTime)
    {
        $this->playlistStartTime = $playlistStartTime;
    }

    /**
     * @return mixed
     */
    public function getPlaylistID()
    {
        return $this->playlistID;
    }

    /**
     * @param mixed $playlistID
     */
    public function setPlaylistID($playlistID)
    {
        $this->playlistID = $playlistID;
    }

    /**
     * @param mixed $ruleRowData
     */
    public function setRuleRowData($ruleRowData)
    {
        $this->ruleRowData = $ruleRowData;
    }

    /**
     * @param mixed|string $ruleData
     */
    public function setRuleData($ruleData)
    {
        $this->ruleData = $ruleData;
    }

    /**
     * @param $name
     * @return mixed|string
     */
    public function getAttribute($name)
    {
        return isset($this->ruleData[$name]) ? $this->ruleData[$name] : '';
    }

    /**
     * Get reverse rule
     * @return boolean
     */
    public function getReverse() {
        return isset($this->ruleData['reverse']) && $this->ruleData['reverse'];
    }

    /**
     * @return mixed
     */
    public function getCurrentPosition()
    {
        return $this->currentPosition;
    }

    /**
     * @param mixed $currentPosition
     */
    public function setCurrentPosition($currentPosition)
    {
        $this->currentPosition = $currentPosition;
    }

    /**
     * @return mixed
     */
    public function getMaxDuration()
    {
        return $this->maxDuration;
    }

    /**
     * @param mixed $maxDuration
     */
    public function setMaxDuration($maxDuration)
    {
        $this->maxDuration = $maxDuration;
    }


}