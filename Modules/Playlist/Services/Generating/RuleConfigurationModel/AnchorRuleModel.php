<?php

namespace Modules\Playlist\Services\Generating\RuleConfigurationModel;

use Modules\Playlist\Services\Generating\RuleConfigurationModel;

class AnchorRuleModel extends RuleConfigurationAbstract
{
    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->getAttribute('time');
    }
}