<?php

namespace Modules\Playlist\Services\Generating\RuleConfigurationModel;


class PremiereRuleModel extends RuleConfigurationAbstract
{
    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->getAttribute('time');
    }
}