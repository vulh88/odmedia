<?php

namespace Modules\Playlist\Services\Generating\RuleConfigurationModel;


class FolderRuleModel extends RuleConfigurationAbstract
{

    public function __construct($ruleRowData)
    {
        parent::__construct($ruleRowData);
    }

    /**
     * @return mixed
     */
    public function getAuthorID()
    {
        return $this->getAttribute('author');
    }
}