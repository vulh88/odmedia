<?php

namespace Modules\Playlist\Services\Generating\RuleConfigurationModel;

class EventBlockRuleModel extends RuleConfigurationAbstract
{
    public function __construct($rule)
    {
        parent::__construct($rule);
    }

    public function getDuration() {
        return $this->getAttribute('eventBlockTime');
    }

    public function getEventBlockID() {
        return $this->getAttribute('eventBlockId');
    }
}