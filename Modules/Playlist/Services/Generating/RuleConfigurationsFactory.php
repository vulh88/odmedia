<?php
namespace Modules\Playlist\Services\Generating;

use Modules\Playlist\Services\Generating\RuleConfigurationModel\AnchorRuleModel;
use Modules\Playlist\Services\Generating\RuleConfigurationModel\EventBlockRuleModel;
use Modules\Playlist\Services\Generating\RuleConfigurationModel\AuthorRuleModel;

class RuleConfigurationsFactory extends AbstractFactory
{

    /**
     * Set rule model
     * @param $ruleRowData
     * @param array $additionalRules
     * @return array
     */
    function get($ruleRowData, $additionalRules = [])
    {
        $ruleType = isset($ruleRowData['rule_type']) ? $ruleRowData['rule_type'] : '';
        $objectClass = false;
        switch ($ruleType) {
            case config('playlist.format.rules.anchor'):
                $objectClass = new AnchorRuleModel();
                break;
            case config('playlist.format.rules.event_block'):
                $objectClass = new EventBlockRuleModel();
                break;
            case config('playlist.format.rules.author'):
                $objectClass =new AuthorRuleModel();
                break;
        }

        if ($objectClass) {
            $currentPosition = isset($additionalRules['current_position']) ? $additionalRules['current_position'] : '';
            $maxDuration = isset($additionalRules['max_duration']) ? $additionalRules['max_duration'] : '';
            $objectClass->setCurrentPosition($currentPosition);
            $objectClass->setMaxDuration($maxDuration);
            $objectClass->setRuleRowData($ruleRowData);
            $objectClass->setRuleData(isset($ruleRowData['data']) ? unserialize($ruleRowData['data']): '');
        }

        return $objectClass;
    }
}