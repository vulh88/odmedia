<?php

namespace Modules\Playlist\Services\Generating\ResourcesFactories;


use Modules\Playlist\Services\Generating\RuleConfigurationModel\PremiereRuleModel;
use Modules\Resource\Repositories\Contracts\ResourceRepositoryInterface;

class PremiereFactory extends ResourcesRuleAbstract
{

    /**
     * Get resources\
     * @return array
     */
    public function getResources()
    {
        $resourceRepo = app(ResourceRepositoryInterface::class);
        return $resourceRepo->getPremiereResources($this->getConfigModel()->getReverse());
    }

    /**
     * Get config model rule
     * @return mixed - config model class
     */
    public function getConfigModel() : PremiereRuleModel
    {
        // TODO: Implement getConfigModel() method.
    }
}