<?php

namespace Modules\Playlist\Services\Generating\ResourcesFactories;

use Modules\Playlist\Services\Generating\RuleConfigurationModel\AuthorRuleModel;
use Modules\Playlist\Services\Generating\ResourcesFactories\ResourcesRuleInterface;

class AuthorFactory extends ResourcesRuleAbstract
{
    /**
     * @return array
     * @internal param $formatLength
     * @internal param $currentPosition
     */
    public function getResources()
    {
        $authorID = $this->configModel->getAuthorID();
        $maxDuration = $this->configModel->getAuthorID();
        $reverse = $this->configModel->getAuthorID();
        $this->configModel->getPlaylistID();

        if (!$authorID) {
            return [];
        }

        return $this->resourceRepo->getResourcesByAuthor($authorID, $maxDuration, $reverse);
    }

    /**
     * Get config model rule
     * @return mixed - config model class
     */
    public function getConfigModel() : AuthorRuleModel {
        return $this->configModel;
    }
}