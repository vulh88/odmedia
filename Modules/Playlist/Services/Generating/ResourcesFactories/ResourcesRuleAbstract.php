<?php

namespace Modules\Playlist\Services\Generating\ResourcesFactories;

use Modules\Resource\Repositories\Contracts\ResourceRepositoryInterface;

abstract class ResourcesRuleAbstract
{
    protected $resourceRepo;
    protected $configModel;

    /**
     * Get resources
     * @return array
     */
    abstract public function getResources();

    /**
     * Get config model rule
     * @return mixed - config model class
     */
    abstract public function getConfigModel();


    public function __construct(ResourceRepositoryInterface $resourceRepository)
    {
        $this->resourceRepo = $resourceRepository;
    }

    /**
     * Set config model rule
     * @param $configModel
     * @return mixed
     */
    public function setConfigModel($configModel)
    {
        $this->configModel = $configModel;
    }
}