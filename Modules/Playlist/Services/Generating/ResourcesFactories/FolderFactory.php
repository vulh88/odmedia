<?php

namespace Modules\Playlist\Services\Generating\ResourcesFactories;

use Modules\Playlist\Services\Generating\RuleConfigurationModel\FolderRuleModel;

class FolderFactory extends ResourcesRuleAbstract
{
    /**
     * Get resources
     * @return array
     */
    public function getResources()
    {
        $authorID = $this->getConfigModel()->getAuthorID();
        if (!$authorID) {
            return [];
        }

        return $this->resourceRepo->getResourcesByFolder($authorID, $this->getConfigModel()->getReverse());
    }


    /**
     * Get config model rule
     * @return mixed - config model class
     */
    public function getConfigModel() : FolderRuleModel
    {
        return $this->configModel;
    }
}