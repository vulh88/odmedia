<?php
namespace Modules\Playlist\Services\Generating\RuleCommands;

interface CommandInterface
{
    /**
     * Execute function
     * @param $ruleData
     * @param array $arg
     * @return mixed
     */
    public function execute($ruleData, $arg = []);
}