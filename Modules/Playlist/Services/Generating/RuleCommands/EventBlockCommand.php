<?php

namespace Modules\Playlist\Services\Generating\RuleCommands;

use Modules\Playlist\Repositories\Contracts\EventBlockRepositoryInterface;
use Modules\Playlist\Repositories\Contracts\PlaylistItemRepositoryInterface;
use Modules\Playlist\Services\Generating\RuleConfigurationModel\EventBlockRuleModel;
use Modules\Playlist\Services\Generating\RuleConfigurationsFactory;

class EventBlockCommand extends CommandRuleAbstract
{
    /**
     * Execute function
     * @return mixed
     */
    public function execute()
    {
        $eventBlockRepo = app(EventBlockRepositoryInterface::class);
        $playlistItemRepo = app(PlaylistItemRepositoryInterface::class);

        $eventBlockData = $eventBlockRepo->find($this->getConfigModel()->getEventBlockID());
        $currentPosition = $this->getConfigModel()->getCurrentPosition();

        $playlistItemData = [
            'playlist_id'   => $this->getConfigModel()->getPlaylistID(),
            'start_time'    => $this->getConfigModel()->getPlaylistStartTime(),
            'resource_id'    => 0,
            'event_block_type'  => $eventBlockData['type'],
            'item_track'  => config('api.content_track'),
        ];


        $increaseDuration = $this->getConfigModel()->getDuration() ? $this->getConfigModel()->getDuration() :
            $eventBlockData['default_duration'];

        $currentPosition = $currentPosition + $increaseDuration;
        $playlistItemData['end_time'] = date("Y-m-d H:i:s", $currentPosition);

        $playlistItemRepo->create($playlistItemData);

        return $currentPosition;
    }

    /**
     * Get config model rule
     * @return mixed - config model class
     */
    public function getConfigModel() : EventBlockRuleModel
    {
        return $this->configModel;
    }
}