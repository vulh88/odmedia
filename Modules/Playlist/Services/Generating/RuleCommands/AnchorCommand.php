<?php

namespace Modules\Playlist\Services\Generating\RuleCommands;


use Modules\Playlist\Repositories\Contracts\PlaylistItemRepositoryInterface;
use Modules\Playlist\Services\Generating\RuleConfigurationModel\AnchorRuleModel;

class AnchorCommand extends CommandRuleAbstract
{
    private $playlistItemRepo;

    public function __construct(PlaylistItemRepositoryInterface $playlistItemRepository)
    {
        $this->playlistItemRepo = $playlistItemRepository;
    }

    /**
     * Execute function
     * @return mixed
     */
    public function execute()
    {
        $currentPosition = $this->getConfigModel()->getCurrentPosition();
        $anchorHour = $this->getConfigModel()->getTime();

        // New situation: delete all playlist items that are generated past the cufoff point - NvA 20180222 (TODO!)
        $currentHour = date("H:i", $currentPosition);
        $currentDate = date("Y-m-d", $currentPosition);

        /**
         * The current position is not over the anchor is perfect
         */
        $currentPosition = strtotime($currentDate. ' '. $anchorHour);
        if (strtotime($anchorHour) >= strtotime($currentHour)) {
            return $currentPosition;
        }

        /**
         * Delete all previous content items after the anchor from the playlist
         */
        $this->playlistItemRepo->deletePlaylistItemAfterAfterTime($currentPosition);

        return $currentPosition;
    }

    /**
     * Get config model rule
     * @return mixed - config model class
     */
    public function getConfigModel() : AnchorRuleModel
    {
        return $this->configModel;
    }
}