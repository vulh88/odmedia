<?php

namespace Modules\Playlist\Services\Generating\RuleCommands;

abstract class CommandRuleAbstract
{
    protected $configModel;

    /**
     * Get config model rule
     * @return mixed - config model class
     */
    abstract public function getConfigModel();

    /**
     * Execute function
     * @return mixed
     */
    abstract public function execute();

    /**
     * @param mixed $configModel
     */
    public function setConfigModel($configModel)
    {
        $this->configModel = $configModel;
    }
}