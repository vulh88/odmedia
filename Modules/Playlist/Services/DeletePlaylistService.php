<?php

namespace Modules\Playlist\Services;

use Modules\Playlist\Repositories\Contracts\PlaylistRepositoryInterface;
use App\Services\Abstracts\DeleteEntityServiceAbstract;

use Log;

class DeletePlaylistService extends DeleteEntityServiceAbstract
{

    public function __construct(PlaylistRepositoryInterface $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Process after destroy
     *
     * @return mixed
     */
    public function afterDestroy()
    {

    }

    /**
     * Process after soft delete
     *
     * @return mixed
     */
    function afterSoftDelete()
    {
        // TODO: Implement afterSoftDelete() method.
    }

    /**
     * Process after restore
     *
     * @return mixed
     */
    function afterRestore()
    {
        // TODO: Implement afterRestore() method.
    }
}
