<?php

namespace Modules\Playlist\Services\Scheduler;

use Modules\Playlist\Services\Import\SchedulerAbstract;
use Modules\Playlist\Repositories\Contracts\EventBlockRepositoryInterface;
use Modules\Playlist\Repositories\Contracts\PlaylistItemRepositoryInterface;
use Modules\Playlist\Repositories\Contracts\PlaylistRepositoryInterface;
use Modules\Resource\Repositories\Contracts\ResourceRepositoryInterface;

class ManualScheduleService extends SchedulerAbstract
{

    /**
     * @var $playlistItemRepository
     */
    private $playlistItemRepository;

    /**
     * @var $resourceRepository
     */
    private $resourceRepository;

    /**
     * @var $playlistRepository
     */
    private $playlistRepository;

    /**
     * @var $eventBlockRepository
     */
    private $eventBlockRepository;



    public function __construct(PlaylistItemRepositoryInterface $playlistItemRepository,
                                ResourceRepositoryInterface $resourceRepository,
                                PlaylistRepositoryInterface $playlistRepository,
                                EventBlockRepositoryInterface $eventBlockRepository)
    {
        $this->playlistItemRepository = $playlistItemRepository;
        $this->resourceRepository = $resourceRepository;
        $this->playlistRepository = $playlistRepository;
        $this->eventBlockRepository = $eventBlockRepository;
    }

    /**
     * Do import
     * @param array $validated
     * @param bool $playlistItemId
     * @return mixed
     */
    public function execute($validated = [], $playlistItemId = false)
    {
        $playlist = $this->playlistRepository->find($validated['playlist_id']);

        // Playlist time
        $playlistStartTime = $playlist->time_start;
        $playlistEndTime = $playlist->time_end;

        // Item time on the timeline
        $itemStartTime = $validated['start_time'];
        $itemEndTime = $validated['end_time'];

        /**
         * @Todo fix time format from the client side
         */

        // Check if the date is invalid, the item time is out of the playlist time range
//        if (strtotime($itemStartTime) < strtotime($playlistStartTime) || strtotime($itemEndTime) > strtotime($playlistEndTime)) {
//            return response()->error(__('Invalid datetime'), 400);
//        }

        if ($playlistItemId) {
            $playlistItem = $this->playlistItemRepository->find($playlistItemId);
        }
        else {
            // Create playlist item
            $playlistItem = $this->playlistItemRepository->newInstance();
        }

        $playlistItem->playlist_id = $validated['playlist_id'];
        $playlistItem->start_time = $itemStartTime;
        $playlistItem->end_time = $itemEndTime;

        // Is event block
        $resource = [];
        if (isset($validated['event_block_id'])) {
            $eventBlock = $this->eventBlockRepository->find($validated['event_block_id']);
            if (!$eventBlock) {
                return response()->error(__('Invalid event block'), 400);
            }

            $playlistItem->item_track = config('api.content_track');
            $playlistItem->event_block_type = $eventBlock->type;
            $playlistItem->event_block_data = isset($validated['event_block_data']) ? $validated['event_block_data'] : '';
        }
        else {
            $resource = $this->resourceRepository->find($validated['resource_id']);

            // Check whether the item type is content or overlay
            if ($validated['item_track'] == 1) { // Overlay item
                // Meaning is overlay item
                $playlistItem->item_track = config('api.overlay_track');
            }
            else { // Content item
                // Meaning is content item
                $playlistItem->item_track = config('api.content_track');
            }
        }
        $playlistItem->resource_id = $validated['resource_id'];
        $playlistItem->save();

        return [
            'playlist'  => $playlist,
            'resource'  => $resource,
            'playlist_item' => $playlistItem
        ];
    }
}