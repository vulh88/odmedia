<?php

namespace Modules\Playlist\Services\Import;


abstract class SchedulerAbstract
{
    protected $importData;

    /**
     * Do import
     * @param $playlistId
     * @param array $data
     * @return mixed
     * @internal param array $args
     */
    abstract public function execute($data = [], $playlistId = false);
}