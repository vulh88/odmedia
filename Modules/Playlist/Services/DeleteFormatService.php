<?php

namespace Modules\Playlist\Services;

use Modules\Playlist\Repositories\Contracts\FormatRepositoryInterface;
use App\Services\Abstracts\DeleteEntityServiceAbstract;

use Log;

class DeleteFormatService extends DeleteEntityServiceAbstract
{

    public function __construct(FormatRepositoryInterface $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Process after destroy
     *
     * @return mixed
     */
    public function afterDestroy()
    {

    }

    /**
     * Process after soft delete
     *
     * @return mixed
     */
    function afterSoftDelete()
    {
        // TODO: Implement afterSoftDelete() method.
    }

    /**
     * Process after restore
     *
     * @return mixed
     */
    function afterRestore()
    {
        // TODO: Implement afterRestore() method.
    }
}
