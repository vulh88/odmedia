<?php

namespace Modules\Playlist\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Resource\Entities\Resource;

class PlaylistItems extends Model
{
    protected $fillable = [
        'playlist_id',
        'start_time',
        'end_time',
        'resource_id',
        'event_block_type',
        'event_block_data',
    ];

    /**
     * Relation with resource entity
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function resource() {
        return $this->belongsTo(Resource::class, "resource_id", "id");
    }
}
