<?php

namespace Modules\Playlist\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Format extends Model
{

    protected $fillable = [
        'name',
        'channel_id',
        'user_id',
        'length',
        'filling_out_time',
        'dynamic_sub_format',
        'active',
        'direct_publish',
    ];

    public function formatRules()
    {
        return $this->hasMany(FormatRule::class)->orderBy("priority", "asc");
    }

}
