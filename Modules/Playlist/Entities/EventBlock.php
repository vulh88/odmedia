<?php

namespace Modules\Playlist\Entities;

use Illuminate\Database\Eloquent\Model;

class EventBlock extends Model
{

    protected $fillable = [
        'name',
        'channel_id',
        'type',
        'default_duration'
    ];
}
