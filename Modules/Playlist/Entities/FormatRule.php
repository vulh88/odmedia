<?php

namespace Modules\Playlist\Entities;

use Illuminate\Database\Eloquent\Model;

class FormatRule extends Model
{
    protected $fillable = [
        'format_id',
        'rule_type',
        'data',
        'container_id',
        'reverse',
        'priority',
    ];
}
