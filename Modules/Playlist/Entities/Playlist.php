<?php

namespace Modules\Playlist\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Playlist extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'start_time',
        'end_time',
        'status',
        'is_auto_generated',
        'channel_id',
        'user_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * append fields
     * @var array
     */
    protected $appends = ['changeStateTimeline'];

    /**
     * Get attribute for changeStateTimeline
     * The variable used to set state then response for client to reload time line in UI
     * @param int $value
     * @return int
     */
    public function getChangeStateTimelineAttribute($value = 0)
    {
        return $value;
    }

    /**
     * Get data relation with playlist items entity
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function playlistItems()
    {
        return $this->hasMany(PlaylistItems::class);
    }
}
