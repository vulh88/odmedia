<?php

namespace Modules\User\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepositoryInterface
 * @package namespace App\Repositories\Contracts;
 */
interface UserRepositoryInterface extends RepositoryInterface
{
    /**
     * @param array|null $columns
     * @param array $params
     * @return mixed
     */
    public function search($columns = array(), $params = []);
}
