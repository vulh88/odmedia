<?php

namespace Modules\User\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepositoryInterface
 * @package namespace App\Repositories\Contracts;
 */
interface ChannelUsersRepositoryInterface extends RepositoryInterface
{
    /**
     * @param array $column
     * @param array $params
     *
     * @return mixed
     */
    public function search($column = array(), $params = []);
    
    /**
     * @param array $field
     *
     * @return mixed
     */
    public function deleteChannel($field = array());

    /**
     * Update fields data for channel assigned
     * @param array $conditions
     * @param array $attributes
     * @return mixed
     */
    public function updateChannelUser(array $conditions, array $attributes);

    /**
     * @param $userId
     * @return string
     */
    public function getChannelsOfUser($userId);
    
}
