<?php

namespace Modules\User\Repositories\Eloquents;

use Elidev\Repository\Traits\PaginationTrait;
use Elidev\Repository\Eloquent\BaseRepository;
use Modules\User\Repositories\Contracts\UserRepositoryInterface;
use App\User;


/**
 * Class UserRepository
 * @package namespace App\Repositories\Eloquents;
 */
class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    use PaginationTrait;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * @param array|null $columns
     * @param array $request
     * @return mixed
     */
    public function search($columns = array(), $request = [])
    {
        $params = $request->all();
        $pagination = $this->extractPagination($params);
        $request->page = $pagination['page'];
        $query = $this->model()
            ::addSelect($columns)
            ->orderBy($pagination['sortColumn'],$pagination['sortOrder']);
        if (!empty($params['keyword'])) {
            $query
                ->where('first_name', 'like', '%'.$params['keyword'].'%')
                ->orWhere('last_name', 'like', '%'.$params['keyword'].'%')
                ->orWhere('email', 'like', '%'.$params['keyword'].'%');
        }

        if (isset($params['trash']) && $params['trash'] == 1) {
            $query->onlyTrashed();
        }


        return $query->paginate($pagination['count'], $columns);
    }
}
