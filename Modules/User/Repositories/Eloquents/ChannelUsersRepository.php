<?php

namespace Modules\User\Repositories\Eloquents;

use Illuminate\Support\Facades\DB;
use Elidev\Repository\Eloquent\BaseRepository;
use Modules\User\Entities\ChannelUsers;
use Modules\User\Repositories\Contracts\ChannelUsersRepositoryInterface;
use Elidev\Repository\Traits\PaginationTrait;


/**
 * Class Channel
 *
 * @package namespace App\Repositories\Eloquents;
 */
class ChannelUsersRepository extends BaseRepository implements ChannelUsersRepositoryInterface
{
    use PaginationTrait;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ChannelUsers::class;
    }
    
    /**
     * @param array $column
     * @param array $params
     *
     * @return \Illuminate\Support\Collection|mixed
     */
    public function search($column = array(), $params = [])
    {
        $pagination = $this->extractPagination($params);
        $params['user_id'] = !empty($params['user_id']) ? 'NOT NULL' : 'NULL';

        $column = implode(', ', $column);
        $table = 'channel_users';
        $orderBy = $pagination['sortColumn'] . ' ' . $pagination['sortOrder'];
        $offset = $pagination['skip'];
        $limit = $pagination['count'];
        $where = 'user_id = ' . $params['user_id'];

        if (!empty($params['keyword'])) {
            $where = $where . " AND name LIKE '" . '%' . $params['keyword'] . '%' . "'";
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS $column FROM $table WHERE $where ORDER BY $orderBy LIMIT $limit OFFSET $offset";
        $query = DB::select($sql);

        return collect([
            'data'  => collect($query),
            'total' => DB::select(DB::raw("SELECT FOUND_ROWS() AS 'total'"))[0]->total
        ]);
    }
    
    /**
     * @param array $field
     *
     * @return int|mixed
     */
    public function deleteChannel($field = [])
    {
        $channel = $this->findWhere($field)->first();
        if ($channel) {
            return $channel->delete();
        }
        return null;
    }

    /**
     * Update fields data for channel assigned
     * @param array $conditions
     * @param array $attributes
     * @return mixed
     */
    public function updateChannelUser(array $conditions, array $attributes)
    {
        $model = $this->findWhere($conditions)->first();
        $model->fill($attributes)->save();
        return $model;
    }

    /**
     * @param $userId
     * @return string
     */
    public function getChannelsOfUser($userId)
    {
        $model = $this->model();
        $query = $model::addSelect(['channel_users.channel_id', 'channels.name']);
        $query->join('channels', 'channel_users.channel_id', '=', 'channels.id');
        $query->where('channel_users.user_id', $userId);
        $query->where('channels.deleted_at', NULL);
        $query->orderBy('channel_users.id', 'DESC');

        return $query->get()->toArray();
    }
    
}
