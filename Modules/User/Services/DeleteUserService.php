<?php

namespace Modules\User\Services;

use Modules\User\Repositories\Contracts\UserRepositoryInterface;
use App\Services\Abstracts\DeleteEntityServiceAbstract;

use Log;

class DeleteUserService extends DeleteEntityServiceAbstract
{

    public function __construct(UserRepositoryInterface $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Process after destroy
     *
     * @return mixed
     */
    public function afterDestroy()
    {

    }

    /**
     * Process after soft delete
     *
     * @return mixed
     */
    function afterSoftDelete()
    {
        // TODO: Implement afterSoftDelete() method.
    }

    /**
     * Process after restore
     *
     * @return mixed
     */
    function afterRestore()
    {
        // TODO: Implement afterRestore() method.
    }
}
