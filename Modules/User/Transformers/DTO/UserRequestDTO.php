<?php

namespace Modules\User\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="UserRequestDTO"
 * )
 */
class UserRequestDTO
{
    /**
     * @var string
     *
     * @SWG\Property(
     *    property="first_name",
     *    type="string",
     *    description="First name"
     * )
     */
    protected $first_name;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="last_name",
     *    type="string",
     *    description="Last name"
     * )
     */
    protected $last_name;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="email",
     *    type="string",
     *    description="Email *"
     * )
     */
    protected $email;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="password",
     *    type="string",
     *    description="Password *"
     * )
     */
    protected $password;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="password_confirmation",
     *    type="string",
     *    description="Password confirmation *"
     * )
     */
    protected $password_confirmation;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="active",
     *    type="integer",
     * )
     */
    protected $active;
}
