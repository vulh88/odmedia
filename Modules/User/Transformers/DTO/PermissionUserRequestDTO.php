<?php

namespace Modules\User\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="PermissionUserRequestDTO"
 * )
 */
class PermissionUserRequestDTO
{
    /**
     * @var string
     *
     * @SWG\Property(
     *    property="permissions_id",
     *    type="string",
     *    description="One or multiple permissions id, separated by commas"
     * )
     */
    protected $permissions_id;
}
