<?php

namespace Modules\User\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="PermissionRoleRequestDTO"
 * )
 */
class PermissionRoleRequestDTO
{
    /**
     * @var string
     *
     * @SWG\Property(
     *    property="permissions_id",
     *    type="string",
     *    description="One or multiple permissions id, separated by commas"
     * )
     */
    protected $permissions_id;
}
