<?php

namespace Modules\User\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="ChannelUserRequestDTO"
 * )
 */
class ChannelUserRequestDTO
{
    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="channel_id",
     *    type="integer",
     *    description="Channel ID"
     * )
     */
    protected $channel_id;

    /**
     * @var $permissions
     *
     * @SWG\Property(
     *    property="permissions",
     *    type="string",
     *    description="Permissions are separated by comas such as users.view,user.edit,user.delete"
     * )
     */
    protected $permissions;

}
