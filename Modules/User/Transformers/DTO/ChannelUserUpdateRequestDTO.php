<?php

namespace Modules\User\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="ChannelUserUpdateRequestDTO"
 * )
 */
class ChannelUserUpdateRequestDTO
{

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="channel_id",
     *    type="integer",
     *    description="Channel ID"
     * )
     */
    protected $channel_id;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="resource_folder_id",
     *    type="integer",
     *    description="Resource Folder ID"
     * )
     */
    protected $resource_folder_id;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="player_directory",
     *    type="string",
     *    description="Player directory"
     * )
     */
    protected $player_directory;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="api_path",
     *    type="string",
     *    description="Api path"
     * )
     */
    protected $api_path;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="api_auth",
     *    type="string",
     *    description="Api auth"
     * )
     */
    protected $api_auth;
}
