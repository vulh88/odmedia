<?php

namespace Modules\User\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="AssignRoleToUserRequestDTO"
 * )
 */
class AssignRoleToUserRequestDTO
{
    /**
     * @var string
     *
     * @SWG\Property(
     *    property="acl_group",
     *    type="string",
     *    description="ACL group"
     * )
     */
    protected $acl_group;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="acl_permission",
     *    type="string",
     *    description="ACL permission"
     * )
     */
    protected $acl_permission;
}
