<?php

namespace Modules\User\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="RoleUserRequestDTO"
 * )
 */
class RoleUserRequestDTO
{
    /**
     * @var int
     *
     * @SWG\Property(
     *    property="role_id",
     *    type="integer",
     *    description="Role Id"
     * )
     */
    protected $role_id;
}
