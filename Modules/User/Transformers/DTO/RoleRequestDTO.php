<?php

namespace Modules\User\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="RoleRequestDTO"
 * )
 */
class RoleRequestDTO
{
    /**
     * @var string
     *
     * @SWG\Property(
     *    property="name",
     *    type="string",
     *    description="Name"
     * )
     */
    protected $name;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="guard_name",
     *    type="string",
     *    description="Guard name"
     * )
     */
    protected $guard_name;
}
