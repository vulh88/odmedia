<?php
	
namespace Modules\User\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Channel\Transformers\ChannelResource;

class ChannelUsersResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        if (!isset($this->channel_id)) {
            return [];
        }

        return [
            'channel_id'            => $this->channel_id,
            'user_id'               => $this->user_id,
            'resource_folder_id'             => $this->resource_folder_id,
            'thumbnail_storage'     => $this->thumbnail_storage,
            'player_directory'      => $this->player_directory,
            'api_path'              => $this->api_path,
            'api_auth'              => $this->api_auth,
            'updated_at'            => $this->updated_at->format(config('api.format_datetime')),
            'created_at'            => $this->created_at->format(config('api.format_datetime')),
            'channel'   => new ChannelResource($this->channel),
        ];
    }
}
