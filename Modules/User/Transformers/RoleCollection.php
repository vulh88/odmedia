<?php

namespace Modules\User\Transformers;

use App\Services\ApiTrait\ResourceCollectionTrait;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class RoleCollection
 * @package Modules\User\Transformers
 */
class RoleCollection extends ResourceCollection
{
    use ResourceCollectionTrait;

    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Support\Collection
     */
    public function toArray($request)
    {
        return $this->collection;
    }

    /**
     * Customize the outgoing response for the resource.
     *
     * @param \Illuminate\Http\Request
     * @param \Illuminate\Http\JsonResponse
     * @return void
     */
    public function withResponse($request, $response)
    {
        $skip = !empty($request->get('skip')) ? $request->get('skip') : 0;
        $count = !empty($request->get('count')) ? $request->get('count') : 100;
        $response->header('X-Total', $this->getTotal());
        $response->header('X-Limit', $count);
        $response->header('X-Limit', $skip);
    }
}
