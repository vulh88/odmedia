<?php

namespace Modules\User\Transformers;

use Illuminate\Http\Resources\Json\Resource;

/**
 * Class RolePermissionResource
 * @package Modules\User\Transformers
 */
class RolePermissionResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'role_id' => $this->id,
            'permission_id' => $this->id,
        ];
    }
}
