<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Modules\Channel\Entities\Channel;

class ChannelUsers extends Model
{
    use HasRoles;

    protected $guard_name = 'api';

	protected $table = 'channel_users';

	protected $fillable =
    [
        'channel_id',
        'user_id',
        'resource_folder_id',
        'player_directory',
        'api_path',
        'api_auth',
    ];

    /**
     * Get the channel record associated with the channel user.
     */
    public function channel()
    {
        return $this->hasOne(Channel::class, 'id', 'channel_id');
    }

}
