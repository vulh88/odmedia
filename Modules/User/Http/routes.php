<?php

Route::post('api/v1/login', 'Modules\User\Http\Controllers\Api\Auth\LoginController@login')->name('auth.login');

// API
Route::group([
    'prefix' => 'api/v1',
    'middleware' => ['auth:api', 'cors'],
    'namespace' => 'Modules\User\Http\Controllers\Api'], function () {
    Route::get('logout', "UserController@logout")->name('users.logout');
    Route::put('users/{id}/trash', "UserController@trash")->name('users.trash');
    Route::put('users/{id}/restore', "UserController@restore")->name('users.restore');
    Route::post('users/{user}/assign-permissions', "UserController@assignPermissionToUser")->name('users.assign-permissions');
    Route::post('users/{user}/assign-role', "UserController@assignRoleToUser")->name('users.assign-role');
    Route::get('users/channel-settings', "UserController@channelSettings")->name('users.channel-settings');
    Route::put('users/update-channel', "UserController@updateChannel")->name('users.update-channel');
    Route::get('users/channels', "UserController@listChannelsUser")->name('users.channels');
    Route::resource('users', 'UserController');


    Route::get('permissions/groups', 'PermissionController@getChannelPermissionGroups')->name('roles.permissionGroups');
    Route::resource('permissions', 'PermissionController');


    Route::post('roles/{role_id}/assign-permissions', "RoleController@assignPermissionsToRole")->name('roles.assign-permissions');
    Route::resource('roles', 'RoleController');


    Route::get('assignments/{user}/assign-channel', "AssignmentController@getAssignedChannel")->name('assignments.getAssignedChannel');
    Route::post('assignments/{user}/assign-channel', "AssignmentController@postAssignChannel")->name('assignments.postAssignChannel');
    Route::resource('assignments', 'AssignmentController');
});