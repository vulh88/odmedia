<?php

namespace Modules\User\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\ApiController;
use Modules\User\Http\Requests\FormPermissionRequest;
use Elidev\ACL\Repositories\Contracts\PermissionRepositoryInterface;
use Modules\User\Transformers\PermissionCollection;
use Modules\User\Transformers\PermissionResource;

class PermissionController extends ApiController
{
    /**
     * Load AppHelpers
     */
    use \AppHelpers;

    /**
     * Get list of permissions.
     *
     * @param PermissionRepositoryInterface $permissionRepository
     * @param Request $request
     * @return PermissionCollection
     *
     * @SWG\Get(
     *     path="/permissions",
     *     tags={"System roles"},
     *     operationId="getPermissions",
     *     description="Get permission items",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="filter[keyword]", in="query", type="string", description="Search by name"),
     *
     *     @SWG\Parameter(name="sort", in="query", type="string", description="Field to sort by, if prefixed with '-', the sorting direction will be descending"),
     *     @SWG\Parameter(name="skip", in="query", type="integer", description="Number of records to skip"),
     *     @SWG\Parameter(name="count", in="query", type="integer", description="Number of records to display"),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *         @SWG\Header(header="X-Total", type="integer", description="Total count of permissions"),
     *         @SWG\Header(header="X-Skipped", type="integer", description="Permissions skipped"),
     *         @SWG\Header(header="X-Limit", type="integer", description="Permission limit")
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function index(PermissionRepositoryInterface $permissionRepository, Request $request)
    {
        $data = $request->all();
        $columns = ['id', 'name', 'guard_name', 'updated_at', 'created_at'];
        $permissionList = $permissionRepository->search($columns, $data);

        return new PermissionCollection($permissionList->getCollection(), $permissionList->total(), $permissionList->currentPage(), $permissionList->lastPage());
    }

    /**
     * Permission groups
     *
     * @return Collection
     *
     * @SWG\Get(
     *     path="/permissions/groups",
     *     tags={"ACL"},
     *     operationId="getPermissions",
     *     description="Get permission items",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function getChannelPermissionGroups() {

        $rolePermissions = acl_get_role_permissions();
        $rolePermissions = (array) $rolePermissions;
        foreach ($rolePermissions as $key =>  $role) {
            $data = [];
            foreach ($role->data as $action) {
                $data[] = [
                    'label' => self::renameLabelPermission(explode('.', $action)[1]),
                    'name' => $key,
                    'value' => $action,
                    'checked' => false
                ];
            }
            $role->data = $data;
        }

//        if (isset($rolePermissions['users'])) {
//            unset($rolePermissions['users']);
//        }

        if (isset($rolePermissions['roles'])) {
            unset($rolePermissions['roles']);
        }

        if (isset($rolePermissions['permissions'])) {
            unset($rolePermissions['permissions']);
        }

        if (isset($rolePermissions['assignments'])) {
            unset($rolePermissions['assignments']);
        }

        if (isset($rolePermissions['channels'])) {
            unset($rolePermissions['channels']);
        }

        return response()->json(array_values($rolePermissions));
    }

    /**
     * Create a permission.
     * @param PermissionRepositoryInterface $permissionRepository
     * @param FormPermissionRequest $request
     * @return PermissionResource
     *
     * @SWG\Post(
     *     path="/permissions",
     *     tags={"System roles"},
     *     operationId="postPermission",
     *     description="Creates new permission.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Permission object that needs to be created",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/PermissionRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successfully created resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of created permission"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function store(PermissionRepositoryInterface $permissionRepository, FormPermissionRequest $request)
    {
        return new PermissionResource($permissionRepository->create($request->validated()));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {

    }

    /**
     * Updates a permission
     *
     * @param $id
     * @param PermissionRepositoryInterface $permissionRepository
     * @param FormPermissionRequest $request
     * @return \Illuminate\Http\JsonResponse|PermissionResource
     *
     * @SWG\Put(
     *     path="/permissions/{id}",
     *     tags={"System roles"},
     *     operationId="putPermission",
     *     description="Creates new (or updates existing) permission.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     * 			name="id",
     * 			in="path",
     * 			required=true,
     * 			type="string",
     * 			description="UUID",
     * 		),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Permission object that needs to be Updated",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/PermissionRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successfully updated resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of updated permission"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function update($id, PermissionRepositoryInterface $permissionRepository, FormPermissionRequest $request)
    {
        $permission = $permissionRepository->find($id);
        if (!$permission) {
            return response()->json(['error' => __('Can not find permission with id ') . $id]);
        }
        $data = $permissionRepository->update($request->all(), $id);
        acl_cache_or_create_roles_permissions();
        return new PermissionResource($data);
    }

    /**
     * Delete existing permission.
     *
     * @param $id
     * @param PermissionRepositoryInterface $permissionRepository
     * @return \Illuminate\Http\JsonResponse
     * @SWG\Delete(
     *     path="/permissions/{id}",
     *     tags={"System roles"},
     *     operationId="deletePermission",
     *     description="Delete existing permission",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of permission that should be deleted",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of deleted permission"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Resource not found.",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function destroy($id, PermissionRepositoryInterface $permissionRepository)
    {
        if ( $permissionRepository->delete($id)) {
            return response()->noContent();
        }

        return response()->error(__('Resource tag does not exist'), 404);
    }
}
