<?php

namespace Modules\User\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Channel\Repositories\Contracts\ChannelRepositoryInterface;
use Modules\Resource\Repositories\Contracts\ResourceRepositoryInterface;
use Modules\User\Http\Requests\UserRequest;
use Modules\User\Repositories\Contracts\ChannelUsersRepositoryInterface;
use Modules\User\Repositories\Contracts\PermissionRepositoryInterface;
use Modules\User\Repositories\Contracts\RoleRepositoryInterface;
use Modules\User\Repositories\Eloquents\ChannelUsersRepository;
use Modules\User\Transformers\ChannelUsersResource;
use Modules\User\Transformers\UserCollection;
use Modules\User\Http\Requests\ChannelUsersRequest;
use Modules\User\Repositories\Contracts\UserRepositoryInterface;
use Modules\User\Transformers\UserResource;
use Modules\ApiController;
use Modules\User\Services\DeleteUserService;
use Illuminate\Support\Facades\Hash;

class UserController extends ApiController
{
    /**
     * Load AppHelpers
     */
    use \AppHelpers;

    /**
     * Get list of users.
     *
     * @param UserRepositoryInterface $userRepository
     * @param Request $request
     * @return UserCollection
     *
     * @SWG\Get(
     *     path="/users",
     *     tags={"Users"},
     *     operationId="getUsers",
     *     description="Gets user items",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="keyword", in="query", type="string", description="Search by email or first name or last name"),
     *     @SWG\Parameter(name="active", in="query", type="string", description="User active status"),
     *
     *     @SWG\Parameter(name="sort", in="query", type="string", description="Field to sort by, if prefixed with '-', the sorting direction will be descending"),
     *     @SWG\Parameter(name="page", in="query", type="integer", description="Page pagination"),
     *     @SWG\Parameter(name="count", in="query", type="integer", description="Number of records to display"),
     *     @SWG\Parameter(name="trash", in="query", type="integer", description="Only trashed users (trash=1)"),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *         @SWG\Header(header="X-Total", type="integer", description="Total count of users"),
     *         @SWG\Header(header="X-Skipped", type="integer", description="Users skipped"),
     *         @SWG\Header(header="X-Limit", type="integer", description="User limit")
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function index(UserRepositoryInterface $userRepository, Request $request)
    {
        $columns = ['id', 'first_name', 'last_name', 'email', 'updated_at', 'created_at'];
        $userList = $userRepository->search($columns, $request);
        return new UserCollection($userList->getCollection(), $userList->total(), $userList->currentPage(), $userList->lastPage());
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('user::create');
    }

    /**
     * Create a user
     *
     * @param UserRepositoryInterface $userRepository
     * @param UserRequest $userRequest
     * @return UserResource
     * @SWG\Post(
     *     path="/users",
     *     tags={"Users"},
     *     operationId="postUser",
     *     description="Creates new user.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="User object that needs to be created",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/UserRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successfully created resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of created user"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function store(UserRepositoryInterface $userRepository, UserRequest $userRequest)
    {
        $attributes = $userRequest->validated();
        $attributes['password'] = Hash::make($attributes['password']);
        return new UserResource($userRepository->create($attributes));
    }

    /**
     * Get detail user
     *
     * @param $id
     * @param UserRepositoryInterface $userRepository
     * @return UserCollection
     * @SWG\Get(
     *     path="/users/{id}",
     *     tags={"Users"},
     *     operationId="getDetailUser",
     *     description="Detail user",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     * 			name="id",
     * 			in="path",
     * 			required=true,
     * 			type="string",
     * 			description="UUID",
     * 		),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function show($id, UserRepositoryInterface $userRepository)
    {
        $data = $userRepository->findOrFail($id);
        return new UserResource($data);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {

    }

    /**
     * Updates a user
     *
     * @param $id
     * @param UserRepositoryInterface $userRepository
     * @param UserRequest $userRequest
     * @return UserResource
     * @SWG\Put(
     *     path="/users/{id}",
     *     tags={"Users"},
     *     operationId="putUser",
     *     description="Creates new (or updates existing) user.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     * 			name="id",
     * 			in="path",
     * 			required=true,
     * 			type="string",
     * 			description="UUID",
     * 		),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="User object that needs to be Updated",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/UserRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successfully updated resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of updated user"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function update($id, UserRepositoryInterface $userRepository, UserRequest $userRequest)
    {
        $userRepository->findOrFail($id);

        $params = $userRequest->validated();

        if (isset($params['email'])) {
            $params['email'] = $userRequest->get('email');
        } else {
            unset($params['email']);
        }

        if (isset($params['password'])) {
            $params['password'] = Hash::make($userRequest->get('password'));
        } else {
            unset($params['password']);
        }

        return new UserResource($userRepository->update($params, $id));

    }

    /**
     * Delete existing user.
     *
     * @param int $id
     * @param DeleteUserService $service
     * @return mixed
     * @SWG\Delete(
     *     path="/users/{id}",
     *     tags={"Users"},
     *     operationId="deleteUser",
     *     description="Delete existing user",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of user that should be deleted",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of deleted user"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Resource not found.",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function destroy($id, DeleteUserService $service)
    {
        if ( !ctype_digit($id) ) {
            return response()->error(__('Invalid user ID.'), 400);
        }

        if ( $service->destroy($id) ) {
            return response()->noContent();
        }

        return response()->error(__('This user has already destroyed permanently or user was not in trash or user does not exist in our system. Please check the user ID.'), 404);
    }

    /**
     * Delete a user temporarily. (Move to trash)
     *
     * @param int $id
     * @param DeleteUserService $service
     * @return mixed
     * @SWG\Put(
     *     path="/users/{id}/trash",
     *     tags={"Users"},
     *     operationId="trashUser",
     *     description="Delete a user temporarily",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of user that should be deleted",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of deleted user"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Resource not found.",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function trash($id, DeleteUserService $service)
    {
        if ( !ctype_digit($id) ) {
            return response()->error(__('Invalid user ID.'), 400);
        }

        if ( $service->softDelete($id) ) {
            return response()->noContent();
        }

        return response()->error(__('This user has already contained in the trash or it does not exist in our system. Please check the user ID.'), 404);
    }

    /**
     * Restore a user.
     *
     * @param int $id
     * @param DeleteUserService $service
     * @return mixed
     * @SWG\Put(
     *     path="/users/{id}/restore",
     *     tags={"Users"},
     *     operationId="restoreUser",
     *     description="Restore a user from the trash",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of user that should be restored",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of deleted user"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Resource not found.",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function restore($id, DeleteUserService $service)
    {
        if ( !ctype_digit($id) ) {
            return response()->error(__('Invalid user ID.'), 400);
        }

        if ( $service->restore($id) ) {
            return response()->noContent();
        }

        return response()->error(__('This user has not existed in the trash yet.'), 404);
    }

    /**
     * @SWG\Get(
     *     path="/logout",
     *     summary="Invalidates API token",
     *     tags={"Auth"},
     *     description="Invalidates API token.",
     *     operationId="logout",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=403,
     *         description="Unauthorized",
     *         @SWG\Header(header="X-Reason", type="string", description="Authorization failure reason")
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        // delete a token
        $request->user()->token()->delete();

        return response()->noContent();
    }

    /**
     * @SWG\Post(
     *     path="/users/{id}/assign-role",
     *     summary="Assign or remove role to a user",
     *     tags={"Users"},
     *     operationId="postRoleUser",
     *     description="Assign role to a user or removed a role out of user. ",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="unassign", in="query", type="integer", description="Id of role that need remove out of user."),
     *     @SWG\Parameter(
     * 			name="id",
     * 			in="path",
     * 			required=true,
     * 			type="string",
     * 			description="UUID",
     * 		),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Role Id that need assign to user.",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/RoleUserRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of user assigned role"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     * @param UserRepositoryInterface $userRepository
     * @param RoleRepositoryInterface $roleRepository
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignRoleToUser(UserRepositoryInterface $userRepository, RoleRepositoryInterface $roleRepository, Request $request)
    {
        $user = $userRepository->findOrFail($request->user_id);

        if ($request->unassign) {
            $role = $roleRepository->find($request->unassign);
            if (!$role || !$user->hasRole($role)) {
                return response()->json([__('message') => __('Role not belong to user with id ') . $request->user_id]);
            }
            $user->removeRole($role);

            return response()->json([__('message') => __('Removed out of user role with id ') . $request->unassign]);
        }

        if (!$roleRepository->find($request->input('role_id'))) {
            return response()->json([__('error') => __('Not exists role with id ') . $request->input('role_id')]);
        }
        $user->syncRoles($request->input('role_id'));

        return response()->json([__('message') => __('Assign role for user successfully')]);
    }

    /**
     * @SWG\Post(
     *     path="/users/{id}/assign-permissions",
     *     summary="Assign or remove permission to a user",
     *     tags={"Users"},
     *     operationId="postPermissionUser",
     *     description="Assign permission to a user or removed a permission out of user. ",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="unassign", in="query", type="integer", description="Id of permission that need remove out of user."),
     *     @SWG\Parameter(
     *            name="id",
     *            in="path",
     *            required=true,
     *            type="string",
     *            description="UUID",
     *        ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Permission Id that need assign to user.",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/PermissionUserRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of user assigned permission"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     * @param UserRepositoryInterface $userRepository
     * @param PermissionRepositoryInterface $permissionRepository
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignPermissionToUser(UserRepositoryInterface $userRepository, PermissionRepositoryInterface $permissionRepository, Request $request)
    {
        $user = $userRepository->findOrFail($request->user_id);

        if ($request->unassign) {
            $permission = $permissionRepository->find($request->unassign);
            if (!$permission || !$user->hasPermissionTo($permission)) {
                return response()->json([__('message') => __('Permission not belong to user with id ') . $request->user_id]);
            }
            $user->revokePermissionTo($permission);

            return response()->json([__('message') => __('Revoked out of user permission with id ') . $request->unassign]);
        }

        $permissionsId = $request->all();
        $permissionsId = explode(',', $permissionsId['permissions_id']);
        foreach ($permissionsId as $permissionId) {
            $permission = $permissionRepository->find($permissionId);
            if(!$permission) {
                return response()->json(['error' => __('There is no permission id ') . trim($permissionId)]);
            }
        }
        $user->syncPermissions($permissionsId);

        return response()->json([__('message') => __('Assign permission for user successfully')]);
    }

    /**
     * @SWG\Put(
     *     path="/users/update-channel",
     *     summary="Update channel data to a user",
     *     tags={"Users"},
     *     operationId="postUpdateChannel",
     *     description="Update channel data for user. ",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Channel Id that need update data to user.",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/ChannelUserUpdateRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of user used to update data"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     * @param ChannelUsersRepository $channelUsersRepository
     * @param ChannelUsersRequest $request
     * @return ChannelUsersResource
     */
    public function updateChannel(ChannelUsersRepository $channelUsersRepository, ChannelUsersRequest $request)
    {
        $data = $request->validated();
        return new ChannelUsersResource($channelUsersRepository->updateChannelUser(
            [
                'channel_id' => $data['channel_id'],
                'user_id' => $request->user()->id
            ],
            $data
        ));
    }

    /**
     * Get channel user setting of the current user
     *
     * @param Request $request
     * @param ChannelUsersRepositoryInterface $channelUsersRepository
     * @return ChannelUsersResource
     * @SWG\Get(
     *     path="/users/channel-settings",
     *     tags={"Users"},
     *     operationId="getChannelSettingsOfCurrentUser",
     *     description="Gets user items",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="channel_id", in="query", type="integer", description="Channel ID"),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function channelSettings(Request $request, ChannelUsersRepositoryInterface $channelUsersRepository) {

        $params = $request->all();
        $data = $channelUsersRepository->findWhere([
            'channel_id'    => $params['channel_id'],
            'user_id'   => $request->user()->id
        ])->first();


        return new ChannelUsersResource($data);
    }


    /**
     * Get channels of current user
     * @param Request $request
     * @param ChannelUsersRepositoryInterface $channelUsersRepository
     * @return \Illuminate\Http\JsonResponse
     * @SWG\Get(
     *     path="/users/channels",
     *     tags={"User Channels"},
     *     operationId="getChannelsOfUser",
     *     description="Gets channels of user",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="user_id", in="query", type="integer", description="User ID"),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function listChannelsUser(Request $request, ChannelUsersRepositoryInterface $channelUsersRepository) {
        $data = $channelUsersRepository->getChannelsOfUser($request->user_id);
        return response()->json($data);
    }
}
