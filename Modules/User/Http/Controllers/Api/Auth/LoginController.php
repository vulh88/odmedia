<?php

namespace Modules\User\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Modules\User\Repositories\Contracts\UserRepositoryInterface;

class LoginController extends Controller
{

    /**
     * @SWG\Post(
     *     path="/login",
     *     summary="Authenticates API user",
     *     tags={"Auth"},
     *     description="Authenticates user.",
     *     operationId="login",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="email",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *         description="password",
     *         in="formData",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Successful operation",
     *         @SWG\Header(header="Authorization", type="string", description="Authorization token")
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @SWG\Header(header="X-Reason", type="string", description="Authorization failure reason")
     *     )
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function login(Request $request)
    {
        
        $this->validate($request, [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|min:6'
        ], [
            'email.exists' => 'The user credentials were incorrect.'
        ]);
        try {
            $http = new Client;
            $response = $http->post(env('APP_URL').'/oauth/token', [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => env('PASSWORD_CLIENT_ID'),
                    'client_secret' => env('PASSWORD_CLIENT_SECRET'),
                    'username' => $request->get('email'),
                    'password' => $request->get('password'),
                    'remember' => $request->get('remember'),
                    'scope' => '',
                ],
            ]);

            $userRepo = app(UserRepositoryInterface::class);
            $user = $userRepo->findWhere([
                'email' => $request->get('email')
            ])->first();

            $result = json_decode((string)$response->getBody(), true);
            $result['user'] = $user;
            return $result;
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'invalid_credentials',
                'message' => "{$e->getCode()}: {$e->getMessage()}"
            ], 401);
        }
    }
}
