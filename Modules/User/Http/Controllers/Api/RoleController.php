<?php

namespace Modules\User\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\ApiController;
use Modules\User\Http\Requests\FormRoleRequest;
use Elidev\ACL\Repositories\Contracts\RoleRepositoryInterface;
use Modules\User\Services\DeleteRoleService;
use Modules\User\Transformers\RoleCollection;
use Modules\User\Transformers\RoleResource;

/**
 * Class RoleController
 * @package Modules\User\Http\Controllers\Api
 */
class RoleController extends ApiController
{
    /**
     * Get list of roles.
     *
     * @param RoleRepositoryInterface $roleRepository
     * @param Request $request
     * @return RoleCollection
     *
     * @SWG\Get(
     *     path="/roles",
     *     tags={"System roles"},
     *     operationId="getRoles",
     *     description="Get Role items",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="filter[keyword]", in="query", type="string", description="Search by name"),
     *
     *     @SWG\Parameter(name="sort", in="query", type="string", description="Field to sort by, if prefixed with '-', the sorting direction will be descending"),
     *     @SWG\Parameter(name="skip", in="query", type="integer", description="Number of records to skip"),
     *     @SWG\Parameter(name="count", in="query", type="integer", description="Number of records to display"),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *         @SWG\Header(header="X-Total", type="integer", description="Total count of roles"),
     *         @SWG\Header(header="X-Skipped", type="integer", description="Roles skipped"),
     *         @SWG\Header(header="X-Limit", type="integer", description="Role limit")
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function index(RoleRepositoryInterface $roleRepository, Request $request)
    {
        $data = $request->all();
        $columns = ['id', 'name', 'guard_name', 'updated_at', 'created_at'];
        $roleList = $roleRepository->search($columns, $data);

        return new RoleCollection($roleList->getCollection(), $roleList->total(), $roleList->currentPage(), $roleList->lastPage());
    }

    /**
     * Create a role.
     * @param RoleRepositoryInterface $roleRepository
     * @param FormRoleRequest $request
     * @return RoleResource
     *
     * @SWG\Post(
     *     path="/roles",
     *     tags={"System roles"},
     *     operationId="postRole",
     *     description="Creates new role.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Role object that needs to be created",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/RoleRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successfully created resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of created role"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function store(RoleRepositoryInterface $roleRepository, FormRoleRequest $request)
    {
        return new RoleResource($roleRepository->create($request->validated()));
    }

    /**
     * Get detail role
     *
     * @param $id
     * @param RoleRepositoryInterface $roleRepository
     * @return RoleCollection
     * @SWG\Get(
     *     path="/roles/{id}",
     *     tags={"System roles"},
     *     operationId="getDetailRole",
     *     description="Detail role",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *            name="id",
     *            in="path",
     *            required=true,
     *            type="string",
     *            description="UUID",
     *        ),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function show($id, RoleRepositoryInterface $roleRepository)
    {
        $data = $roleRepository->findOrFail($id);
        return new RoleCollection($data);
    }

    /**
     * Updates a role
     *
     * @param $id
     * @param RoleRepositoryInterface $roleRepository
     * @param FormRoleRequest $request
     * @return \Illuminate\Http\JsonResponse|RoleResource
     *
     * @SWG\Put(
     *     path="/roles/{id}",
     *     tags={"System roles"},
     *     operationId="putRole",
     *     description="Creates new (or updates existing) role.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     * 			name="id",
     * 			in="path",
     * 			required=true,
     * 			type="string",
     * 			description="UUID",
     * 		),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Role object that needs to be Updated",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/RoleRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successfully updated resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of updated role"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function update($id, RoleRepositoryInterface $roleRepository, FormRoleRequest $request)
    {
        $role = $roleRepository->find($id);
        if (!$role) {
            return response()->json(['error' => __('Can not find role with id ') . $id]);
        }

        return new RoleResource($roleRepository->update($request->all(), $id));
    }

    /**
     * Delete existing role.
     *
     * @param $id
     * @param RoleRepositoryInterface $roleRepository
     * @return \Illuminate\Http\JsonResponse
     * @SWG\Delete(
     *     path="/roles/{id}",
     *     tags={"System roles"},
     *     operationId="deleteRole",
     *     description="Delete existing role",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of role that should be deleted",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of deleted role"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Resource not found.",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function destroy($id, RoleRepositoryInterface $roleRepository)
    {
        if ( $roleRepository->delete($id)) {
            return response()->noContent();
        }

        return response()->error(__('Role does not exist'), 404);
    }
}
