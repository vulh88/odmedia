<?php

namespace Modules\User\Http\Controllers\Api;

use Modules\ApiController;
use Modules\User\Http\Requests\AssignRoleToUserRequest;
use Elidev\ACL\Repositories\Contracts\RoleRepositoryInterface;
use Modules\User\Services\DeleteRoleService;
use Modules\User\Transformers\RoleCollection;
use Modules\User\Transformers\RoleResource;
use Modules\User\Repositories\Contracts\UserRepositoryInterface;
use Modules\User\Transformers\UserResource;
use Modules\User\Http\Requests\AssignChannelRequest;
use Modules\User\Repositories\Contracts\ChannelUsersRepositoryInterface;
use Modules\Channel\Repositories\Contracts\ChannelRepositoryInterface;
use Illuminate\Http\Request;


/**
 * Class RoleController
 * @package Modules\User\Http\Controllers\Api
 */
class AssignmentController extends ApiController
{

    /**
     * Get detail assignment
     *
     * @param $userId
     * @param RoleRepositoryInterface $roleRepository
     * @param UserRepositoryInterface $userRepository
     * @return RoleCollection
     * @SWG\Get(
     *     path="/assignments/{user_id}",
     *     tags={"System roles"},
     *     operationId="getDetailAssignmentRole",
     *     description="Detail user, role list, separated permissions in a group",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *            name="user_id",
     *            in="path",
     *            required=true,
     *            type="string",
     *            description="UUID",
     *        ),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function show($userId, RoleRepositoryInterface $roleRepository, UserRepositoryInterface $userRepository)
    {

    }

    /**
     * Assign roles, permissions to a specific user
     *
     * @param $userId
     * @param AssignRoleToUserRequest $request
     * @param UserRepositoryInterface $userRepo
     * @return \Illuminate\Http\JsonResponse|RoleResource
     * @SWG\Put(
     *     path="/assignments/{user_id}",
     *     tags={"System roles"},
     *     operationId="putAssignRolesPermissionToAUser",
     *     description="Assign roles, permissions to a specific user",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *            name="user_id",
     *            in="path",
     *            required=true,
     *            type="string",
     *            description="User ID",
     *        ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Role object that needs to be Updated",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/AssignRoleToUserRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successfully updated resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of updated role"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function update($userId, AssignRoleToUserRequest $request, UserRepositoryInterface $userRepo)
    {
        $user = $userRepo->find($userId);
        $postData = $request->all();

        $requestRoles = !empty($postData['acl_group']) ? $postData['acl_group'] : [];
        $user->syncRoles($requestRoles);

        $requestPermissions = !empty($postData['acl_permission']) ? $postData['acl_permission'] : [];
        $user->syncPermissions($requestPermissions);

    }

    /**
     * @SWG\Post(
     *     path="/assignments/{id}/assign-channel",
     *     summary="Assign channel to a user",
     *     tags={"ACL"},
     *     operationId="postAssignChannelUser",
     *     description="Assign channel for user. ",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Assignment info",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/ChannelUserRequestDTO"),
     *     ),
     *     @SWG\Parameter(
     * 			name="id",
     * 			in="path",
     * 			required=true,
     * 			type="string",
     * 			description="UUID",
     * 		),
     *     @SWG\Parameter(name="un_assign", in="query", type="integer", description="Id of permission that need remove out of user."),
     *     @SWG\Response(
     *         response=201,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of user assigned channel"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     * @param ChannelUsersRepositoryInterface $channelUsersRepository
     * @param AssignChannelRequest $request
     * @return ChannelUsersResource
     */
    public function postAssignChannel($userId, ChannelUsersRepositoryInterface $channelUsersRepository, AssignChannelRequest $request)
    {
        $data = $request->all();
        $data['user_id'] = $userId;

        $channelUser = $channelUsersRepository->findWhere([
            'user_id'   => $userId,
            'channel_id'    => $data['channel_id']
        ])->first();

        $requestPermissions = !empty($data['permissions']) ? $data['permissions'] : '';
        //$requestPermissions = explode(',', $requestPermissions);


        // Un-assign
        if ($request->get('un_assign')) {
            unset($data['permissions']);
            $channelUsersRepository->deleteChannel($data);

            // Remove permissions
            if ($requestPermissions) {
                foreach ($requestPermissions as $requestPermission) {
                    $channelUser->revokePermissionTo($requestPermission);
                }
            }
            return response()->noContent();
        }


        if (!$channelUser) { // Create assignment
            $channelUser = $channelUsersRepository->create($data);
        }

        // Add permission to user channel
        if ($channelUser && $requestPermissions) {
            $channelUser->syncPermissions($requestPermissions);
        }
        //dd($channelUser->permissions);
        return response()->json(
            [
                'user'  => new UserResource($request->user()),
                'permissions'   => $channelUser->getDirectPermissions(),
            ]
        );
    }

    /**
     * Show assigned channel
     *
     * @param ChannelUsersRepositoryInterface $channelUsersRepository
     * @param $userId
     * @param Request $request
     * @param ChannelRepositoryInterface $channelRepository
     * @return RoleCollection
     * @internal param $userId
     * @internal param UserRepositoryInterface $userRepository
     * @SWG\Get(
     *     path="/assignments/{user_id}/assign-channel/",
     *     tags={"ACL"},
     *     operationId="getDetailAssignmentChannel",
     *     description="Detail permissions of a user on a specific channel",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *            name="user_id",
     *            in="path",
     *            required=true,
     *            type="string",
     *            description="UUID",
     *        ),
     *     @SWG\Parameter(name="channel_id", in="query", type="integer", description="channel ID."),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function getAssignedChannel($userId, Request $request, ChannelUsersRepositoryInterface $channelUsersRepository, ChannelRepositoryInterface $channelRepository)
    {
        $channelId = $request->get('channel_id');
        $channelRepository->findOrFail($channelId);

        $channelUser = $channelUsersRepository->findWhere([
            'user_id'   => $userId,
            'channel_id'    => $channelId
        ])->first();

        $permissions = $channelUser ? $channelUser->permissions : false;
        $arrPermissions = [];
        if ($permissions) {
            $permissions = $permissions->toArray();
            foreach ($permissions as $permission) {
                $arrPermissions[] = $permission['name'];
            }
        }

        return response()->json(
            [
                'user'  => new UserResource($request->user()),
                'permissions'   => $arrPermissions,
            ]
        );
    }
}
