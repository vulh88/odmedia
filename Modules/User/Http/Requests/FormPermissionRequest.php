<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormPermissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->permission) {
            return [
                'name' => 'max:255|unique:permissions,id,',
                'guard_name' => 'min:1|max:255',
            ];
        }

        return [
            'name' => 'required|max:255|unique:permissions',
            'guard_name' => 'required|max:255',
        ];
    }
}