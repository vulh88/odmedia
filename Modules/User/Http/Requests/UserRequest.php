<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->user) {
            return [
                'email' => 'email|max:255|nullable|unique:users,email,'. $this->user,
                'password' => 'min:6|confirmed|nullable',
                'first_name' => 'max:255|nullable',
                'last_name' => 'max:255|nullable',
            ];
        }

        return [
            'first_name' => 'max:255|nullable',
            'last_name' => 'max:255|nullable',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ];
    }
}