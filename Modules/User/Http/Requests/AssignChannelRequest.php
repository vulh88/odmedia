<?php
	
namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CheckExistsDataRule;
use Modules\Channel\Repositories\Contracts\ChannelRepositoryInterface;


class AssignChannelRequest extends FormRequest
{

    private $rules = [
        'channel_id' => [
            'required',
            'integer'
        ],
       // 'permissions'   => 'string',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->rules['channel_id'][] = new CheckExistsDataRule(app()->make(ChannelRepositoryInterface::class));

        return $this->rules;
    }
}
