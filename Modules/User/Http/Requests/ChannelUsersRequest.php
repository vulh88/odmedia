<?php
	
namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CheckExistsDataRule;
use Modules\Resource\Repositories\Contracts\ResourceFolderRepositoryInterface;

class ChannelUsersRequest extends FormRequest
{
    private $rules = [
        'channel_id' => [
            'required',
            'integer'
        ]
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->route()->getName())
        {
            case 'users.update-channel':

                $this->rules = [
                    'resource_folder_id' => [
                        'integer',
                        new CheckExistsDataRule(app()->make(ResourceFolderRepositoryInterface::class))
                    ],
                    'player_directory' => 'string|min:1|max:255',
                    'api_path' => 'string|min:1|max:255',
                    'api_auth' => 'string|min:1|max:255',
                    'channel_id'  => 'required|integer|exists:channel_users,channel_id',
                ];
                break;

            default:
                //
                break;
        }

        return $this->rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'channel_id.exists' => 'The selected channel has not assigned to the current user yet'
        ];
    }

}
