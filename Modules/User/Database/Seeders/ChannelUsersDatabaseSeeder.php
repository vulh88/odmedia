<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use \App\Modules\User\Entities;
use Illuminate\Support\Facades\DB;

class ChannelDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
		$channel_users = [
			['channel_id'=>1, 'user_id'=>1],
			['channel_id' => 1, 'user_id' => 2],
			['channel_id' => 2, 'user_id' => 1],
			['channel_id' => 2, 'user_id' => 2]
		];
	
	   DB::table("channel_users")->insert($channel_users);
    }
}
