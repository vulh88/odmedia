<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");

        $user = \App\User::create([
            'first_name' => 'admin',
            'last_name' => 'odarrange',
            'email' => 'admin@elidev.info',
            'password' => bcrypt('!@#Dev2018'),
            'remember_token' => str_random(10),
        ]);

        $user->assignRole(config('acl.group_with_full_permissions'));
        $user->assignRole(config('acl.subscriber_role'));
    }
}
