<?php

namespace Modules\Theme\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

class ThemeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'channel_id' => $this->channel_id,
            'default_osd_id' => $this->default_osd_id,
            'osd_loop' => $this->osd_loop,
            'api_theme' => $this->api_theme,
            'web_code' => $this->web_code,
            'active' => $this->active,
            'updated_at' => $this->updated_at ? $this->updated_at->format(config('api.format_datetime')) : '',
            'created_at' => $this->created_at ? $this->created_at->format(config('api.format_datetime')) : '',
        ];
    }
}
