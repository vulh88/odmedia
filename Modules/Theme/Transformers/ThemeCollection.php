<?php

namespace Modules\Theme\Transformers;

use App\Services\ApiTrait\ResourceCollectionTrait;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ThemeCollection extends ResourceCollection
{

    use ResourceCollectionTrait;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Support\Collection
     */
    public function toArray($request)
    {
        return $this->collection;
    }

    /**
     * Customize the outgoing response for the resource.
     *
     * @param \Illuminate\Http\Request
     * @param \Illuminate\Http\JsonResponse
     * @return void
     */
    public function withResponse($request, $response)
    {
        $count = !empty($request->get('count')) ? $request->get('count') : config('api.default_limit');
        $response->header('X-Total', $this->getTotal());
        $response->header('X-Limit', $count);
        $response->header('X-Current-Page', $this->getCurrentPage());
        $response->header('X-Last-Page', $this->getLastPage());
    }
}
