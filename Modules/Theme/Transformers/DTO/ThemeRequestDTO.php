<?php

namespace Modules\Resource\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="ThemeRequestDTO"
 * )
 */
class ThemeRequestDTO
{
    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="channel_id",
     *    type="integer",
     *    description="Channel ID *"
     * )
     */
    protected $channel_id;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="name",
     *    type="string",
     *    description="Name *"
     * )
     */
    protected $name;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="default_osd_id",
     *    type="integer",
     *    description="Default OSD"
     * )
     */
    protected $default_osd_id;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="osd_loop",
         *    type="integer",
     *    description="OSD Loop"
     * )
     */
    protected $osd_loop;

    /**
     * @var string $web_code
     *
     * @SWG\Property(
     *    property="web_code",
     *    type="integer",
     *    description="Web code"
     * )
     */
    protected $web_code;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="active",
     *    type="integer",
     *    description="Active"
     * )
     */
    protected $active;
}
