<?php

namespace Modules\Theme\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface ThemeRepositoryInterface
 * @package namespace App\Repositories\Contracts;
 */
interface ThemeRepositoryInterface extends RepositoryInterface
{
    /**
     * @param array $columns
     * @param array $params
     * @return mixed
     */
    public function search($columns = array(), $params = []);
}
