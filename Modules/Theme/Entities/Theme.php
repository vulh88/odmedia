<?php

namespace Modules\Resource\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Theme extends Model
{
    protected $fillable = [
        'channel_id',
        'name',
        'default_osd_id',
        'osd_loop',
        'api_theme',
        'web_code',
        'active',
        'api_theme_id'
    ];
}
