<?php

namespace Modules\Theme\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormThemeRequest extends FormRequest
{

    private $rules = [
        'name' => 'required|max:255',
        'channel_id' => 'required|integer|min:1',
        'default_osd_id' => 'integer|nullable',
        'osd_loop' => 'integer|min:0|max:1|nullable',
        'active' => 'integer|min:0|max:1|nullable',
        'web_code'  => 'string|nullable',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->theme)
        {
            $this->rules = [
                'name' => 'string|min:1|max:255',
                'channel_id' => 'integer|required|min:1',
                'default_osd_id' => 'integer|nullable',
                'osd_loop' => 'integer|min:0|max:1|nullable',
                'active' => 'integer|min:0|max:1|nullable',
                'id' =>'integer|exists:themes,id',
                'web_code'  => 'string|nullable',
            ];
        }

        return $this->rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge(
            [
                'id.exists' => 'Can not find data with id ' . $this->theme
            ],
            array_fill_keys(
                [
                    'active.min',
                    'active.max',
                ],
                __('Can only be 0 or 1. If empty, default 0')
            )
        );
    }
}
