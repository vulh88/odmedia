<?php

namespace Modules\Theme\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\ApiController;
use Modules\Resource\Entities\Theme;
use Modules\Theme\Http\Requests\FormThemeRequest;
use Modules\Theme\Repositories\Contracts\ThemeRepositoryInterface;
use Modules\Theme\Services\DeleteThemeService;
use Modules\Theme\Transformers\ThemeCollection;
use Modules\Theme\Transformers\ThemeResource;

class ThemeController extends ApiController
{
    /**
     * Get list of Themes.
     * @param ThemeRepositoryInterface $themRepository
     * @param Request $request
     * @return ThemeCollection
     *
     * @SWG\Get(
     *     path="/themes",
     *     tags={"Themes"},
     *     operationId="getThemes",
     *     description="Gets theme items",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="keyword", in="query", type="string", description="Search by name"),
     *
     *     @SWG\Parameter(name="sort", in="query", type="string", description="Field to sort by, if prefixed with '-', the sorting direction will be descending"),
     *     @SWG\Parameter(name="skip", in="query", type="integer", description="Number of records to skip"),
     *     @SWG\Parameter(name="count", in="query", type="integer", description="Number of records to display"),
     *     @SWG\Parameter(name="trash", in="query", type="integer", description="Field to filter, if the value equal to 1 then filter with all trashed items"),
     *     @SWG\Parameter(name="channel_id", in="query", type="integer", description="Selected channel"),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *         @SWG\Header(header="X-Total", type="integer", description="Total count of themes"),
     *         @SWG\Header(header="X-Skipped", type="integer", description="Themes skipped"),
     *         @SWG\Header(header="X-Limit", type="integer", description="Themes limit")
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function index(ThemeRepositoryInterface $themRepository, Request $request)
    {
        $columns = ['id', 'name', 'channel_id', 'default_osd_id', 'osd_loop', 'api_theme', 'web_code', 'active', 'updated_at', 'created_at'];
        $results = $themRepository->search($columns, $request);
        return new ThemeCollection($results->getCollection(), $results->total(), $results->currentPage(), $results->lastPage());
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('theme::create');
    }

    /**
     * Create a theme
     *
     * @param ThemeRepositoryInterface $themeRepository
     * @param FormThemeRequest $request
     * @return ThemeResource
     * @SWG\Post(
     *     path="/themes",
     *     tags={"Themes"},
     *     operationId="postCreateTheme",
     *     description="Creates new (or updates existing) theme.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Theme object that needs to be created",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/ThemeRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successfully created resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of created resource type"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function store(ThemeRepositoryInterface $themeRepository, FormThemeRequest $request)
    {
        $data = $request->validated();
        return new ThemeResource($themeRepository->create($data));
    }

    /**
     * Get detail theme
     *
     * @param $id
     * @param ThemeRepositoryInterface $themeRepository
     * @return ThemeResource
     * @SWG\Get(
     *     path="/themes/{id}",
     *     tags={"Themes"},
     *     operationId="getDetailTheme",
     *     description="Detail theme",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *            name="id",
     *            in="path",
     *            required=true,
     *            type="string",
     *            description="UUID",
     *        ),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function show($id, ThemeRepositoryInterface $themeRepository)
    {
        $data = $themeRepository->findOrFail($id);
        return new ThemeResource($data);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('theme::edit');
    }

    /**
     * Updates a theme
     * @param  $id
     * @param  ThemeRepositoryInterface $themeRepository
     * @param  FormThemeRequest $request
     * @return \Illuminate\Http\JsonResponse|ThemeResource
     * @SWG\Put(
     *     path="/themes/{id}",
     *     tags={"Themes"},
     *     operationId="putUpdateTheme",
     *     description="Creates new (or updates existing) theme.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     * 			name="id",
     * 			in="path",
     * 			required=true,
     * 			type="integer",
     * 			description="UUID",
     * 		),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Theme object that needs to be Updated",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/ThemeRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successfully updated resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of updated"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function update($id, ThemeRepositoryInterface $themeRepository, FormThemeRequest $request)
    {
        $data = $request->validated();
        return new ThemeResource($themeRepository->update($data, $id));
    }

    /**
     * Delete existing resource type.
     *
     * @param int $id
     * @param ThemeRepositoryInterface $themeRepository
     * @return mixed
     * @SWG\Delete(
     *     path="/themes/{id}",
     *     tags={"Themes"},
     *     operationId="deleteTheme",
     *     description="Delete existing theme",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of a theme that should be deleted",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of deleted theme"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Resource not found.",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function destroy($id, ThemeRepositoryInterface $themeRepository)
    {

        if ( $themeRepository->delete($id) ) {
            return response()->noContent();
        }

        return response()->error(__('Resource type is not found'), 404);
    }
}
