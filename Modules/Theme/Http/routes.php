<?php

// API
Route::group([
    'prefix' => 'api/v1',
    'middleware' => ['auth:api', 'cors', 'authorizedChannel'],
    'namespace' => 'Modules\Theme\Http\Controllers\Api'], function () {
    Route::resource('themes', 'ThemeController');
});