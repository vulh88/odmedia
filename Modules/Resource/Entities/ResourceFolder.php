<?php

namespace Modules\Resource\Entities;

use Illuminate\Database\Eloquent\Model;

class ResourceFolder extends Model
{
    protected $fillable = [
        'channel_id',
        'name',
        'parent_id'
    ];
}
