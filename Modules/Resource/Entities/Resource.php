<?php

namespace Modules\Resource\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Modules\Channel\Entities\Channel;
use App\User;
use File;
use Modules\Resource\Services\Hooks\ModelChanges;

class Resource extends Model
{
    use ModelChanges;

    protected static function boot()
    {
        // Your boot logic here
        parent::boot();
    }

    protected $fillable = [
        'title',
        'duration',
        'duration_ms',
        'start_time',
        'end_time',
        'play_limit',
        'premiere',
        'frequency',
        'desc_en',
        'desc_nl',
        'status',
        'api_id',
        'api_fetched',
        'api_updated_at',
        'api_created_at',
        'api_video_url',
        'channel_id',
        'theme_id',
        'resource_type_id',
        'resource_folder_id',
        'author',
        'filename',
        'original_filename',
        'thumb_filename',
        'original_id',
        'player_available',
        'is_video',
        'user_id',
        'type',
        'time_not_before',
        'time_not_after',
    ];

    /**
     * append fields
     * @var array
     */
    protected $appends = ['thumbnail'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get attribute for thumbnail
     * @return string
     */
    public function getThumbnailAttribute()
    {
        $channel = Channel::find($this->channel_id);
        $userStorage = generateUserPathStorage(Auth::user()->email, Auth::user()->id);
        $thumbnail = $channel->thumbnail_storage. '/' . $userStorage . '/' . $this->thumb_filename;
        $thumbnailPath = $this->thumb_filename
            ? config('filesystems.odmedia.resources_thumbnail') . '/' . $thumbnail
            : '';
        if (File::exists($thumbnailPath)) {
            return asset( config('filesystems.odmedia.asset_resources_thumbnail') . $thumbnail );
        }
        return '';
    }

    /**
     * Get relation tags
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tags()
    {
        return $this->hasMany(ResourceTag::class);
    }

    /**
     * Get relation user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get relation resource type
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function resourceType()
    {
        return $this->belongsTo(ResourceType::class, "resource_type_id", "id");
    }

    /**
     * Get relation theme
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function theme()
    {
        return $this->belongsTo(Theme::class, "theme_id", "id");
    }
}
