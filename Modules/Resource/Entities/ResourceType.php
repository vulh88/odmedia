<?php

namespace Modules\Resource\Entities;

use Illuminate\Database\Eloquent\Model;

class ResourceType extends Model
{
    protected $fillable = [
        'channel_id',
        'name',
        'class',
        'description',
        'default_osd_id',
        'osd_loop',
        'is_content',
        'is_overlay',
        'wipe',
        'active'
    ];
}
