<?php

namespace Modules\Resource\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResourceTag extends Model
{
    protected $fillable = [
        'name',
        'resource_id'
    ];

    /**
     * append fields
     * @var array
     */
    protected $appends = ['resource'];

    /**
     * Set attribute theme name
     * @return string
     */
    public function getResourceAttribute()
    {
        $resource = Resource::findOrFail($this->resource_id);
        return $resource ? $resource : '';
    }


}
