<?php

namespace Modules\Resource\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

class ResourceFolderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'name' => $this->name,
            'channel_id' => $this->channel_id,
            'parent_id' => $this->parent_id,
            'updated_at' => $this->updated_at->format(config('api.format_datetime')),
            'created_at' => $this->created_at->format(config('api.format_datetime')),
        ];
    }
}
