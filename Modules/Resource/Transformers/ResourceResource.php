<?php

namespace Modules\Resource\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

class ResourceResource extends JsonResource
{
    use \AppHelpers;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'title'             => $this->title,
            'resource_type_id'  => $this->resource_type_id,
            'channel_id'        => $this->channel_id,
            'theme_id'          => $this->theme_id,
            'duration'          => $this->duration,
            'duration_ms'       => $this->duration_ms,
            'start_time'        => $this->start_time ? self::parseDateTime($this->start_time, 'Y-m-d'): '',
            'end_time'          => $this->end_time ? self::parseDateTime($this->end_time, 'Y-m-d') : '',
            'time_not_before'   => $this->time_not_before ? self::parseDateTime($this->time_not_before, 'H:i') : '',
            'time_not_after'    => $this->time_not_after ? self::parseDateTime($this->time_not_after, 'H:i') : '',
            'frequency'         => $this->frequency,
            'desc_en'           => $this->desc_en,
            'desc_nl'           => $this->desc_nl,
            'author'            => $this->user ? $this->user->first_name . ' ' . $this->user->last_name : '',
            'premiere'          => $this->premiere,
            'player_available'  => $this->player_available,
            'tags'              => $this->tags,
            'api_fetched'       => $this->api_fetched,
            'api_video_url'     => $this->api_video_url,
            'resource_type'     => $this->resourceType,
            'theme'             => $this->theme,
            'updated_at'        => $this->updated_at ? $this->updated_at->format(config('api.format_datetime')) : '',
            'created_at'        => $this->created_at ? $this->created_at->format(config('api.format_datetime')) : '',
        ];
    }
}
