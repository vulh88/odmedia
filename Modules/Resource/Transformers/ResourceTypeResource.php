<?php

namespace Modules\Resource\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class ResourceTypeResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'channel_id' => $this->channel_id,
            'name' => $this->name,
            'class' => $this->class,
            'description' => $this->description,
            'default_osd_id' => $this->default_osd_id,
            'osd_loop' => $this->osd_loop,
            'is_content' => $this->is_content,
            'is_overlay' => $this->is_overlay,
            'wipe' => $this->wipe,
            'active' => $this->active
        ];
    }
}
