<?php

namespace Modules\Resource\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

class ResourceTagResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'resource_id'   => $this->resource_id,
            'resource'      => $this->resource,
            'updated_at'    => $this->updated_at->format(config('api.format_datetime')),
            'created_at'    => $this->created_at->format(config('api.format_datetime')),
        ];
    }
}
