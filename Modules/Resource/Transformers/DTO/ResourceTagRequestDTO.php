<?php

namespace Modules\Resource\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="ResourceTagRequestDTO"
 * )
 */
class ResourceTagRequestDTO
{
    /**
     * @var string
     *
     * @SWG\Property(
     *    property="name",
     *    type="string",
     *    description="Name *"
     * )
     */
    protected $name;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="resource_id",
     *    type="integer",
     *    description="Resource ID *"
     * )
     */
    protected $channel_id;
    
}
