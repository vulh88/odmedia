<?php

namespace Modules\Resource\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="ResourceRequestDTO"
 * )
 */
class ResourceRequestDTO
{
    /**
     * @var string
     *
     * @SWG\Property(
     *    property="title",
     *    type="string",
     *    description="Title *"
     * )
     */
    protected $title;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="resource_type_id",
     *    type="integer",
     *    description="Resource type ID *"
     * )
     */
    protected $resource_type_id;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="channel_id",
     *    type="integer",
     *    description="Channel ID *"
     * )
     */
    protected $channel_id;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="theme_id",
     *    type="integer",
     *    description="Theme ID *"
     * )
     */
    protected $theme_id;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="duration",
     *    type="integer",
     *    description="Duration *"
     * )
     */
    protected $duration;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="author",
     *    type="string",
     *    description="Author *"
     * )
     */
    protected $author;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="start_time",
     *    type="string",
     *    description="Start time ( format - Y-m-d H:i:s )"
     * )
     */
    protected $start_time;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="end_time",
     *    type="string",
     *    description="End time ( format - Y-m-d H:i:s )"
     * )
     */
    protected $end_time;

}
