<?php

namespace Modules\Resource\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="ResourceTypeRequestDTO"
 * )
 */
class ResourceTypeRequestDTO
{
    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="channel_id",
     *    type="integer",
     *    description="Channel ID *"
     * )
     */
    protected $channel_id;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="name",
     *    type="string",
     *    description="Name *"
     * )
     */
    protected $name;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="class",
     *    type="string",
     *    description="Class"
     * )
     */
    protected $class;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="description",
     *    type="string",
     *    description="Description"
     * )
     */
    protected $description;

    /**
     * @var string
     *
     * @SWG\Property(
     *    property="default_osd_id",
     *    type="integer",
     *    description="Default OSD"
     * )
     */
    protected $default_osd_id;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="osd_loop",
         *    type="integer",
     *    description="OSD Loop"
     * )
     */
    protected $osd_loop;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="is_content",
     *    type="integer",
     *    description="Is content"
     * )
     */
    protected $is_content;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="is_overlay",
     *    type="integer",
     *    description="Is overlay"
     * )
     */
    protected $is_overlay;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="wipe",
     *    type="integer",
     *    description="Wipe"
     * )
     */
    protected $wipe;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="active",
     *    type="integer",
     *    description="Active"
     * )
     */
    protected $active;
}
