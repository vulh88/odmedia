<?php

namespace Modules\Resource\Transformers\DTO;

/**
 * @SWG\Definition(
 *     type="object",
 *     definition="ResourceFolderRequestDTO"
 * )
 */
class ResourceFolderRequestDTO
{
    /**
     * @var string
     *
     * @SWG\Property(
     *    property="name",
     *    type="string",
     *    description="Name *"
     * )
     */
    protected $name;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="channel_id",
     *    type="integer",
     *    description="Channel ID *"
     * )
     */
    protected $channel_id;

    /**
     * @var integer
     *
     * @SWG\Property(
     *    property="parent_id",
     *    type="integer",
     *    description="Parent ID *"
     * )
     */
    protected $parent_id;

}
