<?php

namespace Modules\Resource\Services;

use Modules\Resource\Repositories\Contracts\ResourceRepositoryInterface;
use App\Services\Abstracts\DeleteEntityServiceAbstract;

use Log;

class DeleteResourceService extends DeleteEntityServiceAbstract
{

    public function __construct(ResourceRepositoryInterface $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Process after destroy
     *
     * @return mixed
     */
    public function afterDestroy()
    {

    }

    /**
     * Process after soft delete
     *
     * @return mixed
     */
    function afterSoftDelete()
    {
        // TODO: Implement afterSoftDelete() method.
    }

    /**
     * Process after restore
     *
     * @return mixed
     */
    function afterRestore()
    {
        // TODO: Implement afterRestore() method.
    }
}
