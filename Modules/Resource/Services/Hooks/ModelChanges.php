<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 24/08/2018
 * Time: 15:24
 */

namespace Modules\Resource\Services\Hooks;

use Log;

trait ModelChanges
{

    /**
     * Hook action when the model change
     */
    public static function bootModelChanges()
    {

        self::creating(function($model)
        {
            /**
             * @todo
             * Your logic here
             */
        });

        self::created(function($model)
        {
            /**
             * @todo
             * Your logic here
             */
        });

        self::updating(function($model)
        {
            /**
             * @todo
             * Your logic here
             */
        });

        self::updated(function($model)
        {
            /**
             * @todo
             * Your logic here
             */
        });

        self::deleting(function($model)
        {
            /**
             * @todo
             * Your logic here
             */
        });

        self::deleted(function($model)
        {
            /**
             * @todo
             * Your logic here
             */
        });
    }
}