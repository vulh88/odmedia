<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 08/08/2018
 * Time: 10:54
 */

namespace Modules\Resource\Services;

use Storage;
use File;

class DownloadResourceService
{
    /**
     * Load AppHelpers
     */
    use \AppHelpers;

    /**
     * Download Url
     * @var
     */
    public $downloadUrl;

    /**
     * The directory for player
     * @var
     */
    public $playerDirectory;

    /**
     * The function used to download file by a url download
     * @return null|string
     */
    public function downloadByUrl()
    {
        /**
         * Create folder if the folder isn't exists
         */
        if (!File::isDirectory($this->playerDirectory)) {
            File::makeDirectory($this->playerDirectory, 0777, true);
        }

        $downloadUrl = str_replace(" ", "%20", $this->downloadUrl);
        $filename = self::generateFileName("mp4");
        $pathFile = $this->playerDirectory . '/' . $filename;

        /**
         * Create a file in system from a file download
         */
        try {
            if ( File::put($pathFile, fopen($downloadUrl, 'r'))) {
                return $filename;
            }
        } catch(\Exception $e) {

        }

        return null;
    }
}