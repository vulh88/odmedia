<?php

namespace Modules\Resource\Services\Import;

use App\Utils\Utils;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Resource\Repositories\Contracts\ResourceRepositoryInterface;
use Modules\Resource\Repositories\Contracts\ResourceTagRepositoryInterface;
use Modules\Theme\Repositories\Contracts\ThemeRepositoryInterface;

class ImportFromAPIService extends ImportAbstract
{

    /**
     * @var $resourceRepo
     */
    private $resourceRepo;

    /**
     * @var $resourceTagRepo
     */
    private $resourceTagRepo;

    /**
     * @var $themeRepository
     */
    private $themeRepository;

    /**
     * Specific channel
     * @var $channelId
    */
    private $channelId;

    /**
     * Default resource type
     * @var $resourceTypeId
     */
    private $resourceTypeId;

    /**
     * Default resource folder
     * @var $resourceFolderId
     */
    private $resourceFolderId;

    /**
     * API page data
     * @var $page
     */
    private $page = 1;

    /**
     * Force update resource even if it has existed
     * @var $forceUpdate
     */
    private $forceUpdate;


    /**
     * @param Request $request
     * @param ResourceRepositoryInterface $resourceRepository
     * @param ResourceTagRepositoryInterface $resourceTagRepository
     * @param ThemeRepositoryInterface $themeRepository
     */
    public function __construct(Request $request, ResourceRepositoryInterface $resourceRepository, ResourceTagRepositoryInterface $resourceTagRepository, ThemeRepositoryInterface $themeRepository)
    {
        $this->resourceRepo = $resourceRepository;
        $this->resourceTagRepo = $resourceTagRepository;
        $this->themeRepository = $themeRepository;
    }

    /**
     * Do import
     * @param array $args
     * @return mixed
     */
    public function execute($args = [])
    {
        $clientResources = $this->processInputData($args);

        if (!$clientResources) {
            return [];
        }

        if ( !empty($args['preview']) ) {
            /**
             * response data if the arg has as element is preview
             */
            return $this->getDataPreview($clientResources);
        }

        $responseResult = [];
        foreach($clientResources as $clientResource) {
            /**
             * @Todo
             * Optimize this code by avoid query many times in foreach
             * Should only has one query to check condition by app_id
             */
            $resource = $this->resourceRepo->findWhere([
                'api_id'    => $clientResource->id,
                'channel_id'    => $this->channelId
            ]);


            // Continue if this resource has existed
            if ($resource->count()) {
                $resource = $resource[0];
                // Ignore if this resource still not updated yet from the client API and our system does not allow force update data
                if ($resource->api_updated_at == $clientResource->lastUpdate && !$this->forceUpdate) {
                    continue;
                }
            }
            else {
                $resource = $this->resourceRepo->newInstance();
            }


            // Create theme
            /**
             * @Todo
             * Optimize this code by avoid query many times in foreach
             * Should only has one query to check condition by api_theme
             */
            $theme = $this->themeRepository->findWhere([
                'api_theme_id'   => $clientResource->themeId,
                'channel_id'   => $this->channelId
            ]);

            if (!$theme = $theme->first()) {
                $theme = $this->themeRepository->create([
                    'channel_id'  => $this->channelId,
                    'name'   => $clientResource->theme,
                    'api_theme_id' => $clientResource->themeId
                ]);
            }

            // Update & Create resource
            $this->prepareResource($resource, $clientResource, $theme->id);
            $resource->save();

            $responseResult[] = $resource;

            // Create tags
            $this->resourceTagRepo->createMultipleTags($resource->id, explode(',', $clientResource->tags));
        }

        return $responseResult;
    }

    /**
     * Get import data
     * @param $args
     * @return array
     */
    private function processInputData($args) {

        if (empty($args['channel_id'])) {
            return false;
        }

        $this->page = empty($args['page']) ? 1: $args['page'];
        $this->channelId = $args['channel_id'];
        $this->resourceFolderId = isset($args['resourceFolderId']) ? $args['resourceFolderId'] : 0;
        $this->resourceTypeId = isset($args['resourceTypeId']) ? $args['resourceTypeId'] : 0;
        $this->forceUpdate = true;

        // Get client resource data from their API
        /**
         * @Todo get configuration data from user channel
         */
        try {
            $http = new Client;
            $response = $http->request('get', $args['api_path'] . '?page='. $this->page, [
                'headers'   => [
                    'Authorization' => "Bearer " . $args['api_auth']
                ]
            ]);
            return (array) \GuzzleHttp\json_decode((string) $response->getBody());
        } catch (\Exception $e) {
            return [];
        }

    }

    /**
     * Prepare a resource for importing
     * @param $resource
     * @param $clientResource
     * @param int $themeId
     * @return array
     */
    private function prepareResource(&$resource, $clientResource, $themeId = 0) {

        $userId = Auth::user() ? Auth::user()->id : 1;
        $inputs = [
            'title' => $clientResource->title,
            'duration' => $clientResource->duration,
            'play_limit' => $clientResource->playLimit,
            'premiere' => $clientResource->premiere,
            'frequency' => $clientResource->frequency,
            'desc_en' => $clientResource->metaEnglish,
            'desc_nl' => $clientResource->metaDutch,
            'status' => $clientResource->isDeleted ? 'draft' : 'publish',

            'api_created_at' => $clientResource->createdAt,
            'api_updated_at' => $clientResource->lastUpdate,
            'api_id' => $clientResource->id,
            'api_video_url' => $clientResource->videoUrl,

            'channel_id' => $this->channelId,
            'resource_type_id'  => $this->resourceTypeId,
            'resource_folder_id'  => $this->resourceFolderId,
            'theme_id'  => $themeId,
            'user_id'   => $userId,
            'type'   => 1
        ];

        foreach ($inputs as $key => $val) {
            $resource->{$key} = $val;
        }

        if ($clientResource->dateStart && Utils::validateDateTime($clientResource->dateStart)) {
            $resource->start_time = $clientResource->dateStart;
        }

        if ($clientResource->dateEnd && Utils::validateDateTime($clientResource->dateEnd)) {
            $resource->end_time = $clientResource->dateEnd;
        }
    }

    /**
     * @param $clientResources
     * @return array
     */
    private function getDataPreview($clientResources)
    {
        $arrData = [];
        foreach($clientResources as $clientResource) {
            $resource = new \stdClass();
            $this->prepareResource($resource, $clientResource, 0);

            $checkResource = $this->resourceRepo->findWhere([
                'api_id'    => $clientResource->id,
                'channel_id'    => $this->channelId
            ])->count();
            $resource->imported = $checkResource ? 1 : 0;
            $arrData[] = $resource;
        }
        return $arrData;
    }

}