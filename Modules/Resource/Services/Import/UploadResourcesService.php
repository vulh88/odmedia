<?php

namespace Modules\Resource\Services\Import;

use Illuminate\Http\Request;
use Modules\Resource\Repositories\Contracts\ResourceRepositoryInterface;
use File;
use Carbon\Carbon;


class UploadResourcesService extends ImportAbstract
{
    /**
     * Load AppHelpers
     */
    use \AppHelpers;

    /**
     * @var $resourceRepo
     */
    private $resourceRepo;


    /**
     * @param Request $request
     * @param ResourceRepositoryInterface $resourceRepository
     */
    public function __construct(Request $request, ResourceRepositoryInterface $resourceRepository)
    {
        $this->resourceRepo = $resourceRepository;
    }

    /**
     * Do import
     * @param array $args
     * @return \Illuminate\Contracts\Routing\ResponseFactory|mixed|\Symfony\Component\HttpFoundation\Response
     */
    public function execute($args = [])
    {
        /**
         * @Todo
         */
        $configurationParameters = [
            'resource_storage_path'     => storage_path(). '/channel1/huy/new-content/',
            'channel_id'                => $args['channel_id'],
            'resource_folder_id'        => $args['resource_folder_id'],
            'user_id'                   => $args['user_id'],
            'upload_type'               => $args['upload_type'],
            'default_type_id'           => $args['only_content_resources'],
            'default_overlay_type_id'   => $args['only_overlay_resources'],
            'thumbnail_storage_path'    => $args['thumbnail_storage_path'],
            'thumbnail_second'          => 20
        ];
        $response = [];
        switch ($args['action']) {
            case 'scan-drive':
                $response = $this->scanDrive($configurationParameters);
                break;
            case 'upload-resources':
                $response = $this->uploadResources($args['file'], $configurationParameters);
                break;
            default:
                break;
        }

        return response($response['message'], 200);
    }

    /**
     * @param array $configurationParameters
     * @return array
     */
    protected function scanDrive(array $configurationParameters)
    {
        $files = $this->getFiles($configurationParameters);
        foreach ( $files as $file ) {
            if ( !File::isDirectory($file) ) {
                $rFile = new \stdClass();
                $rFile->filename = $file->getFilename();
                $rFile->pathname = $file->getPathname();
                $rFile->extension = $file->getExtension();
                $this->generateResources($rFile, $configurationParameters);
            }
        }
        return [
            'message'=> 'Generate success'
        ];
    }

    /**
     * @param $file
     * @param array $configurationParameters
     * @return array
     */
    protected function uploadResources($file, array $configurationParameters)
    {
        $this->generateResources($file, $configurationParameters);
        return [
            'message'=> 'Upload success'
        ];
    }

    /**
     * @param $file
     * @param array $configurationParameters
     */
    private function generateResources($file, array $configurationParameters)
    {
        $data = [
            'thumbFilename' => $file->filename,
            'isUpdate' => false,
            'renameFile' => self::renameFile($file->filename),
            'filename' => $file->filename,
            'original_filename' => $file->filename,
            'seconds' => 0,
            'microSeconds' => 0,
        ];

        $resourceId = 0;
        $isVideo = 0;
        $isSave = true;
        $saveType = $configurationParameters['upload_type'] == 1 ? $configurationParameters['default_overlay_type_id'] : $configurationParameters['default_type_id'];
        $lastModified = File::lastModified($file->pathname);
        $formatLastModified = Carbon::createFromTimestamp($lastModified)->toDateTimeString();

        /**
         * Check exist resource data for each item
         */
        $checkItem = $this->resourceRepo
            ->orderBy('created_at', 'desc')
            ->findWhere([
                'original_filename' =>  $file->filename,
            ])
            ->first();

        if ($checkItem) {
            $isSave = false;
            /**
             * Check the last modified of resource video has changed yet
             */
            if ($checkItem->updated_at < $formatLastModified) {
                $resourceId = $checkItem->id;
                $data['isUpdate'] = true;
                $isSave = true;
            }
        }

        $imageExtensions = self::imageExtensions();
        if (!in_array($file->extension, $imageExtensions)) {
            /**
             * Generate thumbnail for video
             */
            $this->generateThumbVideo($file, $configurationParameters, $data);
            $isVideo = 1;
        }

        if ( $isSave ) {
            $records = [
                'user_id' => $configurationParameters['user_id'],
                'channel_id' => $configurationParameters['channel_id'],
                'filename' => $data['filename'],
                'original_filename' => $data['original_filename'],
                'thumb_filename' => $data['thumbFilename'],
                'title' => $data['renameFile'],
                'duration' => $data['seconds'],
                'duration_ms' => $data['microSeconds'],
                'resource_type_id' => $saveType,
                'resource_folder_id' => $configurationParameters['resource_folder_id'],
                'player_available' => 0,
                'is_video' => $isVideo,
                'original_id' => $resourceId,
                'type' => 2,
                'premiere' => 1
            ];
            /**
             * create new resource
             */
            $this->resourceRepo->create($records);
        }
    }

    /**
     * Get all files in drive
     * @param $configurationParameters
     * @return mixed
     */
    private function getFiles($configurationParameters)
    {
        if (!File::isDirectory($configurationParameters['resource_storage_path']))
        {
            abort(403, 'Can not found the folder');
        }

        return File::files($configurationParameters['resource_storage_path']);
    }

    /**
     * Get duration in seconds of media file from ffmpeg
     * @param $videoPath
     * @param $duration
     */
    private function getDurationForVideo($videoPath, &$duration)
    {
        $ffprobe = \FFMpeg\FFProbe::create();
        $durationSecond =  $ffprobe
            ->format($videoPath) // extracts file information
            ->get('duration');
        if ($durationSecond > 0) {
            $duration['seconds'] = intval(ceil($durationSecond));
            $microSeconds = ltrim(strstr($durationSecond, "."), ".");
            $duration['microSeconds'] = ($duration['seconds'] * 1000) + $microSeconds;
        }
    }

    /**
     * Generate thumbnail for video
     * @param $file
     * @param $configurationParameters
     * @param $data
     * @return bool
     */
    private function generateThumbVideo($file, $configurationParameters, &$data)
    {
        /**
         * Create folder if the folder isn't exists
         */
        if (!File::isDirectory($configurationParameters['thumbnail_storage_path'])) {
            File::makeDirectory($configurationParameters['thumbnail_storage_path'], 0777, true);
        }

        /**
         * Get duration for video
         */
        $this->getDurationForVideo($file->pathname, $data);

        /**
         * Generate thumbnail for video
         */
        //Set name for thumbnail
        //$data['thumbFilename'] = self::generateFileName();
        $data['thumbFilename'] = $data['renameFile']."-thumb.png";
        if ($data['isUpdate'] == true) {
            $data['thumbFilename'] = $data['renameFile'] ."-". self::generateFileName(null) . "-thumb.png";
            $data['filename'] = $data['renameFile'] ."-". self::generateFileName(null) . ".png";
        }

        $fullThumbPath = $configurationParameters['thumbnail_storage_path'] . '/' . $data['thumbFilename'];

        if (!File::exists($fullThumbPath) || $data['isUpdate'] == true) {
            /**
             * Get duration time to create thumbnail for video
             */
            $fetchThumbFrame = floor($data['seconds'] / 5);
            if ( isset($configurationParameters['thumbnail_second']) ) {

                $fetchThumbFrame = $configurationParameters['thumbnail_second'];
                if ($data['seconds'] <= $configurationParameters['thumbnail_second']) {
                    $fetchThumbFrame = floor($data['seconds'] / 2);
                }
            }

            /**
             * Use library ffmpeg to generated thumbnail for video
             */
            $this->generateThumbByffmpeg($fetchThumbFrame, $file->pathname, $fullThumbPath);

        }

        return true;
    }

    /**
     * Handle upload file from client
     * @param $file
     * @param $uploadStorage
     * @return \stdClass
     */
    public function upload($file, array $uploadStorage)
    {
        $filename = $file->getClientOriginalName();

        $extensionFile = $file->extension();
        $filePath = $uploadStorage['resource_path'] . '/' . $filename;
        if (File::exists($filePath)) {
            /**
             * Set file name if it's exists
             */
            $filename = self::fileExists($file, $uploadStorage['resource_path']);
        }

        $rFile = new \stdClass();
        $file->move($uploadStorage['resource_path'], $filename);
        $rFile->filename = $filename;
        $rFile->pathname = $uploadStorage['resource_path'] . '/' . $filename;
        $rFile->extension = $file->getClientOriginalExtension();

        $imageExtensions = self::imageExtensions();
        if ( in_array( $extensionFile, $imageExtensions )) {
            /**
             * Generate thumbnail
             */
            self::generateThumbnail($rFile->pathname, $uploadStorage['thumbnail_storage']. '/' . $filename);
        }
        return $rFile;
    }

}