<?php

namespace Modules\Resource\Services\Import;


abstract class ImportAbstract
{
    protected $importData;

    /**
     * Do import
     * @param array $args
     * @return mixed
     */
    abstract public function execute($args = []);
}