<?php

namespace Modules\Resource\Tests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResourceApiTest extends TestCase
{

    private $authorization = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjdmMjI4NzkyZDFlOGZlMjIxMjhjYjVmNzJkYWVkZDFjNTI5ZmQ5OWMzOTY0MjdjNDdiNGNiYmVmZjIwNTQwMzc1NTFhNWUxMDM4NmNjMzZlIn0.eyJhdWQiOiIyIiwianRpIjoiN2YyMjg3OTJkMWU4ZmUyMjEyOGNiNWY3MmRhZWRkMWM1MjlmZDk5YzM5NjQyN2M0N2I0Y2JiZWZmMjA1NDAzNzU1MWE1ZTEwMzg2Y2MzNmUiLCJpYXQiOjE1MjgyMDAxNjQsIm5iZiI6MTUyODIwMDE2NCwiZXhwIjoxNTI5NDk2MTY0LCJzdWIiOiI1MSIsInNjb3BlcyI6W119.SF_y6tsGhVxeHvwRqOFdHB37WiVU0Ma4VkzCfmW8Rtbms20PAYzRdhVb2pBIZyZ-7QsyOtqF0e805-YfOx2iX2ni50QroA8nJ8SeyJB4P4lVT6l5hy1V05zb6wGDLJ-_wKndj1h-_V7X5QYCn_3pfLkOaPXT4ha5_gfLQqspYxOekt8kWvcUTzuFeTUhMi1cYQKSzta0YDDJ9YAcRipxhCrOoF3G9AVWIiEUYVCwC1vAV32NNHN2xqoul1xdFg-RT36ASBYOnK41_4JWyWfCiDy6se2LoWvS8GsN5PbNuz_CkO60poa4Y5q91OPDzA3r3AB1-1BlBrp06P4WxKwYcH9fvOwvesV5_aDubbV_M7iFolfvPeT_JjV3fFzrirRoB13ehvxib1DWYWcJM4AsdPu122lQiOuP76-qVkk-W2pMRDiWmzQWQ29E9EjZ2PQQYHkDk-XTtx8Jrhu-mSBDA39c6H8gMoeSTcAPHs_FlUeDMDKFB_Usr46gIo866jn_pObJ9TcvUp-qXWuH0sJCgIRWeLWJhF-y2ZkOdSz9l8MhUXOYj12ZN1QSIBgCdGFA-csppytFT7UDxUF6ouzbmxcWWk5NJTST68jxMtOLGytZIq3cGPdFsdnsjbleTr2zj0qU8aVqU6Dr9kK-e9rsWo4Vq4I3IZHViLNr8Q_9k_s';

    /**
     * Get list data
     *
     * @return void
     */
    public function testGetData()
    {
        $client = new \GuzzleHttp\Client();

        $request = $client->request('GET', env('APP_URL') . '/api/v1/resources', [
            'headers' => [
                'Accept'             => 'application/json',
                'Content-Type'       => 'application/json',
                'Authorization'      => $this->authorization
            ],
            'query' => [
                'keyword'                => 'a',
                'resource_type_id'       => 1,
                'channel_id'       => 1,
                'theme_id'       => 1,
                'trash'       => 1,
            ]
        ]);
        $data = json_decode($request->getBody()->getContents());
        dump('======= GET DATA ======');
        dump($data);

        $this->assertEquals(200, $request->getStatusCode());
    }

    /**
     * Store data
     *
     * @return void
     */
    public function testStoreData()
    {
        $client = new \GuzzleHttp\Client();

        $request = $client->request('POST', env('APP_URL') . '/api/v1/resources', [
            'headers' => [
                'Accept'             => 'application/json',
                'Content-Type'       => 'application/json',
                'Authorization'      => $this->authorization
            ],
            'json' => [
                "title" => str_random(15),
                "resource_type_id" => 2,
                "channel_id" => 2,
                "theme_id" => 2,
                "duration" => 1,
                "author" => "aaaaa",
                "start_time" => "2018-10-19 12:00:00",
                "end_time" => "2018-10-22 12:00:00"
            ]
        ]);
        $data = json_decode($request->getBody()->getContents());

        dump('======= STORE DATA ======');
        dump($data);

        $this->assertEquals(201, $request->getStatusCode());
    }

    /**
     * Update data
     *
     * @return void
     */
    public function testUpdateData()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->request('PUT', env('APP_URL') . '/api/v1/resources/4', [
            'headers' => [
                'Accept'             => 'application/json',
                'Content-Type'       => 'application/json',
                'Authorization'      => $this->authorization
            ],
            'json' => [
                "title" => str_random(15),
                "resource_type_id" => 2,
                "channel_id" => 2,
                "theme_id" => 2,
                "duration" => 1,
                "author" => "aaaaa",
                "start_time" => "2018-10-19 12:00:00",
                "end_time" => "2018-10-22 12:00:00"
            ]
        ]);
        $data = json_decode($request->getBody()->getContents());
        dump('======= UPDATE DATA ======');
        dump($data);

        $this->assertEquals(200, $request->getStatusCode());
    }

    /**
     * TRASH data
     *
     * @return void
     */
    public function testTrashData()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->request('PUT', env('APP_URL') . '/api/v1/resources/2/trash', [
            'headers' => [
                'Accept'             => 'application/json',
                'Content-Type'       => 'application/json',
                'Authorization'      => $this->authorization
            ]
        ]);
        $data = json_decode($request->getBody()->getContents());
        dump('======= TRASH DATA ======');
        dump($data);

        $this->assertEquals(204, $request->getStatusCode());
    }

    /**
     * Restore data
     *
     * @return void
     */
    public function testRestoreData()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->request('PUT', env('APP_URL') . '/api/v1/resources/2/restore', [
            'headers' => [
                'Accept'             => 'application/json',
                'Content-Type'       => 'application/json',
                'Authorization'      => $this->authorization
            ]
        ]);
        $data = json_decode($request->getBody()->getContents());
        dump('======= RESTORE DATA ======');
        dump($data);

        $this->assertEquals(204, $request->getStatusCode());
    }

    /**
     * Delete data
     *
     * @return void
     */
    public function testDeleteData()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->request('DELETE', env('APP_URL') . '/api/v1/resources/2', [
            'headers' => [
                'Accept'             => 'application/json',
                'Content-Type'       => 'application/json',
                'Authorization'      => $this->authorization
            ]
        ]);
        $data = json_decode($request->getBody()->getContents());
        dump('======= DELETE DATA ======');
        dump($data);

        $this->assertEquals(204, $request->getStatusCode());
    }
}
