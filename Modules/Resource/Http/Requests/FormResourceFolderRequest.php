<?php

namespace Modules\Resource\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormResourceFolderRequest extends FormRequest
{

    private $rules = [
        'name' => 'required|min:1|max:255',
        'channel_id' => 'required|integer',
        'parent_id' => 'integer',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.exists' => sprintf(__('Can not find data with id %s'), $this->resource_folder)
        ];
    }
}
