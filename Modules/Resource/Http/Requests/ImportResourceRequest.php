<?php

namespace Modules\Resource\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CheckExistsDataRule;
use Modules\Channel\Repositories\Contracts\ChannelRepositoryInterface;

class ImportResourceRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => [
                'required',
                'integer',
                new CheckExistsDataRule(app()->make(ChannelRepositoryInterface::class))
            ],
            'resourceFolderId' => 'required|integer',
            'resourceTypeId' => 'required|integer',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
       return [];
    }
}
