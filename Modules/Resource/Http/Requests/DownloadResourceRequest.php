<?php

namespace Modules\Resource\Http\Requests;

use App\Rules\CheckExistsDataRule;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Resource\Repositories\Contracts\ResourceRepositoryInterface;
use Modules\User\Repositories\Contracts\ChannelUsersRepositoryInterface;

class DownloadResourceRequest extends FormRequest
{

    private $rules = [
        'download_url' => 'required|min:1',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /**
         * Validate Resource_id, channel_id must exist in the database
         */
        $this->rules['channel_id'] = [
            'required',
            'integer',
            new CheckExistsDataRule(app()->make(ChannelUsersRepositoryInterface::class))
        ];

        $this->rules['resource_id'] = [
            'required',
            'integer',
            new CheckExistsDataRule(app()->make(ResourceRepositoryInterface::class))
        ];

        return $this->rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
