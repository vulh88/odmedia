<?php

namespace Modules\Resource\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateResourceTypeRequest extends FormRequest
{
    private $rules = [
        'channel_id' => 'required|integer|min:1',
        'name' => 'required|max:255',
        'class' => 'max:20|nullable',
        'description' => 'nullable',
        'default_osd_id' => 'integer|min:0|nullable',
        'osd_loop' => 'integer|min:0|nullable',
        'is_content' => 'integer|min:0|max:1|nullable',
        'is_overlay' => 'integer|min:0|max:1|nullable',
        'wipe' => 'integer|min:0|max:1|nullable',
        'active' => 'integer|min:0|max:1|nullable',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge(
            [
                'id.exists' => 'Can not find data with id ' . $this->resource_type
            ],
            array_fill_keys(
                [
                    'is_content.min',
                    'is_content.max',
                    'wipe.min',
                    'wipe.max',
                    'active.min',
                    'active.max',
                ],
                __('Can only be 0 or 1. If empty, default 0')
            )
        );
    }
}
