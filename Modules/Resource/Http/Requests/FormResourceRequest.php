<?php

namespace Modules\Resource\Http\Requests;

use App\Rules\CheckExistsDataRule;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Channel\Repositories\Contracts\ChannelRepositoryInterface;
use Modules\Resource\Repositories\Contracts\ResourceTypeRepositoryInterface;
use Modules\Theme\Repositories\Contracts\ThemeRepositoryInterface;

class FormResourceRequest extends FormRequest
{

    private $rules = [
        'title' => 'required|min:1|max:255',
        'duration' => 'integer',
        'author' => 'nullable',
        'desc_en' => 'string|nullable',
        'desc_nl' => 'string|nullable',
        'frequency' => 'string|nullable',
        'premiere' => 'integer|min:0|max:1',
        'tags' => 'array|nullable',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /** Validate start time  **/
        if (isset($this->start_time)) {
            $this->rules['start_time'] = 'date_format:Y-m-d|after:now';
        }
        /** Validate end time  **/
        if (isset($this->end_time)) {
            $this->rules['end_time'] = 'date_format:Y-m-d|after:start_time';
        }
        /** Validate time_not_before  **/
        if (isset($this->time_not_before)) {
            $this->rules['time_not_before'] = 'date_format:H:i';
        }
        /** Validate time_not_after  **/
        if (isset($this->time_not_after)) {
            $this->rules['time_not_after'] = 'date_format:H:i|after:time_not_before';
        }

        /**
         * Validate Resource_type_id, channel_id, theme_id must exist in the database
         */
        $this->rules['resource_type_id'] = [
            'integer',
            new CheckExistsDataRule(app()->make(ResourceTypeRepositoryInterface::class))
        ];
        $this->rules['channel_id'] = [
            'integer',
            new CheckExistsDataRule(app()->make(ChannelRepositoryInterface::class))
        ];
        $this->rules['theme_id'] = [
            'integer',
            new CheckExistsDataRule(app()->make(ThemeRepositoryInterface::class))
        ];

        return $this->rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge(
            [
                'id.exists' => 'Can not find data with id ' . $this->resource
            ],
            array_fill_keys(
                [
                    'premiere.min',
                    'premiere.max'
                ],
                __('Can only be 0 or 1. If empty, default 0')
            )
        );
    }
}
