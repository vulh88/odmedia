<?php

namespace Modules\Resource\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CheckExistsDataRule;
use Modules\Channel\Repositories\Contracts\ChannelRepositoryInterface;
use Modules\User\Repositories\Contracts\ChannelUsersRepositoryInterface;
use App\Rules\CheckChannelStorage;
use App\Rules\CheckExtensionFile;
use Setting;

class UploadResourceRequest extends FormRequest
{
    /**
     * Load AppHelpers
     */
    use \AppHelpers;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //$extensions = str_replace(' ', '', Setting::get('upload_extensions', 'jpg, gif, jpeg, png, mp3, mp4'));
        $maxSizeUpload = self::convertMBtoKB(Setting::get('maximum_upload', '10'));

        return [
            'channel_id' => [
                'required',
                'integer',
                new CheckExistsDataRule(app()->make(ChannelRepositoryInterface::class)),
                new CheckChannelStorage(app()->make(ChannelRepositoryInterface::class)),
            ],
            'file' => [
                'required',
                //'mimetypes:' .$extensions,
                new CheckExtensionFile($this->upload_type),
                'max:' . $maxSizeUpload
            ]
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge(
            [
                'file.max' => 'The file you attempted to upload is too large. Maximum upload file ' . Setting::get('maximum_upload', '10') . ' MB',
                'file.mimes' => 'The file must be a file of type:' . Setting::get('upload_extensions', 'jpg, gif, jpeg, png, mp3, mp4'),
                'file.mimetypes' => 'The file must be a file of type:' . Setting::get('upload_extensions', 'jpg, gif, jpeg, png, mp3, mp4')
            ]
        );
    }

}
