<?php

namespace Modules\Resource\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormResourceTagRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->resource_tag) {
            return [
                'id'    => 'integer|exists:resource_tags,id',
                'name' => 'required|max:255|unique:resource_tags,name,'. $this->resource_tag,
                'resource_id' => 'required|integer',
            ];
        }

        return [
            'name' => 'required|max:255|unique:resource_tags',
            'resource_id' => 'required|integer',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.exists' => 'Can not find data with id ' . $this->resource_tag
        ];
    }
}
