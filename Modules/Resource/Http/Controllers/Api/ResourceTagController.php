<?php

namespace Modules\Resource\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\ApiController;
use Modules\Resource\Repositories\Contracts\ResourceTagRepositoryInterface;
use Modules\Resource\Transformers\ResourceTagCollection;
use Modules\Resource\Http\Requests\FormResourceTagRequest;
use Modules\Resource\Transformers\ResourceTagResource;

class ResourceTagController extends ApiController
{
    /**
     * Get list of resource tags.
     *
     * @param ResourceTagRepositoryInterface $resourceTagRepository
     * @param Request $request
     * @return ResourceTagCollection
     *
     * @SWG\Get(
     *     path="/resource-tags",
     *     tags={"Resource Tags"},
     *     operationId="getResourceTags",
     *     description="Gets resource tag items",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="keyword", in="query", type="string", description="Search by name"),
     *     @SWG\Parameter(name="resource_id", in="query", type="string", description="Search by resource id"),
     *
     *     @SWG\Parameter(name="sort", in="query", type="string", description="Field to sort by, if prefixed with '-', the sorting direction will be descending"),
     *     @SWG\Parameter(name="page", in="query", type="integer", description="Number of page used to pagination"),
     *     @SWG\Parameter(name="count", in="query", type="integer", description="Number of records to display"),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *         @SWG\Header(header="X-Total", type="integer", description="Total count of resource tags"),
     *         @SWG\Header(header="X-Skipped", type="integer", description="Resource tags skipped"),
     *         @SWG\Header(header="X-Limit", type="integer", description="Resource tags limit")
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function index(ResourceTagRepositoryInterface $resourceTagRepository, Request $request)
    {
        $columns = ['resource_tags.id', 'resource_tags.name', 'resource_tags.resource_id'];
        $results = $resourceTagRepository->search($columns, $request);
        return new ResourceTagCollection($results->getCollection(), $results->total(), $results->currentPage(), $results->lastPage());
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Create a resource tag
     *
     * @param ResourceTagRepositoryInterface $resourceTagRepository
     * @param FormResourceTagRequest $request
     * @return ResourceTagResource
     * @SWG\Post(
     *     path="/resource-tags",
     *     tags={"Resource Tags"},
     *     operationId="postCreateResourceTag",
     *     description="Creates new (or updates existing) resource tag.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Resource tag object that needs to be created",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/ResourceTagRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successfully created resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of created resource tag"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function store(ResourceTagRepositoryInterface $resourceTagRepository, FormResourceTagRequest $request)
    {
        $data = $request->validated();
        return new ResourceTagResource($resourceTagRepository->create($data));
    }

    /**
     * Show the specified resource tag
     * @param $id
     * @param ResourceTagRepositoryInterface $resourceTagRepository
     * @return ResourceTagResource
     */
    public function show($id, ResourceTagRepositoryInterface $resourceTagRepository)
    {
        $data = $resourceTagRepository->findOrFail($id);
        return new ResourceTagResource($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Updates a resource tag
     *
     * @param $id
     * @param ResourceTagRepositoryInterface $resourceTagRepository
     * @param FormResourceTagRequest $request
     * @return \Illuminate\Http\JsonResponse|ResourceTagResource
     * @SWG\Put(
     *     path="/resource-tags/{id}",
     *     tags={"Resource Tags"},
     *     operationId="putUpdateResourceTag",
     *     description="Creates new (or updates existing) resource tag.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     * 			name="id",
     * 			in="path",
     * 			required=true,
     * 			type="integer",
     * 			description="UUID",
     * 		),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Resource tag object that needs to be Updated",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/ResourceTagRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successfully updated resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of updated"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function update($id, ResourceTagRepositoryInterface $resourceTagRepository, FormResourceTagRequest $request)
    {
        $data = $request->validated();
        return new ResourceTagResource($resourceTagRepository->update($data, $id));
    }

    /**
     * Delete existing resource tag.
     *
     * @param int $id
     * @param ResourceTagRepositoryInterface $resourceTagRepository
     * @return \Illuminate\Http\JsonResponse
     * @SWG\Delete(
     *     path="/resource-tags/{id}",
     *     tags={"Resource Tags"},
     *     operationId="deleteResourceTag",
     *     description="Delete existing resource tag",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of resource tag that should be deleted",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of deleted resource tag"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Resource not found.",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function destroy($id, ResourceTagRepositoryInterface $resourceTagRepository)
    {
        if ( $resourceTagRepository->delete($id)) {
            return response()->noContent();
        }

        return response()->error(__('Resource tag does not exist'), 404);
    }
}
