<?php

namespace Modules\Resource\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\ApiController;
use Modules\Channel\Repositories\Contracts\ChannelRepositoryInterface;
use Modules\Resource\Http\Requests\FormResourceRequest;
use Modules\Resource\Repositories\Contracts\ResourceRepositoryInterface;
use Modules\Resource\Repositories\Contracts\ResourceTagRepositoryInterface;
use Modules\Resource\Services\DownloadResourceService;
use Modules\Resource\Services\Import\ImportFromAPIService;
use Modules\Resource\Transformers\ResourceResource;
use Modules\Resource\Transformers\ResourceCollection;
use Modules\User\Repositories\Contracts\ChannelUsersRepositoryInterface;
use Modules\User\Repositories\Eloquents\ChannelUsersRepository;
use Modules\Resource\Services\Import\UploadResourcesService;
use Modules\Resource\Http\Requests\UploadResourceRequest;
use Modules\Resource\Http\Requests\ImportResourceRequest;
use Modules\Resource\Http\Requests\PreviewResourceRequest;
use Modules\Resource\Http\Requests\DownloadResourceRequest;

class ResourceController extends ApiController
{

    /**
     * Load AppHelpers
     */
    use \AppHelpers;

    /**
     * Get list of resources.
     *
     * @param ResourceRepositoryInterface $resourceRepository
     * @param Request $request
     * @return ResourceCollection
     *
     * @SWG\Get(
     *     path="/resources",
     *     tags={"Resources"},
     *     operationId="getResource",
     *     description="Gets resource items",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="keyword", in="query", type="string", description="Search by name"),
     *     @SWG\Parameter(name="resource_type_id", in="query", type="string", description="Search by resource type id"),
     *     @SWG\Parameter(name="only_overlay_resources", in="query", type="string", description="Only get overlay resources have type is overlay"),
     *     @SWG\Parameter(name="only_content_resources", in="query", type="string", description="Only get content resources have type is content"),
     *     @SWG\Parameter(name="only_wipe_resources", in="query", type="string", description="Only get content resources have type is wipe"),
     *     @SWG\Parameter(name="resource_folder_id", in="query", type="string", description="Search by resource folder id"),
     *     @SWG\Parameter(name="channel_id", in="query", type="string", description="Search by channel id"),
     *     @SWG\Parameter(name="theme_id", in="query", type="string", description="Search by theme id"),
     *     @SWG\Parameter(name="author", in="query", type="string", description="Search by author"),
     *     @SWG\Parameter(name="trash", in="query", type="string", description="Search by trash"),
     *
     *     @SWG\Parameter(name="sort", in="query", type="string", description="Field to sort by, if prefixed with '-', the sorting direction will be descending"),
     *     @SWG\Parameter(name="page", in="query", type="integer", description="Number of page used to pagination"),
     *     @SWG\Parameter(name="count", in="query", type="integer", description="Number of records to display"),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *         @SWG\Header(header="X-Total", type="integer", description="Total count of resources"),
     *         @SWG\Header(header="X-Skipped", type="integer", description="Resource folders skipped"),
     *         @SWG\Header(header="X-Limit", type="integer", description="Resource folders limit")
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function index(ResourceRepositoryInterface $resourceRepository, Request $request)
    {
        $columns = [
            'resources.id',
            'resources.title',
            'resources.resource_type_id',
            'resources.resource_folder_id',
            'resources.channel_id',
            'resources.theme_id',
            'resources.duration',
            'resources.user_id',
            'resources.start_time',
            'resources.end_time',
            'resources.time_not_before',
            'resources.time_not_after',
            'resources.filename',
            'resources.thumb_filename',
            'resources.updated_at'
        ];
        $params = $request->all();
        $params['user_id'] = Auth::user()->id;
        $results = $resourceRepository->search($columns, $params);
        return new ResourceCollection($results->getCollection(), $results->total(), $results->currentPage(), $results->lastPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Create a resource
     *
     * @param ResourceRepositoryInterface $resourceRepository
     * @param FormResourceRequest $request
     * @return ResourceResource
     * @SWG\Post(
     *     path="/resources",
     *     tags={"Resources"},
     *     operationId="postCreateResource",
     *     description="Creates new (or updates existing) resource.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Resource folder object that needs to be created",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/ResourceRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successfully created resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of created resource"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function store(ResourceRepositoryInterface $resourceRepository, FormResourceRequest $request)
    {
        $data = $request->validated();
        return new ResourceResource($resourceRepository->create($data));
    }


    /**
     * Get detail resource
     *
     * @param $id
     * @param ResourceRepositoryInterface $resourceRepository
     * @return ResourceResource
     * @SWG\Get(
     *     path="/resources/{id}",
     *     tags={"Resources"},
     *     operationId="getDetailResource",
     *     description="Detail Resource",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *            name="id",
     *            in="path",
     *            required=true,
     *            type="string",
     *            description="UUID",
     *        ),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function show($id, ResourceRepositoryInterface $resourceRepository)
    {
        $data = $resourceRepository->findOrFail($id);
        return new ResourceResource($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Updates a resource
     *
     * @param int $id
     * @param ResourceRepositoryInterface $resourceRepository
     * @param FormResourceRequest $request
     * @param ResourceTagRepositoryInterface $resourceTagRepository
     * @return \Illuminate\Http\JsonResponse|ResourceResource
     * @SWG\Put(
     *     path="/resources/{id}",
     *     tags={"Resources"},
     *     operationId="putUpdateResource",
     *     description="Creates new (or updates existing) resource.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *            name="id",
     *            in="path",
     *            required=true,
     *            type="integer",
     *            description="UUID",
     *        ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Resource folder object that needs to be Updated",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/ResourceRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successfully updated resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of updated"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function update(
        $id,
        ResourceRepositoryInterface $resourceRepository,
        FormResourceRequest $request,
        ResourceTagRepositoryInterface $resourceTagRepository
    )
    {
        $data = $request->validated();
        /**
         * Update tags for resource
         */
        if (!empty($data['tags'])) {
            $resourceTagRepository->saveMultipleTags($data['tags'], $id);
        } else {
            $resourceTagRepository->deleteAllTagsOfResource($id);
        }

        return new ResourceResource($resourceRepository->update($data, $id));
    }

    /**
     * Delete existing resource.
     *
     * @param int $id
     * @param ResourceRepositoryInterface $resourceRepository
     * @return mixed
     * @SWG\Delete(
     *     path="/resources/{id}",
     *     tags={"Resources"},
     *     operationId="deleteResource",
     *     description="Delete existing resource",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of resource that should be deleted",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of deleted resource"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Resource not found.",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function destroy($id, ResourceRepositoryInterface $resourceRepository)
    {
        if ($resourceRepository->delete($id)) {
            return response()->noContent();
        }

        return response()->error(__('Resource does not exist'), 404);
    }

    /**
     * Import data from API
     *
     * @param ImportFromAPIService $importFromAPIService
     * @param ImportResourceRequest $request
     * @param ChannelUsersRepository $channelUsersRepository
     * @return \Illuminate\Http\JsonResponse
     * @SWG\Post(
     *     path="/resources/import-api",
     *     tags={"Resources"},
     *     operationId="importAPI",
     *     description="import data from API",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Page",
     *         in="query",
     *         type="integer",
     *         name="page",
     *     ),
     *     @SWG\Parameter(
     *         description="Channel ID",
     *         in="query",
     *         type="integer",
     *         name="channel_id",
     *     ),
     *     @SWG\Parameter(
     *         description="Resource Folder ID",
     *         in="query",
     *         type="integer",
     *         name="resourceFolderId",
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function importAPI(
        ImportFromAPIService $importFromAPIService,
        ImportResourceRequest $request,
        ChannelUsersRepository $channelUsersRepository
    )
    {
        $params = $request->all();

        /**
         * get data channel user from current user
         */
        $channelUser = $channelUsersRepository
            ->findWhere(
                ['user_id' => $request->user()->id, 'channel_id' => $params['channel_id'] ],
                ['channel_id', 'api_path', 'api_auth', 'player_directory']
            )
            ->first();

        $data = $importFromAPIService->execute(
            [
                'channel_id' => $channelUser->channel_id,
                'resourceFolderId' => (int)$params['resourceFolderId'],
                'resourceTypeId' => (int)$params['resourceTypeId'],
                'player_directory' => $channelUser->player_directory,
                'api_path' => $channelUser->api_path,
                'api_auth' => $channelUser->api_auth,
                'page' => $params['page']
            ]
        );
        return response()->json($data);
    }

    /**
     * Preview data from external API
     *
     * @param ImportFromAPIService $importFromAPIService
     * @param PreviewResourceRequest $request
     * @param ChannelUsersRepository $channelUsersRepository
     * @return \Illuminate\Http\JsonResponse
     * @SWG\Get(
     *     path="/resources/preview-import-api",
     *     tags={"Resources"},
     *     operationId="previewImportAPI",
     *     description="Get data from external API",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Page",
     *         in="query",
     *         type="integer",
     *         name="page",
     *     ),
     *     @SWG\Parameter(
     *         description="Channel ID",
     *         in="query",
     *         type="integer",
     *         name="channel_id",
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function previewImportAPI (
        ImportFromAPIService $importFromAPIService,
        PreviewResourceRequest $request,
        ChannelUsersRepository $channelUsersRepository
    )
    {
        $params = $request->all();

        /**
         * get data channel user from current user
         */
        $channelUser = $channelUsersRepository
            ->findWhere(
                ['user_id' => $request->user()->id, 'channel_id' => $params['channel_id'] ],
                ['channel_id', 'api_path', 'api_auth', 'player_directory']
            )
            ->first();

        $data = $importFromAPIService->execute(
            [
                'preview' => 1,
                'channel_id' => $channelUser->channel_id,
                'player_directory' => $channelUser->player_directory,
                'api_path' => $channelUser->api_path,
                'api_auth' => $channelUser->api_auth,
                'page' => $params['page']
            ]
        );
        return response()->json($data);
    }

    public function report() {

    }

    /**
     * Upload resources
     *
     * @param UploadResourcesService $uploadResourcesService
     * @param UploadResourceRequest $request
     * @param ChannelRepositoryInterface $channelRepository
     * @return \Illuminate\Http\JsonResponse
     * @SWG\Post(
     *     path="/resources/upload",
     *     tags={"Resources"},
     *     operationId="uploadResourcesAPI",
     *     description="Upload resources",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="File",
     *         in="formData",
     *         type="file",
     *         format="file",
     *         name="file",
     *     ),
     *     @SWG\Parameter(
     *         description="Channel ID",
     *         in="formData",
     *         type="integer",
     *         name="channel_id",
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function upload(
        UploadResourcesService $uploadResourcesService,
        UploadResourceRequest $request,
        ChannelRepositoryInterface $channelRepository
    )
    {
        /**
         * get data channel from channel ID
         */
        $channel = $channelRepository->findOrFail($request['channel_id']);
        $requestFiles = $request->file('file');
        /**
         * Create storage for each user
         */
        $uploadStorage = createChannelUserStorage(
            $channel->resource_path,
            $channel->thumbnail_storage,
            $request->user()->email,
            $request->user()->id
        );
        $file =  $uploadResourcesService->upload(
            $requestFiles,
            $uploadStorage
        );
        return response()->json($uploadResourcesService->execute(
            [
                'action' => 'upload-resources',
                'file' => $file,
                'channel_id' => $channel->id,
                'resource_folder_id' => $request['resource_folder_id'],
                'user_id' => $request->user()->id,
                'only_overlay_resources' => $request['only_overlay_resources'],
                'only_content_resources' => $request['only_content_resources'],
                'upload_type' => $request['upload_type'],
                'thumbnail_storage_path' => $uploadStorage['thumbnail_storage'],
            ]
        ));
    }

    /**
     * Get authors
     *
     * @param Request $request
     * @param ResourceRepositoryInterface $resourceRepository
     * @return \Illuminate\Http\JsonResponse
     * @SWG\Get(
     *     path="/resources/authors",
     *     tags={"Resources"},
     *     operationId="getAuthorsAPI",
     *     description="Get Authors",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function authors(Request $request, ResourceRepositoryInterface $resourceRepository)
    {
        $params = $request->all();
        $data = $resourceRepository->getAuthors($params['channel_id']);
        return response()->json($data);
    }

    /**
     * Download resources from UI
     *
     * @param DownloadResourceRequest $request
     * @param ResourceRepositoryInterface $resourceRepository
     * @param ChannelUsersRepositoryInterface $channelUsersRepository
     * @param DownloadResourceService $downloadResourceService
     * @return ResourceResource
     * @SWG\Post(
     *     path="/resources/download",
     *     tags={"Resources"},
     *     operationId="downloadResourcesAPI",
     *     description="Download resources",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Download Url",
     *         in="formData",
     *         type="string",
     *         name="download_url",
     *     ),
     *     @SWG\Parameter(
     *         description="Channel ID",
     *         in="formData",
     *         type="integer",
     *         name="channel_id",
     *     ),
     *     @SWG\Response(
     *         response="204",
     *         description="Successful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function download(
        DownloadResourceRequest $request,
        ResourceRepositoryInterface $resourceRepository,
        ChannelUsersRepositoryInterface $channelUsersRepository,
        DownloadResourceService $downloadResourceService
    )
    {
        $data = $request->validated();
        $channelUser = $channelUsersRepository->findWhere(['user_id' => $request->user()->id, 'channel_id' => $data['channel_id']])->first();

        $downloadResourceService->downloadUrl = $data['download_url'];
        $downloadResourceService->playerDirectory = config('filesystems.odmedia.download_resources'). '/' . $channelUser->player_directory;
        $fileName = $downloadResourceService->downloadByUrl();
        $downloadSuccess = 0;
        if ($fileName) {
            $downloadSuccess = $resourceRepository->update([
                'filename' => $fileName,
                'original_filename' => $fileName,
                'api_fetched' => 1,
                'is_video' => 1
            ], $data['resource_id']);
        }

        if ($downloadSuccess) {
            $data = $resourceRepository->findOrFail($data['resource_id']);
            return new ResourceResource($data);
        }
        return response()->error(__('Download fail or connection timeout'), 404);
    }
}
