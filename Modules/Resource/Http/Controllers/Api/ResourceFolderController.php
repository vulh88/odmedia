<?php

namespace Modules\Resource\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\ApiController;
use Modules\Resource\Http\Requests\FormResourceFolderRequest;
use Modules\Resource\Repositories\Contracts\ResourceFolderRepositoryInterface;
use Modules\Resource\Transformers\ResourceFolderResource;
use Modules\Resource\Transformers\ResourceFolderCollection;


class ResourceFolderController extends ApiController
{
    /**
     * Load AppHelpers
     */
    use \AppHelpers;

    /**
     * Get list of resource folders.
     *
     * @param ResourceFolderRepositoryInterface $resourceFolderRepository
     * @param Request $request
     * @return ResourceFolderCollection
     *
     * @SWG\Get(
     *     path="/resource-folders",
     *     tags={"Resource Folders"},
     *     operationId="getResourceFolders",
     *     description="Gets resource folder items",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="keyword", in="query", type="string", description="Search by name"),
     *     @SWG\Parameter(name="parent_id", in="query", type="string", description="Search by parent id"),
     *     @SWG\Parameter(name="trash", in="query", type="string", description="Search by trash"),
     *
     *     @SWG\Parameter(name="sort", in="query", type="string", description="Field to sort by, if prefixed with '-', the sorting direction will be descending"),
     *     @SWG\Parameter(name="page", in="query", type="integer", description="Number of page used to pagination"),
     *     @SWG\Parameter(name="count", in="query", type="integer", description="Number of records to display"),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *         @SWG\Header(header="X-Total", type="integer", description="Total count of resource folders"),
     *         @SWG\Header(header="X-Skipped", type="integer", description="Resource folders skipped"),
     *         @SWG\Header(header="X-Limit", type="integer", description="Resource folders limit")
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function index(ResourceFolderRepositoryInterface $resourceFolderRepository, Request $request)
    {
        $columns = ['id', 'name', 'channel_id', 'parent_id'];
        $results = $resourceFolderRepository->search($columns, $request);
        return new ResourceFolderCollection($results->getCollection(), $results->total(), $results->currentPage(), $results->lastPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Create a resource folder
     *
     * @param ResourceFolderRepositoryInterface $resourceFolderRepository
     * @param FormResourceFolderRequest  $request
     * @return ResourceFolderResource
     * @SWG\Post(
     *     path="/resource-folders",
     *     tags={"Resource Folders"},
     *     operationId="postCreateResourceFolder",
     *     description="Creates new (or updates existing) resource folder.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Resource folder object that needs to be created",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/ResourceFolderRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successfully created resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of created resource folder"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function store(ResourceFolderRepositoryInterface $resourceFolderRepository, FormResourceFolderRequest $request)
    {
        $data = $request->validated();
        return new ResourceFolderResource($resourceFolderRepository->create($data));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Updates a resource folder
     *
     * @param int  $id
     * @param ResourceFolderRepositoryInterface $resourceFolderRepository
     * @param FormResourceFolderRequest $request
     * @return \Illuminate\Http\JsonResponse|ResourceFolderResource
     * @SWG\Put(
     *     path="/resource-folders/{id}",
     *     tags={"Resource Folders"},
     *     operationId="putUpdateResourceFolder",
     *     description="Creates new (or updates existing) resource folder.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     * 			name="id",
     * 			in="path",
     * 			required=true,
     * 			type="integer",
     * 			description="UUID",
     * 		),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Resource folder object that needs to be Updated",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/ResourceFolderRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successfully updated resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of updated"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function update($id, ResourceFolderRepositoryInterface $resourceFolderRepository, FormResourceFolderRequest $request)
    {
        $data = $request->validated();
        return new ResourceFolderResource($resourceFolderRepository->update($data, $id));
    }

    /**
     * Delete existing resource folder.
     *
     * @param int $id
     * @param ResourceFolderRepositoryInterface $folderRepository
     * @return mixed
     * @SWG\Delete(
     *     path="/resource-folders/{id}",
     *     tags={"Resource Folders"},
     *     operationId="deleteResourceFolder",
     *     description="Delete existing resource folder",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of resource folder that should be deleted",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of deleted resource folder"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Resource not found.",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function destroy($id, ResourceFolderRepositoryInterface $folderRepository)
    {
        if ( $folderRepository->delete($id)) {
            return response()->noContent();
        }

        return response()->error(__('Resource folder does not exist'), 404);
    }

    /**
     * @param ResourceFolderRepositoryInterface $resourceFolderRepository
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTreeFolders(ResourceFolderRepositoryInterface $resourceFolderRepository, Request $request) {
        $columns = ['id', 'name', 'parent_id'];
        $results = $resourceFolderRepository->search($columns, $request);

        $data = self::buildTreeFolders($results->getCollection(), 0);
        return response()->json(array_values($data));
    }


}
