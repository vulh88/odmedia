<?php

namespace Modules\Resource\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\ApiController;
use Modules\Resource\Http\Requests\CreateResourceTypeRequest;
use Modules\Resource\Repositories\Contracts\ResourceTypeRepositoryInterface;
use Modules\Resource\Services\DeleteResourceTypeService;
use Modules\Resource\Transformers\ResourceTypeCollection;
use Modules\Resource\Transformers\ResourceTypeResource;

class ResourceTypeController extends ApiController
{
    /**
     * Get list of resource types.
     *
     * @param ResourceTypeRepositoryInterface $resourceTypeRepository
     * @param Request $request
     * @return ResourceTypeCollection
     *
     * @SWG\Get(
     *     path="/resource-types",
     *     tags={"Resource Types"},
     *     operationId="getResourceTypes",
     *     description="Gets resource type items",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="keyword", in="query", type="string", description="Search by name"),
     *
     *     @SWG\Parameter(name="sort", in="query", type="string", description="Field to sort by, if prefixed with '-', the sorting direction will be descending"),
     *     @SWG\Parameter(name="page", in="query", type="integer", description="Number of page used to pagination"),
     *     @SWG\Parameter(name="count", in="query", type="integer", description="Number of records to display"),
     *     @SWG\Parameter(name="channel_id", in="query", type="integer", description="Selected channel"),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *         @SWG\Header(header="X-Total", type="integer", description="Total count of resource types"),
     *         @SWG\Header(header="X-Skipped", type="integer", description="Resource types skipped"),
     *         @SWG\Header(header="X-Limit", type="integer", description="Resource types limit")
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function index(ResourceTypeRepositoryInterface $resourceTypeRepository, Request $request)
    {
        $columns = [
            'id',
            'channel_id',
            'name',
            'class',
            'description',
            'default_osd_id',
            'osd_loop',
            'is_content',
            'is_overlay',
            'wipe',
            'active'
        ];
        $results = $resourceTypeRepository->search($columns, $request);
        return new ResourceTypeCollection($results->getCollection(), $results->total(), $results->currentPage(), $results->lastPage());
    }

    /**
     * Create a resource type
     *
     * @param ResourceTypeRepositoryInterface $resourceTypeRepository
     * @param CreateResourceTypeRequest $request
     * @return \Illuminate\Http\JsonResponse|ResourceTypeResource
     * @SWG\Post(
     *     path="/resource-types",
     *     tags={"Resource Types"},
     *     operationId="postCreateResourceType",
     *     description="Creates new (or updates existing) resource type.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Resource type object that needs to be created",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/ResourceTypeRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successfully created resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of created resource type"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function store(ResourceTypeRepositoryInterface $resourceTypeRepository, CreateResourceTypeRequest $request)
    {
        $data = $request->validated();
        return new ResourceTypeResource($resourceTypeRepository->create($data));
    }

    /**
     * Get detail content type
     *
     * @param $id
     * @param ResourceTypeRepositoryInterface $repository
     * @return ResourceTypeResource

     * @SWG\Get(
     *     path="/resource-types/{id}",
     *     tags={"Resource Types"},
     *     operationId="getDetailResourceType",
     *     description="Detail user",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *            name="id",
     *            in="path",
     *            required=true,
     *            type="string",
     *            description="UUID",
     *        ),
     *     @SWG\Response(
     *         response="200",
     *         description="Successful operation",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function show($id, ResourceTypeRepositoryInterface $repository)
    {
        $data = $repository->findOrFail($id);
        return new ResourceTypeResource($data);
    }

    /**
     * Updates a resource type
     *
     * @param $id
     * @param ResourceTypeRepositoryInterface $resourceTypeRepository
     * @param CreateResourceTypeRequest $request
     * @return \Illuminate\Http\JsonResponse|ResourceTypeResource
     * @SWG\Put(
     *     path="/resource-types/{id}",
     *     tags={"Resource Types"},
     *     operationId="putUpdateResourceType",
     *     description="Creates new (or updates existing) resource type.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     * 			name="id",
     * 			in="path",
     * 			required=true,
     * 			type="integer",
     * 			description="UUID",
     * 		),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Resource type object that needs to be Updated",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/ResourceTypeRequestDTO"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Unsuccessful operation",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Given request failed to pass validation",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successfully updated resource",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of updated"),
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function update($id, ResourceTypeRepositoryInterface $resourceTypeRepository, CreateResourceTypeRequest $request)
    {

        $data = $request->validated();
        return new ResourceTypeResource($resourceTypeRepository->update($data, $id));
    }

    /**
     * Delete existing resource type.
     *
     * @param int $id
     * @param ResourceTypeRepositoryInterface $resourceTypeRepository
     * @return mixed
     * @SWG\Delete(
     *     path="/resource-types/{id}",
     *     tags={"Resource Types"},
     *     operationId="deleteResourceType",
     *     description="Delete existing resource type",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of resource type that should be deleted",
     *         in="path",
     *         type="string",
     *         name="id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         @SWG\Header(header="X-ResourceId", type="integer", description="ID of deleted resource type"),
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Operation unsuccessful",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Resource not found.",
     *     ),
     *     security={
     *       {"api_token": {}}
     *     }
     * )
     */
    public function destroy($id, ResourceTypeRepositoryInterface $resourceTypeRepository)
    {

        if ( $resourceTypeRepository->delete($id) ) {
            return response()->noContent();
        }

        return response()->error(__('Resource type is not found'), 404);
    }

}
