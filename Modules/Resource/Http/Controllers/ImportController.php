<?php
namespace Modules\Resource\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Playlist\Repositories\Contracts\PlaylistItemRepositoryInterface;
use Modules\Playlist\Services\Generating\ResourcesFactory;
use Modules\Playlist\Repositories\Contracts\FormatRepositoryInterface;
use Modules\Playlist\Repositories\Contracts\FormatRuleRepositoryInterface;
use Modules\Playlist\Repositories\Contracts\PlaylistRepositoryInterface;
use Modules\Playlist\Services\Generating\RuleCommands\AnchorCommand;
use Modules\Playlist\Services\Generating\RuleCommands\CommandRuleAbstract;
use Modules\Playlist\Services\Generating\RuleCommands\EventBlockCommand;
use Modules\Playlist\Services\Generating\RuleCommands\RuleCommand;
use Modules\Playlist\Services\Generating\RuleConfigurationsFactory;
use Modules\Resource\Services\Import\ImportFromAPIService;
use Modules\Resource\Services\Import\UploadResourcesService;
use Modules\User\Repositories\Contracts\ChannelUsersRepositoryInterface;
use App\Utils\MyDateTime;

class ImportController extends Controller
{

    private $resources;

    public function import(ImportFromAPIService $importFromAPIService){

        // Import sample resources

        $channelUsersRepository = app(ChannelUsersRepositoryInterface::class);

        $channelUser = $channelUsersRepository
            ->findWhere(
                ['user_id' => 1, 'channel_id' => 1 ],
                ['channel_id', 'api_path', 'api_auth', 'player_directory']
            )
            ->first();

        /**
         * Sample resources data
         */
        $importFromAPIService->execute([
            'channel_id'    => 1,
            'page'  => 1,
            'resource_type_id'  => 1, // content type
            'resource_folder_id'    => 2,
            'api_path' => $channelUser->api_path,
            'api_auth' => $channelUser->api_auth,
        ]);
        $importFromAPIService->execute([
            'channel_id'    => 1,
            'page'  => 2,
            'resource_type_id'  => 2, // overlay type
            'resource_folder_id'    => 2,
            'api_path' => $channelUser->api_path,
            'api_auth' => $channelUser->api_auth,
        ]);


        $importFromAPIService->execute([
            'channel_id'    => 2,
            'page'  => 1,
            'resource_type_id'  => 1, // content type
            'resource_folder_id'    => 4,
            'api_path' => $channelUser->api_path,
            'api_auth' => $channelUser->api_auth,
        ]);
        $importFromAPIService->execute([
            'channel_id'    => 2,
            'page'  => 2,
            'resource_type_id'  => 2, // overlay type
            'resource_folder_id'    => 4,
            'api_path' => $channelUser->api_path,
            'api_auth' => $channelUser->api_auth,
        ]);
    }

    /**
     * @param UploadResourcesService $importFromDriveService
     * @return \Illuminate\Http\JsonResponse
     */
    public function importFromDrive(UploadResourcesService $importFromDriveService) {
        return response()->json($importFromDriveService->execute(['action' => 'scan-drive']));
    }

    /**
     * @param $id
     * @param FormatRepositoryInterface $formatRepository
     * @param PlaylistRepositoryInterface $playlistRepository
     * @param FormatRuleRepositoryInterface $formatRuleRepository
     * @param ResourcesFactory $resourcesFactory
     * @param RuleConfigurationsFactory $ruleConfigurationsFactory
     * @param AnchorCommand $anchorCommand
     * @param EventBlockCommand $eventBlockCommand
     * @param PlaylistItemRepositoryInterface $playlistItemRepository
     */
    public function generatePlaylist($id, FormatRepositoryInterface $formatRepository,
                                     PlaylistRepositoryInterface $playlistRepository,
                                     FormatRuleRepositoryInterface $formatRuleRepository,
                                     ResourcesFactory $resourcesFactory,
                                     RuleConfigurationsFactory $ruleConfigurationsFactory,
                                     AnchorCommand $anchorCommand,
                                     EventBlockCommand $eventBlockCommand,
                                     PlaylistItemRepositoryInterface $playlistItemRepository) {


        $format = $formatRepository->findOrFail($id);

        // Get end time of the last playlist on a specific channel
        $lastPlaylist = $playlistRepository->getTheLastPlaylist($format->channe_id);

        /**
         * Jump to +1 from the above value
         * Get start date of the playlist by getting the biggest end date of a playlist on a specific channel
         */
        $playlistStartTime = date('Y-m-d H:i:s', strtotime($lastPlaylist->start_time) + 1);
        $currentPosition = $playlistStartTime;

        /**
         * Create new playlist
         */

        /**
         * @Todo
         * check publish directly option for the status
         */
        $playlist = $playlistRepository->create([
            'channel_id'    => $format->channel_id,
            'start_time'    => $playlistStartTime,
            'name'  => sprintf('%s - Autogenerated', $format->name),
            'is_auto_generated' => 1,
            'user_id'   => Auth::user()->id,
            'status'    => 'draft'
        ]);

        /**
         * Get format rules
         */
        $formatRules = $formatRuleRepository->getFormatRules($id);

        /**
         * Seconds allotted per rule
         */
        $formatContentRules = $this->getContentRules($formatRules);
        if (count($formatContentRules)) {
            $secondsAllottedPerRule = $format->length/count($formatContentRules);
        }

        /**
         * Get resources by content rules
         */
        $reachedDuration = strtotime($currentPosition) - strtotime($playlistStartTime);
        $remainingDuration = $format->length - $reachedDuration;
        foreach ($formatContentRules as $formatContentRule) {

            $resources = $resourcesFactory->get($formatContentRule, [
                'remaining_duration'    => $remainingDuration,
            ]);
            $this->resources[$formatContentRule->rule_type] = $resources;
        }

        /**
         * Handle format rules
         */
        foreach ($formatRules as $formatRule) {
            $ruleName = $formatRule->rule_type;
            if ($this->isContentRule($ruleName)) {
                // Random a resource from the rule
                $resource = $this->randomResource($this->resources[$ruleName]);

                // Add this resource to the playlist
                $endTime = $currentPosition + $resource->duration();
                $cretePlaylistItemData = [
                    'playlist_id'   => $playlist->id,
                    'start_time'    => MyDateTime::formatDateTimeForMySQL($currentPosition),
                    'end_time'  => MyDateTime::formatDateTimeForMySQL($endTime),
                    'resource_id'   => $resource->id,
                    'item_track'    => config('api.content_track'),
                ];
                $playlistItemRepository->create($cretePlaylistItemData);

                // Re-assign current position
                $currentPosition = $endTime;
                continue;
            }

            switch ($ruleName) {
                case config('playlist.format.event_block'):
                    $currentPosition = $eventBlockCommand->execute($formatRule);
                    break;

                case config('playlist.format.anchor'):
                    $currentPosition = $anchorCommand->execute($formatRule);
            }
        }

    }

    /**
     * Get format content rules
     * @param $formatRules
     * @return int
     */
    private function getContentRules($formatRules) {
        if (!$formatRules) {
            return [];
        }

        $contentRules = config('playlist.format.content_rules');

        $formatContentRules = [];
        foreach ($formatRules as $formatRule) {
            $formatContentRules[] = $formatRule->rule_type;
        }

        return array_intersect($contentRules, $formatContentRules);
    }

    private function isContentRule($rule) {
        $contentRules = config('playlist.format.content_rules');
        return in_array($rule, $contentRules);
    }

    private function randomResource($resources) {
        return $resources[array_rand($resources)];
    }
}