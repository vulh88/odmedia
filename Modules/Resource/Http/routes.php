<?php

Route::group([
    'prefix' => 'test',
    'middleware' => ['web'],
    'namespace' => 'Modules\Resource\Http\Controllers'], function () {
     Route::get('/import', 'ImportController@import');
    Route::get('/import-from-drive', 'ImportController@importFromDrive');
    Route::get('/{id}/generate', 'ImportController@generatePlaylist');

});

// API
Route::group([
    'prefix' => 'api/v1',
    'middleware' => ['auth:api', 'cors', 'authorizedChannel'],
    'namespace' => 'Modules\Resource\Http\Controllers\Api'], function () {

    Route::resource('resource-types', 'ResourceTypeController');

    /**
     * Routes for Resource Folder
     */
    Route::get('resource-folders/tree-folders', 'ResourceFolderController@getTreeFolders')->name('resource-folders.tree_folders');
    Route::resource('resource-folders', 'ResourceFolderController');


    /**
     * Routes for Resource Tag
     */
    Route::resource('resource-tags', 'ResourceTagController');

    /**
     * Routes for Resource
     */
    Route::put('resources/{id}/trash', 'ResourceController@trash' )->name('resources.trash');
    Route::put('resources/{id}/restore', 'ResourceController@restore')->name('resources.restore');


    //Get preview data API from other URL
    Route::get('/resources/preview-import-api', 'ResourceController@previewImportAPI')->name('resources.preview-import-API');

    //Import data API
    Route::post('/resources/import-api', 'ResourceController@importAPI')->name('resources.import-API');

    //Upload resources
    Route::post('/resources/upload', 'ResourceController@upload')->name('resources.upload');

    //get authors
    Route::get('/resources/authors', 'ResourceController@authors')->name('resources.authors');

    //Dwonload resources
    Route::post('/resources/download', 'ResourceController@download')->name('resources.download');

    Route::resource('resources', 'ResourceController');




});

