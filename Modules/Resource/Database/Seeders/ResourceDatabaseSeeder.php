<?php

namespace Modules\Resource\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Resource\Repositories\Contracts\ResourceTypeRepositoryInterface;
use Modules\Resource\Services\Import\ImportFromAPIService;
use Modules\User\Repositories\Contracts\ChannelUsersRepositoryInterface;

class ResourceDatabaseSeeder extends Seeder
{

    private $repo;
    private $importService;

    public function __construct(ResourceTypeRepositoryInterface $repo, ImportFromAPIService $importFromAPIService)
    {
        $this->repo = $repo;
        $this->importService = $importFromAPIService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Model::unguard();

        $types = [
            [
                "name" => "Content",
                "channel_id" =>  1,
                'is_content'    => 1,
                'active'    => 1,
            ],
            [
                "name" => "Overlay",
                "channel_id" =>  1,
                'is_overlay'    => 1,
                'active'    => 1,
            ],
            [
                "name" => "Wipe",
                "channel_id" =>  1,
                'wipe'    => 1,
                'active'    => 1,
            ],



            [
                "name" => "Content",
                "channel_id" =>  2,
                'is_content'    => 1,
                'active'    => 1,
            ],
            [
                "name" => "Overlay",
                "channel_id" =>  2,
                'is_overlay'    => 1,
                'active'    => 1,
            ],
            [
                "name" => "Wipe",
                "channel_id" =>  2,
                'wipe'    => 1,
                'active'    => 1,
            ],
        ];
        foreach ($types as $type) {
            $this->repo->create($type);
        }


        // Import sample resources

        $channelUsersRepository = app(ChannelUsersRepositoryInterface::class);

        $channelUser = $channelUsersRepository
            ->findWhere(
                ['user_id' => 1, 'channel_id' => 1 ],
                ['channel_id', 'api_path', 'api_auth', 'player_directory']
            )
            ->first();

        /**
         * Sample resources data
         */
//        $this->importService->execute([
//            'channel_id'    => 1,
//            'page'  => 1,
//            'resourceTypeId'  => 1, // content type
//            'resourceFolderId'    => 2,
//            'api_path' => $channelUser->api_path,
//            'api_auth' => $channelUser->api_auth,
//        ]);
//        $this->importService->execute([
//            'channel_id'    => 1,
//            'page'  => 2,
//            'resourceTypeId'  => 2, // overlay type
//            'resourceFolderId'    => 2,
//            'api_path' => $channelUser->api_path,
//            'api_auth' => $channelUser->api_auth,
//        ]);
//
//
//        $this->importService->execute([
//            'channel_id'    => 2,
//            'page'  => 1,
//            'resourceTypeId'  => 1, // content type
//            'resourceFolderId'    => 4,
//            'api_path' => $channelUser->api_path,
//            'api_auth' => $channelUser->api_auth,
//        ]);
//        $this->importService->execute([
//            'channel_id'    => 2,
//            'page'  => 2,
//            'resourceTypeId'  => 2, // overlay type
//            'resourceFolderId'    => 4,
//            'api_path' => $channelUser->api_path,
//            'api_auth' => $channelUser->api_auth,
//        ]);
    }
}
