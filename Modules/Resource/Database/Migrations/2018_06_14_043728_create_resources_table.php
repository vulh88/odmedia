<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 255);
            $table->integer('duration')->default(0);
            $table->integer('duration_ms')->default(0);
            $table->dateTime('start_time')->nullable();
            $table->dateTime('end_time')->nullable();
            $table->time('time_not_before')->nullable();
            $table->time('time_not_after')->nullable();
            $table->integer('play_limit')->nullable();
            $table->tinyInteger('premiere')->default(0);
            $table->string('frequency', 255)->nullable();
            $table->text('desc_en')->nullable();
            $table->text('desc_nl')->nullable();
            $table->string('status', 10)->default('pending');

            $table->integer('api_id')->nullable();
            $table->tinyInteger('api_fetched')->default(0)->comment('whether checking resource is download or not');
            $table->dateTime('api_updated_at')->nullable();
            $table->dateTime('api_created_at')->nullable();
            $table->string('api_video_url', 255)->nullable();

            $table->integer('channel_id')->nullable();
            $table->integer('theme_id')->nullable();
            $table->integer('resource_type_id')->nullable();
            $table->integer('resource_folder_id')->nullable();
            $table->integer('user_id')->unsigned();
            $table->string('filename', 255)->nullable();
            $table->string('thumb_filename', 255)->nullable();
            $table->string('original_filename', 255)->nullable();
            $table->integer('original_id')->default(0);
            $table->tinyInteger('player_available')->default(0);
            $table->tinyInteger('is_video')->default(0);
            $table->smallInteger('type')->default(0)->comment('1: import | 2: upload | 3: placeholder');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resources');
    }
}
