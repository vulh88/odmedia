<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_types', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('channel_id');
            $table->string('name', 255);
            $table->string('class', 20)->nullable();
            $table->text('description')->nullable();
            $table->bigInteger('default_osd_id')->nullable()->comment("Default on screen display (default OSD): like a channel logo is always overlayed over the content. Standard OSD is a simple overlay graphic. (Integer). Actually, it is an overlay resource");
            $table->tinyInteger('osd_loop')->default(0)->nullable("is a movie with transparency, allowing more visual options. It contains Yes/No options: * No - Keep the last frame * Yes
");
            $table->tinyInteger('is_content')->default(0)->nullable();
            $table->tinyInteger('is_overlay')->default(0)->nullable();
            $table->tinyInteger('wipe')->default(0)->nullable();
            $table->tinyInteger('active')->default(1)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_types');
    }
}
