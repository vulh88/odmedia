<?php

namespace Modules\Resource\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\Resource\Repositories\Contracts\ResourceFolderRepositoryInterface;
use Modules\Resource\Repositories\Contracts\ResourceRepositoryInterface;
use Modules\Resource\Repositories\Contracts\ResourceTypeRepositoryInterface;
use Modules\Resource\Repositories\Eloquents\ResourceFolderRepository;
use Modules\Resource\Repositories\Eloquents\ResourceRepository;
use Modules\Resource\Repositories\Eloquents\ResourceTypeRepository;
use Modules\Resource\Repositories\Contracts\ResourceTagRepositoryInterface;
use Modules\Resource\Repositories\Eloquents\ResourceTagRepository;

class ResourceServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ResourceTypeRepositoryInterface::class, ResourceTypeRepository::class);
        $this->app->bind(ResourceFolderRepositoryInterface::class, ResourceFolderRepository::class);
        $this->app->bind(ResourceTagRepositoryInterface::class, ResourceTagRepository::class);
        $this->app->bind(ResourceRepositoryInterface::class, ResourceRepository::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('resource.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'resource'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/resource');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/resource';
        }, \Config::get('view.paths')), [$sourcePath]), 'resource');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/resource');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'resource');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'resource');
        }
    }

    /**
     * Register an additional directory of factories.
     * 
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
