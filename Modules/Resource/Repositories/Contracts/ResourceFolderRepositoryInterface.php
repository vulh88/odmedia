<?php

namespace Modules\Resource\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface ResourceFolderRepositoryInterface
 */
interface ResourceFolderRepositoryInterface extends RepositoryInterface
{
    /**
     * @param array $columns
     * @param array $params
     * @return mixed
     */
    public function search($columns = array(), $params = []);

}
