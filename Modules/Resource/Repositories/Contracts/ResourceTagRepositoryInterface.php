<?php

namespace Modules\Resource\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface ResourceTagRepositoryInterface
 */
interface ResourceTagRepositoryInterface extends RepositoryInterface
{
    /**
     * @param array $columns
     * @param array $params
     * @return mixed
     */
    public function search($columns = array(), $params = []);

    /**
     * Create multiple tags
     * @param $resourceId
     * @param $arrTags
     * @return array
     */
    public function createMultipleTags($resourceId, $arrTags);

    /**
     * Update tags
     * @param $newTags
     * @param int $resourceId
     * @return bool
     */
    public function saveMultipleTags($newTags, $resourceId);

    /**
     * Delete all tags for the resource
     * @param $resourceId
     * @return mixed
     */
    public function deleteAllTagsOfResource($resourceId);

}
