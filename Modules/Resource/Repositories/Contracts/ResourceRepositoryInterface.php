<?php

namespace Modules\Resource\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface ResourceRepositoryInterface
 */
interface ResourceRepositoryInterface extends RepositoryInterface
{
    /**
     * @param array $columns
     * @param array $params
     * @return mixed
     */
    public function search($columns = array(), $params = []);

    /**
     * Update Premiere By Channel ID
     * @param $channel_id
     * @return mixed
     */
    public function updatePremiereByChannel($channel_id);

    /**
     * Get resources by playlist
     * @param $playlistID
     * @param array $args
     * @param bool $sum
     * @return mixed
     */
    public function getResourcesForDownloadByPlaylist($playlistID, $args = array(), $sum = false);

    /**
     * Get Authors
     * @param $channel_id
     * @return mixed
     */
    public function getAuthors($channel_id);


    /**
     * Get resources by author
     * @param $userID
     * @return mixed
     */
    public function getResourcesByAuthor($userID);

    /**
     * Get premiere resources
     * @param bool $maxDuration - only get resources with duration is less than or equal max
     * @param bool $reverse
     * @return mixed
     */
    public function getPremiereResources($maxDuration = false, $reverse = false);

    /**
     * Get resources by a theme
     * @param bool $reverse
     * @return mixed
     */
    public function getResourcesByThem($reverse = false);

    /**
     * Get resources by a folder
     * @param $folderID
     * @param bool $reverse
     * @return mixed
     */
    public function getResourcesByFolder($folderID, $reverse = false);

    /**
     * Get resources by tag
     * @param bool $reverse
     * @return mixed
     */
    public function getResourcesByTag($reverse = false);

    /**
     * Clone resource by another resource id
     * @param $args
     * @return mixed
     */
    public function cloneResourcePlaceholder($args);
}
