<?php

namespace Modules\Resource\Repositories\Eloquents;

use Elidev\Repository\Traits\PaginationTrait;
use Modules\Resource\Entities\ResourceTag;
use Elidev\Repository\Eloquent\BaseRepository;
use Modules\Resource\Repositories\Contracts\ResourceTagRepositoryInterface;


/**
 * Class ResourceTagRepository
 */
class ResourceTagRepository extends BaseRepository implements ResourceTagRepositoryInterface
{
    use PaginationTrait;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ResourceTag::class;
    }

    /**
     * @param array $columns
     * @param array $request
     * @return array
     */
    public function search($columns = array(), $request = [])
    {
        $params = $request->all();
        $pagination = $this->extractPagination($params);
        $pagination['count'] = $pagination['page'] == -1 ? 0 : $pagination['count'];
        $query = $this->model()
            ::addSelect($columns)
            ->orderBy($pagination['sortColumn'],$pagination['sortOrder']);
        if (!empty($params['channel_id'])) {
            $query->join('resources', 'resources.id', '=', 'resource_tags.resource_id')
                    ->where('resources.channel_id', $params['channel_id']);
        }

        /**
         * Search items by keyword
         */
        if (!empty($params['keyword'])) {
            $query
                ->where('resource_tags.name', 'like', $params['keyword']);
        }

        /**
         * Search items by parent_id
         */
        if (!empty($params['resource_id'])) {
            $query
                ->where('resource_tags.resource_id', '=', $params['resource_id']);
        }

        return $query->paginate($pagination['count'], $columns);
    }

    /**
     * Create multiple tags
     * @param $resourceId
     * @param $arrTags
     * @return array
     */
    public function createMultipleTags($resourceId, $arrTags)
    {
        if (!$arrTags) {
            return [];
        }

        $createdArr = [];
        foreach ($arrTags as $tag) {
            $tagData = [
                'name'  => $tag,
                'resource_id'   => $resourceId
            ];
            if ($this->findWhere($tagData)->count()) {
                continue;
            }

            $createdArr[] = $tagData;
            $this->create($tagData);
        }

        return $createdArr;
    }

    /**
     * Update tags
     * @param $newTags
     * @param int $resourceId
     * @return bool
     */
    public function saveMultipleTags($newTags, $resourceId)
    {
        $success = FALSE;
        $currentTags = [];
        $resultTags = $this->findWhere(['resource_id' => $resourceId]);
        foreach ($resultTags as $tag) {
            $currentTags[$tag->id] = $tag->name;
        }

        $matchedEls = array_diff($newTags, $currentTags);
        $notMatchEls = array_diff($currentTags, $newTags);
        $newRecordsTag = [];
        foreach ( $matchedEls as $key => $tag ) {
            $newRecordsTag[] = [
                'name' => $tag,
                'resource_id' => $resourceId,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ];
        }
        if ($newRecordsTag) {
            $success = $this->model()::insert($newRecordsTag);
        }

        if ($deleteTagIds = array_keys($notMatchEls)) {
            $this->model()::whereIn('id', $deleteTagIds)->delete();
        }

        return $success;
    }

    /**
     * Delete all tags for the resource
     * @param $resourceId
     * @return mixed
     */
    public function deleteAllTagsOfResource($resourceId)
    {
        return $this->model()::where('resource_id', $resourceId)->delete();
    }
}
