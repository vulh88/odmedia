<?php

namespace Modules\Resource\Repositories\Eloquents;

use Modules\Resource\Entities\Resource;
use Elidev\Repository\Eloquent\BaseRepository;
use Modules\Resource\Repositories\Contracts\ResourceRepositoryInterface;
use Elidev\Repository\Traits\PaginationTrait;
use Illuminate\Support\Facades\DB;

/**
 * Class ResourceRepository
 */
class ResourceRepository extends BaseRepository implements ResourceRepositoryInterface
{
    use PaginationTrait;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Resource::class;
    }

    /**
     * @param array $columns
     * @param array $params
     * @return array
     */
    public function search($columns = array(), $params = [])
    {
        $pagination = $this->extractPagination($params);
        $pagination['count'] = $pagination['page'] == -1 ? 0 : $pagination['count'];
        $query = $this->model()
            ::addSelect($columns)
            ->orderBy($pagination['sortColumn'],$pagination['sortOrder']);

        // Only overlay type resources
        if (!empty($params['only_overlay_resources'])) {
            $query->join('resource_types', 'resource_types.id', '=', 'resources.resource_type_id')
                ->where('resource_types.is_overlay', 1);
        }

        // Only content type resources
        if (!empty($params['only_content_resources'])) {
            $query->join('resource_types', 'resource_types.id', '=', 'resources.resource_type_id')
                ->where('resource_types.is_content', 1);
        }

        // Only wipe type resources
        if (!empty($params['only_wipe_resources'])) {
            $query->join('resource_types', 'resource_types.id', '=', 'resources.resource_type_id')
                ->where('resource_types.wipe', 1);
        }

        /**
         * Handle where for request params
         */
        $this->whereConditions($params, $query);

        /**
         * Search trashed items by trash param
         */
        if (!empty($params['trash']) && $params['trash'] == 1) {
            $query->onlyTrashed();
        }

        return $query->paginate($pagination['count'], $columns);
    }

    /**
     * Handle where for request params
     * @param array $params
     * @param $query
     */
    private function whereConditions($params = [], &$query)
    {
        $arrConditions = [
            'keyword',
            'resource_type_id',
            'resource_folder_id',
            'channel_id',
            'theme_id',
            'user_id',
            'trash',
        ];
        foreach ($params as $k => $v) {
            if ( in_array($k, $arrConditions) ) {
                switch ($k) {
                    case 'keyword':
                        /**
                         * Search items by keyword
                         */
                        $query->where('resources.title', 'like', '%' . $v .'%');
                        break;
                    case 'author':
                        /**
                         * Search items by keyword
                         */
                        $query->where($k, 'like', '%' . $v .'%');
                        break;
                    case 'trash':
                        if ($v == 1) {
                            /**
                             * Search trashed items by trash param
                             */
                            $query->onlyTrashed();
                        }
                        break;
                    default:
                        $query->where('resources.' . $k, '=', $v);
                        break;
                }
            }
        }
    }

    /**
     * Update Premiere By Channel ID
     * @param $channel_id
     * @return mixed
     */
    function updatePremiereByChannel( $channel_id )
    {
        return $this->model()::where([ 'channel_id' => $channel_id ])
            ->update([
                'premiere' => 0
            ]);
    }

    /**
     * Get resources by playlist
     * @param $playlistID
     * @param array $args
     * @param bool $sum
     * @return mixed
     */
    public function getResourcesForDownloadByPlaylist($playlistID, $args = array(), $sum = false)
    {
        $columns = [
            'resources.id',
            'resources.filename',
            'resources.title',
            'resources.api_video_url',
            'resources.api_fetched',
            'resources.duration',
            'resources.duration_ms'
        ];

        $pagination = $this->extractPagination($args);
        $select = implode(',', $columns);


        $query = DB::table('resources')
            ->select(DB::raw($select))
            ->join('playlist_items', 'playlist_items.resource_id', '=', 'resources.id')
            ->where('playlist_items.playlist_id', $playlistID);

        if (!empty($args['only_not_downloaded_yet'])) {
            $query->where('api_fetched', 0);
        }
        else {
            $query->groupBy('resources.id')
                ->orderBy($pagination['sortColumn'], $pagination['sortOrder']);
        }

        if ($sum) {
            return $query->count();
        }

        return $query->get();
    }

    /**
     * Get Authors
     * @param $channel_id
     * @return mixed
     */
    public function getAuthors($channel_id) {
        $query = DB::table('resources')
                ->select(DB::raw('CONCAT(users.first_name, " ", users.last_name) AS full_name'), "users.id")
                ->join('users', 'users.id', '=', 'resources.user_id')
                ->where('resources.channel_id', $channel_id)
                ->distinct('resources.user_id')
                ->get();
        return $query;
    }

    private function _generateDurationAndDateTimeWhere($query, $maxDuration = false) {
        if ($maxDuration) {
            $query->where('duration', '<=', $maxDuration);
        }
        return $query;
    }

    /**
     * Get resources by author
     * @param $userID
     * @param bool $maxDuration - only get resources with duration is less than or equal max
     * @param bool $reverse
     * @return mixed
     */
    public function getResourcesByAuthor($userID, $maxDuration = false, $reverse = false)
    {
        $query = DB::table('resources')
            ->select(DB::raw('resources.*'))
            ->join('users', 'users.id', '=', 'resources.user_id');

        $query = $query->_generateDurationAndDateTimeWhere();

        if ($reverse) {
            $query->where('users.id', '<>', $userID);
        }
        else {
            $query->where('user_id', $userID);
        }

        $query->limit(10);

        return $query->get();
    }

    /**
     * Get premiere resources
     * @param bool $maxDuration - only get resources with duration is less than or equal max
     * @param bool $reverse
     * @return mixed
     */
    public function getPremiereResources($maxDuration = false, $reverse = false)
    {
        $query = DB::table('resources')
            ->select(DB::raw('resources.*'));

        if ($maxDuration) {
            $query->where('duration', '<=', $maxDuration);
        }

        if ($reverse) {
            $query->where('premiere', 0);
        }
        else {
            $query->where('premiere', 1);
        }

        $query->limit(10);

        return $query->get();
    }

    /**
     * Get resources by a theme
     * @param bool $reverse
     * @return mixed
     */
    public function getResourcesByThem($reverse = false)
    {
        // TODO: Implement getResourcesByThem() method.
    }

    /**
     * Get resources by a folder
     * @param $folderID
     * @param bool $reverse
     * @return mixed
     */
    public function getResourcesByFolder($folderID, $reverse = false)
    {
        // TODO: Implement getRandomResourcesByFolder() method.
    }

    /**
     * Get resources by tag
     * @param bool $reverse
     * @return mixed
     */
    public function getResourcesByTag($reverse = false)
    {
        // TODO: Implement getResourcesByTag() method.
    }

    /**
     * Clone resource placeholder
     * @param $args
     * @return mixed
     */
    public function cloneResourcePlaceholder($args)
    {
        $resource = $this->find($args['resource_id']);
        $resourceData = [
            "user_id" => $args['user_id'],
            "channel_id" => $args['channel_id'],
            "resource_folder_id" => $resource->resource_folder_id,
            "resource_type_id" => $resource->resource_type_id,
            "duration" => $args['duration'],
            "title" => $args['title'],
            "original_id" => $resource->id,
            "filename" => $resource->filename,
            "thumb_filename" => $resource->thumb_filename,
            "original_filename" => $resource->original_filename,
            "is_video" => $resource->is_video,
            "type" => 3
        ];
        $id = $this->create($resourceData)->id;
        return $id > 0 ? $this->with("resourceType")->find($id) : [];
    }
}
