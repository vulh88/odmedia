<?php

namespace Modules\Resource\Repositories\Eloquents;

use Modules\Resource\Entities\ResourceType;
use Elidev\Repository\Eloquent\BaseRepository;
use Modules\Resource\Repositories\Contracts\ResourceTypeRepositoryInterface;
use Elidev\Repository\Traits\PaginationTrait;


/**
 * Class ResourceTypeRepository
 */
class ResourceTypeRepository extends BaseRepository implements ResourceTypeRepositoryInterface
{
    use PaginationTrait;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ResourceType::class;
    }

    /**
     * @param array $columns
     * @param array $request
     * @return array
     */
    public function search($columns = array(), $request = [])
    {
        $params = $request->all();
        $pagination = $this->extractPagination($params);
        $pagination['count'] = $pagination['page'] == -1 ? 0 : $pagination['count'];
        $query = $this->model()
            ::addSelect($columns)
            ->orderBy($pagination['sortColumn'],$pagination['sortOrder']);

        /**
         * Handle where for request params
         */
        $this->whereConditions($params, $query);

        return $query->paginate($pagination['count'], $columns);
    }

    /**
     * Handle where for request params
     * @param array $params
     * @param $query
     */
    private function whereConditions($params = [], &$query)
    {
        $arrConditions = [
            'keyword',
            'is_content',
            'is_overlay',
            'channel_id',
            'active',
        ];
        foreach ($params as $k => $v) {
            if ( in_array($k, $arrConditions) ) {
                switch ($k) {
                    case 'keyword':
                        /**
                         * Search items by keyword
                         */
                        $query->where('name', 'like', '%' . $v .'%');
                        break;
                    default:
                        $query->where($k, '=', $v);
                        break;
                }
            }
        }
    }
}
