php artisan key:generate;
php artisan migrate;
php artisan db:seed;
php artisan migrate:refresh --seed;
php artisan module:migrate;
php artisan module:seed;
php artisan passport:install --force;
php artisan l5-swagger:generate;
