/*
 *
 * LanguageProvider actions
 *
 */

import { CHANGE_LOCALE } from './actions';

export function changeLocale(languageLocale) {
  return {
    type: CHANGE_LOCALE,
    locale: languageLocale
  };
}
