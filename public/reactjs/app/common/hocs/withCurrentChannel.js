import {createStructuredSelector} from "reselect";
import {makeSelectChannel} from '../../store/app/selectors';
import {connect} from 'react-redux';

function withCurrentChannel(options = {}) {
  return ComponentClass => {
    const mapStateToProps = createStructuredSelector({
      currentChannel: makeSelectChannel(),
    });
    return connect(mapStateToProps)(ComponentClass);
  };
}

export default withCurrentChannel;

