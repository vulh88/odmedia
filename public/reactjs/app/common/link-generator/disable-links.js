import routePaths from 'settings/route-paths';

/**
 * Set link should be remove if the current channel has disabled the function enable timeline
 * @param currentChanel
 * @param theLink
 * @returns {boolean}
 */
export const linksHidden = (currentChanel, theLink) => {
    let linkHidden = null;
    if (currentChanel.channel.enable_timeline === 0) {
      linkHidden = routePaths.playlists.overview;
    }
    return linkHidden !== theLink;
};

/**
 * Remove button generate in playlist page if the current channel has disabled the function enable timeline
 * @param currentChanel
 * @param actionEls
 * @returns {*}
 */
export const removeActionPlaylistByChannel = (currentChanel, actionEls) => {
  let actions = actionEls;
  if (currentChanel.channel.enable_timeline === 0) {
    actions.splice(1, 1);
  }
  return actions;
};

/**
 * if the current channel has disabled the function enable timeline
 * @param currentChanel
 * @returns {boolean}
 */
export const checkEnableTimeline = (currentChanel) => {
  return currentChanel.channel.enable_timeline === 0;
};