import locationHelper from '../../helpers/location';
import routePaths from '../../settings/route-paths';

const {getLink} = locationHelper;

const resources = {
  single: (e) => getLink(routePaths.resources.single, {id: e.resource_id})
};

const playlists = {
  scheduling: (e) => getLink(routePaths.playlists.scheduling, {id: e.id})
};

const linkGenerator = {
  resources,
  playlists
};

export default  linkGenerator;
