import channelsApiService from '../services/api/channels';
import themesApiService from '../services/api/themes';
import resourceTypesApiService from '../services/api/resource-types';
import resourceFoldersApiService from 'services/api/resource-folders';

export async function preloadChannels(schema) {
  const {data} = await channelsApiService.getList({
    page: -1,
  });

  schema.channel_id = {
    ...schema.channel_id,
    data: data.map((e) => ({
      value: e.id,
      name: e.name
    }))
  };
  return schema;
}

export async function preloadThemes(schema, params) {
  const {data} = await themesApiService.getList({
    page: -1,
    active: 1,
    ...params
  });

  const schemaData = schema.theme_id.data.concat(data.map((e) => ({
    value: e.id,
    name: e.name
  })));

  schema.theme_id = {
    ...schema.theme_id,
    data: schemaData
  };
  return schema;
}

export async function preloadResourceType(schema, params) {
  const {data} = await resourceTypesApiService.getList({
    page: -1,
    active: 1,
    ...params
  });

  const schemaData = schema.resource_type_id.data.concat(data.map((e) => ({
    value: e.id,
    name: e.name
  })));

  schema.resource_type_id = {
    ...schema.resource_type_id,
    data: schemaData
  };
  return schema;
}

export async function preloadResourceFolder(schema, params) {
  const {data} = await resourceFoldersApiService.getList({
    page: -1,
    ...params
  });
  const schemaData = schema.resource_folder_id.data.concat(data.map((e) => ({
    value: e.id,
    name: e.name
  })));
  schema.resource_folder_id = {
    ...schema.resource_folder_id,
    data: schemaData
  };
  return schema;
}