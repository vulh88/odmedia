import {schema} from '../helpers/form';

export const default_osd_id = {
  label: 'Default OSD',
  type: 'select',
  data: [
    {
      name: '--Select Default OSD--',
      value: ''
    }
  ],
  domProps: {
    defaultValue: ''
  }
};


export const radioYesNo = {
  type: 'radio',
  validate: schema
      .number()
      .oneOf([1, 0])
      .required(),
  inline: true,
  data: [
    {
      name: 'Yes',
      value: 1
    },
    {
      name: 'No',
      value: 0
    }
  ]
};

