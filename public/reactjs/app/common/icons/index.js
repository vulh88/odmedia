const iconsReference = {
  start: 'fa fa-play-circle',
  stop: 'fa fa-stop-circle',
  playDuration: 'fa fa-clock-o',
  videoFile: 'fa fa-file-video-o'
};

export default iconsReference;
