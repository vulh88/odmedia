export default {
  'color-black': '#333',
  'color-white': '#fff',
  'color-white-grey': '#eee',
  'color-green': '#32c5d2',
  'background-black': '#364150',
  'font-dark': '#2f353b'
};
