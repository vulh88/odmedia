import React from 'react';
import BodyClass from '../../components/BodyClass';

export default function NoNetworkPage() {
  return (
      <div className="row mt-5">
        <BodyClass classList="page-500-full-page"/>
        <div className="col-md-12 page-500 mt-5">
          <div className="number font-red"> 520</div>
          <div className="details">
            <h3>Oops! Connection's lost.</h3>
            <p>This perhaps caused by network issue or server was down. <br/>Please try again in a while!</p>
            <button onClick={() => window.location.reload()} className="btn btn-primary"><i/> Try again</button>
          </div>
        </div>
      </div>
  );
};
