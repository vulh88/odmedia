import React from 'react';
import {Helmet} from 'react-helmet';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {injectIntl, intlShape} from 'react-intl';
import {withRouter} from 'react-router-dom';
import {initApp} from 'store/app/actions';
import {makeSelectLoading, makeSelectReady, makeSelectIsLoggedIn} from 'store/app/selectors';
import {makeSelectIsOpenSidebar} from 'store/display/selectors';
import {ThemeApp} from '../../js/app';
import LoadingIndicator from '../../components/LoadingIndicator';
import MainRouter from '../../router';
import injectSaga from '../../store/utils/injectSaga';
import {appSaga} from '../../store/app/saga';
import {makeSelectChannel, makeSelectIsInitFailByNetwork} from '../../store/app/selectors';
import NoNetworkPage from '../NoNetworkPage/Loadable';
import {AppContext} from './context';
import {isDevelopment} from '../../helpers/utils';

const mapProps = createStructuredSelector({
  loading: makeSelectLoading(),
  currentChannel: makeSelectChannel(),
  isReady: makeSelectReady(),
  isLoggedIn: makeSelectIsLoggedIn(),
  isOpenSidebar: makeSelectIsOpenSidebar(),
  isNoNetwork: makeSelectIsInitFailByNetwork()
});

@injectIntl
@withRouter
@connect(mapProps)
@injectSaga({key: 'app', saga: appSaga.run})
export class App extends React.PureComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    intl: intlShape.isRequired
  };

  componentDidMount() {
    ThemeApp.init();
    this.initApp();
    document.body.classList.add('fontloaded');
  }

  componentWillReceiveProps(nextProps) {
    const {location} = nextProps;
    const pathname = location.pathname.split('/')[1];
    document.body.classList.add(`${pathname || 'dashboard'}`);
  }

  initApp = () => {
    const {dispatch, isLoggedIn, isReady} = this.props;

    //for quickly hot reload in development mode
    if (isReady && isDevelopment()) {
      return;
    }

    if (isLoggedIn) {
      dispatch(initApp());
    }
  };

  renderPrivateApp() {
    const {isReady, isNoNetwork, currentChannel} = this.props;
    if (isNoNetwork) {
      return <NoNetworkPage/>;
    }
    return isReady ? <AppContext.Provider value={{currentChannel: currentChannel}} ><MainRouter isLogged={true}/></AppContext.Provider> : <LoadingIndicator type="default"/>;
  }

  render() {
    const {isLoggedIn} = this.props;
    return (
        <div className={this.getClasses()}>
          <Helmet titleTemplate="%s - ODarrange" defaultTitle="ODarrange">
            <meta name="description" content="ODarrange"/>
          </Helmet>
          {isLoggedIn ? this.renderPrivateApp() : <MainRouter isLogged={false}/>}
        </div>
    );
  }

  getClasses() {
    const {isOpenSidebar} = this.props;
    return `page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white ${isOpenSidebar ? '' : 'page-sidebar-closed'}`;
  }

}

export default App;
