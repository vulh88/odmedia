import $ from 'jquery';
import 'jquery-backstretch';
import Bg1 from '../images/1.jpg';
import Bg2 from '../images/2.jpg';
import Bg3 from '../images/3.jpg';
import Bg4 from '../images/4.jpg';

const LoginJS = (function() {
  const initBackStretch = () => {
    $('body').backstretch([Bg1, Bg2, Bg3, Bg4], {
      fade: 1000,
      duration: 8000
    });
  };

  const destroyBackStretch = () => {
    $.backstretch('destroy');
  };

  return {
    initBackStretch() {
      initBackStretch();
    },

    destroyBackStretch() {
      destroyBackStretch();
    }
  };
})();

export default LoginJS;
