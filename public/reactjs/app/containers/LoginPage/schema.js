import {schema} from '../../helpers/form';

const loginFormSchema = {
  email: {
    label: 'Email',
    type: 'email',
    validate: schema.string().email().required(),
    domProps: {
      autoComplete: 'off'
    }
  },
  password: {
    label: 'Password',
    type: 'password',
    validate: schema.string().min(6).required()
  },
  isRememberUser: {
    label: false,
    type: 'checkbox',
    validate: schema.number().transform(v => v ? 1 : 0),
    data: [
      {
        name: 'Remember',
        value: 1
      }
    ]
  }
};

export default loginFormSchema;
