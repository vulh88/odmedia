import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import LoginForm from './LoginForm';

import LoginJS from './assets/js/index';
import ImageLogo from './assets/images/logo-big.png';

class LoginPage extends React.PureComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired
  };

  componentDidMount() {
    LoginJS.initBackStretch();
    document.body.classList.add('login');
  }

  componentWillUnmount() {
    LoginJS.destroyBackStretch();
    document.body.classList.remove('login');
  }

  render() {
    return (
      <div>
        <Helmet>
          <title>Login Page</title>
          <meta name="description" content="Login page - ODarrange" />
        </Helmet>
        <div className="logo">
          <a>
            <img src={ImageLogo} alt="" />
          </a>
        </div>
        <LoginForm />
        <div className="copyright"> 2018 © ODArrange. </div>
      </div>
    );
  }
}
const mapStateToProps = createStructuredSelector({});

export default connect(
  mapStateToProps,
  null
)(LoginPage);
