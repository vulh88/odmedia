import React from 'react';
import PropTypes from 'prop-types';
import {FormattedMessage} from 'react-intl';
import {connect} from 'react-redux';
import ButtonLoading from 'components/ButtonLoading';
import messages from './messages';
import {SmartForm} from '../../helpers/form';
import loginFormSchema from './schema';
import HaveApiComponent from '../../components/BaseComponents/HaveApiComponent';
import AuthApiService from '../../services/api/auth';
import {triggerLoginSuccess} from '../../store/app/actions';

@connect()
class LoginForm extends HaveApiComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
  };

  onSubmitForm = data => {
    this.callApi(AuthApiService.login(data), loggedData => {
          if (loggedData) {
            this.props.dispatch(triggerLoginSuccess(loggedData))
          }
        },
        null,
        {autoShowError: false}
    );
  };

  renderField(field, icon) {
    return (
        <div
            key={field.config.name}
            className={`form-group ${field.errors ? 'has-error' : ''}`}
        >
          <label className="control-label visible-ie8 visible-ie9">
            {field.config.label}
          </label>
          <div className="input-icon">
            <i className={icon}/>
            {field.renderInput()}
            {field.renderErrors()}
          </div>
        </div>
    );
  }

  render() {
    return (
        <div className="content">
          <div className="login-form">
            <h3 className="form-title">
              <FormattedMessage {...messages.title} />
            </h3>
            <SmartForm
                className="vertical-form"
                formRowSeparated={false}
                schema={loginFormSchema}
                onSubmit={this.onSubmitForm}
                renderInner={({fields: {email, password, isRememberUser}}) => (
                    <div>
                      {this.renderField(email, 'fa fa-user')}
                      {this.renderField(password, 'fa fa-lock')}
                      <div key="div" className="form-actions clearfix">
                        <div className="pull-left">
                          {isRememberUser.renderInput()}
                        </div>
                        <ButtonLoading
                            className="btn green pull-right mt-ladda-btn"
                            loading={this.apiState.loading}
                        >
                          Login
                        </ButtonLoading>
                      </div>
                    </div>
                )}
            />
          </div>

        </div>
    );
  }
}


export default LoginForm;
