import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'LoginPage.title',
    defaultMessage: 'Login to your account'
  }
});
