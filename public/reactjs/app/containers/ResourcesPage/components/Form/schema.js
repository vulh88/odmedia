import {schema, commonSchema} from 'helpers/form';
import {formatDuration} from '../../../../helpers/utils';
import {radioYesNo} from '../../../../common/schema-fields';
import React from 'react';

/**
 * Schema of resourceForm. See package yup documentation for schema method
 */
const resourceFormSchema = {
  id: {
    label: 'ID',
    validate: commonSchema.integer(),
    type: 'text',
    className: 'hidden'
  },
  title: {
    label: 'Title',
    type: 'text',
    validate: schema.string().required()
  },
  author: {
    label: 'Author',
    type: 'viewOnly',
  },
  duration: {
    label: 'Duration',
    type: 'viewOnly',
    transform: value => <span className="label label-info">{formatDuration(value)}</span>
  },
  resource_type_id: {
    label: 'Resource type',
    type: 'select',
    validate: commonSchema.integer().required(),
    data: [
      {
        name: '--Select resource type--',
        value: null
      }
    ]
  },
  theme_id: {
    label: 'Theme',
    type: 'select',
    validate: commonSchema.integer().required(),
    data: [
      {
        name: '--Select theme--',
        value: 'null'
      }
    ]
  },
  tags: {
    label: 'Tags',
    type: 'tags',
   // validate: schema.array().required()
  },
  start_time: {
    label: 'Start date',
    type: 'datetimepicker',
    validate: schema.string(),
    min: Date.now(),
    disableTime: true,
    format: "YYYY-MM-DD"
  },
  end_time: {
    label: 'End date',
    type: 'datetimepicker',
    validate: schema.string(),
    disableTime: true,
    format: "YYYY-MM-DD"
  },
  time_not_before: {
    label: 'Do not start before',
    type: 'datetimepicker',
    validate: schema.string(),
    disableDate: true,
    format: "HH:mm"
  },
  time_not_after: {
    label: 'Do not start after',
    type: 'datetimepicker',
    validate: schema.string(),
    disableDate: true,
    format: "HH:mm"
  },
  frequency: {
    label: 'Frequency',
    type: 'select',
    data: [
      {
        name: 'High',
        value: 'high'
      },
      {
        name: 'Medium',
        value: 'medium'
      },
      {
        name: 'Low',
        value: 'low'
      }
    ]
  },
  desc_en: {
    label: 'English description',
    type: 'textarea',
    validate: schema.string().max(3000).required(),
    domProps: {
      rows: 5
    }
  },
  desc_nl: {
    label: 'Dutch description',
    type: 'textarea',
    validate: schema.string().max(3000).required(),
    domProps: {
      rows: 5
    }
  },
  premiere: {
    label: 'Premiere',
    ...radioYesNo
  },
  player_available: {
    label: 'Player available',
    type: 'viewOnly',
    noBorder: true,
    transform: v => v ? <span className="label label-success">Yes</span> : <span className="label label-default">No</span>,
  },
 /* api_video_url: {
    label: 'The video url used to download',
    type: 'text'
  },*/
  created_at: {
    label: 'Created at',
    type: 'viewOnly'
  },
  updated_at: {
    label: 'Updated at',
    type: 'viewOnly'
  }
};

export default resourceFormSchema;
