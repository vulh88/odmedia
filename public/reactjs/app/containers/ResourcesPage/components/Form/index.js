import React from 'react';
import resourcesApiService from '../../../../services/api/resources';
import withAsync from '../../../../helpers/react/withAsync/withAsync';
import {withRouter} from 'react-router';
import SmartForm from '../../../../helpers/form/SmartForm';
import resourceFormSchema from './schema';
import {preloadResourceType, preloadThemes} from '../../../../common/shema-preloads';
import {makeSelectChannel} from '../../../../store/app/selectors';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import HaveApiComponent from '../../../../components/BaseComponents/HaveApiComponent';
import toast from '../../../../helpers/user-interface/toast';
import withReload from '../../../../helpers/react/withReload/withReload';
import {stripFields} from '../../../../helpers/utils';
import routePaths from '../../../../settings/route-paths';

@withRouter
@connect(createStructuredSelector({
  channel: makeSelectChannel()
}))
@withReload()
@withAsync()
class ResourceForm extends HaveApiComponent {
  static async preload(props) {
    const schema = {...resourceFormSchema};
    const [updateData] = await Promise.all([
      resourcesApiService.getOne(props.match.params.id),
      preloadThemes(schema, {channel_id: props.channel.id}),
      preloadResourceType(schema, {channel_id: props.channel.id})
    ]);
    return {
      schema,
      updateData
    }
  }

  handleSubmit = data => {
    const {history} = this.props;
    //stripFields(data, ['start_time', 'end_time']);
    let tags = [];
    if ( Array.isArray(data.tags) ) {
      tags = data.tags;
    } else {
      if (data.tags) {
        tags.push(data.tags);
      }
    }
    data.tags = tags;
    this.callApi(resourcesApiService.update(data.id, data), (data) => {
      toast.success('Update resource successfully');
      history.push(routePaths.resources.list);
    });
  };

  render() {
    const {preloadData} = this.props;
    const {loading} = this.apiState;
    const {updateData, schema} = preloadData;
    return (
      <div>
        <div className="row">
          <div className="col-md-8">
            <h1 className="page-title">
              Update Resource
            </h1>
          </div>
          <div className="col-md-12">
            <SmartForm
                className="portlet light"
                defaultData={updateData}
                loading={loading}
                textButton="Save"
                schema={schema}
                toastError={true}
                onSubmit={this.handleSubmit}/>
          </div>
          </div>
      </div>
    );
  }
}

export default ResourceForm;
