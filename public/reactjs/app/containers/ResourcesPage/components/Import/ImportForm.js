import React from 'react';
import ManageTable from '../../../../components/ManageTable';
import ButtonLoading from '../../../../components/ButtonLoading';
import apiService from "../../../../services/api/resources";
import HaveApiComponent from "../../../../components/BaseComponents/HaveApiComponent";
import {createStructuredSelector} from "reselect";
import {connect} from "react-redux";
import {makeSelectChannelSetting} from "store/app/selectors";
import * as actions from 'store/app/actions';
import resourceImportFormSchema, {defaultData} from './importSchema';
import {SmartForm} from '../../../../helpers/form';
import toast from '../../../../helpers/user-interface/toast';
import {formatDuration} from '../../../../helpers/utils';
import {schema, commonSchema} from 'helpers/form';
import userApiService from 'services/api/users';
import {preloadResourceType, preloadResourceFolder} from '../../../../common/shema-preloads';

@connect(
    createStructuredSelector({
      channelSetting: makeSelectChannelSetting()
    })
)
class ImportForm extends HaveApiComponent {

  schema = {...resourceImportFormSchema};
  defaultData = {...defaultData};

  constructor(props) {
    super(props);

    this.apiState.data = [];
    this.apiState.total = 30;

    this.state = {
      page: 1,
      buttonPreviewLoading: false,
      buttonImportLoading: false,
      buttonDisable: false,
      resourceFolders: [],
      resourceTypes: []
    };
  }

  componentWillMount() {
    this.rebuildSchema();
  }

  componentDidMount() {
    this.props.dispatch(actions.refreshChannelSetting());
  }

  componentDidUpdate(prevProps) {
    if (prevProps.channelSetting.channel_id !== this.props.channelSetting.channel_id) {
      this.apiState.data = [];
      this.rebuildSchema();
    }
    return true;
  }

  async rebuildSchema() {
    const {channelSetting} = this.props;
    this.defaultData.api_auth = channelSetting.api_auth;
    this.defaultData.api_path = channelSetting.api_path;
    this.defaultData.author = await this.getUser();
    await this.handleResourcesAPI();
    this.forceUpdate();
    this.form.applyDefaultData();
  }

  async getUser(){
    const {channelSetting} = this.props;
    const userData = await userApiService.getOne(channelSetting.user_id);
    if (userData){
      return userData.first_name + ' ' + userData.last_name;
    }

    return '';
  }

  //handle Button Loading
  funcButtonLoading(previewLoading, importLoading, disable) {
    this.setState({
      buttonPreviewLoading : previewLoading,
      buttonImportLoading :  importLoading,
      buttonDisable : disable
    });
  };

  // Handle click button preview
  handlePreview = e => {
    e.preventDefault();
    let page = this.state.page;

    //handle Button Loading
    this.funcButtonLoading(true, false, true);

    //call API
    this.handlePreviewData(page);
  };

  // Call API get data
  handlePreviewData(page, count = this.apiState.total) {
    let channel_id = this.props.channelSetting.channel_id;
    this.callApi(apiService.previewImportData({
          channel_id,
          page,
          count
        }),
        ({data, paging}) => {
          this.apiState.data = data;
          this.apiState.paging = paging;

          //handle Button Loading
          this.funcButtonLoading(false, false, false);

          //handle show error when call API
          this.handleShowErrorAPI(data, 'preview');
        }
    );
  };

  /**
   * Call API import data
   * @param data
   */
  handleImportData = (data) => {

    //handle Button Loading
    this.funcButtonLoading(false, true, true);
    let body = {
      channel_id: this.props.channelSetting.channel_id,
      page: this.state.page,
      resourceFolderId: data.resource_folder_id,
      resourceTypeId: data.resource_type_id
    };
    this.callApi(apiService.importData(body),
        ({data}) => {

          //handle Button Loading
          this.funcButtonLoading(false, false, false);

          //handle show error when call API
          this.handleShowErrorAPI(data, 'import');
        }
    );
  };

  formChangeData = (data) => {
    this.state.page = data.page;
    this.setState(this.state);
  };


  handleShowErrorAPI(data, type) {
    if (data.length < 1) {
      toast.error("Please recheck information in Scheduler settings", "Error!");
    } else {
      if (type === 'import') {
        toast.success("Import " + this.apiState.total + " items success", "Success!");
      }
    }
  };

  async handleResourcesAPI() {
    const {channel_id} = this.props.channelSetting;
    const params = {
      channel_id: channel_id
    };
    this.schema.resource_folder_id.data = [this.schema.resource_folder_id.data[0]];
    this.schema.resource_type_id.data = [this.schema.resource_type_id.data[0]];
    /**
     * Get resource folders, resource types, themes data from API
     * @type {*}
     */
    const [apiData] = await Promise.all([
      preloadResourceFolder(this.schema, params),
      preloadResourceType(this.schema, {...params, is_content: 1})
    ]);

    this.schema = {...apiData};
  }

  render() {
    const schemaData = this.schema;
    const dataList = this.apiState.data.length > 0 ? <ImportList apiState={this.apiState} /> : '';
    const importAll = this.apiState.data.length > 0
        ? <ButtonLoading
            type="submit"
            className="pull-right btn btn-primary"
            loading={this.state.buttonImportLoading}
            disabled={this.state.buttonDisable}
        >
          Import All ( {this.apiState.total} )
        </ButtonLoading>
        : null;

    return (
        <div>
          <div className="portlet light new-import-form">
            <SmartForm
                ref={e => (this.form = e)}
                loading={this.apiState.loading}
                defaultData={this.defaultData}
                schema={schemaData}
                isShowSubmit={false}
                onSubmit={this.handleImportData}
                onChange={this.formChangeData}
            >
              <div className="smart-form__actions">
                <ButtonLoading
                    type="submit"
                    className="pull-left btn green"
                    onClick={this.handlePreview}
                    loading={this.state.buttonPreviewLoading}
                    disabled={this.state.buttonDisable}
                >
                  Preview
                </ButtonLoading>

                {importAll}

              </div>
            </SmartForm>
          </div>


          {/* Locate : Load html list data */}
          {dataList}

        </div>
    );
  }
}


const ImportList = props => ({
  render() {
    const {data, loading} = props.apiState;
    const columns = [
      {
        name: 'API ID',
        value: e => e.api_id,
        sort: true
      },
      {
        name: 'Title',
        value: e => e.title,
        sort: true
      },
      {
        name: 'Duration',
        value: e => e.duration && formatDuration(e.duration),
        sort: true
      },
      {
        name: 'Status',
        value: e => e.status,
        sort: true
      },
      {
        name: 'Imported',
        value: e => e.imported
            ? ( <i className="fa fa-check-circle green" /> )
            : ( <i className="fa fa-ban red" /> ),
        sort: true,
        style: { justifyContent: 'center', textAlign: 'center', width: '50' }
      },
      {
        name: 'Start Time',
        value: e => e.start_time,
        sort: true
      },
      {
        name: 'End Time',
        value: e => e.end_time,
        sort: true
      }
    ];

    return (
        <div>
          <div className="row">
            <ManageTable
                loading={loading}
                data={data}
                paging={{}}
                columns={columns}
                showActions={false}
                tools={false}
            />
          </div>
        </div>
    );
  }

});

export default ImportForm;