import {schema, commonSchema} from 'helpers/form';

const radioYesNo = {
  type: 'radio',
  validate: schema.number().oneOf([0, 1]).required(),
  inline: true,
  data: [
    {
      name: 'Current user',
      value: 0
    },
    {
      name: 'Custom author',
      value: 1
    }
  ]
};

const resourceImportFormSchema = {
  import_origin: {
    label: 'Import Origin',
    type: 'select',
    validate: schema.number().required(),
    data: [
      {
        name: 'API',
        value: 1
      }
    ]
  },

  api_path: {
    label: 'API Path',
    type: 'text',
    validate: schema.string().required(),
    disabled: true
  },

  api_auth: {
    label: 'API Auth',
    type: 'text',
    validate: schema.string().required(),
    disabled: true
  },

  resource_folder_id: {
    label: 'Resource Folder',
    type: 'select',
    validate: commonSchema.integer().required(),
    data: [
      {
        key: 0,
        value: '',
        name: 'Select a Folder'
      }
    ]
  },

  resource_type_id: {
    label: 'Resource Types',
    type: 'select',
    validate: commonSchema.integer().required(),
    data: [
      {
        key: 0,
        value: '',
        name: 'Select a Type'
      }
    ]
  },

  import_type: {
    label: 'Import Type',
    type: 'select',
    validate: schema.number().required(),
    data: [
      {
        name: 'One Time Import',
        value: 1
      }
    ]
  },

  author: {
    label: 'Author Name',
    type: 'text',
    disabled: true
  },

  page: {
    label: 'Page',
    type: 'number',
    validate: commonSchema.integer('Page must is a number').required()
  },
};


// for testing, remove if no need
export const defaultData = {
  import_origin: 1,
  api_path: null,
  api_auth: null,
  resource_folders: '',
  resource_types: '',
  import_type: 1,
  author: '',
  page: 1

};

export default resourceImportFormSchema;
