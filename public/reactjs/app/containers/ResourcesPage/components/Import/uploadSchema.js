import {schema, commonSchema} from 'helpers/form';

const radioYesNo = {
  type: 'radio',
  validate: schema.number().oneOf([1, 2]).required(),
  inline: true,
  data: [
    {
      name: 'Overlay type',
      value: 1
    },
    {
      name: 'Content type',
      value: 2
    }
  ]
};

const uploadSchema = {
  upload_type: {
    label: 'Resource type',
    ...radioYesNo,
  },
  only_overlay_resources: {
    label: 'Overlay type',
    type: 'select',
    validate: commonSchema.integer('Overlay resource type is a required').required(),
    data: []
  },
  only_content_resources: {
    label: 'Content type',
    type: 'select',
    validate: commonSchema.integer('Content resource type is a required').required(),
    data: []
  },
  resource_folder_id: {
    label: 'Resource Folder',
    type: 'select',
    validate: commonSchema.integer().required(),
    data: []
  },
};


// for testing, remove if no need
export const defaultForm = {
  upload_type: 1

};

export default uploadSchema;
