import React from 'react';
import {TabsLink, TabsLayout} from '../../../../components/Tabs';
import ImportForm from './ImportForm';
import UploadPage from './upload';
import routePaths from 'settings/route-paths';
import ListingPage from "../../../../components/BaseComponents/ListingPage";
import {withRouter} from "react-router-dom";
import locationHelper from '../../../../helpers/location';

@withRouter
export class ImportPage extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      dataList: []
    };
  }

  render() {
    const { status } = locationHelper.getUrlParams();
    const tabLabels = [
      {
        name: 'From URL',
        link: routePaths.resources.import,
        isActive: status != 'upload'
      },
      {
        name: 'Upload Directly',
        link: routePaths.resources.upload,
        isActive: status == 'upload'
      }
    ];
    const html = status != 'upload'
                  ? <ImportForm object={this} />
                  : <UploadPage />;
    const title = status != 'upload'
        ? 'Import'
        : 'Upload';
    return (
        <div>
          <div className="row">
            <div className="col-md-8">
              <h1 className="page-title">
                { title }
              </h1>
            </div>
          </div>
          <div className="portlet box light">
            <TabsLayout labelList={tabLabels}>
              <div>
                <div className="import-page">

                  {/* Locate : Load html form import */}
                  { html }

                </div>
              </div>
            </TabsLayout>
          </div>
        </div>
    );
  }
}

export default ImportPage;