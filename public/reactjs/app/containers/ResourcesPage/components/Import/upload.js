import React from 'react';
import Uploader from 'components/Uploader';
import {createStructuredSelector} from "reselect";
import {connect} from 'react-redux';
import {makeSelectChannel, makeSelectChannelSetting} from 'store/app/selectors';
import withAsync from '../../../../helpers/react/withAsync/withAsync';
import uploadSchema, {defaultForm} from './uploadSchema';
import contentTypesApiService from 'services/api/resource-types';
import PropTypes from 'prop-types';
import SmartForm from '../../../../helpers/form/SmartForm';
import settingsApiService from 'services/api/settings';
import withReload from '../../../../helpers/react/withReload/withReload';
import {preloadResourceFolder} from '../../../../common/shema-preloads';

@withReload()
@withAsync()
class UploadPage extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      params: {},
      changeForm: false,
      activeUpload: 1
    }
  }

  static propTypes = {
    error: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
    errorGlobal: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    loading: PropTypes.bool,
    channelSetting: PropTypes.object,
  };

  static async preload(props) {
    let schema = {...uploadSchema};
    const params = {
      channel_id: props.channelSetting.channel_id
    };
    /**
     * Get resource folders, resource types, settings data from API
     * @type {*}
     */
    const [apiData, contentTypesAPI, settingsData] = await Promise.all([
      preloadResourceFolder(schema, params),
      contentTypesApiService.getList({...params, page: -1, active: 1}),
      settingsApiService.getSystemSettings({})
    ]);

    /**
     * Get data content type by field is_overlay
     * @type {*}
     */
    const contentTypesOverlay = [];
    const contentTypesIsContent = [];
    contentTypesAPI.data.map((el, idz) => {
      let dataContentType = {
        name: el.name,
        value: el.id
      };
      if (el.is_overlay) {
        contentTypesOverlay.push(dataContentType);
      }
      if (el.is_content) {
        contentTypesIsContent.push(dataContentType);
      }
    });
    apiData.only_overlay_resources.data = contentTypesOverlay;
    apiData.only_content_resources.data = contentTypesIsContent;

    /** Reset schema **/
    schema = {...apiData};

    /**
     * Set validate client for upload file
     */
    const allowExtensions = settingsData.common_allow_extensions;
    const maxUpload = parseInt(settingsData.maximum_upload);

    return {
      schema,
      maxUpload,
      allowExtensions
    };
  }

  componentDidMount() {
    const {schema} = this.props.preloadData;
    this.setState({
      params: {
        only_overlay_resources: schema.only_overlay_resources.data.length > 0 ?  schema.only_overlay_resources.data[0].value : [],
        only_content_resources: schema.only_content_resources.data.length > 0 ? schema.only_content_resources.data[0].value : [],
        resource_folder_id: schema.resource_folder_id.data.length > 0 ? schema.resource_folder_id.data[0].value : [],
        upload_type: schema.upload_type.data.length > 0 ? schema.upload_type.data[0].value : 1
      }
    });
  }

  changeForm = async data => {
    this.setState({
      params: data,
      activeUpload: parseInt(data.upload_type)
    });
  };

  componentDidUpdate(previousProps) {
    if (previousProps.channelSetting.channel_id !== this.props.channelSetting.channel_id) {
      this.props.reload();
    }
    return true;
  };

  changeStateUpload = (schema) => {
    const schemaData = {...schema};
    if (this.state.activeUpload === 1) {
      schemaData.only_content_resources.className = 'hidden';
      schemaData.only_overlay_resources.className = null;
    } else {
      schemaData.only_content_resources.className = null;
      schemaData.only_overlay_resources.className = 'hidden';
    }
    return schemaData;
  };

  render() {
    const {preloadData} = this.props;
    const {schema, allowExtensions, maxUpload} = preloadData;
    const schemaData = this.changeStateUpload(schema);

    return (
      <div>
        <SmartForm
            ref={e => (this.form = e)}
            schema={schemaData}
            defaultData={defaultForm}
            onChange={this.changeForm}
            isShowSubmit={false}
        />

        <Uploader
            maxSize={maxUpload}
            mutilple={true}
            accept={allowExtensions}
            params={this.state.params}
        />

      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  channelSetting: makeSelectChannelSetting()
});

export default connect(
    mapStateToProps,
    null
)(UploadPage);

