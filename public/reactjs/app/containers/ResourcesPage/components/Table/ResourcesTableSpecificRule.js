import React from 'react';
import {createStructuredSelector} from 'reselect';
import {connect} from 'react-redux';
import {formatDuration} from 'helpers/utils/index';
import ManageTable from 'components/ManageTable';
import {makeSelectChannel} from 'store/app/selectors';
import ListingPageWithChannel from "../../../../components/BaseComponents/ListingPageWithChannel";
import {ResourcesApiService} from "../../../../services/api/resources";
import FilterTable from './FilterTable';


@connect(
    createStructuredSelector({
      currentChannel: makeSelectChannel()
    })
)
class ResourcesTableSpecificRule extends ListingPageWithChannel {
  restApiService = new ResourcesApiService();
  ignoreQuery = true;

  state = {
    activeRowId: 0
  };

  rowColumns = [
    {
      name: '',
      value: e => (<label className="mt-radio mt-radio-outline">
        <input type="radio" checked={this.state.activeRowId == e.id} onClick={() => this.onChoose(e)} />
        <span></span>
      </label>),
      style: {width: 5}
    },
    {
      name: 'Title',
      value: e => e.title,
      sort: 'title'
    },
    {
      name: 'File Name',
      value: e => e.filename,
      sort: 'filename'
    },
    {
      name: 'Theme',
      value: e => e.theme,
      sort: 'theme_id'
    },
    {
      name: 'Thumbnail',
      value: e => e.thumbnail ? (<img src={e.thumbnail}/>) : '',
      sort: 'thumb_filename',
      style: {width: 130}
    },
    {
      name: 'Updated at',
      value: e => e.updated_at,
      sort: 'updated_at'
    },
    {
      name: 'Duration',
      value: e => formatDuration(e.duration),
      sort: 'duration'
    }
  ];


  componentWillMount() {
    if (this.props.tableRef) {
      this.props.tableRef(this);
    }
    if (this.props.currentChannel) {
      this.restApiService.setGetlistDefaultParams({
        channel_id: this.props.currentChannel && this.props.currentChannel.id
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    //console.log("props : ", this.props);
    //console.log("nextProps : ", nextProps);
    if (this.props !== nextProps) {
      if (nextProps.activeId && nextProps.activeId !== this.state.activeRowId) {
        //console.log("activeRowId : ", this.state.activeRowId);
        this.setState({
          activeRowId: nextProps.activeId
        });
      }
    }
  }

  onChoose = (e) => {
    e.paging = this.apiState.paging.currentPage;
    this.setState({
      activeRowId: e.id
    });
    this.props.onGetResource(e);
  };

  /**
   * Call API filter data
   * @param params
   */
  filterDataTable(params) {
    this.setFilterParams(params);
  }

  render() {
    const {data, paging, loading} = this.apiState;
    const {activeRowId} = this.state;

    if (data) {
      return (
          <div>
            <FilterTable filterDataTable={this.filterDataTable.bind(this)}/>
            <ManageTable
                nameTable="resource"
                columns={this.rowColumns}
                showActions={false}
                tools={false}
                data={data || []}
                paging={paging || {}}
                loading={loading}
                handleChangePage={this.handlePageChange}
                handleLimitChange={this.handleLimitChange}
                handleSortBy={this.handleSortBy}
                activeRowId={activeRowId || 0}
            />
          </div>
      );
    }
    return null;
  }
}

export default ResourcesTableSpecificRule;