import React from 'react';
import { TabsLink } from 'components/Tabs';
import DefaultTable from './ResourcesTable';
import { withRouter } from 'react-router-dom';
import routePaths from 'settings/route-paths';

@withRouter
export class ResourcesTableView extends React.Component {

  render() {
    const { history } = this.props;

    return (
      <div>
        <div className="row">
          <div className="col-md-8">
            <h1 className="page-title">Resources</h1>
          </div>
        </div>
        <div className="portlet box light">
          <TabsLink
            indexActive={0}
            labelList={[
              {
                label: 'Default',
                linkTo: routePaths.resources.list,
                state: { indexActive: 0 },
                content: <DefaultTable history={history} ignoreQuery={false} />,
                hidden: true
              }
            ]}
          />
        </div>
      </div>
    );
  }
}

export default ResourcesTableView;
