import React from 'react';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import {makeSelectChannel} from 'store/app/selectors';
import {getFilterSchema} from './filterSchema';
import SmartForm from '../../../../helpers/form/SmartForm';
import {renderInlineFormLayout} from '../../../../helpers/form/render-layouts';
import {preloadResourceType, preloadThemes, preloadResourceFolder} from '../../../../common/shema-preloads';

@connect(
    createStructuredSelector({
      currentChannel: makeSelectChannel()
    })
)
class FilterTable extends React.PureComponent {

  constructor(props) {
    super(props);
  }

  schema = {};

  componentWillMount() {
    this.rebuildSchema();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.currentChannel.id !== this.props.currentChannel.id) {
      this.rebuildSchema();
    }
    return true;
  }

  async rebuildSchema() {
    this.schema = getFilterSchema();

    /**
     * Get resource folders, resource types, themes data from API
     * @type {*}
     */
    const [apiData] = await Promise.all([
      preloadResourceFolder(this.schema, {channel_id: this.props.currentChannel.id}),
      preloadThemes(this.schema, {channel_id: this.props.currentChannel.id}),
      preloadResourceType(this.schema, {channel_id: this.props.currentChannel.id})
    ]);

    this.schema = apiData;

    /**
     * Reset form data
     */
    if ( this.form ) {
      this.form.reset();
      this.forceUpdate();
      this.form.applyDefaultData();
    }

  }

  handleClickFilter = (data) => {
    this.props.filterDataTable(data);
  };

  render() {
    const {...schema} = this.schema;
    return (
        <div>
          <SmartForm
              ref={e => (this.form = e)}
              schema={schema}
              defaultData={{}}
              isShowSubmit={true}
              textButton="Filter"
              renderInner={renderInlineFormLayout}
              onSubmit={this.handleClickFilter}
          />
        </div>

    );
  }
}

export default FilterTable;