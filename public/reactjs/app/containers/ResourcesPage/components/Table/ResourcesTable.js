import React from 'react';
import {createStructuredSelector} from 'reselect';
import {connect} from 'react-redux';
import {formatDuration} from 'helpers/utils/index';
import ManageTable from 'components/ManageTable';
import {withRouter} from 'react-router-dom';
import {makeSelectChannel} from 'store/app/selectors';
import ListingPageWithChannel from "../../../../components/BaseComponents/ListingPageWithChannel";
import {ResourcesApiService} from "../../../../services/api/resources";
import FilterTable from './FilterTable';
import locationHelper from '../../../../helpers/location';
import routePaths from '../../../../settings/route-paths';

@withRouter
@connect(
    createStructuredSelector({
      currentChannel: makeSelectChannel()
    })
)
class ResourcesTable extends ListingPageWithChannel {
  restApiService = new ResourcesApiService();

  rowColumns = [
    {
      name: 'Title',
      value: e => e.title,
      sort: 'title'
    },
    {
      name: 'File Name',
      value: e => e.filename,
      sort: 'filename'
    },
    {
      name: 'Theme',
      value: e => e.theme,
      sort: 'theme_id'
    },
    {
      name: 'Thumbnail',
      value: e => e.thumbnail ? (<img src={e.thumbnail}/>) : '',
      sort: 'thumb_filename',
      style: {width: 130}
    },
    {
      name: 'Updated at',
      value: e => e.updated_at,
      sort: 'updated_at'
    },
    {
      name: 'Duration',
      value: e => formatDuration(e.duration),
      sort: 'duration'
    }
  ];

  rowActions = [
    {
      icon: 'fa fa-pencil',
      isShow: true,
      handler: ({id}) => this.props.history.push(locationHelper.getLink(routePaths.resources.single, {id}))
    },
    {
      icon: 'fa fa-remove white',
      isShow: false,
      needConfirm: true,
      backgroundColor: 'red',
      handler: ({id}) => this.handleClickDelete(id)
    }
  ];


  /**
   * Call API filter data
   * @param params
   */
  filterDataTable(params) {
    this.setFilterParams(params);
  }

  render() {
    const {data, paging, loading} = this.apiState;

    if (data) {
      return (
          <div>
            <FilterTable filterDataTable={this.filterDataTable.bind(this)}/>
            <ManageTable
                nameTable="resource"
                columns={this.rowColumns}
                actions={this.rowActions}
                tools={false}
                data={data || []}
                paging={paging || {}}
                loading={loading}
                handleChangePage={this.handlePageChange}
                handleLimitChange={this.handleLimitChange}
                handleSortBy={this.handleSortBy}
            />
          </div>
      );
    }
    return null;
  }
}

export default ResourcesTable;