import {schema, commonSchema} from 'helpers/form';

// for testing, remove if no need
export const filterDefaultData = {
  keyword: null,
  resource_folder_id: '',
  theme_id: '',
  resource_type_id: '',

};

export function getFilterSchema() {
  const filterSchema = {
    keyword: {
      label: 'Keyword',
      type: 'text',
      data: null
    },
    resource_folder_id: {
      label: 'Resource Folder',
      type: 'select',
      data: [
        {
          key: 0,
          value: '',
          name: 'Select a folder'
        }
      ]
    },
    theme_id: {
      label: 'Theme',
      type: 'select',
      data: [
        {
          key: 0,
          value: '',
          name: 'Select a theme'
        }
      ]
    },
    resource_type_id: {
      label: 'Resource Type',
      type: 'select',
      data: [
        {
          key: 0,
          value: '',
          name: 'Select a type'
        }
      ]
    },
  };

  return filterSchema;
}
