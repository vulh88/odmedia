import React from 'react';
import { Helmet } from 'react-helmet';

import ListResources from './components/Table';
import FormResources from './components/Form';
import ImportResource from './components/Import';

import routePaths from 'settings/route-paths';
import {renderLocalRoutes} from 'router/createRouter';

export class ResourcesPage extends React.PureComponent {

  localRoutes = [
    {
      path: routePaths.resources.single,
      component: () => <FormResources/>
    },
    {
      path: routePaths.resources.import,
      component: () => <ImportResource/>
    },
    {
      path: routePaths.resources.list,
      component: () => <ListResources/>
    },
    {
      path: routePaths.resources.upload,
      component: () => <ImportResource/>
    }
  ];

  render() {
    return (
      <div>
        <Helmet>
          <title>Resources</title>
        </Helmet>
        {renderLocalRoutes(this.localRoutes)}
      </div>
    );
  }
}

export default ResourcesPage;
