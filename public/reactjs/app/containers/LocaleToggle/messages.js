/*
 * LocaleToggle Messages
 *
 * This contains all the text for the LanguageToggle component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  en: {
    id: 'odmedia.containers.LocaleToggle.en',
    defaultMessage: 'en'
  },
  de: {
    id: 'odmedia.containers.LocaleToggle.de',
    defaultMessage: 'de'
  }
});
