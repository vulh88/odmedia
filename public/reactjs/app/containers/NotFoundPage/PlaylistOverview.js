import React from 'react';
class NotFountPlaylistOverviewPage extends React.PureComponent{

  render () {
    return (
        <div className="row">
          <div className="col-md-12 page-404">
            <div className="details">
              <h2>Timeline is not available in this channel</h2>
            </div>
          </div>
        </div>
    );
  }

}

export default NotFountPlaylistOverviewPage;