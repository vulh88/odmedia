import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'odmedia.containers.NotFoundPage.header'
  },
  mess: {
    id: 'odmedia.containers.NotFoundPage.mess'
  },
  mess1: {
    id: 'odmedia.containers.NotFoundPage.mess1'
  },
  placeholder: {
    id: 'odmedia.containers.NotFoundPage.placeholder'
  }
});
