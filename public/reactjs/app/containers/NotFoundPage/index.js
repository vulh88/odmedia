import React from 'react';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

export default function NotFountPage() {
  return (
    <div className="row">
      <div className="col-md-12 page-404">
        <div className="number font-red"> 404 </div>
        <div className="details">
          <h3>
            <FormattedMessage {...messages.header} />
          </h3>

          <p>
            <FormattedMessage {...messages.mess} />
            <br />
            <FormattedMessage
              {...messages.mess1}
              values={{
                returnHome: <Link to="/home"> Return home </Link>
              }}
            />
          </p>

          <form action="#">
            <div className="input-group input-medium">
              <input
                type="text"
                className="form-control"
                placeholder="keyword..."
              />
              <span className="input-group-btn">
                <button type="submit" className="btn green">
                  <i className="fa fa-search" />
                </button>
              </span>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
