import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';

import injectReducer from 'store/utils/injectReducer';
import injectSaga from 'store/utils/injectSaga';

import {
  makeSelectError,
  makeSelectLoading
} from 'store/app/selectors';

import reducer from './stores/reducer';
import saga from './stores/saga';
import { makeSelectUsers } from './stores/selectors';

import messages from './messages';

export class DashboardPage extends React.PureComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired
  };

  render() {
    return (
      <div>
        <Helmet>
          <title>Dashboard Page</title>
          <meta
            name="description"
            content="A React.js ODarrange application Dashboardpage"
          />
        </Helmet>
        <h1 className="page-title">
          <FormattedMessage {...messages.header} />
        </h1>
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  error: makeSelectError(),
  loading: makeSelectLoading(),
  users: makeSelectUsers()
});
const withConnect = connect(
  mapStateToProps,
  null
);

const withReducer = injectReducer({ key: 'dashboard', reducer });
const withSaga = injectSaga({ key: 'dashboard', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(DashboardPage);
