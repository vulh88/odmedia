import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'odmedia.containers.DashboardPage.header'
  }
});
