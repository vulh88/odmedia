import apiCaller from 'services/apiCaller';

export const requestUsers = () =>
  apiCaller
    .get('/users')
    .then(result => result);
