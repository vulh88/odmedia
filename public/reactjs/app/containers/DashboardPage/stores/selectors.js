import { createSelector } from 'reselect';

const selectDashboard = state => state.get('dashboard');

const makeSelectError = () =>
  createSelector(selectDashboard, loginState => loginState.get('error'));
const makeSelectLoading = () =>
  createSelector(selectDashboard, loginState => loginState.get('loading'));
const makeSelectUsers = () =>
  createSelector(selectDashboard, loginState => loginState.get('users'));

export { selectDashboard, makeSelectUsers, makeSelectLoading, makeSelectError };
