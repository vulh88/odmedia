export const REQUEST_USERS = 'dashboard/REQUEST_USERS';
export const REQUEST_USERS_SUCCESS = 'dashboard/REQUEST_USERS_SUCCESS';
export const REQUEST_USERS_ERROR = 'dashboard/REQUEST_USERS_ERROR';

export const requestUsers = accessToken => ({
  type: REQUEST_USERS,
  accessToken
});
