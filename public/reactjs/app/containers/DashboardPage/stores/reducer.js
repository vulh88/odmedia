import { fromJS } from 'immutable';
import * as actions from './actions';

const initialState = fromJS({
  loading: false,
  error: false,
  users: null
});

function dashboardReducer(state = initialState, action) {
  switch (action.type) {
    case actions.REQUEST_USERS:
      return state.set('loading', true);
    case actions.REQUEST_USERS_SUCCESS:
      return state.set('loading', false).set('users', action.payload);
    case actions.REQUEST_USERS_ERROR:
      return state.set('loading', false);
    default:
      return state;
  }
}

export default dashboardReducer;
