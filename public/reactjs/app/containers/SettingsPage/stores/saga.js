import { put, takeLatest, call } from 'redux-saga/effects';
import { isFunction } from 'lodash';
import Cookies from 'universal-cookie';

import * as actionsGlobal from 'store/app/actions';

import * as actions from './actions';
import * as api from './api';

const cookies = new Cookies();

function* callApi(apiCall, params) {
  yield put({
    type: actions.REQUEST_API
  });
  try {
    const resp = yield call(apiCall, params);
    yield put({
      type: actions.REQUEST_API_SUCCESS
    });
    return resp;
  } catch (error) {
    yield put({
      type: actions.REQUEST_API_ERROR,
      payload: error
    });
    throw error;
  }
}

export const requestUpdateChannel = function*({ payload }) {
  const accessToken = cookies.get('access_token');

  try {
    const request = yield callApi(api.requestUpdateChannel, payload);
    yield put({
      type: actions.UPDATE_CHANNEL_SUCCESS
    });

    yield put({
      type: actionsGlobal.REQUEST_CHANNEL_SETTINGS,
      payload: {
        channel_id: payload.body.channel_id,
        accessToken
      }
    });

    yield put({
      type: actions.SET_UPDATE_CHANNEL,
      payload: request
    });

    if (isFunction(payload.callback)) {
      payload.callback();
    }
  } catch (error) {
    yield put({
      type: actions.UPDATE_CHANNEL_ERROR,
      payload: error
    });
  }
};

const requestResetForm = function*() {
  yield put({
    type: actions.SET_UPDATE_CHANNEL
  });
};

export default function* Resources() {
  yield takeLatest(actions.REQUEST_UPDATE_CHANNEL, requestUpdateChannel);
  yield takeLatest(actionsGlobal.CHANGE_CHANNEL_SUCCESS, requestResetForm);
}
