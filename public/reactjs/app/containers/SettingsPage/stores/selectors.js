import { createSelector } from 'reselect';

const selectSettings = state => state.get('settings');

const makeSelectError = () =>
  createSelector(selectSettings, state => state.get('error'));

const makeSelectLoading = () =>
  createSelector(selectSettings, state => state.get('loading'));

export { selectSettings, makeSelectLoading, makeSelectError };
