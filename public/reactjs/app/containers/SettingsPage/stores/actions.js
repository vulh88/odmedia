export const REQUEST_API = 'settings/REQUEST_API';
export const REQUEST_API_SUCCESS = 'settings/REQUEST_API_SUCCESS';
export const REQUEST_API_ERROR = 'settings/REQUEST_API_ERROR';

export const REQUEST_UPDATE_CHANNEL = 'settings/REQUEST_UPDATE_CHANNEL';
export const UPDATE_CHANNEL_SUCCESS = 'settings/UPDATE_CHANNEL_SUCCESS';
export const UPDATE_CHANNEL_ERROR = 'settings/UPDATE_CHANNEL_ERROR';

export const SET_UPDATE_CHANNEL = 'settings/SET_UPDATE_CHANNEL';

export const requestUpdateChannel = payload => ({
  type: REQUEST_UPDATE_CHANNEL,
  payload
});
