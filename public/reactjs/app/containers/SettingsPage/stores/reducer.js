import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';

import * as actions from './actions';

const initialState = fromJS({
  loading: false,
  error: false
});

function resourceReducer(state = initialState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return (state = initialState);

    case actions.REQUEST_API:
      return state.set('loading', true).set('error', false);
    case actions.REQUEST_API_SUCCESS:
      return state.set('loading', false);
    case actions.REQUEST_API_ERROR:
      return state.set('loading', false).set('error', action.payload);

    case actions.SET_UPDATE_CHANNEL:
      return state.set('error', false);

    default:
      return state;
  }
}

export default resourceReducer;
