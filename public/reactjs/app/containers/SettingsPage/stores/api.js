import apiCaller from 'services/apiCaller';
import apiPaths from 'settings/api/api-path';

export const requestUpdateChannel = params =>
  apiCaller
    .put(apiPaths.users.updateChannel, { body: params.body })
    .then(result => result.data);
