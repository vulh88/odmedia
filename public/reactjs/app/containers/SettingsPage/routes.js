import NotFound from 'containers/NotFoundPage/index';
import routePaths from 'settings/route-paths';
import ImportSettingsForm from './components/Form/index';
import SystemSettingsForm from './components/SystemForm/index';

const settingsRoutes = [
  {
    path: routePaths.pages.settings,
    component: ImportSettingsForm
  },
  {
    path: routePaths.pages.system_settings,
    component: SystemSettingsForm
  },
  {
    path: '*',
    component: NotFound
  }
];

export default settingsRoutes;
