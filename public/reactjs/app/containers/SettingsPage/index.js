import React from 'react';
import {Helmet} from 'react-helmet';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {createStructuredSelector} from 'reselect';
import injectReducer from 'store/utils/injectReducer';
import injectSaga from 'store/utils/injectSaga';

import settingsRoutes from './routes';
import {renderLocalRoutes} from 'router/createRouter';

import reducer from './stores/reducer';
import saga from './stores/saga';


export class SettingsPage extends React.PureComponent {

  render() {
    return (
        <div>
          <Helmet>
            <title>Scheduler Settings</title>
          </Helmet>
          {/* <FormChannel history={history} /> */}
          {renderLocalRoutes(settingsRoutes)}
        </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({});

const withConnect = connect(
    mapStateToProps,
    null
);

const withReducer = injectReducer({key: 'settings', reducer});
const withSaga    = injectSaga({key: 'settings', saga});

export default compose(
    withReducer,
    withSaga,
    withConnect
)(SettingsPage);
