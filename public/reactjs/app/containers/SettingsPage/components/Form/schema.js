import { schema } from 'helpers/form';

/**
 * Schema of resourceForm. See package yup documentation for schema method
 */
const channelFormSchema = {
  player_directory: {
    label: 'Player resource directory',
    type: 'text',
    validate: schema.string().required()
  },
  api_path: {
    label: 'API Path',
    type: 'text',
    validate: schema.string().required()
  },
  api_auth: {
    label: 'API auth',
    type: 'text',
    validate: schema.string().required()
  }
};

export default channelFormSchema;
