import React from 'react';
import PropTypes from 'prop-types';
import lodash from 'lodash';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import Alert from 'react-s-alert';
import { schema } from 'helpers/form';

import { TabsLink } from 'components/Tabs';
import ButtonLoading from 'components/ButtonLoading';
import FormAlert from 'components/FormAlert';

import {
  makeSelectError as makeSelectErrorGlobal,
  makeSelectChannel,
  makeSelectLoadingChannel,
  makeSelectChannelSetting
} from 'store/app/selectors';

import SmartForm from '../../../../helpers/form/SmartForm';

import * as actions from '../../stores/actions';
import { makeSelectError, makeSelectLoading } from '../../stores/selectors';
import {renderHorizontalFormLayout} from '../../../../helpers/form/render-layouts';

class ChannelForm extends React.PureComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    error: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
    errorGlobal: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    loading: PropTypes.bool,
    currentChannel: PropTypes.object,
    channelSettings: PropTypes.oneOfType([PropTypes.array, PropTypes.object])
  };

  state = {
    schema: {
      player_directory: {
        label: 'Player resource directory',
        type: 'text',
        validate: schema.string().required(),
        domProps: {
          defaultValue: ''
        }
      },
      api_path: {
        label: 'API Path',
        type: 'text',
        validate: schema.string().required(),
        domProps: {
          defaultValue: ''
        }
      },
      api_auth: {
        label: 'API auth',
        type: 'text',
        validate: schema.string().required(),
        domProps: {
          defaultValue: ''
        }
      }
    }
  };

  requestSchema = channelSettings => {
    const { schema } = this.state;

    this.setState({
      schema: lodash.mapValues(schema, (schemaItem, key) => {
        if (channelSettings[key]) {
          schemaItem.domProps.defaultValue = channelSettings[key];
        } else {
          schemaItem.domProps.defaultValue = '';
        }

        return schemaItem;
      })
    });
  };

  componentWillMount() {
    const { channelSettings } = this.props;

    if (channelSettings) {
      this.requestSchema(channelSettings);
    }
  }

  shouldComponentUpdate(nextProps) {
    if (nextProps.error) {
      switch (nextProps.error.code) {
        case 404:
          Alert.error('This user has already contained', {
            position: 'bottom-right'
          });
          break;

        default:
          Alert.error('The given data was invalid.', {
            position: 'bottom-right'
          });
          break;
      }
    }

    if (nextProps.channelSettings !== this.props.channelSettings) {
      this.requestSchema(nextProps.channelSettings);
    }

    return true;
  }

  handleSubmit = data => {
    const { dispatch, currentChannel } = this.props;
    const result = { ...data, channel_id: currentChannel.id };

    dispatch(
      actions.requestUpdateChannel({
        body: result,
        callback: () =>
          Alert.success(
            <div>
              <h4>Success!</h4>
              <span>Save current settings channel successuflly!</span>
            </div>,
            { position: 'bottom-right' }
          )
      })
    );
  };

  renderFormInner = (params) => {
    const { error, errorGlobal, loadingChannel } = this.props;

    if (loadingChannel) {
      return null;
    }

    return (
      <div className="portlet light">
        <FormAlert errors={(error && error.message) || errorGlobal} />
        {renderHorizontalFormLayout(params)}
      </div>
    );
  };

  render() {
    const { channelSettings, loading } = this.props;
    const { schema } = this.state;

    return (
      <div>
        <h1 className="page-title">Settings</h1>
        <TabsLink
          indexActive={0}
          labelList={[
            {
              label: 'Import',
              linkTo: '/settings/channel',
              state: { indexActive: 1 },
              content: channelSettings && (
                <SmartForm
                  ref={e => (this.form = e)}
                  schema={schema}
                  loading={loading}
                  textButton="Save"
                  onSubmit={this.handleSubmit}
                  renderInner={this.renderFormInner}
                />
              )
            }
          ]}
        />
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  errorGlobal: makeSelectErrorGlobal(),
  loadingChannel: makeSelectLoadingChannel(),
  currentChannel: makeSelectChannel(),
  channelSettings: makeSelectChannelSetting(),

  error: makeSelectError(),
  loading: makeSelectLoading()
});

export default connect(
  mapStateToProps,
  null
)(ChannelForm);
