import { schema, commonSchema } from 'helpers/form';

/**
 * Schema of resourceForm. See package yup documentation for schema method
 */
const systemSettingsSchema = {
  /*upload_extensions: {
    label: 'Upload extensions',
    type: 'text',
    validate: schema.string().required(),
    summary: "Please input value follow the format like jpg, gif, jpeg"
  },*/
  maximum_upload: {
    label: 'Maximum upload file size',
    type: 'number',
    min: 1,
    validate: commonSchema.integer('Maximum upload is a required').required(),
    summary: "The field is an integer value ( input value is 10 it means 10 MB )"
  },
  content_type_extensions: {
    label: 'Content type extensions',
    type: 'text',
    validate: schema.string().required(),
    summary: ""
  },
  overlay_type_extensions: {
    label: 'Overlay type extensions',
    type: 'text',
    validate: schema.string().required(),
    summary: ""
  }
};

export default systemSettingsSchema;
