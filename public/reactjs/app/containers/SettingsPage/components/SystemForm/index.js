import React from 'react';
import PropTypes from 'prop-types';
import HaveApiComponent from 'components/BaseComponents/HaveApiComponent';
import { createStructuredSelector } from 'reselect';
import Alert from 'react-s-alert';
import { TabsLink } from 'components/Tabs';
import SmartForm from '../../../../helpers/form/SmartForm';
import withAsync from '../../../../helpers/react/withAsync/withAsync';
import systemSettingsSchema from './schema';
import toast from '../../../../helpers/user-interface/toast';
import apiService from 'services/api/settings';
import contentTypesApiService from 'services/api/resource-types';
import {connect} from 'react-redux';
import {makeSelectChannel} from 'store/app/selectors';


@connect(
    createStructuredSelector({
      currentChannel: makeSelectChannel()
    })
)
@withAsync()
class SystemSettingsForm extends HaveApiComponent {
  static propTypes = {
    error: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
    errorGlobal: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    loading: PropTypes.bool,
  };

  static async preload(props) {
    const schema = {...systemSettingsSchema};
    const settingsData = await apiService.getSystemSettings({});

    return {
      schema,
      settingsData
    };
  }

  handleSubmit = data => {
    this.callApi(apiService.setSystemSettings(data), () => {
      toast.success('Submit Settings successfully');
    });
  };

  render() {
    const {preloadData} = this.props;
    const {settingsData, schema} = preloadData;

    return (
        <div>
          <h1 className="page-title">System Settings</h1>
          <TabsLink
              indexActive={0}
              labelList={[
                {
                  label: 'Setting',
                  linkTo: '/settings/system',
                  state: { indexActive: 1 },
                  content: <SmartForm
                      ref={e => (this.form = e)}
                      schema={schema}
                      defaultData={settingsData}
                      onSubmit={this.handleSubmit}
                  />
                }
              ]}
          />
        </div>
    );
  }
}

export default SystemSettingsForm;
