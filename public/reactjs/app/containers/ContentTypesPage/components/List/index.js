import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectChannel } from 'store/app/selectors';

import { ResourceTypesApiService } from '../../../../services/api/resource-types';
import ManageTable from '../../../../components/ManageTable';
import { Link, withRouter } from 'react-router-dom';
import { TabsLink } from '../../../../components/Tabs';
import toast from 'helpers/user-interface/toast';
import routePaths from '../../../../settings/route-paths';
import locationHelper from '../../../../helpers/location';
import ListingPageWithChannel from '../../../../components/BaseComponents/ListingPageWithChannel';

@withRouter
class ListContentTypes extends ListingPageWithChannel {
  restApiService = new ResourceTypesApiService();

  columns = [
    {
      name: 'ID',
      value: e => e.id,
      sort: 'id',
      style: {
        width: 60,
        textAlign: 'center',
        justifyContent: 'center',
        flexGrow: 0
      }
    },
    {
      name: 'Name',
      value: e => e.name,
      sort: 'name'
    },
    {
      name: 'Class',
      value: e => e.class,
      sort: 'class'
    },
    {
      name: 'Status',
      value: e =>
        e.active ? (
          <i className="fa fa-check-circle green" />
        ) : (
          <i className="fa fa-refresh" />
        ),
      sort: 'active',
      style: { justifyContent: 'center', textAlign: 'center' }
    }
  ];

  actions = [
    {
      icon: 'fa fa-pencil',
      handler: e => this.handleEditItem(e)
    },
    {
      icon: 'fa fa-remove white',
      handler: e => this.handleDeleteItem(e),
      needConfirm: true,
      backgroundColor: 'red'
    }
  ];

  handleDeleteItem = async item => {
    this.callApi(this.restApiService.delete(item.id), () => {
      item._deleted = true;
      toast.success('Successfully deleted');
    });
  };

  handleEditItem({ id }) {
    const { history } = this.props;
    history.push(
      locationHelper.getLink(routePaths.contentTypes.single, { id })
    );
  }

  render() {
    const {
      data,
      paging,
      loading,
      success,
      errors,
      loadingChannel
    } = this.apiState;
    const { columns, actions } = this;

    return (
      <div>
        <div className="row">
          <div className="col-md-8">
            <h1 className="page-title">Content Types</h1>
          </div>
          <div className="col-md-4 text-right">
            <Link
              className="btn green btn-actions"
              to={routePaths.contentTypes.createNew}
            >
              <i className="icon-plus" />
              <span> Add New</span>
            </Link>
          </div>
          <div className="clearfix" />
        </div>
        <TabsLink
          indexActive={(location.state && location.state.indexActive) || 0}
          labelList={[
            {
              label: 'Default',
              linkTo: routePaths.contentTypes.list,
              state: { indexActive: 1 },
              content: (
                <div>
                  <ManageTable
                    loading={loadingChannel || loading}
                    data={data || []}
                    paging={paging || {}}
                    handleChangePage={this.handlePageChange}
                    handleLimitChange={this.handleLimitChange}
                    handleSortBy={this.handleSortBy}
                    columns={columns}
                    actions={actions}
                    styleActions={{ width: 120, flexGrow: 0 }}
                  />
                </div>
              ),
              hidden: true
            }
          ]}
        />
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  currentChannel: makeSelectChannel()
});

export default connect(
  mapStateToProps,
  null
)(ListContentTypes);
