import React from 'react';
import PropTypes from 'prop-types';
import HaveApiComponent from '../../../../components/BaseComponents/HaveApiComponent';

import {contentTypeFormSchema, defaultContentTypeForm} from './schema';
import {SmartForm} from '../../../../helpers/form';
import apiService from '../../../../services/api/resource-types';
import toast from 'helpers/user-interface/toast';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {makeSelectChannel} from 'store/app/selectors';
import withAsync from '../../../../helpers/react/withAsync/withAsync';
import {preloadSchema} from '../../../ThemesPage/components/Form/preload-schema';
import routePaths from '../../../../settings/route-paths';

@withAsync()
class ContentTypeForm extends HaveApiComponent {
  static propsTypes = {
    update: PropTypes.number,
    currentChannel: PropTypes.object,
    schema: PropTypes.object.isRequired
  };

  static preload = () => preloadSchema(contentTypeFormSchema);

  pageTitle = this.props.update ? 'Update Content Type' : 'Add Content Type';

  handleSubmit = async data => {
    const {update, history} = this.props;
    if (update) {
      this.callApi(apiService.update(update, data), () => {
        toast.success('Updated Content Type successfully');
        this.apiState.submited = true;
      });
    } else {
      if (this.isChanged && window.confirm('Would you like to save it ?') ) {
        this.sendCreateNew(data);
      }
    }
  };

  sendCreateNew = data => {
    const { history} = this.props;
    if (data.channel_id) {
      this.callApi(apiService.create(data), () => {
        toast.success('Added Content Type successfully');
        this.apiState.submited = true;
        history.push(routePaths.contentTypes.list);
      });

    } else {
      alert('Please select your working channel');
    }
  };

  componentWillUnmount() {
    if (this.isChanged && this.form.isValid() && !this.apiState.submited && window.confirm('Would you like to save it ?') ) {
      const data = this.form.getData();
      this.sendCreateNew(data);
    }
  }

  componentDidMount() {
    if (this.props.update) {
      this.callApi(
          apiService.getOne(this.props.update),
          data => {
            if (data.default_osd_id === null) {
              data.default_osd_id = '';
            }
            this.apiState.updateData = data;
          }
      );
    }
  }

  render() {
    const {update, currentChannel, preloadData} = this.props;
    const {schema} = preloadData;
    const {loading, updateData, submited} = this.apiState;
    const defaultData = update ? updateData : defaultContentTypeForm;
    const confirmUpdate = !submited;

    return (
        <div>
          <div className="row">
            <div className="col-md-8">
              <h1 className="page-title">{this.pageTitle}</h1>
            </div>
          </div>
          <div className="portlet light">
            {update && !updateData ? null : (
                <SmartForm
                    ref={e => (this.form = e)}
                    className="portlet light "
                    loading={loading}
                    defaultData={defaultData}
                    schema={schema}
                    onSubmit={this.handleSubmit}
                    onChange={() => this.isChanged = true}
                    confirmUpdate={confirmUpdate}
                    textButton="Save"
                >
                  {currentChannel && (
                      <input
                          type="hidden"
                          name="channel_id"
                          value={currentChannel.id}
                      />
                  )}
                </SmartForm>
            )}
          </div>
        </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  currentChannel: makeSelectChannel()
});

export default connect(
    mapStateToProps,
    null
)(ContentTypeForm);
