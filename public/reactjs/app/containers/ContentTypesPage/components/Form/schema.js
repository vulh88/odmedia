import {schema, commonSchema} from 'helpers/form';
import {default_osd_id, radioYesNo} from '../../../../common/schema-fields';

export const contentTypeFormSchema = {
  name: {
    label: 'Name',
    type: 'text',
    validate: schema
        .string()
        .max(100, 'Name may not be greater than 100 characters')
        .trim()
        .required()
  },
  class: {
    label: 'Class',
    type: 'text',
    validate: schema
        .string()
        .max(20, 'The class may not be greater than 20 characters')
        .trim()
  },
  description: {
    label: 'Description',
    type: 'textarea',
    validate: schema.string().trim(),
    domProps: {
      rows: 5
    }
  },
  default_osd_id,
  is_content: {
    label: 'Is Content',
    ...radioYesNo
  },
  is_overlay: {
    label: 'Is Overlay',
    ...radioYesNo
  },
  wipe: {
    label: 'Has wipe',
    ...radioYesNo
  },
  osd_loop: {
    label: 'OSD Loop',
    ...radioYesNo
  },
  active: {
    label: false,
    type: 'checkbox',
    validate: commonSchema.checkbox().default(0),
    data: [
      {
        value: 1,
        name: 'Active'
      }
    ]
  }
};

export const defaultContentTypeForm = {
  name: '',
  channel_id: '0',
  class: '',
  description: '',
  default_osd_id: '',
  is_content: '0',
  is_overlay: '0',
  osd_loop: '0',
  wipe: '0',
  active: '1'
};
