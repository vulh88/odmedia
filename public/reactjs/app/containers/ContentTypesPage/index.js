import React from 'react';
import { Helmet } from 'react-helmet';
import ListContentTypes from './components/List';
import ContentTypeForm from './components/Form';
import routePaths from 'settings/route-paths';
import { renderLocalRoutes } from 'router/createRouter';

export class ContentTypesPage extends React.PureComponent {
  localRoutes = [
    {
      path: routePaths.contentTypes.createNew,
      component: ({ history }) => <ContentTypeForm history={history} />
    },
    {
      path: routePaths.contentTypes.single,
      component: ({ match, history }) => (
        <ContentTypeForm update={match.params.id} history={history} />
      )
    },
    {
      path: routePaths.contentTypes.list,
      component: () => <ListContentTypes />
    }
  ];

  render() {
    return (
      <div>
        <Helmet>
          <title>Content types</title>
        </Helmet>
        {renderLocalRoutes(this.localRoutes)}
      </div>
    );
  }
}

export default ContentTypesPage;
