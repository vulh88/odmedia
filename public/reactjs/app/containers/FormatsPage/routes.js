import ListFormats from './List/index';
import NotFound from 'containers/NotFoundPage/index';
import routePaths from 'settings/route-paths';
import FormFormats from './Form/index';
import GenerateFormatForm from './Generate/index';

const formatRoutes = [
  {
    path: routePaths.formats.list,
    component: ListFormats
  },
  {
    path: routePaths.formats.createNew,
    component: FormFormats
  },
  {
    path: routePaths.formats.single,
    component: FormFormats
  },
  {
    path: routePaths.formats.generate,
    component: GenerateFormatForm
  },
  {
    path: '*',
    component: NotFound
  }
];

export default formatRoutes;
