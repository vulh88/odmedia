/* eslint-disable no-template-curly-in-string */
import {schema, commonSchema} from 'helpers/form';
import {radioYesNo} from '../../../common/schema-fields';

const formatsSchema = {
  name: {
    label: 'Name',
    type: 'text',
    validate: schema.string().required()
  },

  length: {
    label: 'Length',
    type: 'input_mask',
    format: {
      days: 99,
      hours: 99,
      minutes: 99,
      seconds: 99
    },
    validate: commonSchema.formatLength(),
    summary: "The format for this field must be DD:HH:MM:SS"
  },

  filling_out_time: {
    label: 'Filling out time',
    ...radioYesNo,
    summary: 'This checkbox will make sure automatic playlist generation will always fill items until the end of the duration is reached, even if the last item will overlap the next hour. In contrast, the overlap will be avoided and the last item truncated to fit'
  },

  dynamic_sub_format: {
    label: 'Dynamic subformat',
    ...radioYesNo,
    summary: 'Using the nested formats to increase the duration of the playlist by compensating into the parent format duration'
  },

  direct_publish: {
    label: 'Publish playlist directly',
    ...radioYesNo,
  },

  active: {
    label: false,
    type: 'checkbox',
    validate: commonSchema.checkbox(),
    data: [
      {
        name: 'Active',
        value: 1,
        domProps: {defaultChecked: true}
      }
    ]
  },

  rules: {
    type: 'textarea',
    className: 'hidden',
    data: []
  }
};



export const defaultForm = {
  length: "00:00:00:00",
  filling_out_time: 0,
  dynamic_sub_format: 0,
  direct_publish: 0,
  active: 1,
  rules: [],
};


export default formatsSchema;
