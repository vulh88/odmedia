import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Link, withRouter} from 'react-router-dom';
import {createStructuredSelector} from 'reselect';
import HaveApiComponent from '../../../components/BaseComponents/HaveApiComponent';
import {makeSelectChannel} from 'store/app/selectors';
import formatsSchema, {defaultForm} from './schema';
import {SmartForm} from '../../../helpers/form';
import apiService from '../../../services/api/formats';
import withAsync from '../../../helpers/react/withAsync/withAsync';
import withReload from '../../../helpers/react/withReload/withReload';
import routePaths from '../../../settings/route-paths';
import toast from '../../../helpers/user-interface/toast';
import FormatBuilder from '../../../components/FormatBuilder';
import LoadingIndicator from '../../../components/LoadingIndicator';
import ButtonLoading from 'components/ButtonLoading';
import locationHelper from 'helpers/location';
import {FormatBuilderContext} from '../../../components/FormatBuilder/components/context';
import quickAlert from 'helpers/user-interface/quick-alert';
import schema from '../../../helpers/form/schema';

@withReload()
@withAsync()
@withRouter
class FormatsForm extends HaveApiComponent {
  static propsTypes = {
    currentChannel: PropTypes.object,
    update: PropTypes.number
  };

  state = {
    duplicate: false,
    validationItems: []
  };

  /**
   * Keep reference to format folder
   * @type {null}
   */
  formatBuilder = null;

  static async preload(props) {
    const schema = {...formatsSchema};
    const update = props.match.params.id;
    const pageTitle = update ? 'Update Format' : 'Add Format';

    return {
      pageTitle,
      schema,
      update
    };
  }

  handleSubmit = async params => {
    const rulesData = this.formatBuilder.getData();
    if (this.handleValidated(rulesData)) {
      const {currentChannel, history, preloadData} = this.props;
      const {update} = preloadData;
      const data = {...params, channel_id: currentChannel.id, rules: rulesData};

      if (update) {
        this.callApi(apiService.update(update, data), () => {
          toast.success('Updated Format Successfully');
          history.push(routePaths.formats.list);
        });
      } else {
        this.callApi(
            apiService.create(data),
            () => {
              toast.success('Added Format Successfully');
              history.push(routePaths.formats.list);
            }
        );
      }
    } else {
      toast.error('There are some errors in your form data. Please check them again!');
    }
  };

  handleDuplicate = async (e) => {
    e.preventDefault();
    const {history, match} = this.props;
    const {id} = match.params;
    if (await quickAlert.confirm(`Would you like to duplicate this format?`)) {
      this.apiState.loading = true;
      this.state.duplicate = true;
      this.setState(this.state);
      this.callApi(
          apiService.duplicateFormat({
            id: id
          }),
          async ({data}) => {
            toast.success('Duplicated Format Successfully');

            if (await quickAlert.confirm(`All's done!`, `Would you like navigating to the cloned format?`, {
              type: 'success',
              yes: `Yes, let's go!`,
              no: `No, thanks!`
            })) {
              history.push(
                  locationHelper.getLink(routePaths.formats.single, {id: data.id})
              );
              this.props.reload();
            }
          }
      );
    }
  };

  componentDidMount() {
    const update = this.props.preloadData.update;
    if (update) {
      this.callApi(
          apiService.getOne(update),
          data => {
            this.apiState.updateData = data;
          }
      );
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.currentChannel.id !== this.props.currentChannel.id) {
      this.props.reload();
    }
    return true;
  }

  parseData = (data) => {
    if (data) {
      return data.formatRules
    }
  };

   handleValidated = (data) =>{
    let validationItems = [];
    data.map((el, idz) =>{
      if (el.validated) {
        if (!el.data.tag) {
          validationItems.push({
            index: el.index,
            type: el.type
          });
        }
      }
    });
    this.setState({
      validationItems: validationItems
    });

    return validationItems.length > 0 ? false : true;
  };

  render() {
    const {preloadData, currentChannel} = this.props;
    const {update, schema, pageTitle} = preloadData;
    const {loading, updateData} = this.apiState;
    const defaultData = update ? updateData : defaultForm;
    const rulesData = this.parseData(defaultData);
    const {validationItems} = this.state;
    const currentFormatId = parseInt(update);

    return (
        <div>
          <div className="row">
            <div className="col-md-8">
              <h1 className="page-title">{pageTitle}</h1>
            </div>
          </div>
          <div className="portlet light">
            {update && !updateData ? null : (
                <SmartForm
                    ref={e => (this.form = e)}
                    loading={loading}
                    defaultData={defaultData}
                    schema={schema}
                    onSubmit={this.handleSubmit}
                    textButton={'Save'}
                    isShowSubmit={false}
                    toastError={true}
                >
                  <div className="form-actions">
                    <div className="row">
                      <div className="col-md-offset-3 col-md-9">
                        <div className="col-md-offset-0">
                          <ButtonLoading
                              type="submit"
                              className="pull-left btn green">
                            Save
                          </ButtonLoading>
                        </div>
                        {
                          !update ? null : (
                              <div className="col-md-offset-1">
                                <ButtonLoading
                                    type="button"
                                    className="pull-left btn red"
                                    onClick={(e) => this.handleDuplicate(e)}
                                >
                                  Duplicate
                                </ButtonLoading>
                              </div>
                          )
                        }

                      </div>
                    </div>
                  </div>

                </SmartForm>
            )}
          </div>
          {
            update && !rulesData ? <LoadingIndicator/> :
                (
                    <FormatBuilderContext.Provider value={{
                      currentChannel,
                      currentFormatId,
                      validationItems
                    }}>

                      <FormatBuilder ref={e => this.formatBuilder = e} rulesData={rulesData} />

                    </FormatBuilderContext.Provider>
                )
          }

        </div>
    );
  }
}


const mapStateToProps = createStructuredSelector({
  currentChannel: makeSelectChannel()
});

export default connect(
    mapStateToProps,
    null
)(FormatsForm);
