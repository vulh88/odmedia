import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import HaveApiComponent from '../../../components/BaseComponents/HaveApiComponent';
import {makeSelectChannel} from 'store/app/selectors';
import generateFormatSchema, {defaultForm} from './schema';
import {SmartForm} from '../../../helpers/form';
import apiService from '../../../services/api/formats';
import withAsync from '../../../helpers/react/withAsync/withAsync';
import routePaths from '../../../settings/route-paths';
import toast from '../../../helpers/user-interface/toast';

@withAsync()
class GenerateFormatForm extends HaveApiComponent {
  static propsTypes = {
    currentChannel: PropTypes.object,
  };

  static async preload(props) {
    const schema = {...generateFormatSchema};
    const pageTitle = 'Generate Format';

    const format = await apiService.getOne(props.match.params.id);
    const updateData = {
      end_time: format.end_time,
      start_time: format.start_time,
      after_previous: defaultForm.after_previous
    };
    return {
      pageTitle,
      updateData,
      schema
    };
  }

  handleSubmit = async params => {
    const {currentChannel, history} = this.props;
    const format_id = window.location.pathname.split('/')[2];
    const data = {...params, format_id: format_id, channel_id: currentChannel.id};
    this.callApi(apiService.generateFormat(data), () => {
      toast.success('Generate Format Playlist Successfully');
      history.push(routePaths.formats.list);
    });

  };

  render() {
    const {preloadData} = this.props;
    const {schema, pageTitle, updateData} = preloadData;
    const {loading} = this.apiState;

    return (
        <div>
          <div className="row">
            <div className="col-md-8">
              <h1 className="page-title">{pageTitle}</h1>
            </div>
          </div>
          <div className="portlet light">
            <SmartForm
                ref={e => (this.form = e)}
                loading={loading}
                defaultData={updateData}
                schema={schema}
                onSubmit={this.handleSubmit}
                textButton={'Save'}
            />
          </div>
        </div>
    );
  }
}


const mapStateToProps = createStructuredSelector({
  currentChannel: makeSelectChannel()
});

export default connect(
    mapStateToProps,
    null
)(GenerateFormatForm);
