/* eslint-disable no-template-curly-in-string */
import {schema, commonSchema} from 'helpers/form';
import moment from 'moment/moment';
import {radioYesNo} from '../../../common/schema-fields';

const generateFormatSchema = {
  start_time: {
    label: 'Starting moment of playlist',
    type: 'datetimepicker',
    validate: schema.string().required()
  },

  end_time: {
    label: 'End time of the playlist',
    type: 'text',
    disabled: true
  },

  after_previous: {
    label: 'Post after the previous playlist',
    ...radioYesNo,
  },

};

export const defaultForm = {
  start_time: moment().format('YYYY-MM-DD HH:mm:ss'),
  after_previous: 0
};


export default generateFormatSchema;
