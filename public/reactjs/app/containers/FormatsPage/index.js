import React from 'react';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import {renderLocalRoutes} from 'router/createRouter';
import formatRoutes from './routes';
import {withRouter} from 'react-router-dom';

@withRouter
export class FormatsPage extends React.PureComponent {
  render() {
    return (
      <div>
        <Helmet>
          <title>Formats Page</title>
          <meta
            name="description"
            content="A React.js Boilerplate application Formats page"
          />
        </Helmet>
        {renderLocalRoutes(formatRoutes)}
      </div>
    );
  }
}

export default FormatsPage;
