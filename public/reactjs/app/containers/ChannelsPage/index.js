import React from 'react';
import { Helmet } from 'react-helmet';
import ListChannels from './components/List';
import ChannelForm from './components/Form';
import routePaths from 'settings/route-paths';
import { renderLocalRoutes } from 'router/createRouter';

export class ChannelsPage extends React.PureComponent {
  localRoutes = [
    {
      path: routePaths.channels.createNew,
      component: ({ history }) => <ChannelForm history={history} />
    },
    {
      path: routePaths.channels.single,
      component: ({ match, history }) => (
        <ChannelForm update={match.params.id} history={history} />
      )
    },
    { path: routePaths.channels.list, component: () => <ListChannels /> }
  ];

  render() {
    return (
      <div>
        <Helmet>
          <title>Channels</title>
        </Helmet>
        {renderLocalRoutes(this.localRoutes)}
      </div>
    );
  }
}

export default ChannelsPage;
