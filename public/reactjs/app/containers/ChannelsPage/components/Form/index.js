import React from 'react';
import PropTypes from 'prop-types';
import HaveApiComponent from 'components/BaseComponents/HaveApiComponent';
import channelSchema, {exampleDefaultData} from './schema';
import {SmartForm} from 'helpers/form';
import apiService from 'services/api/channels';
import ResourcesApiService from 'services/api/resources';
import routePaths from '../../../../settings/route-paths';
import {createStructuredSelector} from 'reselect';
import {connect} from 'react-redux';
import {makeSelectChannelSetting} from 'store/app/selectors';
import withAsync from '../../../../helpers/react/withAsync/withAsync';
import toast from '../../../../helpers/user-interface/toast';
import * as actions from 'store/app/actions';
import ButtonLoading from 'components/ButtonLoading';
import handleCommonChannel from 'containers/ChannelsPage/components/Common/handle';

@connect(createStructuredSelector({channelSetting: makeSelectChannelSetting()}))
@withAsync()
class ChannelForm extends HaveApiComponent {

  pageTitle = this.props.update ? 'Update Channel' : 'Add Channel';

  static async preload(props) {
    const schema = {...channelSchema};

    let updateData = exampleDefaultData;
    const {data} = await ResourcesApiService.getList({
      status: 1,
      channel_id: this.props.channelSetting.channel_id
    });

    if (props.update) {
      schema.wipe_id.data = data.map(e => ({
        name: e.title,
        value: e.id
      }));
      updateData = await apiService.getOne(this.props.update);
    }
    else {
      delete schema.wipe_id;
      delete schema.resource_path;
      delete schema.thumbnail_storage;
    }
    return {
      schema,
      updateData
    };
  }

  handleSubmit = async data => {
    const {update, history} = this.props;
    if (update) {
      this.callApi(apiService.update(update, data), () => {
        toast.success('Updated Channel successfully');
        this.props.dispatch(actions.refreshChannelSetting());
      });
    } else {
      this.callApi(apiService.create(data), () => {
        toast.success('Added Channel successfully');
        history.push(routePaths.channels.list);
      });
    }
  };

  render() {
    const {update, preloadData} = this.props;
    const {updateData, schema} = preloadData;
    const {loading} = this.apiState;

    return (
        <div>
          <div className="row">
            <div className="col-md-8">
              <h1 className="page-title">{this.pageTitle}</h1>
            </div>
          </div>
          <div className="portlet light">
            {update && !updateData ? null : (
              <SmartForm
                  ref={e => (this.form = e)}
                  className="portlet light "
                  loading={loading}
                  defaultData={updateData}
                  schema={schema}
                  onSubmit={this.handleSubmit}
                  isShowSubmit={false}
                  textButton="Save"
              >
                <div className="form-actions">
                  <div className="row">
                    <div className="col-md-offset-3 col-md-9">
                      <div className="col-md-offset-0">
                        <ButtonLoading
                            type="submit"
                            className="pull-left btn green">
                          Save
                        </ButtonLoading>
                      </div>
                      {
                        !update ? null : (
                            <div className="col-md-offset-1">
                              <ButtonLoading
                                  type="button"
                                  className="pull-left btn red"
                                  onClick={e => handleCommonChannel.resetPremiere(update, e)}
                              >
                                Reset Counter
                              </ButtonLoading>
                            </div>
                        )
                      }

                    </div>
                  </div>
                </div>
              </SmartForm>

            )}
          </div>
        </div>
    );
  }
}

ChannelForm.propsTypes = {
  update: PropTypes.number
};

export default ChannelForm;
