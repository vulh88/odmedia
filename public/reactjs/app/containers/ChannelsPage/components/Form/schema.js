import {schema, commonSchema} from 'helpers/form';

const radioYesNo = {
  type: 'radio',
  validate: schema.number().oneOf([1, 0]).required(),
  inline: true,
  data: [
    {
      name: 'Yes',
      value: 1
    },
    {
      name: 'No',
      value: 0
    }
  ]
};

const channelFormSchema = {
  name: {
    label: 'Name',
    type: 'text',
    validate: schema.string().trim().required()
  },
  playout_control_port: {
    label: 'Playout Control Port',
    type: 'number',
    validate: commonSchema.integer(),
  },
  needs_content_download: {
    label: 'Needs content Download',
    ...radioYesNo,
  },
  resource_path: {
    label: 'Resource path',
    type: 'text',
    validate: schema.string().trim().required(),
    disabled: true
  },
  thumbnail_storage: {
    label: 'Thumbnail storage',
    type: 'text',
    validate: schema.string().trim().required(),
    disabled: true
  },
  wipe_id: {
    label: 'Wipe id',
    type: 'select',
    data: [],
  },
  enable_timeline: {
    label: 'Enable Timeline',
    ...radioYesNo,
  },
};


// for testing, remove if no need
export const exampleDefaultData = {
  name: null,
  playout_control_port: null,
  needs_content_download: 1,
  resource_path: null,
  thumbnail_storage: null,
  default_content_type: 0,
  default_overlay_type: 0,
  resource_folder_id: 12,
  wipe_id: 0,
  enable_timeline: 1
};

export default channelFormSchema;
