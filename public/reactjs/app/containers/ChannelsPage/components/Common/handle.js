import HaveApiComponent from 'components/BaseComponents/HaveApiComponent';
import apiService from '../../../../services/api/channels';
import toast from '../../../../helpers/user-interface/toast';
import quickAlert from 'helpers/user-interface/quick-alert';

class HandleCommonChannel extends HaveApiComponent {

  resetPremiere = async (channelId, e = null) => {
    if (e) {
      e.preventDefault();
    }
    if (await quickAlert.confirm(`All premiere labels will be stripped. Are you sure?`))
    {
      await this.callApi(apiService.resetPremiere(channelId), () => {
            toast.success('All premiere have stripped successfully');

          },({errors}) => {
            errors.map(e => {
              toast.error(e, 'Error message', {timeOut: 8000});
            });
          },
          {autoShowError: false}
      );
    }

  }
}

const handleCommonChannel = new HandleCommonChannel();
export default handleCommonChannel;