import React from 'react';
import apiService from '../../../../services/api/channels';
import ManageTable from '../../../../components/ManageTable';
import { Link, withRouter } from 'react-router-dom';
import { TabsLayout } from '../../../../components/Tabs';
import ListingPage from '../../../../components/BaseComponents/ListingPage';
import routePaths from '../../../../settings/route-paths';
import locationHelper from '../../../../helpers/location';
import toast from 'helpers/user-interface/toast';
import handleCommonChannel from 'containers/ChannelsPage/components/Common/handle'
import {connect} from 'react-redux';
import {createStructuredSelector} from "reselect";
import {makeSelectChannelSetting} from 'store/app/selectors';

@connect(createStructuredSelector({channelSetting: makeSelectChannelSetting()}))
@withRouter
class ListChannels extends ListingPage {
  restApiService = apiService;

  columns = [
    {
      name: 'ID',
      value: e => e.id,
      sort: 'id',
      style: {
        width: 60,
        textAlign: 'center',
        justifyContent: 'center',
        flexGrow: 0
      }
    },
    {
      name: 'Name',
      value: e => e.name,
      sort: 'name'
    },
    {
      name: 'Thumbnail storage',
      value: e => e.thumbnail_storage,
      sort: 'thumbnail_storage'
    },
    {
      name: 'Resource path',
      value: e => e.resource_path,
      sort: 'resource_path'
    }
  ];

  get actions() {
    const { status } = this.getQueryParams();
    if (!status) {
      return [
        {
          icon: 'fa fa-pencil',
          handler: e => this.handleEditItem(e)
        },
        {
          icon: 'fa fa-refresh',
          handler: e => handleCommonChannel.resetPremiere(e.id),
        },
        {
          icon: 'fa fa-trash white',
          handler: e => this.handleMoveToTrash(e),
          needConfirm: true,
          backgroundColor: 'red'
        }
      ];
    }
    if (status === 'trash') {
      return [
        {
          icon: 'fa fa-repeat',
          handler: e => this.handleClickRefresh(e.id)
        },
        {
          icon: 'fa fa-remove white',
          needConfirm: true,
          handler: e => this.handleClickDelete(e.id),
          backgroundColor: 'red'
        }
      ];
    }
  }

  handleClickRefresh = async id => {
    this.callApi(apiService.restoreFromTrash(id), () => {
      toast.success('Successfully restored from trash');
      setTimeout(() => {
        this.refreshData();
      }, 300);
    });
  };

  handleMoveToTrash = async item => {
    this.callApi(apiService.moveToTrash(item.id), () => {
      toast.success('Successfully moved to trash');
      setTimeout(() => {
        this.refreshData();
      }, 300);
    });
  };

  handleClickDelete = async id => {
    this.callApi(apiService.delete(id), () => {
      toast.success('Successfully deleted');
      setTimeout(() => {
        this.refreshData();
      }, 300);
    });
  };

  handleEditItem(item) {
    const { history } = this.props;
    history.push(
      locationHelper.getLink(routePaths.channels.single, { id: item.id })
    );
  }

  render() {
    const { data, paging, loading, success, errors } = this.apiState;
    const { columns, actions } = this;
    const query = this.getQueryParams();
    const tabLabels = [
      {
        name: 'All',
        link: routePaths.channels.list,
        isActive: !query.status
      },
      {
        name: 'Trash',
        link: `${routePaths.channels.list}?status=trash`,
        isActive: query.status === 'trash'
      }
    ];
    return (
      <div>
        <div className="row">
          <div className="col-md-8">
            <h1 className="page-title">Channels</h1>
          </div>
          <div className="col-md-4 text-right">
            <Link
              className="btn green btn-actions"
              to={routePaths.channels.createNew}
            >
              <i className="icon-plus" />
              <span> Add New</span>
            </Link>
          </div>
          <div className="clearfix" />
        </div>
        <TabsLayout labelList={tabLabels}>
          <ManageTable
            loading={loading}
            data={data || []}
            paging={paging || {}}
            handleChangePage={this.handlePageChange}
            handleLimitChange={this.handleLimitChange}
            handleSortBy={this.handleSortBy}
            columns={columns}
            actions={actions}
            styleActions={{ width: 140, flexGrow: 0 }}
          />
        </TabsLayout>
      </div>
    );
  }
}

export default ListChannels;
