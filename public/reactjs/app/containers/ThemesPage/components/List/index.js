import React from 'react';
import { ThemesApiService } from '../../../../services/api/themes';
import ManageTable from '../../../../components/ManageTable';
import { Link, withRouter } from 'react-router-dom';
import { TabsLink } from '../../../../components/Tabs';
import locationHelper from '../../../../helpers/location';
import routePaths from '../../../../settings/route-paths';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import toast from 'helpers/user-interface/toast';

import {
  makeSelectChannel,
  makeSelectError as makeSelectErrorGlobal,
  makeSelectLoadingChannel
} from 'store/app/selectors';
import ListingPageWithChannel from '../../../../components/BaseComponents/ListingPageWithChannel';

@connect(
  createStructuredSelector({
    errorGlobal: makeSelectErrorGlobal(),
    loadingChannel: makeSelectLoadingChannel(),
    currentChannel: makeSelectChannel()
  })
)
@withRouter
class ListThemes extends ListingPageWithChannel {
  restApiService = new ThemesApiService();

  columns = [
    {
      name: 'ID',
      value: e => e.id,
      sort: 'id',
      style: {
        width: 60,
        textAlign: 'center',
        justifyContent: 'center',
        flexGrow: 0
      }
    },
    {
      name: 'Name',
      value: e => e.name,
      sort: 'name'
    },
    {
      name: 'Class',
      value: e => e.class
    }
  ];

  actions = [
    {
      icon: 'fa fa-pencil',
      handler: e => this.handleEditItem(e)
    },
    {
      icon: 'fa fa-remove white',
      handler: e => this.handleDeleteItem(e),
      needConfirm: true,
      backgroundColor: 'red'
    }
  ];

  handleDeleteItem = async item => {
    this.callApi(this.restApiService.delete(item.id), () => {
      toast.success('Successfully deleted');
      setTimeout(() => {
        this.refreshData();
      }, 300);
    });
  };

  handleEditItem(item) {
    const { history } = this.props;
    history.push(
      locationHelper.getLink(routePaths.themes.single, { id: item.id })
    );
  }

  render() {
    const { data, paging, loading } = this.apiState;
    const { columns, actions } = this;

    return (
      <div>
        <div className="row">
          <div className="col-md-8">
            <h1 className="page-title">Themes</h1>
          </div>
          <div className="col-md-4 text-right">
            <Link
              className="btn green btn-actions"
              to={routePaths.themes.createNew}
            >
              <i className="icon-plus" />
              <span> Add New</span>
            </Link>
          </div>
          <div className="clearfix" />
        </div>
        <TabsLink
          indexActive={(location.state && location.state.indexActive) || 0}
          labelList={[
            {
              label: 'Default',
              linkTo: routePaths.themes.list,
              state: { indexActive: 1 },
              content: (
                <div>
                  <ManageTable
                    loading={loading}
                    tools={true}
                    handleKeywordChange={this.handleKeywordChange}
                    data={data || []}
                    paging={paging || {}}
                    handleChangePage={this.handlePageChange}
                    handleLimitChange={this.handleLimitChange}
                    handleSortBy={this.handleSortBy}
                    columns={columns}
                    actions={actions}
                    styleActions={{ width: 120, flexGrow: 0 }}
                  />
                </div>
              ),
              hidden: true
            }
          ]}
        />
      </div>
    );
  }
}

export default ListThemes;
