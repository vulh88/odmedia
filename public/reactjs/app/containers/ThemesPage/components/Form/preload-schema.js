import resourcesApiService from '../../../../services/api/resources';

export async function preloadSchema(defaultSchema) {
  const schema = {...defaultSchema};
  const {data} = await resourcesApiService.getList({
    only_overlay_resources: 1,
    count: -1
  });
  const addingOptions = [schema.default_osd_id.data[0]];
  if (Array.isArray(data)) {
    data.map(e => {
      addingOptions.push({
        value: e.id,
        name: e.title
      });
    });
    schema.default_osd_id.data = addingOptions;
  }
  return {
    schema
  };
}
