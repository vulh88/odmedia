/* eslint-disable no-template-curly-in-string */
import {schema, commonSchema} from 'helpers/form';
import {default_osd_id} from 'common/schema-fields';

const themeSchema = {
  name: {
    label: 'Name',
    type: 'text',
    validate: schema.string().required()
  },
  default_osd_id,
  osd_loop: {
    label: false,
    type: 'checkbox',
    validate: commonSchema.checkbox(),
    data: [
      {
        name: 'OSD loop',
        value: 1
      }
    ]
  },
  web_code: {
    label: 'Web code',
    type: 'textarea',
    validate: schema.string(),
    domProps: {
      rows: 5
    }
  },
  active: {
    label: false,
    type: 'checkbox',
    validate: commonSchema.checkbox(),
    data: [
      {
        name: 'Active',
        value: 1,
        domProps: {defaultChecked: true}
      }
    ]
  }
};



export const defaultContentTypeForm = {
  is_content: 0,
  osd_loop: 0,
  wipe: 0
};


export default themeSchema;
