import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';

import HaveApiComponent from 'components/BaseComponents/HaveApiComponent';

import {makeSelectChannel} from 'store/app/selectors';

import themeSchema, {defaultContentTypeForm} from './schema';
import {SmartForm} from '../../../../helpers/form';
import apiService from '../../../../services/api/themes';
import withAsync from 'helpers/react/withAsync/withAsync';
import {preloadSchema} from './preload-schema';
import routePaths from '../../../../settings/route-paths';
import toast from '../../../../helpers/user-interface/toast';

@withAsync()
class ThemeForm extends HaveApiComponent {
  pageTitle = this.props.update ? 'Update Theme' : 'Add Theme';

  static preload = () => preloadSchema(themeSchema);

  handleSubmit = async params => {
    const {update, currentChannel, history} = this.props;
    const data = {...params, channel_id: currentChannel.id};
    if (update) {
      this.callApi(apiService.update(update, data), () => {
        toast.success('Updated Theme Successfully');
      });
    } else {
      this.callApi(
          apiService.create(data),
          () => {
            toast.success('Added Theme Successfully');
            history.push(routePaths.themes.list);
          }
      );
    }
  };

  componentDidMount() {
    if (this.props.update) {
      this.callApi(
          apiService.getOne(this.props.update),
          data => {
            if (data.default_osd_id === null) {
              data.default_osd_id = '';
            }
            this.apiState.updateData = data;
          }
      );
    }
  }

  render() {
    const {update} = this.props;
    const {schema} = this.props.preloadData;
    const {loading, updateData} = this.apiState;
    const defaultData = update ? updateData : defaultContentTypeForm;

    return (
        <div>
          <div className="row">
            <div className="col-md-8">
              <h1 className="page-title">{this.pageTitle}</h1>
            </div>
          </div>
          <div className="portlet light">
            {update && !updateData ? null : (
                <SmartForm
                    ref={e => (this.form = e)}
                    loading={loading}
                    defaultData={defaultData}
                    schema={schema}
                    onSubmit={this.handleSubmit}
                    textButton={'Save'}
                />
            )}
          </div>
        </div>
    );
  }
}

ThemeForm.propsTypes = {
  currentChannel: PropTypes.object,
  update: PropTypes.number
};

const mapStateToProps = createStructuredSelector({
  currentChannel: makeSelectChannel()
});

export default connect(
    mapStateToProps,
    null
)(ThemeForm);
