import React from 'react';
import { Helmet } from 'react-helmet';
import ListThemes from './components/List';
import ThemeForm from './components/Form';
import routePaths from 'settings/route-paths';
import { renderLocalRoutes } from 'router/createRouter';

export class ThemesPage extends React.PureComponent {
  localRoutes = [
    {
      path: routePaths.themes.createNew,
      component: ({ history }) => <ThemeForm history={history} />
    },
    {
      path: routePaths.themes.single,
      component: ({ match, history }) => (
        <ThemeForm update={match.params.id} history={history} />
      )
    },
    { path: routePaths.themes.list, component: () => <ListThemes /> }
  ];

  render() {
    return (
      <div>
        <Helmet>
          <title>Themes</title>
        </Helmet>
        {renderLocalRoutes(this.localRoutes)}
      </div>
    );
  }
}

export default ThemesPage;
