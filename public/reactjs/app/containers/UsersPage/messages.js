import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'LoginPage.title',
    defaultMessage: 'Sign In'
  },
  headerCreateUser: {
    id: 'odmedia.containers.CreatePage.header',
    defaultMessage: 'Create User'
  },

  firstNameRequired: {
    id: 'odmedia.validate.required.firstName',
    defaultMessage: 'The first name is required'
  },
  lastNameRequired: {
    id: 'odmedia.validate.required.lastName',
    defaultMessage: 'The last name is required'
  },
  emailRequired: {
    id: 'odmedia.validate.required.email',
    defaultMessage: 'The email is required'
  },
  passwordRequired: {
    id: 'odmedia.validate.required.password',
    defaultMessage: 'The password is required'
  },
  passwordConfirmRequired: {
    id: 'odmedia.validate.required.passwordConfirm',
    defaultMessage: 'The password confirmation is required'
  },

  isEmail: {
    id: 'odmedia.validate.validator.email',
    defaultMessage: 'The email is invalid'
  },
  lengthPassword: {
    id: 'odmedia.validate.validator.lengthPassword',
    defaultMessage: 'The password must be at least {min} characters.'
  },
  matchPassword: {
    id: 'odmedia.validate.validator.matchPassword',
    defaultMessage: 'The password confirmation do not match'
  }
});
