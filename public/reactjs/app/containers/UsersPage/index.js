import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import injectReducer from 'store/utils/injectReducer';
import injectSaga from 'store/utils/injectSaga';

import UserTable from './components/UserTable';
import UserForm from './components/UserForm';
import UserView from './components/UserView';

import reducer from './stores/reducer';
import saga from './stores/saga';

import { makeSelectError, makeSelectLoading } from './stores/selectors';
import routePaths from '../../settings/route-paths';
import { renderLocalRoutes } from '../../router/createRouter';
import userFormSchema, {
  userFormUpdateSchema
} from './components/UserForm/schema';

export class UsersPage extends React.PureComponent {
  static propTypes = {
    loading: PropTypes.bool,
    error: PropTypes.oneOfType([PropTypes.bool, PropTypes.object])
  };

  localRoutes = [
    {
      path: routePaths.users.createNew,
      component: ({ match, history }) => (
        <UserForm
          match={match}
          isUpdate={false}
          schema={userFormSchema}
          history={history}
        />
      )
    },
    {
      path: routePaths.users.single,
      component: ({ match, history }) => (
        <UserForm
          match={match}
          isUpdate={true}
          schema={userFormUpdateSchema}
          history={history}
        />
      )
    },
    {
      path: routePaths.users.view,
      component: ({ match, history }) => (
          <UserView
              isUpdate={true}
              match={match}
              history={history}
          />
      )
    },
    { path: routePaths.users.list, component: () => <UserTable /> }
  ];

  render() {
    return (
      <div>
        <Helmet>
          <title>Users</title>
          <meta
            name="description"
            content="A React.js Boilerplate application Users page"
          />
        </Helmet>
        {renderLocalRoutes(this.localRoutes)}
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  error: makeSelectError(),
  loading: makeSelectLoading()
});

const withConnect = connect(
  mapStateToProps,
  null
);

const withReducer = injectReducer({ key: 'users', reducer });
const withSaga = injectSaga({ key: 'users', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(UsersPage);
