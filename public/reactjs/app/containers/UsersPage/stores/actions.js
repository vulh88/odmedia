export const REQUEST_API = 'users/REQUEST_API';
export const REQUEST_API_SUCCESS = 'users/REQUEST_API_SUCCESS';
export const REQUEST_API_ERROR = 'users/REQUEST_API_ERROR';

export const REQUEST_USERS = 'users/REQUEST_USERS';
export const REQUEST_USERS_SUCCESS = 'users/REQUEST_USERS_SUCCESS';
export const REQUEST_USERS_ERROR = 'users/REQUEST_USERS_ERROR';

export const REQUEST_MOVE_TRASH = 'users/REQUEST_MOVE_TRASH';
export const MOVE_TRASH_SUCCESS = 'users/MOVE_TRASH_SUCCESS';
export const MOVE_TRASH_ERROR = 'users/MOVE_TRASH_ERROR';

export const REQUEST_EDIT_USER = 'users/REQUEST_EDIT_USER';
export const REQUEST_EDIT_USER_SUCCESS = 'users/REQUEST_EDIT_USER_SUCCESS';
export const REQUEST_EDIT_USER_ERROR = 'users/REQUEST_EDIT_USER_ERROR';

export const REQUEST_DELETE_USER = 'users/REQUEST_DELETE_USER';
export const REQUEST_DELETE_USER_SUCCESS = 'users/REQUEST_DELETE_USER_SUCCESS';
export const REQUEST_DELETE_USER_ERROR = 'users/REQUEST_DELETE_USER_ERROR';

export const REQUEST_RESTORE_USER = 'users/REQUEST_RESTORE_USER';
export const REQUEST_RESTORE_USER_SUCCESS =
  'users/REQUEST_RESTORE_USER_SUCCESS';
export const REQUEST_RESTORE_USER_ERROR = 'users/REQUEST_RESTORE_USER_ERROR';

export const REQUEST_CREATE_USER = 'createUser/REQUEST_CREATE_USER';
export const CREATE_USER_SUCCESS = 'createUser/CREATE_USER_SUCCESS';
export const CREATE_USER_ERROR = 'createUser/CREATE_USER_ERROR';
export const GET_USER = 'editUser/GET_USER';
export const GET_USER_SUCCESS = 'editUser/GET_USER_SUCCESS';
export const GET_USER_ERROR = 'editUser/GET_USER_ERROR';

export const REQUEST_UPDATE_USER = 'editUser/REQUEST_UPDATE_USER';
export const UPDATE_USER_SUCCESS = 'editUser/UPDATE_USER_SUCCESS';
export const UPDATE_USER_ERROR = 'editUser/UPDATE_USER_ERROR';
export const EXIT_EDITING_USER = 'editUser/EXIT_EDITING_USER';

export const requestUsers = payload => ({ type: REQUEST_USERS, payload });

export const requestMoveTrash = payload => ({
  type: REQUEST_MOVE_TRASH,
  payload
});

export const requestEditUser = payload => ({
  type: REQUEST_EDIT_USER,
  payload
});

export const exitEditUser = payload => ({
  type: EXIT_EDITING_USER,
  payload
});

export const requestDeleteUser = payload => ({
  type: REQUEST_DELETE_USER,
  payload
});

export const requestRestoreUser = payload => ({
  type: REQUEST_RESTORE_USER,
  payload
});

export const requestCreateUser = payload => ({
  type: REQUEST_CREATE_USER,
  payload
});

export const getUser = payload => ({ type: GET_USER, payload });

export const requestUpdateUser = payload => ({
  type: REQUEST_UPDATE_USER,
  payload
});
