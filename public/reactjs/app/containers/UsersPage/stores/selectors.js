import {createSelector} from 'reselect';

const selectUsers = state => state.get('users');

const makeSelectError = () =>
    createSelector(selectUsers, loginState => loginState.get('error'));

const makeSelectLoading = () =>
    createSelector(selectUsers, loginState => loginState.get('loading'));

const makeSelectListUsers = () =>
    createSelector(selectUsers, loginState => loginState.get('listUsers'));

const makeSelectPaging = () =>
    createSelector(selectUsers, loginState =>
        loginState.get('paging')
    );

const makeSelectUserEditing = () =>
    createSelector(selectUsers, editUserState =>
        editUserState.get('userEditing')
    );

export {
  selectUsers,
  makeSelectPaging,
  makeSelectUserEditing,
  makeSelectListUsers,
  makeSelectLoading,
  makeSelectError
};
