import { fromJS } from 'immutable';
import * as actions from './actions';

const initialState = fromJS({
  loading: false,
  error: false,
  listUsers: null,
  userEditing: null,
  paging: {
    currentPage: null,
    totalPage: null,
    limit: null,
    totalData: null
  }
});

function dashboardReducer(state = initialState, action) {
  switch (action.type) {
    case actions.REQUEST_API:
      return state.set('loading', true).set('error', false);
    case actions.REQUEST_API_SUCCESS:
      return state.set('loading', false);
    case actions.REQUEST_API_ERROR:
      return state.set('loading', false).set('error', action.payload);

    case actions.REQUEST_USERS_SUCCESS:
      return state
        .set('listUsers', action.payload.data)
        .set('paging', action.payload.paging);

    case actions.EXIT_EDITING_USER:
      return state.set('userEditing', null);
    case actions.GET_USER_SUCCESS:
      return state.set('userEditing', action.payload);
    case actions.UPDATE_USER_SUCCESS:
      return state.set('userEditing', null);
    default:
      return state;
  }
}

export default dashboardReducer;
