import apiCaller from 'services/apiCaller';
import apiPaths from 'settings/api/api-path';

const count = 10;

export const requestUsers = params =>
    apiCaller
        .get(apiPaths.users.base, {
          query: {
            page: params.page,
            count,
            ...params
          }
        });

export const requestMoveTrash = ({id}) =>
    apiCaller
        .put(
            apiPaths.users.moveToTrash,
            {
              inUrl: {id}
            }
        );

export const requestEditUser = ({formEdit}) =>
    apiCaller
        .put(
            apiPaths.users.single,
            {
              inUrl: {id: formEdit.id},
              body: {id: formEdit.id, body: formEdit}
            }
        );

export const requestUsersTrash = params =>
    apiCaller.get(apiPaths.users.base, {
      query: {
        trash: 1,
        page: params.page,
        count
      }
    });

export const requestDeleteUser = ({id}) =>
    apiCaller
        .delete(apiPaths.users.single, {
          inUrl: {id}
        });

export const requestRestoreUser = ({id}) =>
    apiCaller
        .put(apiPaths.users.restoreFromTrash, {inUrl: {id}});

export const requestCreateUser = credentials =>
    apiCaller
        .post(apiPaths.users.base, {
          body: credentials.body
        })
        .then(result => result.data);

export const getUser = ({id}) =>
    apiCaller
        .get(apiPaths.users.single, {inUrl: {id}})
        .then(result => result.data);

export const requestUpdateUser = params =>
    apiCaller
        .put(apiPaths.users.single, {
          body: params.body,
          inUrl: {id: params.id}
        })
        .then(result => result.data);
