import { put, takeLatest, call } from 'redux-saga/effects';
import { push as routerPush } from 'react-router-redux';
import { isFunction } from 'lodash';

import * as actions from './actions';
import * as api from './api';
import routePaths from '../../../settings/route-paths';

function* callApi(apiCall, params) {
  yield put({
    type: actions.REQUEST_API
  });
  try {
    const resp = yield call(apiCall, params);
    yield put({
      type: actions.REQUEST_API_SUCCESS
    });
    return resp;
  } catch (error) {
    yield put({
      type: actions.REQUEST_API_ERROR,
      payload: error
    });
    throw error;
  }
}

const requestUsers = function*(params) {
  try {
    const request = yield callApi(api.requestUsers, params.payload);
    yield put({
      type: actions.REQUEST_USERS_SUCCESS,
      payload: {
        data: request.data,
        paging: request.paging
      }
    });
  } catch (error) {
    yield put({
      type: actions.REQUEST_USERS_ERROR,
      payload: error
    });
  }
};

const requestMoveTrash = function*(params) {
  try {
    yield callApi(api.requestMoveTrash, params.payload);
    yield put({
      type: actions.MOVE_TRASH_SUCCESS
    });
  } catch (error) {
    yield put({
      type: actions.MOVE_TRASH_ERROR,
      payload: error
    });
  }
};

const requestEditUser = function*(params) {
  try {
    const request = yield call(api.requestEditUser, params.payload);
    yield put({
      type: actions.REQUEST_EDIT_USER_SUCCESS,
      payload: request
    });
  } catch (error) {
    yield put({
      type: actions.REQUEST_EDIT_USER_ERROR,
      payload: error
    });
  }
};

const requestDeleteUser = function*(params) {
  try {
    yield callApi(api.requestDeleteUser, params.payload);
    yield put({
      type: actions.REQUEST_DELETE_USER_SUCCESS
    });
  } catch (error) {
    yield put({
      type: actions.REQUEST_DELETE_USER_ERROR,
      payload: error
    });
  }
};

const requestRestoreUser = function*(params) {
  try {
    yield callApi(api.requestRestoreUser, params.payload);
    yield put({
      type: actions.REQUEST_RESTORE_USER_SUCCESS
    });
  } catch (error) {
    yield put({
      type: actions.REQUEST_RESTORE_USER_ERROR,
      payload: error
    });
  }
};

export const requestCreateUser = function*({ payload }) {
  try {
    const result = yield callApi(api.requestCreateUser, payload);
    yield put({
      type: actions.CREATE_USER_SUCCESS,
      payload: result
    });
    if (isFunction(payload.callback)) {
      payload.callback();
    }
  } catch (error) {
    yield put({
      type: actions.CREATE_USER_ERROR,
      payload: error
    });
  }
};

export const getUser = function*({ payload }) {
  try {
    if (payload.id) {
      const result = yield callApi(api.getUser, payload);
      yield put({
        type: actions.GET_USER_SUCCESS,
        payload: result
      });
    } else {
      yield put({
        type: actions.GET_USER_SUCCESS,
        payload: null
      });
    }
  } catch (error) {
    yield put({
      type: actions.GET_USER_ERROR,
      payload: error
    });
  }
};

export const requestUpdateUser = function*({ payload }) {
  try {
    const result = yield callApi(api.requestUpdateUser, payload);
    if (result) {
      yield put({
        type: actions.UPDATE_USER_SUCCESS,
        payload: result
      });
      yield put(routerPush(routePaths.users.list));
    }
  } catch (error) {
    yield put({
      type: actions.UPDATE_USER_ERROR,
      payload: error
    });
  }
};

export default function* Users() {
  yield takeLatest(actions.REQUEST_USERS, requestUsers);
  yield takeLatest(actions.REQUEST_MOVE_TRASH, requestMoveTrash);
  yield takeLatest(actions.REQUEST_EDIT_USER, requestEditUser);
  yield takeLatest(actions.REQUEST_DELETE_USER, requestDeleteUser);
  yield takeLatest(actions.REQUEST_RESTORE_USER, requestRestoreUser);
  yield takeLatest(actions.REQUEST_CREATE_USER, requestCreateUser);
  yield takeLatest(actions.REQUEST_UPDATE_USER, requestUpdateUser);
  yield takeLatest(actions.GET_USER, getUser);
}
