import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import apiService from 'services/api/users';
import HaveApiComponent from 'components/BaseComponents/HaveApiComponent';
import {
  makeSelectError,
  makeSelectLoading,
  makeSelectUserEditing
} from '../../stores/selectors';

import SmartForm from '../../../../helpers/form/SmartForm';
import routePaths from '../../../../settings/route-paths';
import toast from 'helpers/user-interface/toast';

class UserForm extends HaveApiComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    isUpdate: PropTypes.bool.isRequired,
    error: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
    loading: PropTypes.bool,
    userEditing: PropTypes.object,
    schema: PropTypes.object.isRequired
  };

  handleSubmit = async data => {
    const {
      isUpdate,
      match: {params},
      history
    } = this.props;
    if (isUpdate) {
      this.callApi(apiService.update(params.id, data), () => {
        toast.success('Updated User successfully');
      });
    } else {
      this.callApi(apiService.create(data), () => {
        toast.success('Added User successfully');
        history.push(routePaths.users.list);
      });
    }
  };

  componentDidMount() {
    const {
      isUpdate,
      match: {params}
    } = this.props;

    if (isUpdate) {
      this.callApi(
          apiService.getOne(params.id),
          data => (this.apiState.updateData = data)
      );
    }
  }

  render() {
    const {loading} = this.apiState;
    const {isUpdate, schema} = this.props;
    const {updateData} = this.apiState;
    const submitText = isUpdate ? 'Save' : 'Create User';

    return (
        <div>
          <h1 className="page-title">{isUpdate ? 'Update user' : 'Create user'}</h1>
          {isUpdate && !updateData ? null : (
              <SmartForm
                  className="portlet light"
                  ref={e => (this.form = e)}
                  schema={schema}
                  loading={loading}
                  textButton={submitText}
                  defaultData={updateData}
                  onSubmit={this.handleSubmit}
              >
              </SmartForm>
          )}
        </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  error: makeSelectError(),
  loading: makeSelectLoading(),
  userEditing: makeSelectUserEditing()
});

export default connect(
    mapStateToProps,
    null
)(UserForm);
