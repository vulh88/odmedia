import { schema, commonSchema } from 'helpers/form';

/**
 * Schema of userForm. See package yup documentation for schema method
 */
const userFormSchema = {
  first_name: {
    label: 'First Name',
    type: 'text',
    validate: schema.string().required()
  },
  last_name: {
    label: 'Last Name',
    type: 'text',
    validate: schema.string().required()
  },
  email: {
    label: 'Email',
    type: 'email',
    validate: schema
      .string()
      .required()
      .email()
  },
  password: {
    label: 'Password',
    type: 'password',
    validate: schema
      .string()
      .required()
      .min(6)
  },
  password_confirmation: {
    label: 'Password Confirm',
    type: 'password',
    validate: schema
      .string()
      .sameAs(schema.ref('password'))
      .required()
  }
  // active: {
  //   label: false,
  //   type: 'checkbox',
  //   validate: commonSchema.checkbox().default(0),
  //   data: [
  //     {
  //       value: 1,
  //       name: 'Active'
  //     }
  //   ]
  // }
};

export const userFormUpdateSchema = {
  ...userFormSchema,
  password: {
    ...userFormSchema.password,
    validate: schema.lazy(
      value =>
        value
          ? userFormSchema.password.validate
          : schema.mixed().transform(v => null)
    )
  },
  password_confirmation: {
    ...userFormSchema.password_confirmation,
    validate: schema.mixed().when('password', {
      is: val => val,
      then: userFormSchema.password_confirmation.validate,
      otherwise: schema.mixed().oneOf([''], 'Please enter the password first')
    })
  }
};

export default userFormSchema;
