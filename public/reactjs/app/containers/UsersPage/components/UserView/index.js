import React from 'react';
import apiService from 'services/api/users';
import HaveApiComponent from 'components/BaseComponents/HaveApiComponent';
import SmartForm from '../../../../helpers/form/SmartForm';
import withAsync from '../../../../helpers/react/withAsync/withAsync';
import {createUserViewSchema} from './schema';

@withAsync()
class UserView extends HaveApiComponent {

  static async preload(props) {
    const id = props.match.params.id;
    const [userData, channels] = await Promise.all([
      apiService.getOne(id),
      apiService.getChannelsUser({user_id: id})
    ]);

    return {
      schema: createUserViewSchema(userData, channels)
    };
  }

  render() {
    const {preloadData} = this.props;
    const {schema} = preloadData;

    return (
        <div>
          <h1 className="page-title">View user</h1>
          <SmartForm
              className="portlet light user-view"
              ref={e => (this.form = e)}
              schema={schema}
              isShowSubmit={false}
          />

        </div>
    );
  }
}

export default UserView;