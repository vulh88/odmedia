import { schema, commonSchema } from 'helpers/form';
import React from 'react';
import routePaths from '../../../../settings/route-paths';
import locationHelper from '../../../../helpers/location';
import {Link} from 'react-router-dom';

/**
 * Schema of userForm. See package yup documentation for schema method
 */
const userViewSchema = {
  id: {
    label: 'ID',
    type: 'viewOnly'
  },
  email: {
    label: 'Email',
    type: 'viewOnly'
  },
  first_name: {
    label: 'First Name',
    type: 'viewOnly',
  },
  last_name: {
    label: 'Last Name',
    type: 'viewOnly',
  },
  created_at: {
    label: 'Created At',
    type: 'viewOnly',
  },
  channels: {
    label: 'Assigned channels',
    type: 'viewOnly',
    value: ''
  }
};

export function createUserViewSchema(data, channels) {
  const schema = {...userViewSchema};

  Object.entries(schema).map(([k,v]) => {
    if (data[k]) {
      schema[k] = {
        ...schema[k],
        value: data[k]
      };
    }
  });

  if (channels && channels.length) {
    schema.channels.value = channels.map((e, idz) => (
        <span key={idz}>{idz > 0 ? ', ' : ''}<Link to={locationHelper.getLink(routePaths.roles.assignUpdate, {id: data.id, channel_id: e.channel_id})}>{e.name}</Link></span>
    ));
  }
  else {
    schema.channels.value = <span className="label label-default">None</span>;
  }

  return schema;

}

