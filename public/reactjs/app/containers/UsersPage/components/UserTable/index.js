import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { TabsLayout } from 'components/Tabs';
import ListingPage from 'components/BaseComponents/ListingPage';
import ManageTable from 'components/ManageTable';
import toast from 'helpers/user-interface/toast';
import routePaths from '../../../../settings/route-paths';
import locationHelper from '../../../../helpers/location';
import apiService from '../../../../services/api/users';
import {makeSelectUser} from '../../../../store/app/selectors';
import { createStructuredSelector } from 'reselect';
import {connect} from 'react-redux';

@connect(createStructuredSelector({
  user: makeSelectUser()
}))
@withRouter
export class UserTableView extends ListingPage {
  restApiService = apiService;

  get user() {
    return this.props.user;
  }

  columns = [
    {
      name: 'ID',
      value: e => e.id,
      sort: 'id',
      style: {
        width: 60,
        textAlign: 'center',
        justifyContent: 'center',
        flexGrow: 0
      }
    },
    { name: 'First Name', value: e => e.first_name, sort: 'first_name' },
    { name: 'Last Name', value: e => e.last_name, sort: 'last_name' },
    { name: 'Email', value: e => e.email, sort: 'email' },
    { name: 'Created At', value: e => e.created_at, sort: 'created_at' }
  ];

  get actions() {
    const user = this.user;
    const { status } = this.getQueryParams();
    if (!status) {
      return [
        {
          icon: 'fa fa-pencil',
          handler: e => this.handleEditItem(e)
        },
        {
          icon: 'fa fa-trash white',
          handler: e => this.handleMoveToTrash(e),
          needConfirm: true,
          isShow: e => !user || (e.id !== user.id),
          backgroundColor: 'red'
        },
        {
          icon: 'fa fa-search black',
          handler: e => this.handleViewItem(e)

        }
      ];
    }
    if (status === 'trash') {
      return [
        {
          icon: 'fa fa-repeat',
          handler: e => this.handleClickRefresh(e.id)
        },
        {
          icon: 'fa fa-remove white',
          needConfirm: true,
          isShow: e => !user || (e.id !== user.id),
          handler: e => this.handleClickDelete(e.id),
        }
      ];
    }
  }

  handleClickRefresh = async id => {
    this.callApi(apiService.restoreFromTrash(id), () => {
      toast.success('Successfully restored from trash');
      setTimeout(() => {
        this.refreshData();
      }, 300);
    });
  };

  handleMoveToTrash = async item => {
    this.callApi(apiService.moveToTrash(item.id), () => {
      toast.success('Successfully moved to trash');
      setTimeout(() => {
        this.refreshData();
      }, 300);
    });
  };

  handleClickDelete = async id => {
    this.callApi(apiService.delete(id), () => {
      toast.success('Successfully deleted');
      setTimeout(() => {
        this.refreshData();
      }, 300);
    });
  };

  handleEditItem(item) {
    const { history } = this.props;
    history.push(
      locationHelper.getLink(routePaths.users.single, { id: item.id })
    );
  }

  handleViewItem(item) {
    const { history } = this.props;
    history.push(
        locationHelper.getLink(routePaths.users.view, { id: item.id })
    );
  }

  render() {
    const { data, paging, loading, success, errors } = this.apiState;
    const { columns, actions } = this;
    const query = this.getQueryParams();

    const tabLabels = [
      { name: 'All', link: routePaths.users.list, isActive: !query.status },
      {
        name: 'Trash',
        link: `${routePaths.users.list}?status=trash`,
        isActive: query.status === 'trash'
      }
    ];

    return (
      <div>
        <div className="row">
          <div className="col-md-8">
            <h1 className="page-title">Users</h1>
          </div>
          <div className="col-md-4 text-right">
            <Link
              className="btn green btn-actions"
              to={routePaths.users.createNew}
            >
              <i className="icon-plus" />
              <span> Add New</span>
            </Link>
          </div>
        </div>
        <TabsLayout labelList={tabLabels}>
          <ManageTable
            loading={loading}
            data={data || []}
            paging={paging || {}}
            handleChangePage={this.handlePageChange}
            handleLimitChange={this.handleLimitChange}
            handleSortBy={this.handleSortBy}
            columns={columns}
            actions={actions}
            styleActions={{ width: 140, flexGrow: 0 }}
          />
        </TabsLayout>
      </div>
    );
  }
}

export default UserTableView;
