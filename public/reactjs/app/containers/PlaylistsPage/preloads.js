import playlistsApiService from '../../services/api/playlists';
import NotFound from 'containers/NotFoundPage/Loadable';
import apiResponseCodes from '../../settings/api/api-response-codes';

export async function getPlaylist({match}) {
  const playListId = match.params.id;
  try {
    const playlist =  await playlistsApiService.getOne(playListId);
    return playlist;
  }
  catch (error) {
    console.log(error);
    if (error.status === apiResponseCodes.notFound) {
      throw NotFound;
    }
    else {
      throw error;
    }
  }
}

export async function getPlaylistResources({match}) {
  const playListId = match.params.id;
  return playlistsApiService.getListResources(playListId);
}

export async function getVerifyResources({match}) {
  const playListId = match.params.id;
  return playlistsApiService.verifyResources(playListId);
}