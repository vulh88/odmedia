import React from 'react';
import {SmartForm, schema} from '../../../helpers/form/index';
import toast from 'helpers/user-interface/toast';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';
import apiService from '../../../services/api/playlists';
import HaveApiComponent from '../../../components/BaseComponents/HaveApiComponent/index';
import {makeSelectChannel} from 'store/app/selectors';
import locationHelper from 'helpers/location';
import routePaths from 'settings/route-paths';
import moment from 'moment';
import {formatsConfig} from '../../../components/TimelineContainer/config';
import BlockLoading from '../../../components/BlockLoading';
import apiResponseCodes from '../../../settings/api/api-response-codes';
import NotFound from '../../../containers/NotFoundPage';
import withCurrentChannel from '../../../common/hocs/withCurrentChannel';


@withCurrentChannel()
@withRouter
class PlaylistForm extends HaveApiComponent {
  static propsTypes = {
    currentChannel: PropTypes.object,
    updatedCallback: PropTypes.func
  };

  pageTitle = this.updateId ? 'Update Playlist' : 'Add New Playlist';


  get updateId() {
    return this.props.updateId || this.props.match.params.id;
  }

  get playListSchema() {
    const {updateData} = this.apiState;
    const isUpdate = !!this.updateId;
    const res = {
      name: {
        label: 'Name',
        type: 'text',
        validate: schema.string().required()
      },
      channel_id: {
        validate: schema.mixed().default(this.props.currentChannel && this.props.currentChannel.id)
      },
      start_time: {
        label: 'Start time',
        type: 'datetimepicker',
        validate: schema.string().required(),
        min: moment(),
        disabled: isUpdate && updateData.status === 'publish'
      },
      end_time: {
        label: 'End time',
        type: 'datetimepicker',
        validate: schema.string().required(),
        min: moment(),
        disabled: isUpdate && updateData.status === 'publish'
      }
    };
    return res;
  }

  defaultFormValue = {
    start_time: moment().format(formatsConfig.apiDateTime),
    end_time: moment().add(1,'hours').format(formatsConfig.apiDateTime)
  };

  handleSubmit = async (data) => {
    const {updateId} = this;
    if (updateId) {
      this.callApi(apiService.update(updateId, data), (resp) => {
        toast.success('Updated Playlist successfully');
        const editLink = locationHelper.getLink(routePaths.playlists.scheduling, {id: updateId});
        if (this.props.updatedCallback) {
          this.props.updatedCallback(resp.data);
          if (this.props.closePopup) {
            this.props.closePopup(resp.data);
          }
        }
        else {
          this.props.history.push(editLink);
        }

      });
    } else {
      if (data.channel_id) {
        this.callApi(apiService.create(data), (resp) => {
          toast.success('Added Playlist successfully');
          this.form.reset();
          const editLink =
              locationHelper.getLink(routePaths.playlists.scheduling, {id: resp.data.id});
          this.props.history.push(editLink);
        });
      } else {
        alert('Please select your working channel');
      }
    }
  };

  componentDidMount() {
    if (this.updateId) {
      this.callApi(
          apiService.getOne(this.updateId),
          data => this.apiState.updateData = data,
          ({status}) => {
            this.apiState.isNotFound = status === apiResponseCodes.notFound
          }
      );
    }
  }


  render() {
    const {updateId} = this;
    const {loading, updateData, isNotFound} = this.apiState;

    if (isNotFound) {
      return <NotFound/>;
    }

    return (
        <div className="row position-relative">
          {
            updateId && !updateData ? <BlockLoading/> :
                <SmartForm
                    ref={e => this.form = e}
                    loading={loading}
                    defaultData={updateData || this.defaultFormValue}
                    textButton="Save"
                    schema={this.playListSchema}
                    onSubmit={this.handleSubmit}/>
          }
        </div>
    );
  }
}

export default PlaylistForm;
