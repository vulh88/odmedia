import React from 'react';
import TimelineContainer from 'components/TimelineContainer';
import withAsync from '../../../helpers/react/withAsync/withAsync';
import {getPlaylist, getPlaylistResources} from '../preloads';
import routePaths from 'settings/route-paths';
import {createStructuredSelector} from "reselect";
import {makeSelectChannelSetting} from 'store/app/selectors';
import {connect} from 'react-redux';
import {checkEnableTimeline} from 'common/link-generator/disable-links';
import SnapshotLoading from 'components/SnapshotLoading';

@withAsync()
@connect(createStructuredSelector({channelSetting: makeSelectChannelSetting()}))
class PlaylistTimeLine extends React.PureComponent {
  static async preload(props) {
    const [playlist, resources] = await Promise.all([
      getPlaylist(props),
      getPlaylistResources(props)
    ]);
    return {
      playlist,
      resources
    };
  }

  state = {
    loading: false,
    playlist: this.props.preloadData.playlist,
    resources: this.props.preloadData.resources
  };

  handlePlaylistChange = async () => {
    this.setState({
      loading: true
    });
    const [playlist, resources] = await Promise.all([
      getPlaylist(this.props),
      getPlaylistResources(this.props)
    ]);

    this.setState({
      loading: false,
      resources,
      playlist
    });
  };

  componentWillMount() {
    this.redirectTimeline();
  }

  componentDidUpdate(nextProps, nextState) {
    if (nextProps !== this.props) {
      this.redirectTimeline();
    }
  }

  /**
   * Check if not allow access scheduling page then redirect to list page
   */
  redirectTimeline = () => {
    if (checkEnableTimeline(this.props.channelSetting)) {
      this.props.history.push(routePaths.playlists.list);
    }
  };

  render() {
    const {loading, playlist, resources} = this.state;
    return (
        <div className="timeline-page">
          <SnapshotLoading  name="timeline" loading={loading}>
            {
              () =>  <TimelineContainer onPlaylistChange={this.handlePlaylistChange} playlist={playlist} resources={resources || []}/>
            }
          </SnapshotLoading>
        </div>
    );
  }
}

export default PlaylistTimeLine;
