import React from 'react';
import ListPlaylists from './List/index';
import NotFound from 'containers/NotFoundPage/index';
import routePaths from 'settings/route-paths';
import StepLayout from './StepLayout';
import TimelineOverview from '../../components/TimelineContainer/TimelineOverview';

const playlistLocalRoutes = [
  {
    path: routePaths.playlists.list,
    component: ListPlaylists
  },
  {
    path: routePaths.playlists.single,
    exact: false,
    component: StepLayout
  },
  {
    path: routePaths.playlists.createNew,
    component: StepLayout
  },
  {
    path: routePaths.playlists.overview,
    component: TimelineOverview
  },
  {
    path: '*',
    component: NotFound
  }
];

export default playlistLocalRoutes;
