import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { PlaylistsApiService } from '../../../services/api/playlists';
import ManageTable from '../../../components/ManageTable/index';
import toast from 'helpers/user-interface/toast';
import routePaths from 'settings/route-paths';
import locationHelper from '../../../helpers/location';
import ListingPageWithChannel from '../../../components/BaseComponents/ListingPageWithChannel';
import { makeSelectChannel, makeSelectChannelSetting } from 'store/app/selectors';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {duplicatePlaylist} from 'common/shema-preloads';
import quickAlert from '../../../helpers/user-interface/quick-alert';
import playlistsApiService from '../../../services/api/playlists';
import withReload from '../../../helpers/react/withReload/withReload';
import {removeActionPlaylistByChannel} from 'common/link-generator/disable-links';


export const playListColumns = [
  {
    name: 'ID',
    value: e => e.id,
    sort: 'id',
    style: {
      width: 60,
      textAlign: 'center',
      justifyContent: 'center',
      flexGrow: 0
    }
  },
  {
    name: 'Name',
    value: e => e.name,
    sort: 'name'
  },
  {
    name: 'Start time',
    value: e => e.start_time,
    sort: 'start_time'
  },
  {
    name: 'End time',
    value: e => e.end_time,

  }
];

@withReload()
@connect(createStructuredSelector({currentChannel: makeSelectChannel()}))
@connect(createStructuredSelector({channelSetting: makeSelectChannelSetting()}))
@withRouter
class ListPlaylists extends ListingPageWithChannel {
  restApiService = new PlaylistsApiService();

  columns = [...playListColumns];

  getQueryParams() {
    const query = super.getQueryParams();
    if (!query.status) {
      query.status = 'pending';
    }
    return query;
  }

  get actions() {
    const { status } = this.getQueryParams();
    if (status === 'publish') {
      return [
        {
          icon: 'fa fa-pencil',
          handler: e => this.handleEditItem(e.id)
        },
        {
          icon: 'fa fa-clock-o',
          handler: e => this.props.history.push(locationHelper.getLink(routePaths.playlists.scheduling, {id: e.id}))
        },
        {
          icon: 'fa fa-clone',
          handler: e => this.handleDuplicate(e.id),
        },
        {
          icon: 'fa fa-trash white',
          handler: e => this.handleClickTrash(e.id),
          needConfirm: true,
          backgroundColor: 'red'
        }
      ];
    }
    if (status === 'pending') {
      return [
        {
          icon: 'fa fa-pencil',
          handler: e => this.handleEditItem(e.id)
        },
        {
          icon: 'fa fa-clock-o',
          handler: e => this.props.history.push(locationHelper.getLink(routePaths.playlists.scheduling, {id: e.id}))
        },
        {
          icon: 'fa fa-clone',
          handler: e => this.handleDuplicate(e.id, 1),
        },
        {
          icon: 'fa fa-trash white',
          handler: e => this.handleClickTrash(e.id),
          needConfirm: true,
          backgroundColor: 'red'
        }
      ];
    }
    if (status === 'trash') {
      return [
        {
          icon: 'fa fa-repeat',
          handler: e => this.handleClickRefresh(e.id)
        },
        {
          icon: 'fa fa-remove white',
          needConfirm: true,
          handler: e => this.handleClickDelete(e.id),
          backgroundColor: 'red'
        }
      ];
    }
    return [];
  }

  handleClickDelete = async id => {
    this.callApi(this.restApiService.delete(id), () => {
      toast.success('Successfully deleted');
      setTimeout(() => {
        this.refreshData();
      }, 300);
    });
  };

  handleClickTrash = async id => {
    this.callApi(this.restApiService.moveToTrash(id), () => {
      toast.success('Successfully moved to trash');
      setTimeout(() => {
        this.refreshData();
      }, 300);
    });
  };

  handleClickRefresh = async id => {
    this.callApi(this.restApiService.restoreFromTrash(id), () => {
      toast.success('Successfully restored from trash');
      setTimeout(() => {
        this.refreshData();
      }, 300);
    });
  };

  handleEditItem(id) {
    const { history } = this.props;
    const link = locationHelper.getLink(routePaths.playlists.single, { id });
    history.push(link);
  }

  async handleDuplicate(id, tabPending = 0) {
    if (await quickAlert.confirm(`Would you like to duplicate this playlist ?`)) {
      this.callApi(
        playlistsApiService.duplicatePlaylist({
          id: id
        }),
        async () => {
          toast.success('The playlist was cloned successfully');
          if (tabPending) {
            this.props.reload();
          }
        }
      );
    }
  };

  render() {
    const { data, paging, loading } = this.apiState;
    const { columns, actions } = this;
    const query = this.getQueryParams();

    return (
      <div>
        <div className="row">
          <div className="col-md-8">
            <h1 className="page-title">Playlists</h1>
          </div>
          <div className="col-md-4 text-right">
            <Link
              className="btn green btn-actions"
              to={routePaths.playlists.createNew}
            >
              <i className="icon-plus" />
              <span> Add New</span>
            </Link>
          </div>
          <div className="clearfix" />
        </div>
        <div className="portlet box light">
          <div className="portlet-body">
            <div className="tabbable-line">
              <ul className="nav nav-tabs">
                <li className={query.status === 'publish' ? 'active' : ''}>
                  <Link to={`${routePaths.playlists.list}?status=publish`}>
                    Publish
                  </Link>
                </li>
                <li className={query.status === 'pending' ? 'active' : ''}>
                  <Link to={`${routePaths.playlists.list}?status=pending`}>
                    Pending
                  </Link>
                </li>
                <li className={query.status === 'trash' ? 'active' : ''}>
                  <Link to={`${routePaths.playlists.list}?status=trash`}>
                    Trash
                  </Link>
                </li>
              </ul>
              <div className="tab-content">
                <ManageTable
                  loading={loading}
                  data={data || []}
                  paging={paging || {}}
                  handleChangePage={this.handlePageChange}
                  handleLimitChange={this.handleLimitChange}
                  handleSortBy={this.handleSortBy}
                  columns={columns}
                  actions={removeActionPlaylistByChannel(this.props.channelSetting, actions)}
                  styleActions={{ width: 180, flexGrow: 0 }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


export default ListPlaylists;
