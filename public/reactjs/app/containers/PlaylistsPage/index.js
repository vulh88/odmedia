import React from 'react';
import {Helmet} from 'react-helmet';
import {withRouter} from 'react-router-dom';
import {renderLocalRoutes} from 'router/createRouter';
import playlistLocalRoutes from './routes';
import {connect} from 'react-redux';
import {createStructuredSelector} from "reselect";
import {makeSelectChannelSetting} from 'store/app/selectors';
import {linksHidden} from 'common/link-generator/disable-links';
import NotFountPlaylistOverviewPage from 'containers/NotFoundPage/PlaylistOverview';
import TimelineOverview from '../../components/TimelineContainer/TimelineOverview';
import routePaths from 'settings/route-paths';

@connect(createStructuredSelector({channelSetting: makeSelectChannelSetting()}))
@withRouter
class PlaylistsPage extends React.PureComponent {

  handleReloadRouter = () => {
    let routes = playlistLocalRoutes;
    routes.filter((el, idz) => {
      if (el.path == routePaths.playlists.overview) {
        el.component = !linksHidden(this.props.channelSetting, el.path) ? NotFountPlaylistOverviewPage : TimelineOverview;
      }
    });
    return routes;
  };

  render() {
    let routes = this.handleReloadRouter();
    return (
        <div>
          <Helmet>
            <title>Playlists</title>
          </Helmet>
          {renderLocalRoutes(routes)}
        </div>
    );
  }
}

export default PlaylistsPage;
