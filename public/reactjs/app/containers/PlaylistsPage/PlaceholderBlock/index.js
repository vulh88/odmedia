import React from 'react';
import {SmartForm} from '../../../helpers/form/index';
import toast from 'helpers/user-interface/toast';
import PropTypes from 'prop-types';
import resourceApiService from '../../../services/api/resources';
import apiService from '../../../services/api/playlists';
import HaveApiComponent from '../../../components/BaseComponents/HaveApiComponent/index';
import {makeSelectChannel} from 'store/app/selectors';
import placeholderBlockSchema, {defaultFormValue} from './schema';
import withAsync from '../../../helpers/react/withAsync/withAsync';
import withCurrentChannel from '../../../common/hocs/withCurrentChannel';

@withCurrentChannel()
@withAsync()
class PlaceholderBlock extends HaveApiComponent {
  static propsTypes = {
    currentChannel: PropTypes.object,
    updatedCallback: PropTypes.func
  };

  pageTitle = 'Add New PlaceholderBlock';

  static async preload(props) {
    const schema = {...placeholderBlockSchema};

    /**
     * Get data for schema
     * @type {*[]}
     */
    schema.resource_id.data = [schema.resource_id.data[0]];
    const resourcesData = await resourceApiService.getList({
      channel_id: props.currentChannel.id,
      page: -1
    });
    resourcesData.data.map(e => (schema.resource_id.data.push({
      name: e.title,
      value: e.id,
      key: e.id
    })));

    return {
      schema
    };
  }

  handleSubmit = async (data) => {
    data.channel_id = this.props.playlist.channel_id;
    await this.callApi(apiService.addPlaceHolderPlaylist(data), (resp) => {
      toast.success('Added placeholder resource successfully');
      this.form.reset();
      if(this.props.onUpdated) {
        this.props.onUpdated(resp);
      }
    });
  };


  render() {
    const {schema} = this.props.preloadData;
    const {loading} = this.apiState;

    return (
        <div className="row position-relative">
          <SmartForm
              ref={e => this.form = e}
              loading={loading}
              defaultData={defaultFormValue}
              textButton="Save"
              schema={schema}
              onSubmit={this.handleSubmit}
          />
        </div>
    );
  }
}

export default PlaceholderBlock;
