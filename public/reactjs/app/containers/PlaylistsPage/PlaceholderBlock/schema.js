/* eslint-disable no-template-curly-in-string */
import {schema, commonSchema} from 'helpers/form';

const placeholderBlockSchema = {

  resource_id: {
    label: 'Resource',
    type: 'select',
    validate: schema.string().required(),
    data: [
      {
        name: '--Select resource--',
        value: ''
      }
    ],
  },

  title: {
    label: 'Placeholder title (EPG)',
    type: 'text',
    validate: schema.string().required()
  },

  duration: {
    label: 'Duration of title (seconds)',
    type: 'number',
    min: 0,
    validate: commonSchema.integer()
  }

};

export const defaultFormValue = {
  title: '',
  duration: 0
};


export default placeholderBlockSchema;
