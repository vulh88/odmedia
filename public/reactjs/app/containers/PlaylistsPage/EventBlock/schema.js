/* eslint-disable no-template-curly-in-string */
import {schema, commonSchema} from 'helpers/form';

const eventBlockSchema = {

  event_block_id: {
    label: 'Event block',
    type: 'select',
    validate: schema.string().required(),
    data: [
      {
        name: '--Select event block--',
        value: ''
      }
    ],
  },

  event_block_data: {
    label: 'Event block data',
    type: 'text',
    validate: schema.string().required(),
    summary: 'Extra data: (eg "now | This Program" or "later | Something else")'
  }

};

export const defaultFormValue = {
  event_block_data: '',
};


export default eventBlockSchema;
