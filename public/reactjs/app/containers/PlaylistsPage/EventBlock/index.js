import React from 'react';
import {SmartForm} from '../../../helpers/form/index';
import toast from 'helpers/user-interface/toast';
import PropTypes from 'prop-types';
import EventBlocksApiService from 'services/api/event-blocks';
import HaveApiComponent from '../../../components/BaseComponents/HaveApiComponent/index';
import {makeSelectChannel} from 'store/app/selectors';
import eventBlockSchema, {defaultFormValue} from './schema';
import withAsync from '../../../helpers/react/withAsync/withAsync';
import withCurrentChannel from '../../../common/hocs/withCurrentChannel';
import resourceApiService from '../../../services/api/resources';

@withCurrentChannel()
@withAsync()
class EventBlockPlaylist extends HaveApiComponent {
  static propsTypes = {
    currentChannel: PropTypes.object,
    updatedCallback: PropTypes.func
  };

  pageTitle = 'Add New EventBlock';

  static async preload(props) {
    const schema = {...eventBlockSchema};

    /**
     * Get event block data for schema
     * @type {*[]}
     */
    schema.event_block_id.data = [schema.event_block_id.data[0]];
    const eventBlocksData = await EventBlocksApiService.getList({
      channel_id: props.currentChannel.id,
      page: -1
    });
    eventBlocksData.data.map(e => (schema.event_block_id.data.push({
      name: e.name,
      value: e.id,
      key: e.id
    })));

    const eventBlocks = eventBlocksData.data;

    return {
      schema,
      eventBlocks
    };
  }

  handleSubmit = (data) => {
    const {eventBlocks} = this.props.preloadData;
    this.form.reset();
    if(this.props.onUpdated) {
      data.eventBlocks = eventBlocks;
      toast.success('Added event block successfully');
      this.props.onUpdated(data);
    }
  };


  render() {
    const {schema} = this.props.preloadData;
    const {loading} = this.apiState;

    return (
        <div className="row position-relative">
          <SmartForm
              ref={e => this.form = e}
              loading={loading}
              defaultData={defaultFormValue}
              textButton="Save"
              schema={schema}
              onSubmit={this.handleSubmit}
          />
        </div>
    );
  }
}

export default EventBlockPlaylist;
