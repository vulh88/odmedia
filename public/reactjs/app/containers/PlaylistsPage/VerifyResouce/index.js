import React from 'react';
import withAsync from '../../../helpers/react/withAsync/withAsync';
import {getPlaylist, getVerifyResources} from '../preloads';
import {connect} from 'react-redux';
import {createStructuredSelector} from "reselect";
import {makeSelectChannel} from '../../../store/app/selectors';
import AlertBlock from '../../../components/AlertBlock';
import ManageTable from '../../../components/ManageTable';
import {formatDuration} from '../../../helpers/utils';
import {withRouter} from 'react-router';
import playlistsApiService from 'services/api/playlists';
import toast from '../../../helpers/user-interface/toast';
import HaveApiComponent from '../../../components/BaseComponents/HaveApiComponent';
import { BarLoader } from 'react-spinners';

@withRouter
@withAsync()
@connect(createStructuredSelector({currentChannel: makeSelectChannel()}))
class VerifyResouces extends HaveApiComponent {
  static async preload(props) {
    try {
      const [playlist, resources] = await Promise.all([getPlaylist(props), getVerifyResources(props)]);
      return {
        playlist,
        resources
      }
    }
    catch (err) {
      console.error(err);
      throw err;
    }
  }

  state = {
    downloadItems: []
  };

  /*actions = [
    {
      icon: 'fa fa-download font-success',
      handler: e => this.onDownloadResource(e),
      backgroundColor: 'green',
      props: {
        title: 'Download',
      }
    },
  ];*/

  columns = [
    {
      name: 'ID',
      value: e => e.id,
      sort: true,
      style: {
        flexGrow: 0,
        width: 80
      }
    },
    {
      name: 'Title',
      value: e => e.title,
      sort: true
    },
    {
      name: 'Duration',
      value: e => e.duration && formatDuration(e.duration),
      sort: true
    },
    {
      name: 'Download status',
      sort: true,
      style: {
        width: 200,
        justifyContent: 'center',
        textAlign: 'center'
      },
      value: e => e.api_fetched
          ? (<i className="fa fa-check-circle green"/>)
          : (<i className="fa fa-remove red"/>)
    },
    {
      name: 'Download',
      icon: 'fa fa-download font-success',
      style: {
        width: 200,
        justifyContent: 'center',
        textAlign: 'center'
      },
      backgroundColor: 'green',
      value: e => this.renderDownload(e),
    }
  ];

  renderDownload = (e) => {
    let downloadBody = (<a className="row__action-button btn default green" title="Download" onClick={() => this.onDownloadResource(e)}><i className="fa fa-download font-success"></i></a>);
    if (e.isDownload) {
      downloadBody = (<BarLoader color={'#36c6d3'} loading={true} /> );
    } else {
      if (e.msg) {
        const status = e.responseStatus == 1 ? 'label-success' : 'label-danger';
        downloadBody = (<span className={"msg-verify label label-sm  " + status}>{e.msg}</span>);
      }
    }
    return downloadBody;
  };

  getAlertProps() {
    const {currentChannel} = this.props;
    const {resources} = this.props.preloadData;

    if (!(resources && resources.length)) {
      return  {
        style: AlertBlock.styles.warning,
        messages: ['This playlist has no added content item. Please check your scheduling.']
      };
    }

    if (currentChannel.needs_content_download && resources[0].api_fetched === 0) {
      return {
        style: AlertBlock.styles.error,
        messages: ['This channel requires all content items must be downloaded to the player directory.']
      };
    }
    return {
      style: AlertBlock.styles.success,
      messages: ['All playlist\'s content items are valid for publishing.']
    }
  }

  onDownloadResource = (e) => {
    this.handleDownloadItems(e.id);

    e.isDownload = true;
    e.msg = null;
    const {currentChannel} = this.props;
    this.callApi(playlistsApiService.downloadResource({
      download_url: e.api_video_url,
      channel_id: currentChannel.id,
      resource_id: e.id
    }), ({data}) => {
          e.api_fetched = data.api_fetched;
          e.responseStatus = 1;
          this.handleProgressDownload(e);
        },
        ({errors}) => {
          e.responseStatus = 0;
          toast.error('Connection time out');
          this.handleProgressDownload(e);
        },
        {autoShowError: false}
    );
  };

  handleProgressDownload = (e) => {
    e.isDownload = false;
    if (e.responseStatus) {
      e.msg = 'Download successfully';
    } else {
      e.msg = 'Download fail';
    }
    setTimeout(() => {
      this.state.downloadItems.splice(this.state.downloadItems.indexOf(e.id), 1);
      this.setState(this.state);

      e.msg = null;
      this.forceUpdate();
    },3000);
  };

  handleDownloadItems = (id) => {
    if (this.state.downloadItems.indexOf(id) < 0) {
      this.state.downloadItems.push(id);
      this.setState(this.state);
    }
  };

  render() {
    const alertProps = this.getAlertProps();
    const {preloadData} = this.props;
    const {resources} = preloadData;

    return (
        <div className="wrapper mt-3">
          <AlertBlock {...alertProps}/>
          <ManageTable
              loading={false}
              data={resources}
              paging={{}}
              columns={this.columns}
              showActions={false}
              tools={false}
          />
          <div className="clearfix"/>
        </div>
    );
  }
}

export default VerifyResouces;