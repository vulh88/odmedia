import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import locationHelper from '../../../helpers/location/index';
import routePaths from '../../../settings/route-paths/index';
import injectMixins from '../../../helpers/react/inject-mixins/index';
import {utils} from '../../../helpers/utils/index';
import {stepLayoutRoutes} from './routes';
import {renderLocalRoutes} from '../../../router/createRouter';
import {connect} from 'react-redux';
import {createStructuredSelector} from "reselect";
import {makeSelectChannelSetting} from 'store/app/selectors';
import {removeActionPlaylistByChannel} from 'common/link-generator/disable-links';

@connect(createStructuredSelector({channelSetting: makeSelectChannelSetting()}))
@withRouter
class StepLayout extends React.PureComponent {
  uiState = injectMixins.autoRenderOnChange.call(this, {
    isFullScreen: false
  });

  get links() {
    const {id} = this.props.match.params;
    return {
      edit: locationHelper.getLink(routePaths.playlists.single, {id}),
      scheduling: locationHelper.getLink(routePaths.playlists.scheduling, {id}),
      verifyResources: locationHelper.getLink(routePaths.playlists.verifyResources, {id}),
    }
  }

  get currentStep() {
    const {edit, scheduling, verifyResources} = this.links;
    const {pathname} = this.props.location;
    switch (pathname) {
      case edit:
        return 1;
      case scheduling:
        return 2;
      case verifyResources:
        return 3;
    }
  }

  get steps() {
    const {id} = this.props.match.params;
    if (!id) {
      return [
        {
          name: 'Create playlist',
          href: '#'
        },
        {
          name: 'Schedule resources',
          href: '#'
        },
        {
          name: 'Download resources',
          href: '#'
        }
      ];
    }
    else {
      const {edit, scheduling, verifyResources} = this.links;
      return [
        {
          name: 'Edit playlist info',
          href: edit
        },
        {
          name: 'Schedule resources',
          href: scheduling
        },
        {
          name: 'Download resources',
          href: verifyResources
        }
      ];
    }
  }

  toggleFullScreen = () => {
    this.uiState.isFullScreen = !this.uiState.isFullScreen;
    utils.triggerWindowResized();
  };

  render() {
    const currentStep = this.props.match.params.id ? this.currentStep : 1;
    const {isFullScreen} = this.uiState;
    const steps = removeActionPlaylistByChannel(this.props.channelSetting, this.steps);
    const maxStep = steps.length;

    return (
        <div className={`portlet step-layout light bordered step-layout ${isFullScreen ? 'portlet-fullscreen' : ''}`}
             style={{marginTop: isFullScreen ? 0 : 30}}>
          <a className="btn btn-circle  btn-default fullscreen hand" onClick={this.toggleFullScreen}>
            <i className={`fa ${isFullScreen ? 'fa-compress' : 'fa-expand' }`}/>
          </a>
          <div className="portlet-body">
            <div className="form-wizard">
              <div className="form-body">
                <ul className="nav nav-pills nav-justified steps">
                  {
                    steps.map((e, idz) => {
                      return (
                          <li className={idz <= currentStep - 1 ? 'active' : ''} key={idz}>
                            <Link className="step" to={e.href}>
                              <span className="number"> {idz + 1} </span>
                              <span className="desc">
                          <i className="fa fa-check"/>{e.name} </span>
                            </Link>
                          </li>
                      );
                    })
                  }
                </ul>
                <div className="progress progress-striped" role="progressbar">
                  <div
                      className="progress-bar progress-bar-success"
                      style={{
                        width: `${(currentStep / maxStep).toFixed(2) * 100}%`
                      }}/>
                </div>
                <div className="tab-content">
                  {renderLocalRoutes(stepLayoutRoutes)}
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

export default StepLayout;
