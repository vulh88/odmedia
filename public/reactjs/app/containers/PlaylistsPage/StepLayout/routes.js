import routePaths from '../../../settings/route-paths';
import VerifyResouces from '../VerifyResouce';
import PlaylistTimeLine from '../Timeline';
import PlaylistForm from '../Form';

export const stepLayoutRoutes = [
  {
    path: routePaths.playlists.createNew,
    component: PlaylistForm
  },
  {
    path: routePaths.playlists.scheduling,
    component: PlaylistTimeLine
  },
  {
    path: routePaths.playlists.verifyResources,
    component: VerifyResouces
  },
  {
    path: routePaths.playlists.single,
    component: PlaylistForm
  },
];
