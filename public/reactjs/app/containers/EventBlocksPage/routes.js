import List from './List/index';
import NotFound from 'containers/NotFoundPage/index';
import routePaths from 'settings/route-paths';
import Form from './Form/index';

const eventBlockRoutes = [
  {
    path: routePaths.eventBlocks.list,
    component: List
  },
  {
    path: routePaths.eventBlocks.createNew,
    component: Form
  },
  {
    path: routePaths.eventBlocks.single,
    component: Form
  },
  {
    path: '*',
    component: NotFound
  }
];

export default eventBlockRoutes;
