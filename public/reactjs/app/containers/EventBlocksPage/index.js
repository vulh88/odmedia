import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {renderLocalRoutes} from 'router/createRouter';
import eventBlockRoutes from './routes';
import {withRouter} from 'react-router-dom';

@withRouter
export class EventBlocksPage extends React.PureComponent {
  render() {
    return (
      <div>
        <Helmet>
          <title>Event Blocks Page</title>
          <meta
            name="description"
            content="A React.js Boilerplate application Event Blocks page"
          />
        </Helmet>
        {renderLocalRoutes(eventBlockRoutes)}
      </div>
    );
  }
}

export default EventBlocksPage;
