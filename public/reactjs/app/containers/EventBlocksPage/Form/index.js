import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import HaveApiComponent from '../../../components/BaseComponents/HaveApiComponent';
import {makeSelectChannel} from 'store/app/selectors';
import eventBlockSchema, {defaultForm} from './schema';
import {SmartForm} from '../../../helpers/form';
import apiService from '../../../services/api/event-blocks';
import withAsync from '../../../helpers/react/withAsync/withAsync';
import routePaths from '../../../settings/route-paths';
import toast from '../../../helpers/user-interface/toast';

@withAsync()
class EventBlocksForm extends HaveApiComponent {
  static propsTypes = {
    currentChannel: PropTypes.object,
    update: PropTypes.number,
  };

  static async preload(props) {
    const schema = {...eventBlockSchema};
    const update = props.match.params.id;
    const pageTitle = update ? 'Update Event Block' : 'Add Event Block';

    /**
     * Get data for schema type
     * @type {*[]}
     */
    schema.type.data = [schema.type.data[0]];
    const typesData = await apiService.getTypes();
    const types = typesData.map(e => ({
      name: e.name,
      value: e.value
    }));
    schema.type.data = schema.type.data.concat(types);

    return {
      pageTitle,
      schema,
      update
    };
  }

  handleSubmit = async params => {
    const {currentChannel, history, preloadData} = this.props;
    const {update} = preloadData;
    const data = {...params, channel_id: currentChannel.id};

    if (update) {
      this.callApi(apiService.update(update, data), () => {
        toast.success('Updated Event Blocks Successfully');
        history.push(routePaths.eventBlocks.list);
      });
    } else {
      this.callApi(
          apiService.create(data),
          () => {
            toast.success('Added Event Blocks Successfully');
            history.push(routePaths.eventBlocks.list);
          }
      );
    }
  };

  componentDidMount() {
    const update = this.props.preloadData.update;
    if (update) {
      this.callApi(
          apiService.getOne(update),
          data => {
            this.apiState.updateData = data;
          }
      );
    }
  }

  render() {
    const {preloadData} = this.props;
    const {update, schema, pageTitle} = preloadData;
    const {loading, updateData} = this.apiState;
    const defaultData = update ? updateData : defaultForm;

    return (
        <div>
          <div className="row">
            <div className="col-md-8">
              <h1 className="page-title">{pageTitle}</h1>
            </div>
          </div>
          <div className="portlet light">
            {update && !updateData ? null : (
                <SmartForm
                    ref={e => (this.form = e)}
                    loading={loading}
                    defaultData={defaultData}
                    schema={schema}
                    onSubmit={this.handleSubmit}
                    textButton={'Save'}
                />
            )}
          </div>
        </div>
    );
  }
}


const mapStateToProps = createStructuredSelector({
  currentChannel: makeSelectChannel()
});

export default connect(
    mapStateToProps,
    null
)(EventBlocksForm);
