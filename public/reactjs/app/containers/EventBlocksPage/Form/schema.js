/* eslint-disable no-template-curly-in-string */
import {schema, commonSchema} from 'helpers/form';

const eventBlockSchema = {
  name: {
    label: 'Name',
    type: 'text',
    validate: schema.string().required()
  },

  type: {
    label: 'Type',
    type: 'select',
    validate: schema.string().required(),
    data: [
      {
        name: 'Select type',
        value: ''
      }
    ]
  },

  default_duration: {
    label: 'Default duration',
    type: 'number',
    min: 0,
    validate: schema.string().required()
  },

};

export const defaultForm = {
  name: null,
  type: '',
  default_duration: 0
};


export default eventBlockSchema;
