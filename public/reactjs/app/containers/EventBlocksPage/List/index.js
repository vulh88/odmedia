import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { EventBlocksApiService } from 'services/api/event-blocks';
import ManageTable from '../../../components/ManageTable/index';
import toast from 'helpers/user-interface/toast';
import routePaths from 'settings/route-paths';
import locationHelper from '../../../helpers/location';
import ListingPageWithChannel from '../../../components/BaseComponents/ListingPageWithChannel';
import { makeSelectChannel } from 'store/app/selectors';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

@connect(
    createStructuredSelector({
      currentChannel: makeSelectChannel()
    })
)
@withRouter
class ListEventBlocks extends ListingPageWithChannel {
  restApiService = new EventBlocksApiService();

  columns = [
    {
      name: 'ID',
      value: e => e.id,
      sort: 'id',
      style: {
        width: 60,
        textAlign: 'center',
        justifyContent: 'center',
        flexGrow: 0
      }
    },
    {
      name: 'Name',
      value: e => e.name,
      sort: 'name'
    },
    {
      name: 'Type',
      value: e => e.type,
      sort: 'type'
    },
    {
      name: 'Default duration',
      value: e => e.default_duration,
      sort: 'default_duration'
    },
    {
      name: 'Created at',
      value: e => e.created_at,
      sort: 'created_at'
    },
    {
      name: 'Updated at',
      value: e => e.updated_at
    }
  ];

  actions = [
    {
      icon: 'fa fa-pencil',
      handler: e => this.handleEditItem(e)
    },
    {
      icon: 'fa fa-remove white',
      handler: e => this.handleDeleteItem(e),
      needConfirm: true,
      backgroundColor: 'red'
    }
  ];

  handleDeleteItem = async item => {
    this.callApi(this.restApiService.delete(item.id), () => {
      toast.success('Successfully deleted');
      setTimeout(() => {
        this.refreshData();
      }, 300);
    });
  };

  handleEditItem(item) {
    const { history } = this.props;
    history.push(
        locationHelper.getLink(routePaths.eventBlocks.single, { id: item.id })
    );
  }

  render() {
    const { data, paging, loading } = this.apiState;
    const { columns, actions } = this;

    return (
        <div>
          <div className="row">
            <div className="col-md-8">
              <h1 className="page-title">Event Blocks</h1>
            </div>
            <div className="col-md-4 text-right">
              <Link
                  className="btn green btn-actions"
                  to={routePaths.eventBlocks.createNew}
              >
                <i className="icon-plus" />
                <span> Add New</span>
              </Link>
            </div>
            <div className="clearfix" />
          </div>
          <div className="portlet box light">
            <div className="portlet-body">
              <div className="tabbable-line">
                <ul className="nav nav-tabs"/>
                <div className="tab-content">
                  <ManageTable
                      loading={loading}
                      data={data || []}
                      paging={paging || {}}
                      handleChangePage={this.handlePageChange}
                      handleLimitChange={this.handleLimitChange}
                      handleSortBy={this.handleSortBy}
                      columns={columns}
                      actions={actions}
                      styleActions={{ width: 120, flexGrow: 0 }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

export default ListEventBlocks;
