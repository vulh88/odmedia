import React from 'react';

export default function WrongPage() {
  return (
    <div className="row">
      <div className="col-md-12 page-500">
        <div className="number font-red"> 500 </div>
        <div className="details">
          <h3>Oops! Something went wrong.</h3>
          <p>
            We are fixing it! Please come back in a while.
            <br />
            <a href="/"> Return home </a> or try the search bar below.
          </p>
        </div>
      </div>
    </div>
  );
}
