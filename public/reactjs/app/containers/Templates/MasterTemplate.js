import React from 'react';
import {PropTypes} from 'prop-types';
import {withRouter} from 'react-router-dom';
import Header from 'components/Header/index';
import Sidebar from 'components/Sidebar/index';
import BreadCrumb from 'components/BreadCrumb/index';
import Footer from 'components/Footer/index';
import {makeSelectLoading} from '../../store/app/selectors';
import {createStructuredSelector} from 'reselect';
import {connect} from 'react-redux';
import * as actions from '../../store/display/actions';
import {utils} from 'helpers/utils/index';
import {makeSelectIsOpenSidebar} from '../../store/display/selectors';

@connect(
    createStructuredSelector({
      loading: makeSelectLoading(),
      isOpenSidebar: makeSelectIsOpenSidebar(),
    })
)
@withRouter
class MasterTemplate extends React.PureComponent {
  toggleSidebar = () => {
    this.props.dispatch(actions.toggleSidebar());
    utils.triggerWindowResized();
  };

  render() {
    const {children, location, match, isOpenSidebar} = this.props;
    return (
        <div className="page-wrapper">
          <Header onToggleSidebar={this.toggleSidebar} location={location}/>
          <div className="clearfix"/>
          <div className="page-container">
            <Sidebar
                location={location}
                isOpenSidebar={isOpenSidebar}
                match={match}
            />
            <div className="page-content-wrapper">
              <div className="page-content">
                <BreadCrumb location={location} match={match}/>
                {React.Children.toArray(children)}
              </div>
            </div>
          </div>
          <Footer/>
        </div>
    );
  }
}

MasterTemplate.propTypes = {
  children: PropTypes.node.isRequired,
  match: PropTypes.object,
  location: PropTypes.object,
};

export default MasterTemplate;
