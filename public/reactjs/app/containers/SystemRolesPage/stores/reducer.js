import { fromJS } from 'immutable';
import * as actions from './actions';

const initialState = fromJS({
  loading: false,
  error: false,
  listPermissions: null,
  listAssignments: null,
  listGroups: null,

  rolePermissions: null,
  listUsers: null,
  paging: {
    currentPage: null,
    totalPage: null,
    limit: null,
    totalData: null
  }
});

function systemRolesReducer(state = initialState, action) {
  switch (action.type) {
    case actions.REQUEST_API:
      return state.set('loading', true).set('error', false);
    case actions.REQUEST_API_SUCCESS:
      return state.set('loading', false);
    case actions.REQUEST_API_ERROR:
      return state.set('loading', false).set('error', action.payload);

    case actions.REQUEST_USERS_SUCCESS:
      return state
        .set('listUsers', action.payload.data)
        .set('paging', action.payload.paging);

    case actions.REQUEST_CHANNEL_CONFIG_SUCCESS:
      return state.set('rolePermissions', action.payload.rolePermissions);

    default:
      return state;
  }
}

export default systemRolesReducer;
