import { put, takeLatest, call } from 'redux-saga/effects';
import { push as routerPush } from 'react-router-redux';

import * as actions from './actions';
import * as api from './api';

function* callApi(apiCall, params) {
  yield put({
    type: actions.REQUEST_API
  });
  try {
    const resp = yield call(apiCall, params);
    yield put({
      type: actions.REQUEST_API_SUCCESS
    });
    return resp;
  } catch (error) {
    yield put({
      type: actions.REQUEST_API_ERROR,
      payload: error
    });
    throw error;
  }
}

const requestUsers = function*(params) {
  try {
    const request = yield callApi(api.requestUsers, params.payload);
    yield put({
      type: actions.REQUEST_USERS_SUCCESS,
      payload: {
        data: request.data,
        paging: request.paging
      }
    });
  } catch (error) {
    yield put({
      type: actions.REQUEST_USERS_ERROR,
      payload: error
    });
  }
};

const requestAssignChannel = function*(params) {
  try {
    yield callApi(api.requestAssignChannel, params.payload);
    yield put({
      type: actions.REQUEST_ASSIGN_CHANNEL_SUCCESS
    });
    yield put(routerPush('/system/user-system/roles/assignments'));
  } catch (error) {
    yield put({
      type: actions.REQUEST_ASSIGN_CHANNEL_ERROR
    });
  }
};

const requestChannelConfig = function*(params) {
  try {
    const request = yield callApi(api.requestChannelConfig, params.payload);
    yield put({
      type: actions.REQUEST_CHANNEL_CONFIG_SUCCESS,
      payload: {
        rolePermissions: request.data.role_permissions
      }
    });
  } catch (error) {
    yield put({
      type: actions.REQUEST_CHANNEL_CONFIG_ERROR,
      payload: error
    });
  }
};

export default function* SystemRoles() {
  yield takeLatest(actions.REQUEST_USERS, requestUsers);
  yield takeLatest(actions.REQUEST_ASSIGN_CHANNEL, requestAssignChannel);
  yield takeLatest(actions.REQUEST_CHANNEL_CONFIG, requestChannelConfig);
}
