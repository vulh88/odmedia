export const REQUEST_API = 'users/REQUEST_API';
export const REQUEST_API_SUCCESS = 'users/REQUEST_API_SUCCESS';
export const REQUEST_API_ERROR = 'users/REQUEST_API_ERROR';

export const REQUEST_USERS = 'users/REQUEST_USERS';
export const REQUEST_USERS_SUCCESS = 'users/REQUEST_USERS_SUCCESS';
export const REQUEST_USERS_ERROR = 'users/REQUEST_USERS_ERROR';

export const REQUEST_ASSIGN_CHANNEL = 'users/REQUEST_ASSIGN_CHANNEL';
export const REQUEST_ASSIGN_CHANNEL_SUCCESS =
  'users/REQUEST_ASSIGN_CHANNEL_SUCCESS';
export const REQUEST_ASSIGN_CHANNEL_ERROR =
  'users/REQUEST_ASSIGN_CHANNEL_ERROR';

export const REQUEST_CHANNEL_CONFIG = 'users/REQUEST_CHANNEL_CONFIG';
export const REQUEST_CHANNEL_CONFIG_SUCCESS =
  'users/REQUEST_CHANNEL_CONFIG_SUCCESS';
export const REQUEST_CHANNEL_CONFIG_ERROR =
  'users/REQUEST_CHANNEL_CONFIG_ERROR';

export const REQUEST_PERMISSIONS = 'system/REQUEST_PERMISSIONS';
export const REQUEST_PERMISSIONS_SUCCESS = 'users/REQUEST_PERMISSIONS_SUCCESS';
export const REQUEST_PERMISSIONS_ERROR = 'users/REQUEST_PERMISSIONS_ERROR';

export const REQUEST_MOVE_TRASH = 'users/REQUEST_MOVE_TRASH';
export const MOVE_TRASH_SUCCESS = 'users/MOVE_TRASH_SUCCESS';
export const MOVE_TRASH_ERROR = 'users/MOVE_TRASH_ERROR';

export const REQUEST_EDIT_USER = 'users/REQUEST_EDIT_USER';
export const REQUEST_EDIT_USER_SUCCESS = 'users/REQUEST_EDIT_USER_SUCCESS';
export const REQUEST_EDIT_USER_ERROR = 'users/REQUEST_EDIT_USER_ERROR';

export const REQUEST_USERS_TRASH = 'users/REQUEST_USERS_TRASH';
export const REQUEST_USERS_TRASH_SUCCESS = 'users/REQUEST_USERS_TRASH_SUCCESS';
export const REQUEST_USERS_TRASH_ERROR = 'users/REQUEST_USERS_TRASH_ERROR';

export const REQUEST_DELETE_USER = 'users/REQUEST_DELETE_USER';
export const REQUEST_DELETE_USER_SUCCESS = 'users/REQUEST_DELETE_USER_SUCCESS';
export const REQUEST_DELETE_USER_ERROR = 'users/REQUEST_DELETE_USER_ERROR';

export const REQUEST_REFRESH_USER = 'users/REQUEST_REFRESH_USER';
export const REQUEST_REFRESH_USER_SUCCESS =
  'users/REQUEST_REFRESH_USER_SUCCESS';
export const REQUEST_REFRESH_USER_ERROR = 'users/REQUEST_REFRESH_USER_ERROR';

export const REQUEST_CREATE_USER = 'createUser/REQUEST_CREATE_USER';
export const CREATE_USER_SUCCESS = 'createUser/CREATE_USER_SUCCESS';
export const CREATE_USER_ERROR = 'createUser/CREATE_USER_ERROR';
export const GET_USER = 'editUser/GET_USER';
export const GET_USER_SUCCESS = 'editUser/GET_USER_SUCCESS';
export const GET_USER_ERROR = 'editUser/GET_USER_ERROR';

export const REQUEST_UPDATE_USER = 'editUser/REQUEST_UPDATE_USER';
export const UPDATE_USER_SUCCESS = 'editUser/UPDATE_USER_SUCCESS';
export const UPDATE_USER_ERROR = 'editUser/UPDATE_USER_ERROR';

export const requestUsers = payload => ({ type: REQUEST_USERS, payload });

export const requestAssignChannel = payload => ({
  type: REQUEST_ASSIGN_CHANNEL,
  payload
});

export const requestChannelConfig = payload => ({
  type: REQUEST_CHANNEL_CONFIG,
  payload
});

export const requestPermissions = payload => ({
  type: REQUEST_PERMISSIONS,
  payload
});

export const requestMoveTrash = payload => ({
  type: REQUEST_MOVE_TRASH,
  payload
});

export const requestEditUser = payload => ({
  type: REQUEST_EDIT_USER,
  payload
});

export const requestUsersTrash = payload => ({
  type: REQUEST_USERS_TRASH,
  payload
});

export const requestDeleteUser = payload => ({
  type: REQUEST_DELETE_USER,
  payload
});

export const requestRefreshUser = payload => ({
  type: REQUEST_REFRESH_USER,
  payload
});

export const requestCreateUser = payload => ({
  type: REQUEST_CREATE_USER,
  payload
});

export const getUser = payload => ({ type: GET_USER, payload });

export const requestUpdateUser = payload => ({
  type: REQUEST_UPDATE_USER,
  payload
});
