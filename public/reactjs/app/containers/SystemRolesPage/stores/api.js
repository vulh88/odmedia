import apiCaller from 'services/apiCaller';
import apiPaths from 'settings/api/api-path';

const count = 10;

export const requestUsers = params =>
  apiCaller.get(apiPaths.users.base, {
    query: {
      page: params.page,
      count,
      ...params
    }
  });

export const requestChannelConfig = params =>
  apiCaller.get(apiPaths.users.channelConfig, {
    inUrl: { id: params.id },
    query: {
      channel_id: params.channel_id
    }
  });

export const requestAssignChannel = params =>
  apiCaller.post(apiPaths.users.assignChannel, {
    inUrl: { id: params.id },
    body: {
      channel_id: params.channel_id,
      permissions: params.permissions
    }
  });
