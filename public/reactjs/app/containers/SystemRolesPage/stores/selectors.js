import { createSelector } from 'reselect';

const selectSystemRoles = state => state.get('systemRoles');

const makeSelectError = () =>
  createSelector(selectSystemRoles, systemState => systemState.get('error'));

const makeSelectLoading = () =>
  createSelector(selectSystemRoles, systemState => systemState.get('loading'));

const makeSelectListUsers = () =>
  createSelector(selectSystemRoles, systemState =>
    systemState.get('listUsers')
  );

const makeSelectRolePermissions = () =>
  createSelector(selectSystemRoles, systemState =>
    systemState.get('rolePermissions')
  );

const makeSelectListPermissions = () =>
  createSelector(selectSystemRoles, systemState =>
    systemState.get('listPermissions')
  );

const makeSelectListAssignments = () =>
  createSelector(selectSystemRoles, systemState =>
    systemState.get('listAssignments')
  );

const makeSelectListGroups = () =>
  createSelector(selectSystemRoles, systemState =>
    systemState.get('listGroups')
  );

const makeSelectPaging = () =>
  createSelector(selectSystemRoles, systemState => systemState.get('paging'));

export {
  selectSystemRoles,
  makeSelectRolePermissions,
  makeSelectListUsers,
  makeSelectListAssignments,
  makeSelectListPermissions,
  makeSelectListGroups,
  makeSelectPaging,
  makeSelectLoading,
  makeSelectError
};
