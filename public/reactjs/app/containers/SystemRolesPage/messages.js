import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'odmedia.containers.NotFoundPage.header',
    defaultMessage: 'Page not found.'
  },

  superAdmin: {
    id: 'odmedia.containers.SystemRoles.Settings.superAdmin',
    defaultMessage: 'Super Administrator'
  },
  admin: {
    id: 'odmedia.containers.SystemRoles.Settings.admin',
    defaultMessage: 'Administrator'
  }
});
