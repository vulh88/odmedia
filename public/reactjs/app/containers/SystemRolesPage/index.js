import React from 'react';
import PropTypes from 'prop-types';
import {Helmet} from 'react-helmet';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {withRouter} from 'react-router-dom';
import injectReducer from 'store/utils/injectReducer';
import injectSaga from 'store/utils/injectSaga';

import TabGroup from './components/TabGroup';
import AssignChannel from './components/Form/AssignChannel';

import reducer from './stores/reducer';
import saga from './stores/saga';

import {makeSelectError, makeSelectLoading} from './stores/selectors';
import routePaths from 'settings/route-paths';


@withRouter
@injectReducer({key: 'systemRoles', reducer})
@injectSaga({key: 'systemRoles', saga})
@connect(
    createStructuredSelector({
      error: makeSelectError(),
      loading: makeSelectLoading()
    }))
export class SystemRolesPage extends React.PureComponent {
  static propTypes = {
    loading: PropTypes.bool,
    error: PropTypes.oneOfType([PropTypes.bool, PropTypes.object])
  };

  render() {
    const {match, loading, error} = this.props;
    return (
        <div>
          <Helmet>
            <title>System Roles</title>
          </Helmet>
          {
            <TabGroup
                loading={loading}
                error={error}
            />
          }
        </div>
    );
  }
}

export default SystemRolesPage;