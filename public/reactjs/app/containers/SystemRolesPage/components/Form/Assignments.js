import React from 'react';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

class FormAssignments extends React.PureComponent {
  state = {
    groupsAdmin: {
      super_administrator: true,
      administrator: true,
      new_publisher: false,
      full_permissions: false
    },
    user: {
      users: {
        read: true,
        create: true,
        edit: true,
        trash: true,
        destroy: true
      },
      assignments: {
        read: true,
        edit: true
      },
      roles: {
        read: true,
        create: true,
        edit: true,
        destroy: true
      },
      permissions: {
        read: true
      },
      security: {
        read: true,
        edit: true,
        unblock: true
      }
    },
    news: {
      news: {
        publish: true,
        read: true,
        create: true,
        edit: true,
        trash: true,
        destroy: true
      },
      categories: {
        read: true,
        create: true,
        edit: true,
        destroy: true
      },
      tags: {
        read: true,
        create: true,
        edit: true,
        destroy: true
      }
    },
    author: {
      authors: {
        read: true,
        create: true,
        edit: true,
        trash: true,
        destroy: true
      },
      salutations: {
        read: true,
        create: true,
        edit: true,
        destroy: true
      }
    },
    fileMananagement: {
      files: {
        read: true,
        create: true,
        edit: true,
        trash: true,
        delete: true
      },
      folders: {
        read: true,
        create: true,
        edit: true,
        trash: true,
        delete: true
      }
    }
  };

  componentWillMount() {}

  handleCheckboxGroups = e => {
    const elementSkeleton = e.target.name;
    const element = e.target.value;

    this.setState({
      [elementSkeleton]: {
        ...this.state[elementSkeleton],
        [element]: e.target.checked
      }
    });
  };

  handleCheckboxTable = e => {
    const elementSkeleton = e.target.name;
    const value = e.target.value;
    const elementParent = value.split('.')[0];
    const element = value.split('.')[1];

    this.setState({
      [elementSkeleton]: {
        ...this.state[elementSkeleton],
        [elementParent]: {
          ...this.state[elementSkeleton][elementParent],
          [element]: e.target.checked
        }
      }
    });
  };

  submitForm = () => {
    console.log('submit', this.state);
  };

  renderGroupsAdmin = () => {
    const {
      groupsAdmin: {
        super_administrator,
        administrator,
        new_publisher,
        full_permissions
      }
    } = this.state;

    return (
      <div className="form-group form-md-line-input">
        <table className="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th>Groups</th>
              <th className="role-cell-w" />
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                Super Administrator
                <a href="http://fomo.elidev.info/admin/roles/1/edit-assign-permissions">
                  <small> Assign</small>
                </a>
              </td>
              <td>
                <div className="md-checkbox has-success">
                  <input
                    type="checkbox"
                    id="acl-group0"
                    className="md-check"
                    value="super_administrator"
                    name="groupsAdmin"
                    checked={super_administrator}
                    onChange={this.handleCheckboxGroups}
                  />
                  <label htmlFor="acl-group0">
                    <span />
                    <span className="check center" />
                    <span className="box center" />
                  </label>
                </div>
              </td>
            </tr>

            <tr>
              <td>
                Administrator
                <a href="http://fomo.elidev.info/admin/roles/2/edit-assign-permissions">
                  <small> Assign</small>
                </a>
              </td>
              <td>
                <div className="md-checkbox has-success">
                  <input
                    type="checkbox"
                    id="acl-group1"
                    className="md-check"
                    value="administrator"
                    name="groupsAdmin"
                    checked={administrator}
                    onChange={this.handleCheckboxGroups}
                  />
                  <label htmlFor="acl-group1">
                    <span />
                    <span className="check center" />
                    <span className="box center" />
                  </label>
                </div>
              </td>
            </tr>

            <tr>
              <td>
                New Publisher
                <a href="http://fomo.elidev.info/admin/roles/3/edit-assign-permissions">
                  <small> Assign</small>
                </a>
              </td>
              <td>
                <div className="md-checkbox has-success">
                  <input
                    type="checkbox"
                    id="acl-group2"
                    className="md-check"
                    value="new_publisher"
                    name="groupsAdmin"
                    checked={new_publisher}
                    onChange={this.handleCheckboxGroups}
                  />
                  <label htmlFor="acl-group2">
                    <span />
                    <span className="check center" />
                    <span className="box center" />
                  </label>
                </div>
              </td>
            </tr>

            <tr>
              <td>
                full permissions
                <a href="http://fomo.elidev.info/admin/roles/6/edit-assign-permissions">
                  <small> Assign</small>
                </a>
              </td>
              <td>
                <div className="md-checkbox has-success">
                  <input
                    type="checkbox"
                    id="acl-group3"
                    className="md-check"
                    value="full_permissions"
                    name="groupsAdmin"
                    checked={full_permissions}
                    onChange={this.handleCheckboxGroups}
                  />
                  <label htmlFor="acl-group3">
                    <span />
                    <span className="check center" />
                    <span className="box center" />
                  </label>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  };

  renderUser = () => {
    const {
      user: { users, assignments, roles, permissions, security }
    } = this.state;

    const rowUsers = (
      <tr>
        <td> -- Users </td>
        <td>
          <div className="md-checkbox has-success">
            <input
              name="user"
              type="checkbox"
              id="permission-cbox-users-0"
              className="md-check"
              value="users.read"
              checked={users.read}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-users-0">
              <span className="inc" />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="user"
              type="checkbox"
              id="permission-cbox-users-1"
              className="md-check"
              value="users.create"
              checked={users.create}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-users-1">
              <span className="inc" />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="user"
              type="checkbox"
              id="permission-cbox-users-2"
              className="md-check"
              value="users.edit"
              checked={users.edit}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-users-2">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="user"
              type="checkbox"
              id="permission-cbox-users-3"
              className="md-check"
              value="users.trash"
              checked={users.trash}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-users-3">
              <span className="inc" />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="user"
              type="checkbox"
              id="permission-cbox-users-4"
              className="md-check"
              value="users.destroy"
              checked={users.destroy}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-users-4">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td />
      </tr>
    );

    const rowAssignments = (
      <tr>
        <td> -- Assignments </td>
        <td>
          <div className="md-checkbox has-success">
            <input
              name="user"
              type="checkbox"
              id="permission-cbox-assignments-0"
              className="md-check"
              value="assignments.read"
              checked={assignments.read}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-assignments-0">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td />

        <td>
          <div className="md-checkbox has-success">
            <input
              name="user"
              type="checkbox"
              id="permission-cbox-assignments-2"
              className="md-check"
              value="assignments.edit"
              checked={assignments.edit}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-assignments-2">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td />

        <td />

        <td />
      </tr>
    );

    const rowGroups = (
      <tr>
        <td> -- Groups </td>
        <td>
          <div className="md-checkbox has-success">
            <input
              name="user"
              type="checkbox"
              id="permission-cbox-roles-0"
              className="md-check"
              value="roles.read"
              checked={roles.read}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-roles-0">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="user"
              type="checkbox"
              id="permission-cbox-roles-1"
              className="md-check"
              value="roles.create"
              checked={roles.create}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-roles-1">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="user"
              type="checkbox"
              id="permission-cbox-roles-2"
              className="md-check"
              value="roles.edit"
              checked={roles.edit}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-roles-2">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td />

        <td>
          <div className="md-checkbox has-success">
            <input
              name="user"
              type="checkbox"
              id="permission-cbox-roles-4"
              className="md-check"
              value="roles.destroy"
              checked={roles.destroy}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-roles-4">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td />
      </tr>
    );

    const rowPermissions = (
      <tr>
        <td> -- Permissions </td>
        <td>
          <div className="md-checkbox has-success">
            <input
              name="user"
              type="checkbox"
              id="permission-cbox-permissions-0"
              className="md-check"
              value="permissions.read"
              checked={permissions.read}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-permissions-0">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>
        <td />
        <td />
        <td />
        <td />
        <td />
      </tr>
    );

    const rowSecurity = (
      <tr>
        <td> -- Security </td>
        <td>
          <div className="md-checkbox has-success">
            <input
              name="user"
              type="checkbox"
              id="permission-cbox-security-0"
              className="md-check"
              value="security.read"
              checked={security.read}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-security-0">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td />

        <td>
          <div className="md-checkbox has-success">
            <input
              name="user"
              type="checkbox"
              id="permission-cbox-security-2"
              className="md-check"
              value="security.edit"
              checked={security.edit}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-security-2">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td />

        <td />

        <td>
          <div className="md-checkbox has-success">
            <input
              name="user"
              type="checkbox"
              id="permission-cbox-security-5"
              className="md-check"
              value="security.unblock"
              checked={security.unblock}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-security-5">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>
      </tr>
    );

    return (
      <table className="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th>User</th>
            <th className="role-cell-w"> Read </th>
            <th className="role-cell-w"> Create </th>
            <th className="role-cell-w"> Edit </th>
            <th className="role-cell-w"> Trash </th>
            <th className="role-cell-w"> Destroy </th>
            <th className="role-cell-w"> Unblock </th>
          </tr>
        </thead>

        <tbody>
          {rowUsers}

          {rowAssignments}

          {rowGroups}

          {rowPermissions}

          {rowSecurity}
        </tbody>
      </table>
    );
  };

  renderNews = () => {
    const {
      news: { news, categories, tags }
    } = this.state;

    const renderNews = (
      <tr>
        <td> -- News </td>
        <td>
          <div className="md-checkbox has-success">
            <input
              name="news"
              type="checkbox"
              id="permission-cbox-news-0"
              className="md-check"
              value="news.publish"
              checked={news.publish}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-news-0">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="news"
              type="checkbox"
              id="permission-cbox-news-1"
              className="md-check"
              value="news.read"
              checked={news.read}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-news-1">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="news"
              type="checkbox"
              id="permission-cbox-news-2"
              className="md-check"
              value="news.create"
              checked={news.create}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-news-2">
              <span className="inc" />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="news"
              type="checkbox"
              id="permission-cbox-news-3"
              className="md-check"
              value="news.edit"
              checked={news.edit}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-news-3">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="news"
              type="checkbox"
              id="permission-cbox-news-4"
              className="md-check"
              value="news.trash"
              checked={news.trash}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-news-4">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="news"
              type="checkbox"
              id="permission-cbox-news-5"
              className="md-check"
              value="news.destroy"
              checked={news.destroy}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-news-5">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>
      </tr>
    );

    const renderCategories = (
      <tr>
        <td> -- Categories </td>
        <td />

        <td>
          <div className="md-checkbox has-success">
            <input
              name="news"
              type="checkbox"
              id="permission-cbox-categories-1"
              className="md-check"
              value="categories.read"
              checked={categories.read}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-categories-1">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="news"
              type="checkbox"
              id="permission-cbox-categories-2"
              className="md-check"
              value="categories.create"
              checked={categories.create}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-categories-2">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="news"
              type="checkbox"
              id="permission-cbox-categories-3"
              className="md-check"
              value="categories.edit"
              checked={categories.edit}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-categories-3">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td />

        <td>
          <div className="md-checkbox has-success">
            <input
              name="news"
              type="checkbox"
              id="permission-cbox-categories-5"
              className="md-check"
              value="categories.destroy"
              checked={categories.destroy}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-categories-5">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>
      </tr>
    );

    const renderTags = (
      <tr>
        <td> -- Tags </td>
        <td />

        <td>
          <div className="md-checkbox has-success">
            <input
              name="news"
              type="checkbox"
              id="permission-cbox-tags-1"
              className="md-check"
              value="tags.read"
              checked={tags.read}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-tags-1">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="news"
              type="checkbox"
              id="permission-cbox-tags-2"
              className="md-check"
              value="tags.create"
              checked={tags.create}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-tags-2">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="news"
              type="checkbox"
              id="permission-cbox-tags-3"
              className="md-check"
              value="tags.edit"
              checked={tags.edit}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-tags-3">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td />

        <td>
          <div className="md-checkbox has-success">
            <input
              name="news"
              type="checkbox"
              id="permission-cbox-tags-5"
              className="md-check"
              value="tags.destroy"
              checked={tags.destroy}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-tags-5">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>
      </tr>
    );

    return (
      <table className="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th>News</th>
            <th className="role-cell-w"> Publish </th>
            <th className="role-cell-w"> Read </th>
            <th className="role-cell-w"> Create </th>
            <th className="role-cell-w"> Edit </th>
            <th className="role-cell-w"> Trash </th>
            <th className="role-cell-w"> Destroy </th>
          </tr>
        </thead>
        <tbody>
          {renderNews}

          {renderCategories}

          {renderTags}
        </tbody>
      </table>
    );
  };

  renderAuthor = () => {
    const {
      author: { authors, salutations }
    } = this.state;

    const rowAuthors = (
      <tr>
        <td> -- Authors </td>
        <td>
          <div className="md-checkbox has-success">
            <input
              name="author"
              type="checkbox"
              id="permission-cbox-authors-0"
              className="md-check"
              value="authors.read"
              checked={authors.read}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-authors-0">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="author"
              type="checkbox"
              id="permission-cbox-authors-1"
              className="md-check"
              value="authors.create"
              checked={authors.create}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-authors-1">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="author"
              type="checkbox"
              id="permission-cbox-authors-2"
              className="md-check"
              value="authors.edit"
              checked={authors.edit}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-authors-2">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="author"
              type="checkbox"
              id="permission-cbox-authors-3"
              className="md-check"
              value="authors.trash"
              checked={authors.trash}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-authors-3">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="author"
              type="checkbox"
              id="permission-cbox-authors-4"
              className="md-check"
              value="authors.destroy"
              checked={authors.destroy}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-authors-4">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>
      </tr>
    );

    const rowSalutations = (
      <tr>
        <td> -- Salutations </td>
        <td>
          <div className="md-checkbox has-success">
            <input
              name="author"
              type="checkbox"
              id="permission-cbox-salutations-0"
              className="md-check"
              value="salutations.read"
              checked={salutations.read}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-salutations-0">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="author"
              type="checkbox"
              id="permission-cbox-salutations-1"
              className="md-check"
              value="salutations.create"
              checked={salutations.create}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-salutations-1">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="author"
              type="checkbox"
              id="permission-cbox-salutations-2"
              className="md-check"
              value="salutations.edit"
              checked={salutations.edit}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-salutations-2">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td />

        <td>
          <div className="md-checkbox has-success">
            <input
              name="author"
              type="checkbox"
              id="permission-cbox-salutations-4"
              className="md-check"
              value="salutations.destroy"
              checked={salutations.destroy}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-salutations-4">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>
      </tr>
    );

    return (
      <table className="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th>Author</th>
            <th className="role-cell-w"> Read </th>
            <th className="role-cell-w"> Create </th>
            <th className="role-cell-w"> Edit </th>
            <th className="role-cell-w"> Trash </th>
            <th className="role-cell-w"> Destroy </th>
          </tr>
        </thead>
        <tbody>
          {rowAuthors}

          {rowSalutations}
        </tbody>
      </table>
    );
  };

  renderFileManagement = () => {
    const {
      fileMananagement: { files, folders }
    } = this.state;

    const renderFiles = (
      <tr>
        <td> -- Files </td>
        <td>
          <div className="md-checkbox has-success">
            <input
              name="fileMananagement"
              type="checkbox"
              id="permission-cbox-files-0"
              className="md-check"
              value="files.read"
              checked={files.read}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-files-0">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="fileMananagement"
              type="checkbox"
              id="permission-cbox-files-1"
              className="md-check"
              value="files.create"
              checked={files.create}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-files-1">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="fileMananagement"
              type="checkbox"
              id="permission-cbox-files-2"
              className="md-check"
              value="files.edit"
              checked={files.edit}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-files-2">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="fileMananagement"
              type="checkbox"
              id="permission-cbox-files-3"
              className="md-check"
              value="files.trash"
              checked={files.trash}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-files-3">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="fileMananagement"
              type="checkbox"
              id="permission-cbox-files-4"
              className="md-check"
              value="files.delete"
              checked={files.delete}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-files-4">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>
      </tr>
    );

    const renderFolders = (
      <tr>
        <td> -- Folders </td>
        <td>
          <div className="md-checkbox has-success">
            <input
              name="fileMananagement"
              type="checkbox"
              id="permission-cbox-folders-0"
              className="md-check"
              value="folders.read"
              checked={folders.read}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-folders-0">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="fileMananagement"
              type="checkbox"
              id="permission-cbox-folders-1"
              className="md-check"
              value="folders.create"
              checked={folders.create}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-folders-1">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="fileMananagement"
              type="checkbox"
              id="permission-cbox-folders-2"
              className="md-check"
              value="folders.edit"
              checked={folders.edit}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-folders-2">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="fileMananagement"
              type="checkbox"
              id="permission-cbox-folders-3"
              className="md-check"
              value="folders.trash"
              checked={folders.trash}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-folders-3">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>

        <td>
          <div className="md-checkbox has-success">
            <input
              name="fileMananagement"
              type="checkbox"
              id="permission-cbox-folders-4"
              className="md-check"
              value="folders.delete"
              checked={folders.delete}
              onChange={this.handleCheckboxTable}
            />
            <label htmlFor="permission-cbox-folders-4">
              <span />
              <span className="check center" />
              <span className="box center" />
            </label>
          </div>
        </td>
      </tr>
    );

    return (
      <table className="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th>File management</th>
            <th className="role-cell-w"> Read </th>
            <th className="role-cell-w"> Create </th>
            <th className="role-cell-w"> Edit </th>
            <th className="role-cell-w"> Trash </th>
            <th className="role-cell-w"> Delete </th>
          </tr>
        </thead>
        <tbody>
          {renderFiles}

          {renderFolders}
        </tbody>
      </table>
    );
  };

  render() {
    return (
      <div className="portlet light bordered">
        <div className="form-body">
          <div className="form-group form-md-line-input">
            <input
              type="text"
              className="form-control"
              id="form_control_1"
              readOnly
              value="admin@elidev.info"
              aria-invalid="false"
            />
            <label htmlFor="form_control_1">User Email</label>
          </div>

          {this.renderGroupsAdmin()}

          <div className="form-group form-md-line-input">
            <label className="bold-color">Permissions</label>

            {this.renderUser()}
            {this.renderNews()}
            {this.renderAuthor()}
            {this.renderFileManagement()}
          </div>
        </div>
        <div className="form-actions noborder">
          <button type="submit" className="btn green" onClick={this.submitForm}>
            Save
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({});

export default connect(
  mapStateToProps,
  null
)(FormAssignments);
