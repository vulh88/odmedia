import React from 'react';
import PropTypes from 'prop-types';
import lodash from 'lodash';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { createForm, formShape } from 'rc-form';

import ButtonLoading from 'components/ButtonLoading';

import {
  makeSelectChannel,
  makeSelectListChannel
} from 'store/app/selectors';

import * as actions from '../../stores/actions';
import {
  makeSelectError,
  makeSelectLoading,
  makeSelectRolePermissions
} from '../../stores/selectors';


class AssignChannel extends React.PureComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
    error: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
    loading: PropTypes.bool,
    currentChannel: PropTypes.object,
    rolePermissions: PropTypes.object,
    form: formShape
  };

  state = {
    rolePermissions: null,
    resultResult: [],
    roles: ['read', 'create', 'edit', 'trash', 'destroy', 'unblock']
  };

  componentWillMount() {
    const { match, currentChannel } = this.props;

    if (currentChannel) {
      this.props.dispatch(
        actions.requestChannelConfig({
          id: match.params.id,
          channel_id: currentChannel.id
        })
      );
    }
  }

  shouldComponentUpdate(nextProps) {
    if (nextProps.rolePermissions) {
      const rolePermissions = nextProps.rolePermissions;
      const result = lodash.mapValues(rolePermissions, page => {
        const type = lodash.mapValues(page.children, type => {
          const result = lodash.reduce(
            this.state.roles,
            (obj, param, index) => {
              if (type.permissions[index]) {
                obj[param] = true;
              } else {
                obj[param] = 'hidden';
              }
              return obj;
            },
            {}
          );
          return { label: type.label, permissions: result };
        });
        return { ...page, children: type };
      });
      this.setState({ rolePermissions: result });
    }
    return true;
  }

  onSubmitForm = e => {
    e.preventDefault();
    const { dispatch, match } = this.props;
    const { resultResult } = this.state;
    this.props.form.validateFields((error, values) => {
      if (!error) {
        dispatch(
          actions.requestAssignChannel({
            id: match.params.id,
            channel_id: values.channel_id,
            permissions: resultResult.join(',')
          })
        );
      }
    });
  };

  handleCheckboxTable = (e, elementParent, element) => {
    const { rolePermissions, resultResult } = this.state;
    const elementSkeleton = e.target.name;
    const value = e.target.checked;
    const result = `${elementParent}.${element}`;

    rolePermissions[elementSkeleton].children[elementParent].permissions[
      element
    ] = value;

    this.setState({
      rolePermissions,
      resultResult: [...resultResult, result]
    });
  };

  renderPermissions = () => {
    const { rolePermissions } = this.state;

    if (rolePermissions) {
      const row = page =>
        lodash.map(page, (type, keyType) => {
          const rowType = (
            <tr key={type.label}>
              <td>{type.label}</td>
              {lodash.map(type.permissions, (item, key) => {
                if (item === 'hidden') {
                  return <td />;
                }
                return (
                  <td key={`${keyType}-${key}`}>
                    <div className="md-checkbox has-success">
                      <input
                        name="user"
                        type="checkbox"
                        id={`cbox-${keyType}-${key}`}
                        className="md-check"
                        checked={item}
                        onChange={e =>
                          this.handleCheckboxTable(e, keyType, key)
                        }
                      />
                      <label htmlFor={`cbox-${keyType}-${key}`}>
                        <span className="inc" />
                        <span className="check center" />
                        <span className="box center" />
                      </label>
                    </div>
                  </td>
                );
              })}
            </tr>
          );
          return rowType;
        });

      return lodash.map(rolePermissions, (page, key) => {
        if (lodash.isEmpty(page.children)) {
          return null;
        }

        return (
          <div key={key}>
            <label className="bold-color">{page.label}</label>
            <table className="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th />
                  <th className="role-cell-w"> Read </th>
                  <th className="role-cell-w"> Create </th>
                  <th className="role-cell-w"> Edit </th>
                  <th className="role-cell-w"> Trash </th>
                  <th className="role-cell-w"> Destroy </th>
                  <th className="role-cell-w"> Unblock </th>
                </tr>
              </thead>

              <tbody>{row(page.children)}</tbody>
            </table>
          </div>
        );
      });
    }

    return null;
  };

  renderSelect = () => {
    const { form, listChannel } = this.props;
    const { getFieldProps, getFieldError, isFieldValidating } = form;
    const errorSelect = getFieldError('channel_id');
    const classSelect = errorSelect ? 'has-error' : '';

    return (
      <div className={`form-group ${classSelect}`}>
        <label className="control-label">Channel</label>
        <div className="control__input">
          <select
            className="form-control"
            defaultValue={-1}
            {...getFieldProps('channel_id', {
              rules: [
                {
                  required: true,
                  message: 'Please select a channel'
                }
              ]
            })}
          >
            <option value="-1" disabled hidden>
              Select a channel
            </option>

            {listChannel &&
              listChannel.map(item => (
                <option key={item.id} value={item.id}>
                  {item.name}
                </option>
              ))}
          </select>
        </div>

        <span className="help-block">{errorSelect}</span>
        <div>{isFieldValidating('channel_id') ? 'validating' : null}</div>
      </div>
    );
  };

  renderAlert = () => {
    const { error } = this.props;
    if (error) {
      return (
        <div className="alert alert-danger">
          <button className="close" data-close="alert" />
          <span>{error.errors && lodash.map(error.errors, item => item)}</span>
        </div>
      );
    }

    return false;
  };

  render() {
    const { loading } = this.props;

    if (loading) {
      return null;
    }

    return (
      <div>
        <h1 className="page-title">Assign Channel</h1>

        <div className="smart-form vertical-form left">
          <div className="portlet light ">
            {this.renderAlert()}

            {this.renderSelect()}

            {this.renderPermissions()}

            <div className="smart-form__actions">
              <ButtonLoading
                className="btn green mt-ladda-btn pull-right"
                onClick={this.onSubmitForm}
                loading={loading}
              >
                Assign
              </ButtonLoading>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  rolePermissions: makeSelectRolePermissions(),
  error: makeSelectError(),
  loading: makeSelectLoading(),

  listChannel: makeSelectListChannel(),
  currentChannel: makeSelectChannel()
});

export default connect(
  mapStateToProps,
  null
)(createForm()(AssignChannel));
