import { schema, commonSchema } from 'helpers/form';

const AssignmentSchema = {
    user: {
        label: 'User',
        value: '',
        type: 'viewOnly'
    },
    channel_id: {
        label: 'Channel',
        type: 'select',
        validate: schema.string().required(),
        data: [
          {
            name: 'Select channel',
            value: ''
          }
        ]
    },

    permissions: {
        label: 'Permissions',
        type: 'checkboxGroup',
        data: [
            {
                label: "User",
                data: [
                    {
                        value: 'users.read',
                        label: 'Read'
                    },
                    {
                        value: 'users.write',
                        label: 'Write'
                    }
                ]
            },{
                label: "Theme",
                data: [
                    {
                        value: 'themes.read',
                        label: 'Read'
                    },
                    {
                        value: 'themes.write',
                        label: 'Write'
                    }
                ]
            }
        ]
    },
};


export const defaultForm = {
  channel_id: ''
};

export default AssignmentSchema;
