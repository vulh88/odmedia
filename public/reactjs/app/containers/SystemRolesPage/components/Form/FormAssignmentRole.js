import React from 'react';
import {connect} from 'react-redux';
import { createStructuredSelector } from 'reselect';
import AssignmentSchema, {defaultForm} from './AssignmentSchema';
import SmartForm from '../../../../helpers/form/SmartForm';
import apiService from 'services/api/permissions';
import channelApiService from 'services/api/channels';
import withAsync from '../../../../helpers/react/withAsync/withAsync';
import PropTypes from 'prop-types';
import routePaths from '../../../../settings/route-paths';
import toast from '../../../../helpers/user-interface/toast';
import HaveApiComponent from '../../../../components/BaseComponents/HaveApiComponent';
import {makeSelectChannel} from 'store/app/selectors';
import userApiService from 'services/api/users';

@withAsync()
class FormAssignmentRole extends HaveApiComponent {

  static propsTypes = {
    currentChannel: PropTypes.object,
  };

  static async preload(props) {
    let schema = {...AssignmentSchema};
    let defaultData = {...defaultForm};

    const data = await apiService.getPermissionGroups({});
    schema.permissions.data = [schema.permissions.data];
    schema.permissions.data = data;

    if (props.match.params.channel_id) {
      const permissionChannel = await apiService.getPermissionChannel(
          props.match.params.id,
          {
            channel_id: props.match.params.channel_id
          }
      );
      defaultData.channel_id = props.match.params.channel_id;
      schema.permissions.data.map((el, key) => {
        el.data.map((child, idx) => {
          if (permissionChannel.permissions.indexOf(child.value) > -1) {

            child.checked = true;
          }
        })
      });
    }

    const user = await userApiService.getOne(props.match.params.id);
    schema.user.value = user.email;

    const channelsData = await channelApiService.getList({
      count: 1000
    });
    schema.channel_id.data = channelsData.data.map(e => ({
      name: e.name,
      value: e.id
    }));
    schema.channel_id.data.push({
      name: 'Select channel',
      value: ''
    });

    return {
      schema,
      defaultData
    };
  };

  componentDidMount() {

  };

  handleSubmit = params => {
    const {history, preloadData, match} = this.props;
    let permissions = [];
    if (params) {
      for (let key in  params) {
        if (key.indexOf('permissions') > -1 && params[key] != 0) {
          permissions.push(params[key]);
        }
      }
    }

    const data = {
      channel_id: params.channel_id,
      permissions: permissions
    };
    this.callApi(
        apiService.assignPermissionChannel(match.params.id, data),
        () => {
          toast.success('Assigned the roles for user successfully');
          history.push(routePaths.roles.assignments);
        }
    );
  };

  render() {
      const {preloadData, loading} = this.props;
      const {schema, defaultData} = preloadData;

      return(
          <SmartForm
              className="portlet light"
              ref={e => (this.form = e)}
              loading={loading}
              defaultData={defaultData}
              schema={schema}
              onSubmit={this.handleSubmit}
          >
          </SmartForm>
      );
  }
}

const mapStateToProps = createStructuredSelector({
  currentChannel: makeSelectChannel()
});

export default connect(
    mapStateToProps,
    null
)(FormAssignmentRole);