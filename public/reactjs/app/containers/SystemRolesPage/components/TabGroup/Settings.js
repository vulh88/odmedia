import React from 'react';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import styled from 'styled-components';

import Toggle from 'components/Toggle';

import { makeSelectToken } from 'store/app/selectors';
import * as actions from '../../stores/actions';
import {
  makeSelectError,
  makeSelectLoading,
  makeSelectPaging
} from '../../stores/selectors';

import messages from '../../messages';

const Wrapper = styled.div`
  padding: 0 10%;

  .wrapper-action {
    margin-top: 20px;
  }
`;

const WrapperField = styled.div`
  display: flex;
  align-items: center;

  .wrapper-label {
    flex-grow: 1;
  }

  .wrapper-toggle {
    flex-grow: 10;
  }
`;

class Settings extends React.PureComponent {
  componentWillMount() {
    // this.props.dispatch(
    //   actions.requestUsers({
    //     accessToken: this.props.accessToken,
    //     page: 1
    //   })
    // );
  }

  onChangeSettings = e => {
    console.log(e.target.value);
  };

  handleSaveSettings = () => {};

  render() {
    const { users, currentPage, totalPage, limitPage, loading } = this.props;

    return (
      <Wrapper>
        <WrapperField>
          <div className="wrapper-label">
            <label className="control-label">
              Super admin group <span className="error-star">*</span>
            </label>
          </div>
          <div className="wrapper-toggle">
            <Toggle
              values={['superAdmin', 'admin']}
              messages={messages}
              onToggle={this.onChangeSettings}
            />
          </div>
        </WrapperField>
        <div className="wrapper-action text-right">
          <button className="btn green" onClick={this.handleSaveSettings}>
            Save
          </button>
        </div>
      </Wrapper>
    );
  }
}
const mapStateToProps = createStructuredSelector({
  paging: makeSelectPaging(),
  error: makeSelectError(),
  loading: makeSelectLoading()
});

export default connect(
  mapStateToProps,
  null
)(Settings);
