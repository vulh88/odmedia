import React from 'react';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { PermissionsApiService } from 'services/api/permissions';
import ManageTable from 'components/ManageTable/index'
import { makeSelectToken } from 'store/app/selectors';
import ListingPageWithChannel from '../../../../components/BaseComponents/ListingPageWithChannel';
import {withRouter} from 'react-router-dom';

@withRouter
class Permissions extends ListingPageWithChannel {

  restApiService = new PermissionsApiService();

  columns = [
    {
      name: 'ID',
      value: e => e.id,
      sort: 'id',
      style: {
        width: 60,
        textAlign: 'center',
        justifyContent: 'center',
        flexGrow: 0
      }
    },
    {
      name: 'System Name',
      value: e => e.name,
      sort: 'name'
    },
    {
      name: 'Description',
      value: ''
    }

  ];

  render() {
    const { data, paging, loading } = this.apiState;
    const { columns } = this;

    return (
        <div>
          <div className="portlet box light">
            <div className="portlet-body">
              <div className="tab-content">
                <ManageTable
                    loading={loading}
                    data={data || []}
                    paging={paging || {}}
                    handleChangePage={this.handlePageChange}
                    handleLimitChange={this.handleLimitChange}
                    handleSortBy={this.handleSortBy}
                    columns={columns}
                    showActions={false}
                    styleActions={{ width: 120, flexGrow: 0 }}
                />
              </div>
            </div>
          </div>
        </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({

});

export default connect(
  mapStateToProps,
  null
)(Permissions);
