import React from 'react';
import Alert from 'react-s-alert';

import { TabsLink } from 'components/Tabs';

import Assignments from './Assignments';
import FormAssignments from '../Form/Assignments';
import Groups from './Groups';
import Permissions from './Permissions';
import Settings from './Settings';
import {withRouter} from 'react-router-dom';
import routePaths from '../../../../settings/route-paths';
import FormAssignmentRole from '../Form/FormAssignmentRole';

@withRouter
export class UserTableView extends React.PureComponent {
  shouldComponentUpdate(nextProps) {
    if (nextProps.error) {
      switch (nextProps.error.code) {
        case 404:
          Alert.error('This user has already contained', {
            position: 'bottom-right'
          });
          break;

        default:
          Alert.error('Resource not found.', {
            position: 'bottom-right'
          });
          break;
      }
    }

    return true;
  }

  render() {
    const { location, loading, match } = this.props;
    const isUpdate = match.path === routePaths.roles.assignAddNew || match.path === routePaths.roles.assignUpdate;

    return (
      <div>
        <h1 className="page-title">System Roles</h1>
        <TabsLink
          loading={loading}
          indexActive={(location.state && location.state.indexActive) || 0}
          labelList={[
            {
              label: 'Assignments',
              linkTo: routePaths.roles.assignments,
              state: { indexActive: 0 },
              content: isUpdate ? (
                <FormAssignmentRole {...this.props} />
              ) : (
                <Assignments {...this.props} />
              )
            },
            {
              label: 'Groups',
              linkTo: routePaths.roles.groups,
              state: { indexActive: 1 },
              content: <Groups {...this.props} />
            },
            {
              label: 'Permissions',
              linkTo: routePaths.roles.permissions,
              state: { indexActive: 2 },
              content: <Permissions {...this.props} />
            }
          ]}
        />
      </div>
    );
  }
}

export default UserTableView;
