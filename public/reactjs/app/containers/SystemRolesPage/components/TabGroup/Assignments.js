import React from 'react';
import locationHelper from '../../../../helpers/location';
import routePaths from '../../../../settings/route-paths';
import ListingPage from 'components/BaseComponents/ListingPage';
import usersApiService from 'services/api/users';
import ManageTable from '../../../../components/ManageTable';
import FormAlert from '../../../../components/FormAlert';

class Assignments extends ListingPage {
  restApiService = usersApiService;

  actions = [
    {
      icon: 'fa fa-shield',
      backgroundColor: '#e1e5ec',
      handler: e => this.handleClickAssign(e.id)
    }
  ];

  columns = [
    {
      name: 'ID',
      value: e => e.id,
      sort: 'id',
      style: {
        width: 60,
        textAlign: 'center',
        justifyContent: 'center',
        flexGrow: 0
      }
    },
    {
      name: 'User Email',
      value: e => e.email,
      sort: 'email'
    }
  ];

  handleClickAssign = id => {
    const { history } = this.props;
    history.push(
      locationHelper.getLink(routePaths.roles.assignAddNew, { id })
    );
  };

  render() {
    const { columns, actions } = this;
    const { loading, data, paging, errors, success } = this.apiState;
    return (
      <div>
        <FormAlert errors={errors} success={success} />
        <ManageTable
          loading={loading}
          handleSortBy={this.handleSortBy}
          data={data || []}
          paging={paging || {}}
          columns={columns}
          actions={actions}
          styleActions={{ width: 120, flexGrow: 0 }}
        />
      </div>
    );
  }
}

export default Assignments;
