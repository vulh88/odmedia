import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

export class SupportPage extends React.PureComponent {
  static propTypes = { component: PropTypes.any };

  componentDidMount() {}

  render() {
    return (
      <div>
        <Helmet>
          <title>Support Page</title>
          <meta
            name="description"
            content="A React.js Boilerplate application Supportpage"
          />
        </Helmet>
        <h1 className="page-title">
          <small>Coming soon</small>
        </h1>
      </div>
    );
  }
}
const mapStateToProps = createStructuredSelector({});
const withConnect = connect(mapStateToProps, null);

export default withConnect(SupportPage);
