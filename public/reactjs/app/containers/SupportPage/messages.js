import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'odmedia.containers.NotFoundPage.header',
    defaultMessage: 'Page not found.'
  }
});
