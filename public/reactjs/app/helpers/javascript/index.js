export function hookFunction(target, functionName, callBefore, name) {
  const _originalFunction = target[functionName];
  if (_originalFunction) {
    const newFunction = function (...params) {
      if (callBefore) {
        callBefore.apply(this, params);
      }
      const res = _originalFunction.apply(this, params);
      console.log(`\n::${name} ::`);
      console.log(`Run: ${functionName}:`);
      console.log('Arguments:', params);
      console.log('Return:', typeof res, res);
      return res;
    };
    target[functionName] = newFunction;
  }
}

export function withDebug(component) {
  return class extends component {
    constructor(props) {
      super(props);
      for (const key of Object.getOwnPropertyNames(this)) {
        console.log(key);
        if (key !== 'constructor' && typeof val === 'function') {
          hookFunction(this, key, null, component.name);
        }
      }
    }
  };
}