/* eslint-disable prefer-template */
import {addYears} from 'date-fns';
import lodash from 'lodash';

export const optionsCookies = {
  path: '/',
  expires: addYears(new Date(), 10)
};

export const checkSamePasswords = x => x.password === x.password_confirmation;

export const isEmail = s =>
    /^(([^<>()[\]\\.,;:`~#$%^&*+=!\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        s
    );


export const convertToArray = (data) => Object.entries(data).map(([key, v]) => v);

export function stripFields(data,fields ) {
  fields.map(e => {
    if(data[e] === '' || data[e] === undefined || data[e] === null) {
      delete data[e];
    }
  });
}

export function plural( count, name, ext = 's') {
  if (count <= 1) {
    return `${count} ${name}`;
  }
  return `${count} ${name + ext}`;

}

export function isDevelopment() {
  return process && process.env && process.env.NODE_ENV === 'development';
}
/**
 * time as seconds
 * @param time
 * @returns {string}
 */
export const formatDuration = time => {
  let duration = time ? parseInt(time) : 0;
  let hours = Math.floor(duration / 3600);
  let minutes = Math.floor((duration - hours * 3600) / 60);
  let seconds = (duration - hours * 3600 - minutes * 60).toFixed(0);

  if (hours < 10) {
    hours = `0${hours}`;
  }
  if (minutes < 10) {
    minutes = `0${minutes}`;
  }
  if (seconds < 10) {
    seconds = `0${seconds}`;
  }

  return `${hours}:${minutes}:${seconds}`;
};

export const isInteger = s => /^[0-9]*[1-9]+$|^[1-9]+[0-9]*$/.test(s);

function escapeRegExp(str) {
  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

export function arrayToRegexAll(arrString) {
  return arrString.map(e => `(${e})`).join('|');
}

export function delayForATime(time = 0) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(true);
    }, time);
  });
}

export function truncateFileName(filename, n = 10, omission = '[...]') {
  if (filename.length > n + omission.length) {
    const pat = /\.(\w{0,3})$/;
    let extension = '';
    if (pat.test(filename)) {
      extension = pat.exec(filename)[1];
    }
    return `${lodash.truncate(filename, {
      length: n, omission: '[...]'
    })}.${extension}`;
  }
  return filename;
}

export const utils = {
  intervalWorker(condition, timeRecheck = 100) {
    return new Promise((resolve, reject) => {
      const interval = setInterval(() => {
        if (condition()) {
          clearInterval(interval);
          resolve(true);
        }
      }, timeRecheck);
    });
  },
  scrollToElement(selector, offset = 0) {
    const el = typeof selector === 'string' ? $(selector).get(0) : selector;
    if (el) {
      window.scrollTo(0, el.offsetTop - offset);
    }
  },
  isInternalUrl(url) {
    return url && url[0] === '/';
  },
  escapeRegExp(str) {
    return str.replace(/[\-\[\]\/{}()*+?.\\^$|]/g, "\\$&");
  },
  isExternalLink(url, location) {
    const domainRe = /https?:\/\/([\w:.-]+)(\/|$)/i;

    function domain(url) {
      if (domainRe.test(url)) {
        return domainRe.exec(url)[1];
      }
      return null;
    }

    return domain(url) !== null && domain(location) !== domain(url);
  },
  isNullAll(obj) {
    for (const key of Object.keys(obj)) {
      if (obj[key]) {
        return false;
      }
    }
    return true;
  },
  getDateWithTimeZone(offset) {
    const d = new Date();
    const utc = d.getTime() - (d.getTimezoneOffset() * 60000);
    return new Date(utc + (3600000 * offset));
  },
  convertToLocalTime(date, offset) {
    const utc = date.getTime() - (3600000 * offset);
    return new Date(utc + (new Date().getTimezoneOffset() * 60000));
  },
  minutes: (second) => {
    return self.leftPad(Math.floor(second / 60)) + ':' + self.leftPad(second % 60);
  },
  leftPad: (number, pad = 2) => {
    const str = number.toString();
    pad = new Array(pad + 1).join('0');
    return pad.substring(0, pad.length - str.length) + str;
  },
  makeArray(item) {
    if (!item) {
      return [];
    }
    else {
      if (Array.isArray(item)) {
        return item;
      }
      else {
        return [item];
      }
    }
  },
  pointNumber: (bigNumber, separator = ',') => {
    if (bigNumber && bigNumber.toString()) {
      return bigNumber.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1" + separator);
    }
    else {
      return '0';
    }
  },
  pathParams: (str, params) => {
    Object.keys(params).map(key => {
      str = str.replace(`:${key}`, params[key]);
    });
    return str;
  },
  smartGo(url, history, isNewTab = true) {
    if (utils.isExternalLink(url, window.location)) {
      window.open(url, isNewTab ? "_self" : null);
    }
    else {
      if (history) {
        history.push(url.replace(/https?:\/\/([\w:.-]+)(\/|$)/i, '/'));
      }
    }
  },
  standardUrl(path) {
    // Reformat path
    let res = path;
    if (path[path.length - 1] !== '/') {
      res += '/';
    }
    return res;
  },
  isSamePath(path1, path2) {
    return utils.standardUrl(path1) === utils.standardUrl(path2);
  },
  objectToArray(obj) {
    return Object.entries(obj).map(([key, val]) => {
      val._key = key;
      return val;
    });
  },
  convertToFormData(data) {
    const formData = new FormData();
    Object.entries(data).map(([key, val]) => {
      if (key && val) {
        formData.append(key, val);
      }
    });
    return formData;
  },
  cutStringByChar(content, nChar) {
    if (content.length > nChar) {
      return content.substring(0, nChar + 1) + '...';
    } else {
      return content;
    }
  },
  cutString: (string, num) => {
    if (string && num) {
      let arr = string.split(' ');
      if (arr.length > num) {
        arr = arr.slice(0, num);
        string = arr.join(' ') + '...';
      }
    }
    return string;
  },
  triggerWindowResized: () => {
    if (window) {
      window.requestAnimationFrame(() => {
        window.dispatchEvent(new Event('resize'));
      });
    }
  }
};
