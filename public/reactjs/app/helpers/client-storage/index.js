import Cookies from 'universal-cookie';

const cookies = new Cookies({path: '/'});
const tokenKey = 'access_token';
const selectedChannelKey = 'selected_channel';
const userKey = 'user';

class ClientStorageHelper {
  static getToken() {
    return cookies.get(tokenKey);
  }

  static saveUserData(payload) {
    if (payload.user) {
      try {
        localStorage.setItem(userKey, JSON.stringify(payload.user));
      }
      catch (e) {

      }
    }
    ClientStorageHelper.__saveTokenData(payload);
  }

  static clearUserData() {
    ClientStorageHelper.__clearToken();
    cookies.remove(userKey);
  }

  static __saveTokenData({expires_in, token_type, access_token}) {
    cookies.set(tokenKey, `${token_type} ${access_token}`, {
      maxAge: expires_in
    });
  }

  static getUserData() {
    try {
      const data = localStorage.getItem(userKey);
      if (data) {
        return JSON.parse(data);
      }
      return null;
    }
    catch (err) {
      cookies.remove(userKey);
      console.warn(err);
      return null;
    }
  }

  static __clearToken() {
    cookies.remove(tokenKey);
  }

  // Channel
  static saveSelectedChannel(channel) {
    if (channel) {
      cookies.set(selectedChannelKey, channel.id);
    }
  }

  static getSelectedChannel() {
    return cookies.get(selectedChannelKey);
  }

  static clearAllData() {
    cookies.remove(userKey);
    cookies.remove(tokenKey);
    cookies.remove(selectedChannelKey);
  }

}

export default ClientStorageHelper;

