import React from 'react';
import {SmartForm} from './';
import schema from './schema';

class ExampleFormComponent extends React.PureComponent {
  schema = {
    email: {
      label: 'Email',
      type: 'text',
      validate: schema.string().email().required(),
      domProps: {
        defaultValue: 'defaultEmail@gmail.com'
      }
    },
    password: {
      label: 'Password',
      type: 'password',
      validate: schema.string().min(6).required()
    },
    fullName: {
      type: 'text',
      label: 'Full name',
      validate: schema.string().required()
    },
    gender: {
      label: 'Gender',
      type: 'select',
      validate: schema.string().oneOf(['female', 'male']).required(),
      data: [
        {
          name: 'Female',
          value: 'female'
        },
        {
          name: 'Male',
          value: 'male'
        }
      ]
    }
  };

  onSubmit(data) {
    // call api
    console.log('Submit data', data);
    return null;
  }

  render() {
    return (
        <SmartForm
            onSubmit={this.onSubmit}
            liveValidate={true}
            schema={this.schema}
            defaultData={{
              email: 'donle@gmail.com',
              password: 'donle@gmail.comdonle@gmail.com',
              fullName: 'lalalalalal',
            }}
            renderInner={({email, gender, password, fullName}) => (
                <div className="inner">
                  <div className="row">
                    <div className="col-lg-12">
                      {fullName.render()}
                      {gender.render()}
                    </div>
                    <div className="col-md-6">
                      {email.render()}
                    </div>
                    <div className="col-md-6">
                      {password.render()}
                    </div>
                  </div>
                  <button type="submit">Submit</button>
                </div>
            )}/>
    );
  }
}


export default ExampleFormComponent;
