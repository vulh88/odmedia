import React from 'react';
import lodash from 'lodash';
import ButtonLoading from '../../../components/ButtonLoading';


function _renderSubmitButton({textButton, loading, isShowSubmit}) {
  let submitButton = null;
  if (isShowSubmit) {
    submitButton = <ButtonLoading
        loading={loading}
        type="submit"
        className="btn green mt-ladda-btn">{textButton || 'Submit'}</ButtonLoading>
  }
  return submitButton;
}

export function renderHorizontalFormLayout({fields, textButton, isShowSubmit, children, loading}) {
  const submitButton = _renderSubmitButton({textButton, isShowSubmit, loading});

  return (
      <div className="form-horizontal form-bordered form-row-stripped">
        <div className="form-body">
          {
            lodash.map(fields, (e, idz) => {
              if (e.config.type ){
                return (
                    <div key={idz} className={`form-group ${e.getControlClassName()}`}>
                      <label className="control-label col-md-3">
                        {e.renderLabel()}
                      </label>
                      <div className="col-md-9">
                        {e.renderInput()}
                        {e.renderErrors()}
                        {e.renderHelpBlocks()}
                      </div>
                    </div>)

              }
            })
          }
          {
            submitButton ?
                <div className="form-actions">
                  <div className="row">
                    <div className="col-md-offset-3 col-md-9">
                      {submitButton}
                    </div>
                  </div>
                </div> : null
          }
          {children}
        </div>
      </div>
  );
}

export function renderInlineFormLayout({fields, textButton, isShowSubmit, children, loading}) {
  return (
      <div className="form-inline">
        {
          lodash.map(fields, (e, idz) => {
            if (e.config.type ){
              return (
                  <div key={idz} className={`form-group ${e.getControlClassName()}`}>
                    <label className="sr-only">
                      {e.renderLabel()}
                    </label>
                    {e.renderInput()}
                    {e.renderErrors()}
                    {e.renderHelpBlocks()}
                  </div>)
            }
          })
        }
        {_renderSubmitButton({textButton, isShowSubmit, loading})}
        {children}
      </div>
  )
}

