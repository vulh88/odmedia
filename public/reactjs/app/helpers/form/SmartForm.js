/* eslint-disable prefer-const,no-await-in-loop */
import React from 'react';
import lodash from 'lodash';
import PropTypes from 'prop-types';
import FormControlFactory from './controls/FormControlFactory';
import FormHelper from './helper';
import schema from './schema';
import {renderHorizontalFormLayout} from './render-layouts';
import BlockLoading from '../../components/BlockLoading';
import toast from '../user-interface/toast';


// Implement smart form
class SmartForm extends React.Component {
  static propsTypes = {
    renderInner: PropTypes.func.isRequired,
    onChange: PropTypes.func,
    schema: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onError: PropTypes.func,
    liveValidate: PropTypes.bool,
    loading: PropTypes.bool,
    formRowSeparated: PropTypes.bool,
    defaultData: PropTypes.object,
    confirmUpdate: PropTypes.bool,
    isShowSubmit: PropTypes.bool,
    toastError: PropTypes.bool
  };

  static defaultProps = {
    renderInner: renderHorizontalFormLayout,
    liveValidate: true,
    formRowSeparated: true,
    loading: false,
    confirmUpdate: false,
    isShowSubmit: true,
    toastError: false
  };

  controls = null;
  formSchema = null;
  // Form ref
  form = null;

  constructor(props) {
    super(props);
    this.parseFormControls();
  }

  componentDidMount() {
    this._mounted = true;
    this.applyDefaultData();
  }

  componentWillUnmount() {
    this._mounted = false;
  }

  componentDidUpdate(prevProps) {
    if (prevProps.schema !== this.props.schema) {
      this.parseFormControls();
    }
  }

  applyDefaultData() {
    if (this.props.defaultData) {
      FormHelper.applyData(this.form, this.props.defaultData);
    }
  }

  parseFormControls() {
    this.controls = {};
    this.formSchema = {};

    for (let [name, config] of Object.entries(this.props.schema)) {
      // Create controls
      config = {
        ...config,
        name,
        onFormChange: this.handleFormDataChange,
        initValue: this.props.defaultData && this.props.defaultData[name]
      };
      this.controls[name] = FormControlFactory.getControl(config);

      // Build schema
      this.formSchema[name] = config.validate || schema.mixed();
    }
    this.update();
  }

  getData() {
    if (this.form) {
      return FormHelper.formToObject(this.form);
    }
    return null;
  }

  reset() {
    if (this.form) {
      this.form.reset();
      this.applyDefaultData();
      this.update();
    }
  }

  handleSubmit = e => {
    e.preventDefault();
    const data = this.getData();
    if (this.validateAll(data)) {
      this.props.onSubmit(
          schema
              .object()
              .shape(this.formSchema)
              .cast(data)
      );
    }

  };

  isValid() {
    const data = this.getData();
    if (this.validateAll(data)) {
      return true;
    }
    return false;
  }

  handleFormDataChange = ({target}) => {
    const {name} = target;
    if (name && this.controls[name]) {
      this.handleInputChange(name, target);
    }
    if (this.props.onChange) {
      this.props.onChange(this.getData(), target);
    }
  };

  handleInputChange = (name, input) => {
    if (this.props.liveValidate) {
      this.validateControl(name, input.value);
    }
  };

  clearErrors() {
    for (let [, control] of Object.entries(this.controls)) {
      control.setErrors(null);
    }
    this.update();
  }

  validateAll(data) {
    this.clearErrors();
    try {
      schema
        .object()
        .shape(this.formSchema)
        .validateSync(data, {
          abortEarly: false,
          stripUnknown: true
        });
      return true;
    } catch (errors) {
      if (errors.inner) {
        errors.inner.map(e => {
          const [name, errors] = [e.path, e.errors];
          if (this.controls[name]) {
            this.controls[name].setErrors(errors);
          }
          return null;
        });
        this.update();
        this.handleError(errors);
      } else {
        console.error(errors);
      }
      return false;
    }
  }

  validateControl(name, value) {
    if (this.formSchema[name].validate) {
      this.controls[name].setErrors(null);
      this.update();
      try {
        schema
          .object()
          .shape({ [name]: this.formSchema[name] })
          .validateSync(
            { [name]: value },
            {
              abortEarly: false,
              stripUnknown: true
            }
          );
        return true;
      } catch ({ errors }) {
        this.controls[name].setErrors(errors);
        this.update();
        return false;
      }
    }
    return true;
  }

  handleError(errors) {
    if (this.props.toastError) {
      toast.error('There are some errors in your form data. Please check them again!');
    }
    if (this.props.onError) {
      this.props.onError(errors.inner);
    }

  }


  renderFormContent() {
    if (this.controls) {
      if (lodash.isFunction(this.props.renderInner)) {
        return this.props.renderInner({
          fields: this.controls,
          loading: this.props.loading,
          textButton: this.props.textButton,
          isShowSubmit: this.props.isShowSubmit,
          children: this.props.children
        });
      }
    }
    return null;
  }

  update() {
    if (this._mounted) {
      this.forceUpdate();
    }
  }

  render() {
    const { className, loading } = this.props;
    return (
      <form
        ref={e => (this.form = e)}
        onChange={this.handleFormDataChange}
        onSubmit={this.handleSubmit}
        className={`smart-form form ${className || ''}`}
      >
        {
          loading &&
          <div className="wrapper-loading">
            <BlockLoading/>
          </div>
        }
        {this.renderFormContent()}
      </form>
    );
  }
}

export default SmartForm;
