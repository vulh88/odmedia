import BaseFormControl from './BaseFormControl';
import React from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';

class DatePickerInput extends React.PureComponent {

  state = {
    selected: this.props.config.initSetting ? moment(this.props.config.initSetting, this.props.format) : moment()
  };

  handleChange = (date) => {
    const {config} = this.props;
    this.setState({
      selected: date
    }, () => {
      config.onFormChange({target: {name: config.name}});
    });
  };

  render() {
    const {name, format} = this.props.config;
    return <DatePicker
        dateFormat={format}
        name={name}
        readOnly={true}
        selected={this.state.selected}
        onChange={this.handleChange}
        className="form-control bg-white"
    />;
  }
}

class InputDatePickerControl extends BaseFormControl {
  renderInput() {
    this.config.format = this.config.format || 'YYYY-MM-DD';
    return <DatePickerInput config={this.config} />;
  }
}

export default InputDatePickerControl;