import React from 'react';
import BaseFormControl from './BaseFormControl';

class RadioControl extends BaseFormControl{

  renderLabel() {
    return super.renderLabel() || ' ';
  }

  renderInput() {
    const {name, data, inline} = this.config;
    const className = `mt-radio-${inline ? 'inline' : 'list'}`;
    return (
        <div className={className}>
          {
            data && data.map((e, idz) => (
                <label key={idz} className="mt-radio  mt-radio-outline">
                  <input type="radio" name={name} id={`option-${name}-${idz}`} value={e.value} {...e.domProps}/> {e.name}
                    <span/>
                </label>
            ))
          }
        </div>
    );
  }
}

export default RadioControl;
