import BaseFormControl from './BaseFormControl';
import React from 'react';

class ViewOnlyControl extends BaseFormControl {
  renderInput() {
    const {value, transform, initValue, fakeInput} = this.config;
    let style = null;
    if (fakeInput) {
      style = {cursor: 'not-allowed'};
    }
    else {
      style = {
        border: 'none',
        backgroundColor: 'transparent',
        paddingLeft: 0
      }
    }
    let renderValue = initValue || value;
    if (typeof transform === 'function') {
      renderValue = transform(renderValue);
    }
    return <div className="form-control" readOnly style={style}> {renderValue}</div>;
  }
}

export default ViewOnlyControl;