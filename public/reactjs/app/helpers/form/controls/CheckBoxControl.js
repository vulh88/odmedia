import React from 'react';
import BaseFormControl from './BaseFormControl';

class CheckBoxControl extends BaseFormControl{


  renderLabel() {
    return super.renderLabel() || ' ';
  }

  renderInput() {
    const {name, data, inline} = this.config;
    const className = `no-padding mt-checkbox-${inline ? 'inline' : 'list'}`;
    return (
        <div className={className}>
          {
            data && data.map((e, idz) => (
                <label key={idz} className="mt-checkbox  mt-checkbox-outline">
                  <input type="checkbox"
                         name={name}
                         id={`option-${name}-${idz}`}
                         value={e.value} {...e.domProps}/> {e.name}
                  <span/>
                </label>
            ))
          }
        </div>
    );
  }

}

export default CheckBoxControl;
