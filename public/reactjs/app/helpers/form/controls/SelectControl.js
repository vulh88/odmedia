import BaseFormControl from './BaseFormControl';
import React from 'react';

class SelectControl extends BaseFormControl {
  renderInput() {
    const {name, data, domProps} = this.config;
    return (
        <select name={name} className="form-control" {...domProps}>{
          data && data.map((e) => <option key={e.value} value={e.value} {...e.domProps}>{e.name}</option>)}
        </select>
    );
  }
}

export default SelectControl;

