import BaseFormControl from './BaseFormControl';
import 'rc-time-picker/assets/index.css';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

const appRoot = document.getElementById('app');

export class FormatDurationControl extends BaseFormControl {
  renderInput() {
    console.log('config :', this.config);
    return <FormatDurationInput config={this.config} />;
  }
}

class FormatDurationInput extends Component {
  constructor(props) {
    super(props);
    this.containerEl = document.createElement('div');
    this.state = {
      changeInput: false
    }
  }

  componentDidMount() {
    appRoot.appendChild(this.containerEl);
  }

  renderDuration = (e) => {
    console.log(11111);
    this.setState({
      changeInput: true
    });
  };

  render() {
    const {name, type, domProps, placeholder, label, disabled} = this.props.config;
    const {changeInput} = this.state;
    console.log('changeInput  ', changeInput);
    return(
        <div>
          <input className="form-control" placeholder={placeholder || label} name={name} type={type} onBlur={this.props.config.onFormChange} {...domProps} disabled={disabled || false} onClick={(e) => this.renderDuration(e)} />
          {
            changeInput ? <DurationList containerEl={this.containerEl} /> : null
          }
        </div>
    );
  }
}

const DurationList = (props) => {
  return (
      <div className="rc-time-picker-panel  rc-time-picker-panel-column-3 rc-time-picker-panel-placement-bottomLeft">
        <div className="rc-time-picker-panel-inner">
          <div className="rc-time-picker-panel-input-wrap">

          </div>
          <div className="rc-time-picker-panel-combobox">
            <div className="rc-time-picker-panel-select">
              <ul>
                <li className="rc-time-picker-panel-select-option-selected">00</li>
                <li className>01</li></ul>
            </div>
            <div className="rc-time-picker-panel-select">
              <ul>
                <li className="rc-time-picker-panel-select-option-selected">00</li>
                <li className>01</li>
                <li className>02</li>
              </ul>
            </div>
            <div className="rc-time-picker-panel-select">
              <ul>
                <li className="rc-time-picker-panel-select-option-selected">00</li>
                <li className>01</li>
                <li className>02</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
  );
};
