import BaseFormControl from './BaseFormControl';
import React from 'react';
import moment from 'moment';
import rome from 'rome/dist/rome.js';
import 'rome/dist/rome.css';

class DateTimePickerInput extends React.PureComponent {

  state = {
    selected: this.props.config.initValue && this.props.config.initValue.length ? moment(this.props.config.initValue, this.props.format) : moment()
  };

  componentDidMount() {
    const {format, min, disableTime, options, disableDate} = this.props.config;
    this.rome = rome(this.input, {
      appendTo: document.body,
      initialValue: this.state.selected,
      inputFormat: format,
      min,
      date: !disableDate,
      time: !disableTime,
      ...options
    });
  }

  componentWillUnmount() {
    this.rome.destroy();
  }

  render() {
    const {name, format, disabled} = this.props.config;
    return <input
        ref={e => this.input = e}
        name={name}
        disabled={disabled}
        autoComplete="off"
        placeholder={format}
        className="form-control"
    />;
  }
}

class DateTimePickerControl extends BaseFormControl {
  renderInput() {
    this.config.format = this.config.format || 'YYYY-MM-DD HH:mm:ss';
    return <DateTimePickerInput config={this.config}/>;
  }
}

export default DateTimePickerControl;