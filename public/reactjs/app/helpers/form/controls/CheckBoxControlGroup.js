import React from 'react';
import BaseFormControl from './BaseFormControl';

class CheckBoxGroup extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  onChange = (e, index) => {

  };

  render() {
    const {name, data, inline} = this.props.config;
    return (
        <div>
          {
            data && data.map((e, key) => (
              <div key={'p-'+key}>
                <div className="form-group form-md-checkboxes">{e.label}</div>
                <div className="md-checkbox-inline">
                  {
                    e.data.map((eChild, keyChild) => (
                        <div className="md-checkbox" key={'c-'+ keyChild}>
                          <input type="checkbox" id={eChild.value} className="md-check" name={name + "[" + eChild.name + "]" + "[" + keyChild + "]" } value={eChild.value} onChange={e => this.onChange(e, keyChild)} defaultChecked={eChild.checked} />
                          <label htmlFor={eChild.value}>
                            <span className="inc"></span>
                            <span className="check"></span>
                            <span className="box"></span> {eChild.label}
                          </label>
                        </div>
                    ))
                  }

                </div>
              </div>
            ))

          }
        </div>
    );
  }
}

class CheckBoxControlGroup extends BaseFormControl{

  renderLabel() {
    return super.renderLabel() || ' ';
  }

  renderInput() {
    return (
        <CheckBoxGroup config={this.config} />
    );
  }

}

export default CheckBoxControlGroup;
