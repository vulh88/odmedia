import BaseFormControl from './BaseFormControl';
import React from 'react';

class InputControl extends BaseFormControl {
  renderInput() {
    const {name, type, domProps, placeholder, label, min, max, disabled} = this.config;
    let input = null;
    if ( min || max) {
       input = <input className="form-control" placeholder={placeholder || label} name={name} type={type} onBlur={this.config.onFormChange} {...domProps} min={min} max={max} disabled={disabled || false} />;
    } else {
       input = <input className="form-control" placeholder={placeholder || label} name={name} type={type} onBlur={this.config.onFormChange} {...domProps} disabled={disabled || false} />;
    }

    return input;
  }
}

export default InputControl;