import BaseFormControl from './BaseFormControl';
import React from 'react';
import {Creatable} from 'react-select'



class TagInput extends React.PureComponent {

  render() {
    const {single, name} = this.props.config;
    const {selectedOptions} = this.props;
    return <Creatable
        isClearable
        defaultValue={selectedOptions}
        name={name}
        onChange={console.log}
        isMulti={!single}/>
  }
}


class TagsControl extends BaseFormControl {

  renderInput() {
    const {initValue} = this.config;
    let selectedOptions = [];


    if (Array.isArray(initValue)) {
      selectedOptions = initValue.map(e => {
        return {
          value: e.name,
          label: e.name
        }
      });
    }

    return <TagInput config={this.config} selectedOptions={selectedOptions}/>
  }
}

export default TagsControl;
