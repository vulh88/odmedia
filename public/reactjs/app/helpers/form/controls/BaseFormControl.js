import React from 'react';

class BaseFormControl {
  config = {};
  errors = null;

  constructor(config) {
    this.config = config;
  }

  setErrors(errors) {
    this.errors = errors && errors.map(this.formatMessage);
  }

  formatMessage = (error) => {
    const pat = new RegExp(`(^|\\s)${this.config.name}([\\s]|$)`, 'g');
    if (this.config.label) {
      error = error.replace(pat, `$1${this.config.label}$2`);
    }
    return error;
  };

  isRequired() {
    return this.config.validate && this.config.validate.tests && this.config.validate.tests.find(e => e.TEST && e.TEST.name === 'required');
  }

  renderLabel() {
    const {label, name} = this.config;
    if (label) {
      return (
          <span>
            {label}
            {this.isRequired() && <span className="error-star"> *</span>}
          </span>
      );
    }
    return null;
  }

  renderInput() {
    return null;
  }

  renderErrors() {
    return this.errors && this.errors.map((e, idz) => <span key={idz} className="help-block">{e}</span>);
  }

  renderHelpBlocks() {
    const {summary} = this.config;
    if (summary) {
      return <span className="help-block help-block--not-error">{summary}</span>;
    }
    return null;
  }

  getControlClassName() {
    const {type, name, className} = this.config;
    return `form-control--name-${name} form-control--${type} ${className || ''} ${this.errors && this.errors.length ? 'has-error' : ''}`
  }

  render() {
    const {type, name} = this.config;
    if (!type) {
      return null;
    }
    return (
        <div key={name} className={`form-group ${this.getControlClassName()}`}>
          {this.renderLabel()}
          <div className="control__input">
            {this.renderInput()}
            {this.renderErrors()}
            {this.renderHelpBlocks()}
          </div>
        </div>
    );
  }
}

export default BaseFormControl;
