import InputControl from './InputControl';
import SelectControl from './SelectControl';
import CheckBoxControl from './CheckBoxControl';
import TextAreaControl from './TextAreaControl';
import RadioControl from './RadioControl';
import BaseFormControl from './BaseFormControl';
import Select2Control from './Select2Control';
import InputDatePickerControl from './InputDatePickerControl';
import DateTimePickerControl from './DateTimePickerControl';
import ViewOnlyControl from './ViewOnlyControl';
import CheckboxControlGroup from './CheckBoxControlGroup';
import TagsControl from './TagsControl';
import FormatDurationControl from './FormatDurationControl';
import InputMaskControl from './InputMaskControl';

class FormControlFactory {
  static getControl(config) {
    switch (config.type) {
      case 'text':
      case 'email':
      case 'password':
      case 'number':
        return new InputControl(config);
      case 'textarea':
        return new TextAreaControl(config);
      case 'select':
        return new SelectControl(config);
      case 'select2':
        return new Select2Control(config);
      case 'checkbox':
        return new CheckBoxControl(config);
      case 'radio':
        return new RadioControl(config);
      case 'datepicker':
        return new InputDatePickerControl(config);
      case 'datetimepicker':
        return new DateTimePickerControl(config);
      case 'viewOnly':
          return new ViewOnlyControl(config);
      case 'tags':
        return new TagsControl(config);
      case 'checkboxGroup':
          return new CheckboxControlGroup(config);
      case 'duration_format':
        return new FormatDurationControl(config);
      case 'input_mask':
        return new InputMaskControl(config);
    }
    return new BaseFormControl(config);
  }
}

export default FormControlFactory;