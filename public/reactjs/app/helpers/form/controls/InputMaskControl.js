import BaseFormControl from './BaseFormControl';
import React, { Component } from 'react';
import InputMask from 'react-input-mask';

class InputMaskControl extends BaseFormControl {

  defaultFormat = "99:99:99:99";

  renderInput() {
    const {name, type, domProps, disabled, format, initValue} = this.config;
    const formatVal = format ? format.days + ':' + format.hours + ':' + format.minutes + ':' + format.seconds : this.defaultFormat;

    return(
        <InputMask mask={formatVal} defaultValue={initValue} onBlur={this.config.onFormChange} disabled={disabled || false}>
          {(inputProps) => <input className="form-control" name={name} type={type} {...inputProps} />}
        </InputMask>
    );
  }
}

export default InputMaskControl;