import BaseFormControl from './BaseFormControl';
import React from 'react';

class TextAreaControl extends BaseFormControl {
  renderInput() {
    const {name, domProps, placeholder, label} = this.config;
    return <textarea className="form-control" placeholder={placeholder || label} name={name} onBlur={this.config.onFormChange} {...domProps} />;
  }
}

export default TextAreaControl;