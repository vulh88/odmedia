import BaseFormControl from './BaseFormControl';
import React from 'react';
import $ from 'jquery';
import 'select2';
import toast from '../../user-interface/toast';
import ThemesApiService from 'services/api/themes';
import ClientStorageHelper from '../../../helpers/client-storage';


class Select2Component extends React.PureComponent {
  componentDidMount() {
    this.initSelect2();
  }

  componentDidUpdate() {
    this.initSelect2();
  }

  initSelect2() {
    const accessToken = ClientStorageHelper.getToken();
    console.log('ThemesApiService ', ThemesApiService);
    let self = this;
    if ($().select2) {
      $(this.select).select2({
        //placeholder: `Please select ${this.props.config.label || ''}`,
        //allowClear: false,
        ajax: {
          url: 'https://api.github.com/search/repositories',
          dataType: "json",
          // headers: {
          //   Accept: "text/plain; charset=utf-8",
          //   "Content-Type": "text/plain; charset=utf-8"
          // }
          processResults: function (data) {
            let requestData = self.callRequest({
                  channel_id: 7,
                });
            let results = requestData.map(e => ({
              id: e.id,
              text: e.name
            }));
            console.log('results ', results);

            /*let results = [
              {
                "id": 1,
                "text": "Option 1"
              },
              {
                "id": 2,
                "text": "Option 2"
              }
            ];*/

            return {
              results: results
            };
          },
          // transport: function (params, success, failure) {
          //   let data = self.callRequest({
          //     channel_id: 7,
          //   });
          //   console.log('data ', data);
          //   return {
          //     results: [
          //       {
          //         "id": 1,
          //         "text": "Option 1"
          //       },
          //       {
          //         "id": 2,
          //         "text": "Option 2"
          //       }
          //     ]
          //   };
          // }
        }
      });
    }
  }

  async callRequest(params) {
    let response = await ThemesApiService.getList(params);
    return response.data.length > 0 ? response.data : [];
  }


  render() {
    const {name, data, initValue} = this.props.config;
    return (
        <select ref={e => this.select = e}
                defaultValue={initValue}
                name={name}
                className="form-control select2-control">
          <option/>
          {data && data.map((e) => <option key={e.value} value={e.value}>{e.name}</option>)}
        </select>
    );
  }
}


class Select2Control extends BaseFormControl {
  renderInput() {
    return (
        <Select2Component config={this.config}/>
    );
  }
}

export default Select2Control;

