import schema from './schema';

const commonSchema = {
  dateFormat: (message) => schema.string().matches(/[\d]+4-[\d]+2-[\d]+2/, message || 'Date must have format of YYYY-MM-DD'),
  checkbox: () => schema.number().transform((value) => value ? 1 : 0),
  integer: (message = '${path} must be a integer number') => schema.number().typeError(message).integer().transform(v => isNaN(v) ? undefined : v),
  string: (message = '${path} is a required field') => schema.string().typeError(message).transform(value => value.trim() ? value : null),
  formatLength: (message = '${path} is not valid. The valid format must be days <= 7, hours <= 23, minutes <= 59, seconds <= 59 ') => schema.string().typeError(message).transform(value => validateFormatLength(value) ? value : null),
};

/**
 * Validate field format length
 * @param value
 * @returns {boolean}
 */
function validateFormatLength(value) {
  let arrVal = value.split(':');
  if (parseInt(arrVal[0]) > 7 || parseInt(arrVal[1]) > 23 || parseInt(arrVal[2]) > 59 || parseInt(arrVal[3]) > 59) {
    return false
  }
  return true;
}

export default commonSchema;
