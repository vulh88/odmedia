/* eslint-disable no-continue,consistent-return */
const formHelper = {
  formToObject: (form) => {
    if (!form || form.nodeName !== 'FORM') {
      return;
    }
// eslint-disable-next-line one-var,one-var-declaration-per-line,prefer-const
    let i, j, q = [];
    for (i = form.elements.length - 1; i >= 0; i--) {
      if (form.elements[i].name === '') {
        continue;
      }
      switch (form.elements[i].nodeName) {
        case 'INPUT':
          switch (form.elements[i].type) {
            case 'number':
              try {
                q.push([form.elements[i].name, parseFloat(form.elements[i].value)]);
              } catch (err) {
              }
              break;
            case 'text':
            case 'email':
            case 'hidden':
            case 'password':
            case 'button':
            case 'reset':
            case 'submit':
            case 'duration_format':
            case 'input_mask':
              q.push([form.elements[i].name, form.elements[i].value]);
              break;
            case 'checkbox':
              if (form.elements[i].checked) {
                let value = form.elements[i].value;
                value = value === 'on' ? 1 : value === 'off' ? 0 : value;
                q.push([form.elements[i].name, value]);
              } else {
                q.push([form.elements[i].name, 0]);
              }
              break;
            case 'radio':
              if (form.elements[i].checked) {
                q.push([form.elements[i].name, form.elements[i].value]);
              }
              break;
            case 'file':
              break;
          }
          break;
        case 'TEXTAREA':
          q.push([form.elements[i].name, form.elements[i].value]);
          break;
        case 'SELECT':
          switch (form.elements[i].type) {
            case 'select-one':
              q.push([form.elements[i].name, form.elements[i].value]);
              break;
            case 'select-multiple':
              for (j = form.elements[i].options.length - 1; j >= 0; j--) {
                if (form.elements[i].options[j].selected) {
                  q.push([form.elements[i].name, form.elements[i].options[j].value]);
                }
              }
              break;
          }
          break;
        case 'BUTTON':
          switch (form.elements[i].type) {
            case 'reset':
            case 'submit':
            case 'button':
              q.push([form.elements[i].name, form.elements[i].value]);
              break;
          }
          break;
      }
    }
    const res = {};
    for (const [key, val] of q) {
      if (res[key] === undefined) {
        res[key] = val;
      } else if (Array.isArray(res[key])) {
        res[key].push(val);
      } else {
        res[key] = [res[key]];
        res[key].push(val);
      }
    }
    return res;
  },
  applyData: (form, data) => {
    let input = null;
    let val = null;
    if (form && data) {
      Object.keys(data).map((key) => {
        input = form[key];
        val = data[key];
        if (input) {
          if (input.type === 'checkbox' || input.type === 'radio') {
            input.checked = !!val;
          }
          else if (input.nodeName === 'SELECT') {
            input.value = val;
          }
          else {
            input.value = val;
          }
        }
        return null;
      });
    }
    return this;
  }
};


export default formHelper;
