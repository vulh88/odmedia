/* eslint-disable object-shorthand,no-template-curly-in-string */
import * as yup from 'yup';

/**
 * See https://github.com/jquense/yup for validate api
 */

yup.addMethod(yup.mixed, 'sameAs', function (ref, message) {
  return this.test(
      {
        name: 'sameAs',
        message: message || '${path} must be the same as ${reference}',
        params: {
          reference: ref.path,
        },
        test: function (value) {
          const other = this.resolve(ref);
          return !other || !value || value === other;
        }
      }
  );
});

export default yup;
