import {call, fork} from 'redux-saga/effects';

export class AbstractSaga {

  store = null;

  autoBind() {
    const properties = Object.getOwnPropertyNames(this.constructor.prototype).concat(Object.getOwnPropertyNames(this));
    for (const key of properties) {
      const val = this[key];
      if (key !== 'constructor' && typeof val === 'function') {
        this[key] = val.bind(this);
      }
    }
    return this;
  }

  /**
   * Custom select function use to access store
   * @param selector
   * @returns {*}
   */
  selectFromState(selector) {
    if (selector) {
      return selector(this.store.getState());
    }
    else {
      return this.store.getState();
    }
  }

  run = function* (props, store) {
    this.autoBind();
    this.store = store;
    yield this.watcher.apply(this, arguments);
  }.bind(this);

  * watcher () {
    throw `You have to implement watcher method`;
  };

}

export default AbstractSaga;

