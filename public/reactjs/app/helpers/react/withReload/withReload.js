import React from 'react';
import {isFunction} from 'lodash';

function withReload(options = {}) {
  return (Component) => {
    class WrappedComponent extends React.PureComponent {
      reloadKey = 0;

      onReload = () => {
        this.reloadKey++;
        this.forceUpdate();
      };

      render() {
        const props = this.props;
        return <Component key={this.reloadKey} {...props} reload={this.onReload}/>;
      }
    }

    WrappedComponent.displayName = `withReload(${Component.displayName || Component.name})`;
    return WrappedComponent;
  };
}

export default withReload;
