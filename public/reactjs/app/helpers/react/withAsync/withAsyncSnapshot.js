import React from 'react';
import WrongPage from '../../../containers/WrongPage';
import {isFunction} from 'lodash';
import PropTypes from 'prop-types';
import BlockLoading from '../../../components/BlockLoading';

const snapshotManager = {
  subscribers: [],
  subscribe: (handler) => {
    if (snapshotManager.subscribers.indexOf(handler) === -1 ){
      snapshotManager.subscribers.push(handler);
    }
  },
  unsubscribe:(handler) => {
    const id = snapshotManager.subscribers.indexOf(handler);
    if (id !== -1 ){
      snapshotManager.subscribers.splice(id);
    }
  },
  trigger: () => {
    snapshotManager.subscribers.map(e => {
      e.call();
    });
  }
};
function triggerGetSnapshot() {
  console.log(console.log('let get snap shot'));
}

function withAsync({errorComponent = WrongPage} = {}) {
  return (Component) => {
    Component.snapshot = null;
    class WrappedComponent extends React.PureComponent {

      isRunned = false;
      isResolved = false;
      resolvedData = null;
      isError = false;
      isMounting =  false;

      async asyncFunction() {
        if (!this.isRunned) {
          this.isRunned     = true;
          this.resolvedData = await Component.preload.call(this, this.props)
              .catch(error => {
                this.isError = error;
                if (!isFunction(error)) {
                  console.error(error);
                }
              });
          this.isResolved   = true;
          if (this.isMounting) {
            this.forceUpdate(snapshotManager.trigger);
          }
        }
      }

      componentWillMount() {
        this.isMounting = true;
        snapshotManager.subscribe(this.getSnapshot);
        this.asyncFunction();
      }

      componentWillUnmount() {
        this.isMounting   = false;
        this.isRunned     = false;
        this.isResolved   = false;
        this.resolvedData = null;
        this.isError      = false;
        snapshotManager.unsubscribe(this.getSnapshot);
      }

      getSnapshot =() => {
        if (this.domNode) {
          if (!Component.snapshot || (Component.snapshot.length < this.domNode.innerHTML.length)) {
            Component.snapshot = this.domNode.innerHTML;
            console.log(Component.snapshot);
          }
        }
      };

      render() {
        const props = this.props;
        if (!this.isResolved) {
          if (Component.snapshot) {
            return <div className="no-event" dangerouslySetInnerHTML={{__html: Component.snapshot}}/>
          }
          return <BlockLoading/>;
        }
        if (this.isError) {
          // If error is an component, mount it
          const ErrorComponent = isFunction(this.isError) ? this.isError : errorComponent;
          return <ErrorComponent/>;
        }
        return <Component snapshot={e => this.domNode = e} {...props} preloadData={this.resolvedData}/>;
      }
    }

    WrappedComponent.displayName = `withAsync(${Component.displayName || Component.name})`;
    WrappedComponent.propsTypes = {
      ...WrappedComponent.propsTypes,
      preloadData: PropTypes.object.required
    };
    return WrappedComponent;
  };
}

export default withAsync;