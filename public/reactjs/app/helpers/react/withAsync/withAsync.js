import React from 'react';
import WrongPage from '../../../containers/WrongPage';
import {isFunction} from 'lodash';
import PropTypes from 'prop-types';
import BlockLoading from '../../../components/BlockLoading';
import FadeIn from '../../../components/EffectComponents/FadeIn';

function withAsync({errorComponent = WrongPage, loadingComponent : loadingComponent = BlockLoading} = {}) {
  return (Component) => {
    class WrappedComponent extends React.PureComponent {

      isRunned = false;
      isResolved = false;
      resolvedData = null;
      isError = false;
      isMounting =  false;

      async asyncFunction() {
        if (!this.isRunned) {
          this.isRunned     = true;
          this.resolvedData = await Component.preload.call(this, this.props)
              .catch(error => {
                this.isError = error;
                if (!isFunction(error)) {
                  console.error(error);
                }
              });
          this.isResolved   = true;
          if (this.isMounting) {
            this.forceUpdate();
          }
        }
      }

      componentWillMount() {
        this.isMounting = true;
        this.asyncFunction();
      }

      componentWillUnmount() {
        this.isMounting   = false;
        this.isRunned     = false;
        this.isResolved   = false;
        this.resolvedData = null;
        this.isError      = false;
      }

      render() {
        const props = this.props;
        const {isResolved} = this;
        const LoadingCompoennt = loadingComponent === null ? () => null : loadingComponent;

        if (this.isError) {
          // If error is an component, mount it
          const ErrorComponent = isFunction(this.isError) ? this.isError : errorComponent;
          return <ErrorComponent/>;
        }

        return (
          <React.Fragment>
            <FadeIn show={!isResolved}>
              <LoadingCompoennt/>
            </FadeIn>
            {isResolved && <Component {...props} preloadData={this.resolvedData}/>}
          </React.Fragment>
        );
      }
    }

    WrappedComponent.displayName = `withAsync(${Component.displayName || Component.name})`;
    WrappedComponent.propsTypes = {
      ...WrappedComponent.propsTypes,
      preloadData: PropTypes.object.required
    };
    return WrappedComponent;
  };
}

export default withAsync;