export function returnHocWithName(hocName, Component, WrappedComponent) {
  WrappedComponent.displayName = `${hocName}(${Component.displayName})`;
  return WrappedComponent;
}

