/* eslint-disable object-shorthand */
const injectMixins = {
  autoRenderOnChange: function (initial = {}) {
    injectMixins.injectIsMounted.call(this);
    const comp = this;
    let isPlaying = false;
    const forceUpdate = () => {
      if (!isPlaying) {
        isPlaying = true;
        requestAnimationFrame(() => {
          isPlaying = false;
          try {
            if (this._isMounted) {
              comp.forceUpdate();
            }
          } catch (error) {
            console.error(error);
          }
        });
      }
    };
    return new Proxy(initial, {
      get: function (target, name) {
        if (!(name in target)) {
          return undefined;
        }
        return target[name];
      },
      set: function (target, name, value) {
        target[name] = value;
        forceUpdate();
        return true;
      }
    });
  },
  injectIsMounted() {
    if (this._isMounted === undefined) {
      const _componentDidMount = this.componentDidMount;
      const _componentWillUnmount = this.componentWillUnmount;
      this._isMounted = false;

      const componentDidMount = () => {
        this._isMounted = true;
        if (_componentDidMount) {
          _componentDidMount.call(this);
        }
      };

      const componentWillUnmount = () => {
        this._isMounted = false;
        if (_componentWillUnmount) {
          _componentWillUnmount.call(this);
        }
      };
      [this.componentDidMount, this.componentWillUnmount] = [componentDidMount, componentWillUnmount];
    }
  }
};

export default injectMixins;
