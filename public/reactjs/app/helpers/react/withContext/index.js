import PropTypes from 'prop-types';
import React from 'react';

export function withContext(Context) {
  return (Component) => {
    const WrappedComponent = (props) => <Context.Consumer
        children={contextData => <Component {...props} contextData={contextData}/>}/>;

    WrappedComponent.displayName = `withContext(${Component.displayName || Component.name})`;

    Component.propsTypes = {
      ...Component.propsTypes,
      contextData: PropTypes.any.required
    };

    return WrappedComponent
  };
}