import swal from 'sweetalert2/dist/sweetalert2';
import 'sweetalert2/dist/sweetalert2.css';
import './assets/sweetalert.css';

const swalBoostrap = swal.mixin({
  confirmButtonClass: 'btn green mr-1',
  cancelButtonClass: 'btn btn-danger',
  buttonsStyling: false});

const quickAlert = {
  confirm: (title = 'Are you sure?', text = '', {yes = 'Continue', no = 'Cancel', ...options} = {}) => {
    return swalBoostrap({
      title,
      text,
      type: options.type || 'warning',
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: yes,
      cancelButtonText: no,
    }).then((result) => result.value);
  },
  success: (title = 'Success!', text = '', options) => {
    return swalBoostrap({
      title,
      text,
      type: 'success'
    });
  }
};

// window.quickAlert = quickAlert;

export default quickAlert;

