import BaseRestApiService from './base-rest';
import apiPaths from '../../settings/api/api-path';

export class ThemesApiService extends BaseRestApiService {
  resources = apiPaths.themes;
}

const themesApiService = new ThemesApiService();

export default themesApiService;