import BaseRestApiService from './base-rest';
import apiPaths from '../../settings/api/api-path';
import apiCaller from '../apiCaller';

export class PermissionsApiService extends BaseRestApiService {
  resources = apiPaths.permissions;

  async getPermissionGroups(params) {
    const res = await apiCaller.get(this.resources.groups, {
      query: {
        ...params
      }
    });
    return res.data;
  }

  async assignPermissionChannel(id, body) {
    const res = await apiCaller.post(this.resources.submitPermission, {
      inUrl: {id},
      body
    });
    return res.data;
  };

  async getPermissionChannel(id, params) {
    const res = await apiCaller.get(this.resources.getPermission, {
      inUrl: {id},
      query: {
        ...params
      }
    });
    return res.data;
  }

}

const permissionsApiService = new PermissionsApiService();

export default permissionsApiService;