import BaseRestApiService from './base-rest';
import apiPaths from '../../settings/api/api-path';

export class ResourceTypesApiService extends BaseRestApiService {
  resources = apiPaths.resourceTypes;
}

const resourceTypesApiService = new ResourceTypesApiService();

export default resourceTypesApiService;
