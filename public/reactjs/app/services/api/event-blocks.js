import BaseRestApiService from './base-rest';
import apiPaths from '../../settings/api/api-path';
import apiCaller from '../apiCaller';

export class EventBlocksApiService extends BaseRestApiService {
  resources = apiPaths.eventBlocks;

  async getTypes() {
    const res = await apiCaller.get(this.resources.getTypes, {
      query: {}
    });

    return res.data;
  }
}

const eventBlocksApiService = new EventBlocksApiService();

export default eventBlocksApiService;