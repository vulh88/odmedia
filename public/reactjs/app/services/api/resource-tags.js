import BaseRestApiService from './base-rest';
import apiPaths from '../../settings/api/api-path';

class ResourceTagsApiService extends BaseRestApiService {
  resources = apiPaths.resourceTags;
}

const resourceTagsApiService = new ResourceTagsApiService();

export default resourceTagsApiService;
