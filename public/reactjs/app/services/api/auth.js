import apiCaller from '../apiCaller';
import apiPaths from '../../settings/api/api-path';
import ClientStorageHelper from '../../helpers/client-storage';

class AuthApiService {
  static async login(formData, apiOptions) {
    const resp = await apiCaller.post(apiPaths.auth.login, {
      body: formData,
      ...apiOptions
    });
    return resp.data;
  }

  static async logout() {
    return (await apiCaller.get(apiPaths.auth.logout));
  }
}

export default AuthApiService;
