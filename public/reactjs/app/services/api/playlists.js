import BaseRestApiService from './base-rest';
import apiPaths from '../../settings/api/api-path';
import apiCaller from '../../services/apiCaller';


export class PlaylistsApiService extends BaseRestApiService {

  static playlistStatus = {
    publish: 'publish'
  };

  resources = apiPaths.playlists;
  /**
   * @param body
   * @param playlistId id of playlist scheduling
   * @param resourceId id of resources
   * @param timeStart moment object
   * @param timeEnd moment object
   * @param scheduledId id of item already scheduled
   * @param item_track track that resource contained
   */
  async scheduleResource(body) {
    const {scheduledId} = body;
    let resp;
    if (scheduledId) {
      resp = await apiCaller.put(this.resources.updatePlaylistItem, {
        inUrl: {id: scheduledId},
        body: body
      });
    } else {
      resp = await apiCaller.post(this.resources.addPlaylistItem, {
        body: body
      });
    }
    return resp.data;
  }

  async deleteResource({id}) {
    return (await apiCaller.delete(this.resources.deletePlaylistItem, {
      inUrl: {id}
    })).data;
  }

  async getListResources(playlistId) {
    const resp = await apiCaller.get(this.resources.listResources, {
      inUrl: {playlistId}
    });
    return resp.data;
  }

  async verifyResources(playlistId) {
    const resp = await apiCaller.get(this.resources.resourcesForDownloading, {
      inUrl: {playlistId},
      query: {
        sort: '-api_fetched'
      }
    });
    return resp.data;
  }

  async publish(playlistId) {
    const resp = await apiCaller.put(this.resources.publish, {
      inUrl: {playlistId}
    });
    return resp.data;
  }

  async duplicatePlaylist(body) {
    const res = await apiCaller.post(apiPaths.playlists.duplicate, {
      body
    });
    return res;
  }

  async downloadResource(body) {
    const res = await apiCaller.post(apiPaths.resources.download, {
      body,
      noTimeOut: true
    });
    return res;
  }

  async addPlaceHolderPlaylist(body) {
    const res = await apiCaller.post(apiPaths.playlists.addPlaceholder, {
      body
    });
    return res.data;
  }

}

const playlistsApiService = new PlaylistsApiService();

export default playlistsApiService;
