import BaseRestApiService from './base-rest';
import apiPaths from '../../settings/api/api-path';
import apiCaller from '../apiCaller';

class SettingsApiService extends BaseRestApiService {
  resources = apiPaths.settings;

  async getSystemSettings(params) {
    const resp = await apiCaller.get(this.resources.systemGet, {
      query: {
        ...params
      }
    });
    if (resp.status == 200) {
      return resp.data;
    }
    return resp;
  }

  async setSystemSettings(body) {
    const resp = await apiCaller.put(this.resources.systemSet, {
      body
    });
    return resp;
  }

}

const settingsApiService = new SettingsApiService();

export default settingsApiService;