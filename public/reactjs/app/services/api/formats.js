import BaseRestApiService from './base-rest';
import apiPaths from '../../settings/api/api-path';
import apiCaller from '../apiCaller';

export class FormatsApiService extends BaseRestApiService {
  resources = apiPaths.formats;

  async generateFormat(body) {
    const res = await apiCaller.post(apiPaths.playlists.generateFormat, {
      body
    });
    return res;
  }

  async duplicateFormat(body) {
    const res = await apiCaller.post(apiPaths.formats.duplicate, {
      body
    });
    return res;
  }
}

const formatsApiService = new FormatsApiService();

export default formatsApiService;