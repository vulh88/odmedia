import BaseRestApiService from './base-rest';
import apiPaths from '../../settings/api/api-path';
import apiCaller from '../apiCaller';

class UsersApiService extends BaseRestApiService {
  resources = apiPaths.users;

  getChannelSettings = async ({channel_id}) => {
    return (await this.apiCaller.get(this.resources.channelSettings, {
      query: {
        channel_id
      }
    }));
  };

  async getChannelsUser(params) {
    const res = await apiCaller.get(this.resources.channelsUser, {
      query: {
          ...params
      }
    });
    return res.data;
  };

}

const usersApiService = new UsersApiService();

export default usersApiService;