import BaseRestApiService from './base-rest';
import apiPaths from '../../settings/api/api-path';
import apiCaller from '../apiCaller';

export class ResourcesApiService extends BaseRestApiService {
  resources = apiPaths.resources;
  resourceFolders = apiPaths.resourceFolders;

  importData(body) {
    return apiCaller.post(this.resources.importData, {
      body
    });
  };

  previewImportData(params) {
    return apiCaller.get(this.resources.previewImportData, {
      query: {
        ...params
      }
    });
  };

  async getResourceFolders(params) {
    const resp = await apiCaller.get(this.resourceFolders.base, {
      query: {
        ...params
      }
    });
    return resp;
  }

  async upload(body, onProgress) {
    const resp = await apiCaller.post(this.resources.uploadVideo, {
      isUpload: true,
      body,
      onProgress,
    });
    return resp;
  }

  async getAuthors(params) {
    const resp = await apiCaller.get(this.resources.authors, {
      query: {
        ...params
      }
    });
    return resp;
  }
}

const resourcesApiService = new ResourcesApiService();

export default resourcesApiService;
