import apiCaller from '../apiCaller';

/**
 * Base Rest Api service
 */
class BaseRestApiService {
  apiCaller = apiCaller;
  resources = {};
  defaultParams = {};

  getOne = async (id) => {
    const resp = await apiCaller.get(this.resources.single, {
      inUrl: {
        id
      }
    });
    return resp && resp.data;
  };

  getList = async (params) => {
    if (typeof params.status != undefined && params.status) {
      if (params.status !== 'trash') {
        params = {...params, status: params.status};
      } else {
        params = {...params, [params.status]: 1};
        delete params.status;
      }
    }
    const resp = await apiCaller.get(this.resources.base, {
      query: {
        ...params,
        ...this.defaultParams.getlist
      }
    });
    return resp;
  };

  update = async (id, body) => {
    const resp = await apiCaller.put(this.resources.single, {
      inUrl: {id},
      body
    });
    return resp;
  };

  create = async (body) => {
    const resp = await apiCaller.post(this.resources.base, {
      body
    });
    return resp;
  };

  delete = async (id) => {
    const resp = await apiCaller.delete(this.resources.single, {
      inUrl: {id}
    });
    return resp;
  };

  moveToTrash = async (id) => {
    if (this.resources.moveToTrash) {
      return apiCaller.put(this.resources.moveToTrash, {
        inUrl: {id}
      });
    } else {
      throw new Error('This api resource does not support trash');
    }
  };

  restoreFromTrash = async (id) => {
    if (this.resources.restoreFromTrash) {
      return apiCaller.put(this.resources.restoreFromTrash, {
        inUrl: {id}
      });
    } else {
      throw new Error('This api resource does not support restore from trash');
    }
  };

  setGetlistDefaultParams = async (params) => {
    this.defaultParams.getlist = params;
  }
}

export default BaseRestApiService;
