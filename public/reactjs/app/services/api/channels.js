import BaseRestApiService from './base-rest';
import apiPaths from '../../settings/api/api-path';
import apiCaller from '../apiCaller';

class ChannelsApiService extends BaseRestApiService {
  resources = apiPaths.channels;

  async resetPremiere(id) {
    const res = await apiCaller.put(this.resources.resetPremiere, {
      inUrl: {id}
    });

    return res.data;
  };

}

const channelsApiService = new ChannelsApiService();

export default channelsApiService;