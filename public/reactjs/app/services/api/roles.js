import BaseRestApiService from './base-rest';
import apiPaths from '../../settings/api/api-path';

export class RolesApiService extends BaseRestApiService {
  resources = apiPaths.roles;
}

const rolesApiService = new RolesApiService();

export default rolesApiService;