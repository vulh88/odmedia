import BaseRestApiService from './base-rest';
import apiPaths from '../../settings/api/api-path';
import apiCaller from '../apiCaller';

class ResourceFoldersApiService extends BaseRestApiService {
  resources = apiPaths.resourceFolders;

  async getTreeFolders(params) {
    const resp = await apiCaller.get(this.resources.tree_folders, {
      query: {
        ...params
      }
    });
    return resp.data;
  }
}

const resourceFoldersApiService = new ResourceFoldersApiService();

export default resourceFoldersApiService;