/* eslint-disable prefer-rest-params */
/**
 * Handle api response
 */
import ClientStorageHelper from 'helpers/client-storage';
import toast from 'helpers/user-interface/toast';
import apiPaths from '../../settings/api/api-path';
import routePaths from '../../settings/route-paths';
import apiResponseCodes from '../../settings/api/api-response-codes';

export class APIResponseHandler {
  handleNetworkError(params, error) {
    const mockErrors = {
      status: error.response && error.response.status ? error.response.status : apiResponseCodes.networkError,
      errors: [`Network Error: ${error.message}`],
      message: error.message
    };
    console.error('APIResponseHandler - Network:', mockErrors);
    return mockErrors;
  }

  handleBusinessError(params, {response}) {
    if (!response.data.errors) {
      response.data.errors = [response.data.message];
    }
    if (response.status === apiResponseCodes.unAuthenticated) {
      if (params.originUrl === apiPaths.auth.login) {
        toast.error('Incorrect email or password. Please try again!', 'Login error!');
        return response.data;
      }
      else {
        toast.error('Please login again!', 'Session Timeout');
        ClientStorageHelper.clearUserData();
        if (window.appHistory) {
          window.appHistory.loginRedirectTo = window.location.pathname.replace(routePaths.begin, '');
          window.appHistory.push(routePaths.pages.login);
        }
        else {
          window.location.pathname = routePaths.pages.login;
        }
      }
    }
    console.error('APIResponseHandler - Server:', response);
    return {
      ...response.data,
      status: response.status
    };
  }

  handleTimeout(params) {
    console.warn(`APIResponseHandler - Timeout: ${params.originUrl}`);
    toast.error('Request timeouted');
    return params;
  }
}
