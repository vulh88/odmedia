import axios, {create} from 'axios';
import {APIResponseHandler} from './ResponseHandler';
import {utils} from '../../helpers/utils/index';
import ClientStorageHelper from '../../helpers/client-storage';

const BASE_URL = process.env.API;

const TIMEOUT = 20 * 1000;

export class ApiCaller {
  _axios = create({});
  responseHandler = new APIResponseHandler();

  _createCallerMethod = (method) => {
    return (url, opt = {}) => this._callApi(url, opt, method);
  };

  _callApi = async (url, opt, method) => {
    const headers = {...opt.headers};
    const originUrl = url;

    // Add access token to request
    const accessToken = ClientStorageHelper.getToken();

    if (!opt.noAuth && accessToken) {
      headers.Authorization = accessToken;
    }
    // Fix the internal url
    if (/^/.test(url)) {
      url = BASE_URL + url;
    }

    /* Prepare the api data */

    // Check if is files
    if (!opt.isUpload) {
      headers.Accept = 'application/json';
      headers['Content-Type'] = 'application/json; charset=utf-8';
      if (opt.body) {
        opt.body = JSON.stringify(opt.body);
      }
    }

    // Parse query params
    if (opt.query) {
      url += `?${
          Object.entries(opt.query).filter(arr => arr[0] !== undefined && arr[1] !== undefined).map(arr => {
            const v2 = Array.isArray(arr[1]) ? `[${arr[1].join(',')}]` : arr[1].toString();
            return `${arr[0].toString()}=${v2}`;
          }).join('&')
          }`;
    }

    // Parse inUrl params
    if (opt.inUrl) {
      Object.keys(opt.inUrl).map(key => {
        if (opt.inUrl[key]) {
          return url = url.replace(`:${key}`, opt.inUrl[key]);
        }
        throw new Error(`You didn't pass :${key} params in api url => ${originUrl}`);
      });
    }

    // Call api
    const sendingParams = {url, method, body: opt.body, headers, originUrl};
    const apiPromise = this._doSendApi({url, method, body: opt.body, headers, opt});
    let isDone = false;

    setTimeout(() => {
      if (!isDone && !opt.isUpload && !opt.noTimeOut) {
        this.responseHandler.handleTimeout(sendingParams);
      }
    }, opt.timeout || TIMEOUT);

    try {
      const resp = this._afterSuccess(await apiPromise);
      isDone = true;
      return resp;
    } catch (err) {
      isDone = true;
      if (err.response && (err.response.data.errors || err.response.data.message)) {
        return Promise.reject(this.responseHandler.handleBusinessError(sendingParams, err));
      } else {
        return Promise.reject(this.responseHandler.handleNetworkError(sendingParams, err));
      }
    }
  };

  _doSendApi({url, method, body, headers, opt}) {
    if (opt.isUpload) {
      const cancelSource = axios.CancelToken.source();
      return this._axios[method](url, utils.convertToFormData(body), {
        headers,
        cancelToken: cancelSource.token,
        onUploadProgress: (event) => opt.onProgress(event, cancelSource)
      });
    } else {
      if (['get', 'delete'].includes(method)) {
        return this._axios[method](url, {headers});
      }
      return this._axios[method](url, body, {headers});
    }
  }


  _afterSuccess(resp) {
    resp.paging = this._parsePaging(resp);
    return resp;
  }

  _parsePaging(resp) {
    if (resp.headers && resp.headers['x-current-page']) {
      return {
        currentPage: parseInt(resp.headers['x-current-page']),
        totalPage: parseInt(resp.headers['x-last-page']),
        limit: parseInt(resp.headers['x-limit']),
        totalData: parseInt(resp.headers['x-total'])
      };
    }
    return null;
  }

  // Create public api call methods
  get = this._createCallerMethod('get');
  post = this._createCallerMethod('post');
  put = this._createCallerMethod('put');
  delete = this._createCallerMethod('delete');
}

export default new ApiCaller();
