export const apiPaths = {
  channels: {
    base: '/channels',
    single: '/channels/:id',
    moveToTrash: '/channels/:id/trash',
    restoreFromTrash: '/channels/:id/restore',
    resetPremiere: '/channels/:id/reset-premiere',
  },
  playlists: {
    base: '/playlists',
    single: '/playlists/:id',
    moveToTrash: '/playlists/:id/trash',
    restoreFromTrash: '/playlists/:id/restore',
    addPlaylistItem: '/playlist-items',
    updatePlaylistItem: '/playlist-items/:id',
    deletePlaylistItem: '/playlist-items/:id',
    listResources: '/playlists/:playlistId/resources',
    resourcesForDownloading: '/playlists/:playlistId/resources-for-downloading',
    publish: '/playlists/:playlistId/publish',
    generateFormat: '/playlists/generate',
    duplicate: '/playlists/duplicate',
    addPlaceholder: '/playlists/add-placeholder',
  },
  resources: {
    base: '/resources',
    single: '/resources/:id',
    previewImportData: '/resources/preview-import-api',
    importData: '/resources/import-api',
    uploadVideo: '/resources/upload',
    authors: '/resources/authors',
    download: '/resources/download',
  },
  resourceFolders: {
    base: '/resource-folders',
    single: '/resource-folders/:id',
    tree_folders: '/resource-folders/tree-folders'
  },
  resourceTags: {base: '/resource-tags', single: '/resource-tags/:id'},
  resourceTypes: {base: '/resource-types', single: '/resource-types/:id'},
  themes: {base: '/themes', single: '/themes/:id'},
  auth: {login: '/login', logout: '/logout'},
  permissions: {
    base: '/permissions',
    single: '/permissions/:id',
    groups: '/permissions/groups',
    submitPermission: '/assignments/:id/assign-channel',
    getPermission: '/assignments/:id/assign-channel',
  },
  roles: {
    base: '/roles',
    single: '/roles/:id',
    moveToTrash: '/roles/:id/trash'
  },
  users: {
    base: '/users',
    single: '/users/:id',
    moveToTrash: '/users/:id/trash',
    restoreFromTrash: '/users/:id/restore',
    assignRole: '/users/:id/assign-role',
    assignPermissions: '/users/:id/assign-permission',
    assignChannel: '/users/:id/assign-channel',
    updateChannel: '/users/update-channel',
    channelConfig: '/users/:id/channel-configuration',
    channelSettings: '/users/channel-settings',
    channelsUser: '/users/channels'
  },
  formats: {
    base: '/formats',
    single: '/formats/:id',
    duplicate: '/formats/duplicate',
  },
  eventBlocks: {
    base: '/event-blocks',
    single: '/event-blocks/:id',
    getTypes: '/event-blocks/types'
  },
  settings: {
    systemSet: '/settings',
    systemGet: '/settings'
  },
};

export default apiPaths;
