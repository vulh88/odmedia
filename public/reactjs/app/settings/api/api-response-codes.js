const apiResponseCodes = {
  networkError: 0,
  requestTimeout: 1,
  success: 200,
  unAuthenticated: 401,
  notFound: 404
};

export default apiResponseCodes;