const basePath = '/playlists';

const playlists = {
  root: basePath,
  list: basePath,
  single: `${basePath}/:id([0-9]+)`,
  createNew: `${basePath}/create-new`,
  scheduling: `${basePath}/:id([0-9]+)/scheduling`,
  verifyResources: `${basePath}/:id([0-9]+)/verify-resources`,
  overview: `${basePath}/overview`
};

export default playlists;

