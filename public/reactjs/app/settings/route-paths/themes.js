const basePath = '/themes';

const themes = {
  list: basePath,
  single: `${basePath}/:id([0-9]+)`,
  createNew: `${basePath}/create-new`
};

export default themes;

