const basePath = '/event-blocks';

const eventBlocks = {
  list: basePath,
  createNew: `${basePath}/create-new`,
  single: `${basePath}/:id([0-9]+)`,
};

export default eventBlocks;

