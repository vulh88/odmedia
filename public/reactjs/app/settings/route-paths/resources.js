const basePath = '/resources';

const resources = {
  list: basePath,
  single: `${basePath}/:id([0-9]+)`,
  import: `${basePath}/import`,
  upload: `${basePath}/import?status=upload`
};

export default resources;

