const basePath = '/system/roles';

const roles = {
  base: basePath,
  assignments: `${basePath}/assignments`,
  groups: `${basePath}/groups`,
  permissions: `${basePath}/permissions`,
  settings: `${basePath}/settings`,
  assignAddNew: `${basePath}/assignments/:id/assign-channel`,
  assignUpdate: `${basePath}/assignments/:id/assign-channel/:channel_id`,
};

export default roles;
