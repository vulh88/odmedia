import playlists from './playlists';
import themes from './themes';
import users from './users';
import channels from './channels';
import resources from './resources';
import contentTypes from './content-types';
import pages from './pages';
import roles from './roles';
import formats from './formats';
import eventBlocks from './event-blocks';

const routePaths = {
  root: '/',
  begin: '/scheduler',
  users,
  playlists,
  themes,
  channels,
  resources,
  contentTypes,
  pages,
  roles,
  formats,
  eventBlocks,
};

export default routePaths;
