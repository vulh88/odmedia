const basePath = '/content-types';

const contentTypes = {
  list: basePath,
  single: `${basePath}/:id([0-9]+)`,
  createNew: `${basePath}/create-new`
};

export default contentTypes;

