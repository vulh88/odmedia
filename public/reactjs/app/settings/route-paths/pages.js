export default {
  support: '/support',
  formats: '/formats',
  settings: '/settings/channel',
  system_settings: '/system/settings',
  page500: '/500',
  login: '/login',
};

