const basePath = '/formats';

const formats = {
  list: basePath,
  createNew: `${basePath}/create-new`,
  single: `${basePath}/:id([0-9]+)`,
  generate: `${basePath}/:id([0-9]+)/generate`
};

export default formats;

