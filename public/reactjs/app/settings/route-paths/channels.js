const basePath = '/system/channels';
const channels = {
  list: basePath,
  single: `${basePath}/:id([0-9]+)`,
  createNew: `${basePath}/create-new`
};

export default channels;

