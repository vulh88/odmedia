const basePath = '/system/users';

const users = {
  list: basePath,
  single: `${basePath}/:id([0-9]+)`,
  createNew: `${basePath}/create-new`,
  view: `${basePath}/view/:id([0-9]+)`,
};

export default users;

