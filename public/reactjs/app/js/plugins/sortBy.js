Array.prototype.sortBy = function (name, reverse = 1) {
  if (name) {
    this.sort(function (a, b) {
      return (a[name] - b[name])*reverse;
    })
  }
  return this;
};