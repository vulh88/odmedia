// Initial routing state
import {LOCATION_CHANGE} from 'react-router-redux';
import {fromJS} from "immutable";

const routeInitialState = fromJS({
  location: null
});

/**
 * Merge route into the global application state
 */
function routeReducer(state = routeInitialState, action) {
  switch (action.type) {
      /* istanbul ignore next */
    case LOCATION_CHANGE:
      return state.merge({
        location: action.payload
      });
    default:
      return state;
  }
}
export default routeReducer;

