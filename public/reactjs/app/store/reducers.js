/**
 * Combine all reducers in this file and export the combined reducers.
 */

import {combineReducers} from 'redux-immutable';

import languageProviderReducer from 'containers/LanguageProvider/stores/reducer';
import routeReducer from './route/reducer';
import appReducer from './app/reducer';
import displayReducer from './display/reducer';

/**
 * Creates the main reducer with the dynamically injected ones
 */
export default function createReducer(injectedReducers) {
  return combineReducers({
    route: routeReducer,
    app: appReducer,
    display: displayReducer,
    language: languageProviderReducer,
    ...injectedReducers
  });
}
