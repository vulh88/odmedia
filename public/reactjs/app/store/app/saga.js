import {put, takeLatest, call} from 'redux-saga/effects';
import {push as routerPush} from 'react-router-redux';
import {isFunction} from 'lodash';

import * as actions from 'store/app/actions';
import ClientStorageHelper from '../../helpers/client-storage';
import routePaths from '../../settings/route-paths';
import {makeSelectChannel, makeSelectIsLoggedIn, makeSelectListChannel} from './selectors';
import channelsApiService from '../../services/api/channels';
import toast from '../../helpers/user-interface/toast';
import AbstractSaga from '../../helpers/saga/abstract-saga';
import usersApiService from '../../services/api/users';
import AuthApiService from '../../services/api/auth';
import apiResponseCodes from '../../settings/api/api-response-codes';

export class AppSaga extends AbstractSaga {

  * watcher() {
    yield takeLatest(actions.INIT_APP, this.initApp);
    yield takeLatest(actions.REQUEST_LOGOUT, this.requestLogout);
    yield takeLatest(actions.REFRESH_CHANNEL_SETTINGS, this.refreshChannelSetting);
    yield takeLatest(actions.REQUEST_LOGIN_SUCCESS, this.loginSuccess);
    yield takeLatest(actions.SET_CURRENT_CHANNEL, this.setCurrentChannel);
    yield takeLatest(actions.REQUEST_CHANNEL_SETTINGS, this.requestChannelSettings);
  };

  * initApp() {
    const isLoggedIn = this.selectFromState(makeSelectIsLoggedIn());
    if (isLoggedIn) {
      try {
        yield call(this.requestListChannel);
        const channels = this.selectFromState(makeSelectListChannel());
        if (channels) {
          const previousChannelId = ClientStorageHelper.getSelectedChannel();
          const selectedChannel = channels.find(e => e.id.toString() === previousChannelId) || channels[0];
          yield call(this.setCurrentChannel, {payload: {channel: selectedChannel}});
          yield put({type: actions.INIT_APP_SUCCESS});
        }
        else {
          yield put({type: actions.INIT_APP_SUCCESS_WITHOUT_CHANNEL});
        }
      }
      catch ({status}) {
        switch (status) {
          case apiResponseCodes.unAuthenticated:
            yield this.sessionTimeout();
            break;
          case apiResponseCodes.networkError:
            yield put({type: actions.INIT_APP_FAIL_BY_NETWORK});
            break;
        }
      }
    }
    else {
      yield put(routerPush(routePaths.pages.login));
    }
  };

  * setCurrentChannel({payload: {channel, callback}}) {
    if (channel) {
      ClientStorageHelper.saveSelectedChannel(channel);
      yield call(this.requestChannelSettings, {
        payload: {channel_id: channel.id}
      });
      yield put({
        type: actions.CHANGE_CHANNEL_SUCCESS,
        payload: {channel}
      });
      if (isFunction(callback)) {
        callback();
      }
    }
  };

  * requestListChannel() {
    const channels = (yield call(channelsApiService.getList, {assigned_channels: 1})).data;
    if (channels && channels.length) {
      yield put({
        type: actions.REQUEST_CHANNEL_SUCCESS,
        payload: channels
      });
    }
    else {
      toast.warning('You are working without default channel!');
      yield put(routerPush(routePaths.channels.createNew))
    }
  }


  * loginSuccess({payload}) {
    ClientStorageHelper.saveUserData(payload);
    const nextUrl = window.appHistory
    && window.appHistory.loginRedirectTo
    && window.appHistory.loginRedirectTo !== routePaths.pages.login
        ? window.appHistory.loginRedirectTo
        : routePaths.playlists.overview;

    yield put(routerPush(nextUrl));
    yield call(this.initApp);
    window.appHistory.loginRedirectTo = null;
  };

  * requestLogout() {
    try {
      yield call(AuthApiService.logout);
    }
    catch (err) {
      console.error(err);
    }
    finally {
      ClientStorageHelper.clearUserData();
      yield put(routerPush(routePaths.pages.login));
      yield put({type: actions.REQUEST_LOGOUT_SUCCESS});
    }
  };

  * sessionTimeout() {
    yield put(routerPush(routePaths.pages.login));
    yield put({type: actions.REQUEST_LOGOUT_SUCCESS});
  };

  * requestChannelSettings({payload: {channel_id}}) {
    try {
      const {data} = yield call(usersApiService.getChannelSettings, {channel_id});
      yield put({
        type: actions.CHANNEL_SETTINGS_SUCCESS,
        payload: data
      });
    } catch (error) {
      yield put({
        type: actions.CHANNEL_SETTINGS_ERROR,
        payload: error
      });
    }
  };


  * refreshChannelSetting() {
    const currentChannel = this.selectFromState(makeSelectChannel());
    if (currentChannel) {
      yield call(this.requestChannelSettings, {payload: {channel_id: currentChannel.id}})
    }
  };

}

export const appSaga = new AppSaga();