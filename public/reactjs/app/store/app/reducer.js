/*
 * AppReducer
 *
 */

import { fromJS } from 'immutable';

import * as actions from './actions';
import ClientStorageHelper from '../../helpers/client-storage';

const initialState = fromJS({
  loading: false,
  isLoggedIn: !!ClientStorageHelper.getToken(),
  user: ClientStorageHelper.getUserData(),
  error: false,
  isReady: false,
  listChannel: null,
  currentChannel: null,
  loadingChannel: false,
  channelSettings: null,
  initFailByNetwork: false
});


function appReducer(state = initialState, action) {
  switch (action.type) {
    case actions.INIT_APP:
      return state
          .set('loading', true)
          .set('isReady', false);
    case actions.INIT_APP_SUCCESS:
      return state
          .set('loading', false)
          .set('isReady', true);
    case actions.INIT_APP_SUCCESS_WITHOUT_CHANNEL:
      return state
          .set('currentChannel',{})
          .set('channelSettings',{})
          .set('loading', false)
          .set('isReady', true);
    case actions.INIT_APP_FAIL_BY_NETWORK:
      return state
          .set('initFailByNetwork',true)
          .set('loading', false);
    case actions.REQUEST_LOGIN_SUCCESS:
      return state
          .set('isLoggedIn',true)
          .set('user',ClientStorageHelper.getUserData());
    case actions.REQUEST_LOGOUT_SUCCESS:
      return (state = initialState
          .set('isLoggedIn', false)
          .set('isReady',false));
    case actions.SET_ACCESS_TOKEN:
      return state.set('accessToken', action.data);
    case actions.REQUEST_CHANNEL_SUCCESS:
      return state.set('listChannel', action.payload);
    case actions.SET_CURRENT_CHANNEL:
      return state
          .set('loadingChannel', true)
          .set('error', false);
    case actions.CHANGE_CHANNEL_SUCCESS:
      return state
          .set('currentChannel', action.payload.channel);
    case actions.CHANNEL_SETTINGS_SUCCESS:
      return state
          .set('channelSettings', action.payload)
          .set('loadingChannel', false);
    case actions.CHANNEL_SETTINGS_ERROR:
      return state
          .set('channelSettings', null)
          .set('error', action.error)
          .set('loadingChannel', false);
    default:
      return state;
  }
}

export default appReducer;

