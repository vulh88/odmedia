/**
 * The app state selectors
 * Use selectors for better performance
 */

import {createSelector} from 'reselect';

const selectGlobalState = state => state;

const selectAppState = state => state.get('app');

const selectRoute = state => state.get('route');

const makeSelectLoading = () => createSelector(selectAppState, appState => appState.get('loading'));

const makeSelectLoadingChannel = () => createSelector(selectAppState, appState => appState.get('loadingChannel'));
const makeSelectChannelSetting = () => createSelector(selectAppState, appState => appState.get('channelSettings'));

const makeSelectError = () => createSelector(selectAppState, appState => appState.get('error'));

const makeSelectToken = () => createSelector(selectAppState, appState => appState.get('accessToken'));

const makeSelectListChannel = () => createSelector(selectAppState, appState => appState.get('listChannel'));

const makeSelectChannel = () => createSelector(selectAppState, appState => appState.get('currentChannel'));

const makeSelectReady = () => createSelector(selectAppState, appState => appState.get('isReady'));

const makeSelectLocation = () => createSelector(selectRoute, routeState => routeState.get('location').toJS());

const makeSelectIsLoggedIn = () => createSelector(selectAppState, appState => appState.get('isLoggedIn'));

const makeSelectUser = () => createSelector(selectAppState, appState => {
  try {
    const user = appState.get('user');
    if (user) {
      return user.toJS();
    }
    return null;
  } catch(e) {
    console.log('makeSelectUser Error '+ e.message);
  }

});

const makeSelectIsInitFailByNetwork = () => createSelector(selectAppState, appState => appState.get('initFailByNetwork'));

export {
  makeSelectUser,
  makeSelectIsLoggedIn,
  selectAppState,
  makeSelectLoading,
  makeSelectLoadingChannel,
  makeSelectError,
  makeSelectLocation,
  makeSelectToken,
  makeSelectListChannel,
  makeSelectChannel,
  makeSelectChannelSetting,
  makeSelectReady,
  makeSelectIsInitFailByNetwork
};
