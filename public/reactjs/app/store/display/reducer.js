/*
 * Create by donlq on 7/21/18
 * ...
 */

import { fromJS } from 'immutable';

import * as actions from './actions';

const initialState = fromJS({
  isOpenSidebar: true
});

function displayReducer(state = initialState, action) {
  switch (action.type) {
    case actions.TOGGLE_SIDEBAR:
      return state.set('isOpenSidebar',!state.get('isOpenSidebar'));
    default:
      return state;
  }
}

export default displayReducer;
