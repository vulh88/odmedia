export const TOGGLE_SIDEBAR = 'display/TOGGLE_SIDEBAR';
export const toggleSidebar = params => {
  return {
    type: TOGGLE_SIDEBAR,
    params
  };
};
