/**
 * The display state selectors
 * Use selectors for better performance
 */

import {createSelector} from 'reselect';

const selectDisplayState = state => state.get('display');

const makeSelectIsOpenSidebar = () => createSelector(selectDisplayState, displayState => displayState.get('isOpenSidebar') );

export {
  makeSelectIsOpenSidebar
};
