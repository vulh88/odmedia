/**
 * Use this file to declare routes
 */
import routePaths from 'settings/route-paths';
import DashboardPage from 'containers/DashboardPage/Loadable';
import ContentTypesPage from 'containers/ContentTypesPage/Loadable';
import ThemesPage from 'containers/ThemesPage/Loadable';
import ResourcesPage from 'containers/ResourcesPage/Loadable';
import ChannelsPage from 'containers/ChannelsPage/Loadable';
import UsersPage from 'containers/UsersPage/Loadable';
import SupportPage from 'containers/SupportPage/Loadable';
import WrongPage from 'containers/WrongPage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import FormatsPage from 'containers/FormatsPage/Loadable';
import LoginPage from 'containers/LoginPage/Loadable';
import SettingsPage from 'containers/SettingsPage/Loadable';
import SystemRolesPage from 'containers/SystemRolesPage/Loadable';
import createAppRouter from 'router/createRouter';
import PlaylistsPage from 'containers/PlaylistsPage';
import EventBlocksPage from 'containers/EventBlocksPage';

const declaredRoutes = [
  // Root
  {
    path: routePaths.root,
    exact: true,
    layout: true,
    component: DashboardPage
  },
  // ContentTypes
  {
    path: [
      routePaths.contentTypes.list,
      routePaths.contentTypes.createNew,
      routePaths.contentTypes.single
    ],
    exact: true,
    layout: true,
    component: ContentTypesPage
  },
  // Themes
  {
    path: [
      routePaths.themes.list,
      routePaths.themes.createNew,
      routePaths.themes.single
    ],
    exact: true,
    layout: true,
    component: ThemesPage
  },
  // Resources
  {
    path: [
      routePaths.resources.list,
      routePaths.resources.createNew,
      routePaths.resources.single,
      routePaths.resources.import,
      routePaths.resources.upload
    ],
    exact: true,
    layout: true,
    component: ResourcesPage
  },
  // Playlist
  {
    path: routePaths.playlists.root, // deligate to local routes to
    exact: false,
    layout: true,
    component: PlaylistsPage
  },
  // Channels
  {
    path: [
      routePaths.channels.list,
      routePaths.channels.createNew,
      routePaths.channels.single
    ],
    exact: true,
    layout: true,
    component: ChannelsPage
  },
  // Users
  {
    path: [
      routePaths.users.list,
      routePaths.users.createNew,
      routePaths.users.single,
      routePaths.users.view
    ],
    exact: true,
    layout: true,
    component: UsersPage
  },
  // Pages
  {
    path: routePaths.pages.login,
    exact: true,
    layout: false,
    isPublic: true,
    component: LoginPage
  },
  {
    path: routePaths.pages.support,
    exact: true,
    layout: true,
    component: SupportPage
  },
  {
    path: routePaths.pages.page500,
    exact: true,
    layout: true,
    component: WrongPage
  },
  {
    path: routePaths.pages.settings,
    exact: true,
    layout: true,
    component: SettingsPage
  },
  {
    path: routePaths.pages.system_settings,
    exact: true,
    layout: true,
    component: SettingsPage
  },
  //Format
  {
    path: [
      routePaths.formats.list,
      routePaths.formats.createNew,
      routePaths.formats.single,
      routePaths.formats.generate
    ],
    exact: true,
    layout: true,
    component: FormatsPage
  },
  //Event Block
  {
    path: [
      routePaths.eventBlocks.list,
      routePaths.eventBlocks.createNew,
      routePaths.eventBlocks.single
    ],
    exact: true,
    layout: true,
    component: EventBlocksPage
  },
  // Misc
  {
    path: [
      routePaths.roles.groups,
      routePaths.roles.assignments,
     // routePaths.roles.assignOneUser,
      routePaths.roles.permissions,
      routePaths.roles.settings,
      routePaths.roles.assignAddNew,
      routePaths.roles.assignUpdate,
    ],
    exact: true,
    layout: true,
    component: SystemRolesPage
  },
  // NotFound page
  {
    path: '*',
    exact: true,
    layout: true,
    isPublic: true,
    component: NotFoundPage
  }
];


export default createAppRouter(declaredRoutes);
