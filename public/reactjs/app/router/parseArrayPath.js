export function parseArrayPath(declaredRoutes) {
  const appRoutes = [];
  /**
   * Transform array path to normal path
   */
  declaredRoutes.forEach(r => {
    if (Array.isArray(r.path)) {
      r.path.map(p => {
        appRoutes.push({
          ...r,
          path: p
        });
      });
    } else {
      appRoutes.push(r);
    }
  });
  return appRoutes;
}