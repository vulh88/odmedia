import appRoutes from '../routes';

describe('App Routes', () => {
  it('Should have no undefined path', () => {
    const filtered = appRoutes.filter(e => typeof e.path === 'string');
    expect(filtered.length).toEqual(appRoutes.length);
  });
});