import routePaths from '../settings/route-paths';
import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import MasterTemplate from 'containers/Templates/MasterTemplate';
import {parseArrayPath} from './parseArrayPath';

function createAppRouter(appRoutes) {
  const routes = parseArrayPath(appRoutes);
  return ({isLogged}) => (
      <Switch>
        {
          routes.filter(e => e.path).map((e, idz) => {
            let renderFunc = (props) => <e.component {...props} />;
            if (e.layout) {
              renderFunc = (props) => (
                  <MasterTemplate>
                    <e.component {...props} />
                  </MasterTemplate>);
            }
            if (!e.isPublic && !isLogged) {
              renderFunc = (props) => <Redirect to={{
                pathname: routePaths.pages.login,
                state: {from: props.location}
              }}/>;
            }
            return (
                <Route
                    key={idz}
                    path={e.path}
                    exact={e.exact}
                    render={renderFunc}
                />
            );
          })
        }
      </Switch>
  );
}

export function renderLocalRoutes(localRoutes) {
  return (
      <Switch>
        {
          localRoutes
              .filter(e => e.path).map((e, idz) => (
              <Route
                  key={idz}
                  exact={e.exact !== false}
                  component={e.component}
                  cac={1}
                  path={e.path}
              />
          ))
        }
      </Switch>
  );
}

export default createAppRouter;
