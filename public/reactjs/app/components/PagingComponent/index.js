import React from 'react';
import ReactPaginate from 'react-paginate';
import PropTypes from 'prop-types';

export class PagingComponent extends React.PureComponent {
  static propsTypes = {
    paging: PropTypes.object.isRequired,
    onChangePage: PropTypes.func.isRequired
  };


  handlePageClick = ({selected}) => {
    const {onChangePage} = this.props;
    onChangePage(selected + 1);
  };

  render() {
    const {paging} = this.props;
    if (!paging) {
      return null;
    }
    const {totalPage, currentPage} = paging;
    return (
        totalPage > 1 && <ReactPaginate
            pageCount={totalPage}
            previousLabel={<i className="fa fa-angle-left"/>}
            nextLabel={<i className="fa fa-angle-right"/>}
            breakLabel={<a>...</a>}
            breakClassName="break-me"
            containerClassName="pagination"
            activeClassName="active"
            marginPagesDisplayed={1}
            pageRangeDisplayed={1}
          //  forcePage={Math.max(currentPage - 1, 0)}
            onPageChange={this.handlePageClick}
        />
    );
  }
}

export default PagingComponent;
