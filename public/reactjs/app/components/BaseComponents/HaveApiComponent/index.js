import React from 'react';
import injectMixins from '../../../helpers/react/inject-mixins';
import toast from '../../../helpers/user-interface/toast';
import lodash from 'lodash';

class HaveApiComponent extends React.Component {
  apiState =  {
    loading: null,
    errors: null,
    success: null
  };

  constructor(props){
    super(props);
    this.apiState = this.initApiState();
    this.apiState = injectMixins.autoRenderOnChange.call(this,this.apiState);
  }

  initApiState() {
    return this.apiState;
  }


  async callApi(api, onSuccess, onError, options = {}) {
    try {
      this.apiState.errors = null;
      this.apiState.success = null;
      this.apiState.submited = false;
      this.apiState.loading = true;
      const resp = await api;
      if (onSuccess) {
        onSuccess(resp);
      }
    } catch (e) {
      this.handleApiError(e, onError, options);
    } finally {
      this.apiState.loading = false;

      setTimeout(() => {
        this.apiState.success = null;
      }, 300);
    }
  }

  handleApiError = (error, onError, {autoShowError = true, ...options}) => {
    if (error.errors) {
      if (autoShowError) {
        lodash.map(error.errors, m => toast.error(m));
      }
      if (onError) {
        onError(error);
      }
      this.apiState.errors = error.errors;
    } else {
      throw error;
    }
  };
}

export default HaveApiComponent;
