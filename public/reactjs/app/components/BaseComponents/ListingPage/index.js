import HaveApiComponent from '../HaveApiComponent';
import locationHelper from '../../../helpers/location';
import BaseRestApiService from '../../../services/api/base-rest';

function toggleSort(fieldValue) {
  if (fieldValue[0] === '-') {
    return fieldValue.replace('-', '');
  }
  return `-${fieldValue}`;
}

function isActiveSort(field, sort) {
  return field === sort || `-${field}` === sort;
}

class ListingPage extends HaveApiComponent {
  // Child page must override
  restApiService = new BaseRestApiService();

  excludeParams = ['keyword'];

  filterParams = {};

  ignoreQuery = false;

  getQueryParams() {
    if (this.ignoreQuery) {
      return {};
    }

    const {page, count, status, sort} = locationHelper.getUrlParams();
    const res = {
      page: page || 1,
      count: count || 10,
      ...(sort ? {sort} : null)
    };
    if (['publish', 'trash', 'pending'].includes(status)) {
      res.status = status;
    }
    return res;
  }

  setFilterParams(params, isRefresh = true) {
    // filter null
    const filterParams = Object.keys(params).reduce((newObj, key) => {
      const value = params[key];
      if (value && value !== null) {
        newObj[key] = value;
      }
      return newObj;
    }, {});

    this.filterParams = filterParams;
    if (isRefresh) {
      this.requestData({});
    }
  }

  requestData(params, remapUrl = true) {
    this.callApi(this.restApiService.getList({...params, ...this.filterParams}), ({data, paging}) => {
      this.apiState.data = data;
      this.apiState.paging = paging;
    });
    if (remapUrl && !this.ignoreQuery) {
      this.excludeParams.map(key => {
        delete params[key];
      });
      locationHelper.setUrlParams(params);
    }
  }

  refreshData() {
    this.requestData(this.getQueryParams(), true);
  }

  handlePageChange = page => {
    const query = this.getQueryParams();
    query.page = page;
    this.requestData(query);
  };

  handleKeywordChange = keyword => {
    const query = this.getQueryParams();
    query.keyword = keyword;
    this.requestData(query);
  };

  handleLimitChange = limit => {
    const query = this.getQueryParams();
    query.count = limit;
    this.requestData(query);
  };

  handleSortBy = (field) => {
    const query = this.getQueryParams();
    query.sort = isActiveSort(field, query.sort)  ? toggleSort(query.sort) : field;
    this.requestData(query);
  };

  componentWillReceiveProps({location}) {
    if (location.search !== this.props.location.search) {
      this.refreshData();
    }
  }

  componentDidMount() {
    this.refreshData();
  }
}

export default ListingPage;
