import BaseRestApiService from '../../../services/api/base-rest';
import ListingPage from '../ListingPage';

class ListingPageWithChannel extends ListingPage {
  // child have to construct this
  restApiService = new BaseRestApiService();


  constructor() {
    super();
    this.excludeParams.push('channel_id');
  }
 componentWillMount() {
   if (this.props.currentChannel) {
     this.restApiService.setGetlistDefaultParams({
       channel_id: this.props.currentChannel && this.props.currentChannel.id
     });
   }
 }

  shouldComponentUpdate(nextProps) {
    if (nextProps.currentChannel !== this.props.currentChannel) {
      this.handleSwitchChannel(nextProps.currentChannel)
    }
    return true;
  }

  handleSwitchChannel = (channel) => {
    this.restApiService.setGetlistDefaultParams({
      channel_id: channel.id
    });
    this.setFilterParams({}, false);
    this.handlePageChange(1);
  }


}

export default ListingPageWithChannel;
