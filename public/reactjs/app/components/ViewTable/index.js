import React from 'react';
import PropsTypes from 'prop-types';
import ManageTable from '../ManageTable';
import {utils} from '../../helpers/utils';

class ViewTable extends React.PureComponent {
  static propsTypes = {
    columns: PropsTypes.object.required,
    data: PropsTypes.array.required,
    canSort: PropsTypes.bool
  };

  render() {
    return (
        <div className="view-table">
          <ManageTable
              loading={false}
              showActions={false}
              paging={{}}
              data={utils.makeArray(this.props.data)}
              columns={this.props.columns.map(e => {e.sort=this.props.canSort; return e;})}
          />
        </div>
    )
  }
}

export default ViewTable;
