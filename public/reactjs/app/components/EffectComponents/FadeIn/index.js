import React from 'react';

const FadeIn = (props) => {
  const {show, children} = props;
  const className = `fade-block ${show ? 'fade-block--in' : 'fade-block--out'}`;

  return (
      <div className={className}>
        {children}
      </div>
  );
};

export default FadeIn;
