import React from 'react';
import Wrapper from './Wrapper';

const LoadingIndicator = ({type = 'success'}) => (
  <Wrapper>
    <div className={`m-loader  m-loader--${type} m-loader--lg`}/>
  </Wrapper>
);

export default LoadingIndicator;
