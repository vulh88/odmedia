import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Timeline from 'react-calendar-timeline';
import {resourceTypes, timelineConfig} from '../../config';
import {timelineGroupsConverter} from './adapters/group';
import {timelineItemConverter} from './adapters/item';
import {tryClass, copyTime, isVisible, resetTime, minutesOfDay} from '../../utils';
import {createStructuredSelector} from 'reselect';
import {Group, Week, WrapperTimeLine} from '../../components/styled-components';
import $ from 'jquery';
import ZoomControl from '../../components/ZoomControl';
import {TimelineItem} from '../../components/TimelineItem';
import quickAlert from '../../../../helpers/user-interface/quick-alert';

const today = moment();
const timeRight = today.clone().hour(23).minute(59).second(59);
const timeLeft = resetTime(today.clone());

class TimelineWeekView extends React.PureComponent {
  static propTypes = {
    resources: PropTypes.arrayOf(PropTypes.object).isRequired,
    scheduleResource: PropTypes.func.isRequired,
    onDeleteResource: PropTypes.func.isRequired,
    onSelectedItem: PropTypes.func.isRequired
  };

  listItems = this.props.resources;

  state = {
    groups: timelineGroupsConverter(resetTime(moment().day(1))),
    currentMonday: resetTime(moment().day(1)), // 0 is Sundays
    items: null,
    defaultTimeStart: timeLeft,
    defaultTimeEnd: timeRight,
    weekChange: 0,
    minZoom: moment.duration(1, 'second').asMilliseconds(),
    maxZoom: moment.duration(1, 'days').asMilliseconds(),
    timeLeft,
    timeRight,
    loading: false,
    itemResize: null,
    itemDragging: null
  };

  componentWillMount() {
    this.syncTimelineItems(null);
  }

  isOverRight(time) {
    return time > this.state.timeRight;
  }

  isOverLeft(time) {
    return time < this.state.timeLeft;
  }

  fixMiddleItems(listItems) {
    const newList = [];
    listItems.filter(e => {
      const {timeEnd, timeStart} = e.data;
      // Difference date
      if (resetTime(timeEnd.clone()) > resetTime(timeStart.clone())) {
        newList.push({
          id: Math.random(),
          data: {
            ...e.data,
            timeStart: timeStart.clone(),
            timeEnd: timeStart.clone().hour(23).minute(59).second(59),
          },
          itemProps: {
            canMove: false,
            canResize: false,
            canChangeGroup: false
          }
        });

        newList.push({
          id: Math.random(),
          data: {
            ...e.data,
            timeStart: resetTime(timeEnd.clone()),
            timeEnd: timeEnd.clone(),
          },
          itemProps: {
            canMove: false,
            canResize: false,
            canChangeGroup: false
          }
        });
      }
      else {
        newList.push(e);
      }
    });

    return newList;
  }

  syncTimelineItems(changedItem) {
    const convertedItems = this.fixMiddleItems(this.listItems).map(timelineItemConverter).filter(e => isVisible(e, this.state.groups));
    this.setState({
      items: convertedItems
    });
    if (changedItem) {
      this.scheduleItem(changedItem)
    }
  }

  scheduleItem = (changedItem) => {
    this.props.scheduleResource(changedItem);
  };

  addItemToTimeline(item) {
    this.listItems.push(item);
    this.syncTimelineItems(item);
  }

  itemRenderer = (params) => {
    return <TimelineItem
        item={params.item}
        onDeleteItem={() => this.deleteItem(params.item.id)}/>;
  };

  async deleteItem(id) {
    const index = this.listItems.findIndex(e => e.id ===  id);
    if (index !== -1) {
      if (await quickAlert.confirm(`Do you want to remove: ${this.listItems[index].data.title}?`)) {
        this.props.onDeleteResource(this.listItems[index]);
        this.listItems.splice(index, 1);
        this.syncTimelineItems();
      }
    }
  }

  onChangeGroup = () => {
    this.setState({
      groups: timelineGroupsConverter(this.state.currentMonday)
    }, () => {
      this.syncTimelineItems(false)
    });
  };

  findByEndPoint(endTime, item) {
    let duration = item.data.timeEnd - item.data.timeStart;
    let newEnd = endTime;
    let newStart = null;

    if (timeLeft <= newEnd && newEnd <= timeRight ) {
      newStart = item.data.timeStart;
    }
    else if (newEnd > timeRight) {
      newEnd = timeRight;
      newStart = item.data.timeStart;
    }
    else if (newEnd < timeLeft) {
      newStart = timeLeft;
      newEnd = moment(newStart + duration);
      if (newEnd > timeRight) {
        newEnd = timeRight;
      }
    }
    return {
      newEnd,
      newStart
    }
  }


  findByStartPoint(startTime, item) {
    let duration = item.data.timeEnd - item.data.timeStart;
    let newStart = startTime;
    let newEnd = null;
    if (timeLeft <= newStart && newStart <= timeRight ) {
      newEnd = newStart + duration;
      if (newEnd > timeRight) {
        newEnd = timeRight;
        newStart = newEnd - duration;
      }
    }
    else if (newStart > timeRight) {
      newEnd = timeRight;
      newStart = timeRight - duration;
      if (newStart < timeLeft) {
        newStart = timeLeft;
      }
    }
    else if (newStart < timeLeft) {
      newStart = timeLeft;
      newEnd = newStart + duration;
      if (newEnd > timeRight) {
        newEnd = timeRight;
      }
    }
    return {
      newEnd,
      newStart
    }
  }


  /**
   * Convert weekday group to timeline groups, this is an abstraction
   * @param type:string  direction next or prev
   * @returns no return, just set new currentMonday
   */
  onChangeWeek = type => {
    const reverter = type === 'next' ? 1 : -1;
    this.setState({
      currentMonday: this.state.currentMonday.add(7 * reverter, 'days')
    }, this.onChangeGroup);
  };

  handleItemMove = (itemId, dragTime, newGroupOrder) => {
    const {items, groups} = this.state;
    const item = items.find(e => e.id === itemId);
    const weekday = groups[newGroupOrder].weekday;
    const {newStart, newEnd} = this.findByStartPoint(dragTime, item);
    item.data.timeStart = copyTime(weekday.date, newStart);
    item.data.timeEnd = copyTime(weekday.date, newEnd);
    this.syncTimelineItems(item);
  };


  handleItemResize = (itemId, time, edge) => {
    const {items} = this.state;
    const item = items.find(e => e.id === itemId);
    const {newStart, newEnd} = this.findByEndPoint(time, item);
    item.data.timeStart = copyTime(item.data.timeStart, newStart);
    item.data.timeEnd = copyTime(item.data.timeEnd, newEnd);
    this.syncTimelineItems(item);
  };


  handleDrop = (event, group, timeStart) => {
    try {
      const itemData = JSON.parse(event.dataTransfer.getData('data'));
      this.onItemDroppedSuccess(itemData, group, timeStart);
    } catch (err) {
      // This is not what I want.
    }
  };

  onItemDroppedSuccess(data, group, timeStart) {
    const {weekday} = group;
    const item = {
      data
    };
    item.id = Date.now();
    item.data.timeStart = copyTime(weekday.date, timeStart);
    item.data.timeEnd = moment(item.data.timeStart + data.duration * 1000);
    this.addItemToTimeline(item);
  }

  limitScrollHeader = (
      visibleTimeStart,
      visibleTimeEnd,
      updateScrollCanvas
  ) => {
    const {timeRight, timeLeft} = this.state;
    if (visibleTimeStart < timeLeft && visibleTimeEnd > timeRight) {
      updateScrollCanvas(timeLeft, timeRight);
    } else if (visibleTimeStart < timeLeft) {
      updateScrollCanvas(
          timeLeft,
          timeLeft + (visibleTimeEnd - visibleTimeStart)
      );
    } else if (visibleTimeEnd > timeRight) {
      updateScrollCanvas(
          timeRight - (visibleTimeEnd - visibleTimeStart),
          timeRight
      );
    } else {
      updateScrollCanvas(visibleTimeStart, visibleTimeEnd);
    }
  };

  groupRenderer = ({group}) => {
    const {type, weekday} = group;
    return (
        <Group className={`day-group day-group--${type} ${tryClass(weekday.date.isSame(today, 'd'), 'today')}`}>
          {
            type === resourceTypes.overlay.id
            &&
            <div>
              <span>{weekday.dayName}</span>
              <br/>
              <span>{weekday.dateString}</span>
            </div>
          }
        </Group>
    );
  };

  convertPageXtoStartTime = (pageX) => {
    const {state, scrollComponent} = this.timelineRef;
    const {canvasTimeStart, visibleTimeEnd, visibleTimeStart, width} = state;
    const zoom = visibleTimeEnd - visibleTimeStart;
    const canvasTimeEnd = canvasTimeStart + zoom * 3;
    const canvasWidth = width * 3;
    const ratio = (canvasTimeEnd - canvasTimeStart) / canvasWidth;
    const boundingRect = scrollComponent.getBoundingClientRect();
    return moment(visibleTimeStart + ratio * (pageX - boundingRect.left));
  };

  hookTimeline() {
    if (this.timelineRef) {
      const canvasDiv = this.timelineRef.canvasComponent;
      const rowsSelector = '.rct-horizontal-lines > [class^=rct-hl]';
      const dragOverClassName = 'row-drag-over';
      // start hook
      $(canvasDiv)
          .on('dragover', rowsSelector, (e) => {
            e.preventDefault();
            $(rowsSelector).removeClass(dragOverClassName);
            $(e.target).addClass(dragOverClassName);
          })
          .on('drop', rowsSelector, (e) => {
            e.preventDefault();
            $(e.target).removeClass(dragOverClassName);
            const id = $(rowsSelector).index(e.target);
            this.handleDrop(
                e.originalEvent,
                this.state.groups[id], this.convertPageXtoStartTime(event.pageX));
          });
    }
  }

  componentDidMount() {
    this.hookTimeline();
  }

  handleZoomOut = () => {
    this.timelineRef.changeZoom(2);
  };

  handleZoomIn = () => {
    this.timelineRef.changeZoom(0.5);
  };

  handleSelectedItem = (id) => {
    const item = this.state.items.find(e => e.id === id);
    item.actions = {
      remove: () => this.deleteItem(id)
    };
    this.props.onSelectedItem(item);
  };

  validateMoveOrResize= (action, item, time) => {
    if (action === 'resize') {
      if (time > item.data.duration*1000 + copyTime(today, item.data.timeStart)) {
        return item.data.duration*1000 + copyTime(today, item.data.timeStart);
      }
    }
    return time;
  };

  /**
   * Main render function
   * @returns {*} jsx timeline
   */
  render() {
    const {
            groups,
            items,
            defaultTimeStart,
            defaultTimeEnd,
            minZoom,
            maxZoom
          } = this.state;
    const sidebarRender = (
        <div className="timeline-sidebar__header">
          <Week>
            <i
                className="fa fa-chevron-left icon-controller"
                onClick={() => this.onChangeWeek('prev')}
            />
            <i className="fa fa-calendar calendar"/>
            <i
                className="fa fa-chevron-right icon-controller"
                onClick={() => this.onChangeWeek('next')}
            />
          </Week>
          <ZoomControl
              onZoomOut={this.handleZoomOut}
              onZoomIn={this.handleZoomIn}
          />
        </div>
    );

    return (
        <div className="timeline-weekview">
          <WrapperTimeLine>
            <Timeline
                ref={e => this.timelineRef = e}
                groups={groups}
                items={items}
                groupRenderer={this.groupRenderer}
                itemRenderer={this.itemRenderer}
                sidebarContent={sidebarRender}
                defaultTimeStart={defaultTimeStart}
                defaultTimeEnd={defaultTimeEnd}
                onItemMove={this.handleItemMove}
                onItemResize={this.handleItemResize}
                onTimeChange={this.limitScrollHeader}
                onItemSelect={this.handleSelectedItem}
                onItemDeselect={() => this.props.onSelectedItem(null)}
                moveResizeValidator={this.validateMoveOrResize}
                minZoom={minZoom}
                maxZoom={maxZoom}
                {...timelineConfig}
            />
          </WrapperTimeLine>
        </div>
    );
  }
}

export default TimelineWeekView;