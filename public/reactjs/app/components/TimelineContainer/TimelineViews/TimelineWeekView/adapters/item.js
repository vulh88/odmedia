import moment from 'moment';
import {formatsConfig} from '../../../config';

export const timelineItemConverter = (item) => {
  const {id, data} = item;
  const {type, timeStart, timeEnd} = data;
  const date = timeStart.format(formatsConfig.dateFormat);
  // Use current date, to fake position so that it's can appear on canvas
  const currentDate = moment();
  return {
    id,
    groupId: `${date}/${type}`,
    title: item.title,
    timeStart: currentDate.clone().hour(timeStart.hour()).minute(timeStart.minute()),
    timeEnd: currentDate.clone().hour(timeEnd.hour()).minute(timeEnd.minute()),
    // Store full object
    data,
    ...item.itemProps
  };
};

