import lodash from 'lodash';
import moment from 'moment';
import {formatsConfig, resourceTypes} from '../../../config';


const weekDayGroups = [
  // Monday
  {
    dayName: 'Monday',
    date: null, // Need dynamic calculation,
    isToday: false
  },
  // Tuesday
  {
    dayName: 'Tuesday',
    date: null, // Need dynamic calculation,
    isToday: false
  },
  // Wed
  {
    dayName: 'Wednesday ',
    date: null, // Need dynamic calculation,
    isToday: false
  },
  // Thursday
  {
    dayName: 'Thursday',
    date: null, // Need dynamic calculation,
    isToday: false
  },
  // Friday
  {
    dayName: 'Friday',
    date: null, // Need dynamic calculation,
    isToday: false
  },
  // Saturday
  {
    dayName: 'Saturday',
    date: null, // Need dynamic calculation,
    isToday: false
  },
  // Sundays
  {
    dayName: 'Sundays',
    date: null, // Need dynamic calculation,
    isToday: false
  }
];


/**
 * Convert weekday group to timeline groups, this is an abstraction
 * @param currentMonday: moment object
 * @returns {Array}
 */
const timelineGroupsConverter = (currentMonday) => {
  const timelineGroups = [];

  weekDayGroups.map((e, idz) => {
    e.dateString = currentMonday.clone().add(idz, 'days').format(formatsConfig.dateFormat);
    e.date = currentMonday.clone().add(idz, 'days');
    e.isToday = e.date === moment().format(formatsConfig.dateFormat);
  });

  weekDayGroups.map((weekday) => {
    lodash.map(resourceTypes, type => {
      const group = {
        id: `${weekday.dateString}/${type.id}`,
        type: type.id,
        // Store full object
        weekday,
      };
      timelineGroups.push(group);
    });
  });
  return timelineGroups;
};

export {timelineGroupsConverter};
