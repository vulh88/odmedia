import React from 'react';
import TimelineDynamicView from '../TimelineDynamicView';
import {dynamicTimelineConfig} from '../TimelineDynamicView/custom-config';
import ZoomControl from '../../components/ZoomControl';
import {WrapperTimeLine} from '../../components/styled-components';
import Timeline from 'react-calendar-timeline';
import moment from 'moment/moment';
import {formatsConfig} from '../../config';
import {timelineItemConverter} from '../TimelineDynamicView/adapters/item';
import {markActivePlaylist} from '../../utils';


class TimelineDynamicViewOnly extends TimelineDynamicView {

  static defaultProps = {};

  timelineContext = {
    mode: 'viewOnly'
  };

  _doNothing = () => {
  };

  /**
   * Disable interact
   */
  hookTimeline = this._doNothing;

  initTimelineState() {
    super.initTimelineState();
    const {startTime, endTime, playlists} = this.props;
    const {timelineState} = this;

    timelineState.maxZoom = moment.duration(1, 'day').asMilliseconds();
    timelineState.defaultTimeStart = startTime;
    timelineState.defaultTimeEnd = endTime;
    timelineState.playlistStartTime = startTime;
    timelineState.playlistEndTime = endTime;
    timelineState.timeLeft = startTime;
    timelineState.timeRight = endTime;

    // Init scheduler range
    timelineState.ranges =
        playlists.map((e, idz) => {
          return {
            id: `playlistRange-${idz}`,
            className: `playlist-active-range playlist-${e.id}`,
            timeStart: moment(e.start_time, formatsConfig.datetimeFormat),
            timeEnd: moment(e.end_time, formatsConfig.datetimeFormat)
          }
        });
  }

  componentDidUpdate({resources, startTime}) {
    if (resources !== this.timelineState.resources || startTime !== this.props.startTime) {
      this.timelineState.resources = resources;
      this.syncTimelineItems();
    }
  }

  syncTimelineItems(changedItem) {
    super.syncTimelineItems(null);
    this.timelineState.items = this.timelineState.items.map(e => {
      return {
        ...e,
        canMove: false,
        canResize: false,
        canChangeGroup: false
      };
    });
  }


  render() {
    const {defaultTimeStart, defaultTimeEnd, items, minZoom, maxZoom, groups, ranges} = this.timelineState;
    const sidebarRender = (
        <div className="timeline-sidebar__header">
          <ZoomControl
              onResetZoom={this.handleResetZoom}
              onZoomOut={this.handleZoomOut}
              onZoomIn={this.handleZoomIn}
          />
        </div>
    );
    return (
        <div className="timeline-dynamic-view">
          <WrapperTimeLine>
            <Timeline
                ref={e => this.timelineRef = e}
                {...dynamicTimelineConfig}
                groups={groups}
                ranges={ranges}
                items={items}
                sidebarContent={sidebarRender}
                defaultTimeStart={defaultTimeStart}
                defaultTimeEnd={defaultTimeEnd}
                onItemSelect={this.handleSelectedItem}
                onItemDeselect={this.handleDeselect}
                onTimeChange={this.handleOnTimeChange}
                groupRenderer={this.groupRenderer}
                itemRenderer={this.itemRenderer}
                moveResizeValidator={this.validateMoveOrResize}
                minZoom={minZoom}
                maxZoom={maxZoom}
            />
          </WrapperTimeLine>
        </div>
    );
  }

}

export default TimelineDynamicViewOnly;
