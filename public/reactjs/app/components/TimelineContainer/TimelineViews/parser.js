import {formatsConfig, resourceTypes} from '../config';
import moment from 'moment';
import lodash from 'lodash';

export function parseStoredResources(resourceList, playlist) {
  return resourceList.map(e => {
    return {
      id: Math.random(),
      data: {
        ...(parseResourceData(e)),
        playlist
      }
    };
  });
}


export function parseResourceData(e) {
  return {
    id: e.resource_id,
    scheduledId: e.id,
    title: e.title || e.filename || 'untitled',
    timeStart: moment(e.start_time, formatsConfig.apiDateTime),
    timeEnd: moment(e.end_time, formatsConfig.apiDateTime),
    duration: e.duration,
    filename: e.filename,
    type:  lodash.find(resourceTypes, type => type.track === e.item_track).id
  }
}