import {resourceTypes} from '../../../config';

export const groups = [
  {
    title: 'Overplay',
    type: resourceTypes.overlay.id,
    id: resourceTypes.overlay.id
  },
  {
    title: 'Content',
    type: resourceTypes.content.id,
    id: resourceTypes.content.id
  }
];


