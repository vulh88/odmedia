export const timelineItemConverter = (item) => {
  const {id, data} = item;
  const {type, timeStart, timeEnd} = data;
  return {
    id,
    className: `resource-id-${data.id} scheduled-id-${data.scheduledId} `,
    groupId: `${type}`,
    title: item.title,
    timeStart: timeStart,
    timeEnd: timeEnd,
    // Store full object
    data,
  };
};

