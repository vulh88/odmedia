// Helpers function
export function findMinMaxTime(resources) {
  // find min start
  let minStart = resources[0].data.timeStart.clone();
  let maxEnd = resources[0].data.timeEnd.clone();

  resources.forEach(e => {
    if (e.data.timeStart < minStart) {
      minStart = e.data.timeStart.clone();
    }
  });

  //find max end
  resources.forEach(e => {
    if (e.data.timeEnd > maxEnd) {
      maxEnd = e.data.timeEnd.clone();
    }
  });
  return [minStart,maxEnd];
}
