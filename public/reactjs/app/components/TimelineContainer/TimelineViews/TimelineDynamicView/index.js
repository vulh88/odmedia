import React from 'react';
import Timeline from 'react-calendar-timeline';
import moment from 'moment/moment';
import injectMixins from '../../../../helpers/react/inject-mixins';
import {Group, WrapperTimeLine} from '../../components/styled-components';
import {timelineItemConverter} from './adapters/item';
import PropTypes from 'prop-types';
import ZoomControl from '../../components/ZoomControl';
import {TimelineItem} from '../../components/TimelineItem';
import {groups} from './adapters/groups';
import {dynamicTimelineConfig} from './custom-config';
import quickAlert from '../../../../helpers/user-interface/quick-alert';
import {findMinMaxTime, findZoomOffset} from './helper';

class TimelineDynamicView extends React.PureComponent {

  static propsTypes = {
    playlist: PropTypes.object.isRequired,
    onDeleteResource: PropTypes.func.isRequired,
    onDropSpecialBlock: PropTypes.func.isRequired
  };

  timelineState = null;

  timelineContext = {};

  constructor(props) {
    super(props);
    this.initTimelineState();
  }

  initTimelineState() {
    const timelineState = {
      playlistStartTime: moment(this.props.playlist.start_time),
      playlistEndTime: moment(this.props.playlist.end_time),
      items: [],
      groups,
      resources: this.props.resources,
      minZoom: moment.duration(10, 'minutes').asMilliseconds(),
      maxZoom: moment.duration(1, 'years').asMilliseconds()
    };


    // Calculate visible viewport
    const offset = (timelineState.playlistEndTime - timelineState.playlistStartTime) * 2 / 100;
    Object.assign(timelineState, {
      timeLeft: moment(timelineState.playlistStartTime - offset),
      timeRight: moment(timelineState.playlistEndTime + offset)
    });

    if (timelineState.resources.length) {
      [timelineState.defaultTimeStart, timelineState.defaultTimeEnd] = findMinMaxTime(timelineState.resources);
    }
    else {
      [timelineState.defaultTimeStart, timelineState.defaultTimeEnd] = [timelineState.timeLeft, timelineState.timeRight]
    }
    // Init scheduler range
    timelineState.ranges = [
      {
        id: 'limitRange',
        className: 'playlist-active-range',
        timeStart: timelineState.playlistStartTime,
        timeEnd: timelineState.playlistEndTime
      }
    ];


    this.timelineState = injectMixins.autoRenderOnChange.call(this, timelineState);
  }

  componentDidMount() {
    this.syncTimelineItems(null);
    this.hookTimeline();
  }

  convertPageXtoStartTime = (pageX) => {
    const {state, scrollComponent} = this.timelineRef;
    const {canvasTimeStart, visibleTimeEnd, visibleTimeStart, width} = state;
    const zoom = visibleTimeEnd - visibleTimeStart;
    const canvasTimeEnd = canvasTimeStart + zoom * 3;
    const canvasWidth = width * 3;
    const ratio = (canvasTimeEnd - canvasTimeStart) / canvasWidth;
    const boundingRect = scrollComponent.getBoundingClientRect();
    return moment(visibleTimeStart + ratio * (pageX - boundingRect.left));
  };

  handleDrop = async (event, timeStart) => {
    try {
      let itemData = JSON.parse(event.dataTransfer.getData('data'));
      if (itemData.specialBlockId) {
        itemData = await this.props.onDropSpecialBlock(itemData, timeStart);
      }
      if (itemData) {
        this.onItemDroppedSuccess(itemData, timeStart);
      }
    } catch (err) {
      console.error(err);
      // This is not what I want.
    }
  };

  syncTimelineItems(changedItem) {
    this.timelineState.items = this.timelineState.resources.map(timelineItemConverter);
    if (changedItem) {
      this.props.scheduleResource(changedItem);
    }
  }


  addItemToTimeline(item) {
    this.fixOverEdgedItem(item);
    this.timelineState.resources.push(item);
    this.syncTimelineItems(item);
  }

  async deleteItem(id) {
    const index = this.timelineState.resources.findIndex(e => e.id === id);
    if (index !== -1) {
      if (await quickAlert.confirm(`Do you want to remove: ${this.timelineState.resources[index].data.title}?`)) {
        this.props.onDeleteResource(this.timelineState.resources[index]);
        this.props.resources.splice(index, 1);
        this.syncTimelineItems();
      }
    }
  }

  async onItemDroppedSuccess(data, timeStart) {
    let item = {data};
    item.id = Date.now();
    item.data.timeStart = timeStart;
    item.data.timeEnd = moment(item.data.timeStart + data.duration*1000);
    this.addItemToTimeline(item);
  }

  hookTimeline() {
    if (this.timelineRef) {
      const canvasDiv = this.timelineRef.canvasComponent;
      const rowsSelector = '.rct-horizontal-lines > [class^=rct-hl]';
      const dragOverClassName = 'row-drag-over';
      // start hook
      $(canvasDiv)
          .on('dragover', rowsSelector, (e) => {
            e.preventDefault();
            $(rowsSelector).removeClass(dragOverClassName);
            $(e.target).addClass(dragOverClassName);
          })
          .on('drop', rowsSelector, (e) => {
            e.preventDefault();
            $(e.target).removeClass(dragOverClassName);
            this.handleDrop(e.originalEvent, this.convertPageXtoStartTime(event.pageX));
          });
    }
  }

  fixOverEdgedItem(item) {
    const {timeStart, timeEnd} = item.data;
    const duration = timeEnd - timeStart;
    const {playlistStartTime, playlistEndTime} = this.timelineState;
    if (timeStart < playlistStartTime) {
      item.data.timeStart = playlistStartTime;
      const newEnd = moment(playlistStartTime + duration);
      item.data.timeEnd = newEnd > playlistEndTime ? playlistEndTime : newEnd;
      return;
    }
    if (timeEnd > playlistEndTime) {
      item.data.timeEnd = playlistEndTime;
      const newStart = moment(playlistEndTime - duration);
      item.data.timeStart = newStart < playlistStartTime ? playlistStartTime : newStart;
    }
  }

  handleItemMove = (itemId, dragTime) => {
    const {items} = this.timelineState;
    const item = items.find(e => e.id === itemId);
    const duration = item.data.timeEnd.clone() - item.data.timeStart.clone();
    item.data.timeStart = moment(dragTime);
    item.data.timeEnd = moment(item.data.timeStart + duration);
    this.fixOverEdgedItem(item);
    this.syncTimelineItems(item);
  };

  handleItemResize = (itemId, time, edge) => {
    const {items} = this.timelineState;
    const item = items.find(e => e.id === itemId);
    item.data.timeEnd = moment(time);
    this.fixOverEdgedItem(item);
    this.syncTimelineItems(item);
  };


  /**
   * Handle scroll, limit the view port here
   */
  handleOnTimeChange = (visibleTimeStart, visibleTimeEnd, updateScrollCanvas) => {
    const [minTime, maxTime] = [this.timelineState.timeLeft, this.timelineState.timeRight];
    if (visibleTimeStart < minTime && visibleTimeEnd > maxTime) {

      updateScrollCanvas(minTime, maxTime)

    } else if (visibleTimeStart < minTime) {

      updateScrollCanvas(minTime, minTime + (visibleTimeEnd - visibleTimeStart))
    } else if (visibleTimeEnd > maxTime) {

      updateScrollCanvas(maxTime - (visibleTimeEnd - visibleTimeStart), maxTime)
    } else {

      updateScrollCanvas(visibleTimeStart, visibleTimeEnd)
    }
  };


  handleZoomOut = () => {
    this.timelineRef.changeZoom(2);
  };

  handleZoomIn = () => {
    this.timelineRef.changeZoom(0.5);
  };


  groupRenderer = ({group}) => {
    const {type} = group;
    return (
        <Group className="type-group">
          <h4>{type}</h4>
        </Group>
    );
  };

  itemRenderer = (params) => {
    return <TimelineItem
        timelineContext={this.timelineContext}
        item={params.item}
        onDeleteItem={() => this.deleteItem(params.item.id)}/>;
  };

  handleSelectedItem = (id) => {
    const item = this.timelineState.items.find(e => e.id === id);
    item.actions = {
      remove: () => this.deleteItem(id)
    };
    this.props.onSelectedItem(item);
  };

  validateMoveOrResize= (action, item, time) => {
    if (action === 'resize') {
      if (time > item.data.duration*1000 + item.data.timeStart) {
        return item.data.duration*1000 + item.data.timeStart;
      }
    }
    return time;
  };

  handleDeselect = () => {
    this.props.onSelectedItem(null);
  };

  handleResetZoom = () => {
    this.timelineRef.changeZoom(100);
  };

  render() {
    const {defaultTimeStart, defaultTimeEnd, items, minZoom, maxZoom, groups, ranges} = this.timelineState;
    const sidebarRender = (
        <div className="timeline-sidebar__header">
          <ZoomControl
              onResetZoom={this.handleResetZoom}
              onZoomOut={this.handleZoomOut}
              onZoomIn={this.handleZoomIn}
          />
        </div>
    );
    return (
        <div className="timeline-dynamic-view">
          <WrapperTimeLine>
            <Timeline
                ref={e => this.timelineRef = e}
                {...dynamicTimelineConfig}
                groups={groups}
                items={items}
                sidebarContent={sidebarRender}
                defaultTimeStart={defaultTimeStart}
                defaultTimeEnd={defaultTimeEnd}
                onTimeChange={this.handleOnTimeChange}
                groupRenderer={this.groupRenderer}
                itemRenderer={this.itemRenderer}
                onItemSelect={this.handleSelectedItem}
                onItemDeselect={this.handleDeselect}
                onItemMove={this.handleItemMove}
                onItemResize={this.handleItemResize}
                moveResizeValidator={this.validateMoveOrResize}
                minZoom={minZoom}
                maxZoom={maxZoom}
                ranges={ranges}
            />
          </WrapperTimeLine>
        </div>
    );
  }
}

export default TimelineDynamicView;
