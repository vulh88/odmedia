import {formatsConfig, timelineConfig} from '../../config';

export const dynamicTimelineConfig = {
  ...timelineConfig,
  lineHeight: 90,
  itemHeightRatio: 0.5,
  timeSteps: {
    // second: 1,
    minute: 5,
    hour: 1,
    day: 1,
    month: 1,
    year: 1
  },
  headerLabelFormats: {
    ...timelineConfig.headerLabelFormats,
    dayShort: formatsConfig.dateFormat,
    dayLong: formatsConfig.dateFormat,
    hourShort: formatsConfig.datetimeFormat,
    hourMedium: formatsConfig.datetimeFormat,
    hourMediumLong: formatsConfig.datetimeFormat,
    hourLong: formatsConfig.dateFormat,
    time: formatsConfig.datetimeFormat
  }
};
