import { fromJS } from 'immutable';
import * as actions from './actions';

const initialState = fromJS({
  isSaving: false,
  error: false,
  resources: null,
  dynamicItems: [],
  weekViewItems: []
});

function timelineReducer(state = initialState, action) {
  switch (action.type) {
    case actions.INIT_APP:
      return state
          .set('loading', true)
          .set('isReady', false);
    case actions.INIT_APP_SUCCESS:
      return state
          .set('loading', false)
          .set('isReady', true);
    case actions.INIT_APP_SUCCESS_WITHOUT_CHANNEL:
      return state
          .set('currentChannel',{})
          .set('channelSettings',{})
          .set('loading', false)
          .set('isReady', true);
    case actions.REQUEST_LOGIN_SUCCESS:
      return state
          .set('isLoggedIn',true);
    case actions.REQUEST_LOGOUT_SUCCESS:
      return (state = initialState
          .set('isLoggedIn', false)
          .set('isReady',false));
    case actions.SET_ACCESS_TOKEN:
      return state.set('accessToken', action.data);
    case actions.REQUEST_CHANNEL_SUCCESS:
      return state.set('listChannel', action.payload);
    case actions.SET_CURRENT_CHANNEL:
      return state
          .set('loadingChannel', true)
          .set('error', false);
    case actions.CHANGE_CHANNEL_SUCCESS:
      return state
          .set('currentChannel', action.payload.channel);
    case actions.CHANNEL_SETTINGS_SUCCESS:
      return state
          .set('channelSettings', action.payload)
          .set('loadingChannel', false);
    case actions.CHANNEL_SETTINGS_ERROR:
      return state
          .set('channelSettings', null)
          .set('error', action.error)
          .set('loadingChannel', false);
    default:
      return state;
  }
}

export default timelineReducer;

