export const SCHEDULE_AN_ITEM         = 'timeline/SCHEDULE_AN_ITEM';
export const REMOVE_AN_SCHEDULED_ITEM = 'timeline/REMOVE_AN_SCHEDULED_ITEM';
export const PARSE_STORED_RESOURCES   = 'timeline/PARSE_STORED_RESOURCES';