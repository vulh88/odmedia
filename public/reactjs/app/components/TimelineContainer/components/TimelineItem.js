import React from 'react';
import { ContextMenu, ContextMenuProvider } from 'react-contexify';
import locationHelper from '../../../helpers/location';
import routePaths from '../../../settings/route-paths';

export function TimelineItem({item, onDeleteItem, timelineContext = {}}) {
  const {timeStart, timeEnd, data} = item;
  const {isAddingNew, error} = item.data;
  const idMenu  = `menu-${item.id}`;
  return (
      <div className={`timeline-item resource-id-${data.id} timeline-item--${data.type} ${isAddingNew ? 'timeline-item--adding' : ''}`} onMouseDown={(e) => preventRightClickCapture(e, item)}>
        <ContextMenuProvider id={idMenu} >
          <div className="timeline-item__inner">
            <p>
              {isAddingNew && <i className="fa flashing fa-save mr-0-5"/>}
              {error && <i className="fa  font-red flashing fa-exclamation-triangle mr-0-5"/>}
              {`${data.title}`}</p>
            <p className="item-info">
              {timeStart.format('HH:mm:ss')}
              &nbsp;-&nbsp;
              {timeEnd.format('HH:mm:ss')}
            </p>
          </div>
        </ContextMenuProvider>
        {
          <ItemContextMenu
              item={item}
              timelineContext={timelineContext}
              isShow={!isAddingNew}
              onDeleteItem={onDeleteItem}
              id={idMenu}/>
        }
      </div>
  );
}

/**
 * Use to prevent select item by right click
 * @param e
 * @param item
 */
function preventRightClickCapture(e, item) {
  const {isAddingNew} = item.data;
  if (isAddingNew ||e.button === 2) {
    e.stopPropagation();
    return null;
  }
}

const Wrapper = ({children}) => {
  return <div>{children}</div>
};

const ItemContextMenu = ({id, item, onDeleteItem, isShow, timelineContext}) => {
  let inner = null;

  if (timelineContext.mode === 'viewOnly') {
    inner = (
        <ul className={`context-menu dropdown-menu dropdown-menu--context-menu ${isShow ? '' : 'hidden'}`}>
          <li>
            <a className="hand" target="_blank" href={locationHelper.getLink(routePaths.playlists.scheduling,{id: item.data.playlist.id})}>
              <i className="fa fa-edit"/>Edit schedule
            </a>
          </li>
        </ul>
    )
  }

  else {
    inner =  (
        <ul className={`context-menu dropdown-menu dropdown-menu--context-menu ${isShow ? '' : 'hidden'}`}>
          <li>
            <a className="hand"
               onClick={onDeleteItem}>
              <i className="fa fa-remove font-red"/>Delete</a>
          </li>
        </ul>
    );
  }

  return (
      <ContextMenu id={id}>
        <Wrapper>
          {inner}
        </Wrapper>
      </ContextMenu>
  )
};
