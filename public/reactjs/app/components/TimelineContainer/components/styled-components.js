import styled from 'styled-components';

export const WrapperTimeLine = styled.div`
  .react-calendar-timeline {
    background: #23272b;
  }

  .rct-sidebar-header {
    background: transparent !important;
    color: #fff !important;
  }

  .rct-horizontal-lines {
    background: #3a4146;
  }

  .rct-cursor-line {
    background: #36c6d3;
  }

  .rct-label-group,
  .rct-label {
    background: #23272b !important;
    color: #fff !important;
  }

  .rct-sidebar-row {
    background-color: #000 !important;
    color: #b4bcc8 !important;
  }

  .rct-item {
    background: rgba(255, 255, 255, 0.2) !important;
    border-color: transparent;
    color: #fff !important;

    .rct-item-content {
      max-width: none !impotant;
      width: 100%;
      padding: 0;
    }

    &.selected {
      border: 2px solid #f5d327;
      color: #fff !important;

      &:hover {
        .controller {
          display: none;
        }
      }

      &.can-resize-right {
        border-right-width: 5px;
      }

      &.dropdown-up {
        .content-controller {
          bottom: calc(100% + 20px);
          top: auto;

          &:before {
            top: auto;
            bottom: -17px;
            border-bottom-width: 17px;
            border-top-width: 0;
          }
        }
      }

      .rct-item-content {
        overflow: initial;
      }

      .content-controller {
        display: block;
      }
    }

    &:hover {
      .controller {
        display: block;
      }
    }
  }
`;

export const HeaderTimeLine = styled.div`
  display: flex;
  height: 100%;
  justify-content: center;
  background-color: #24272c;
`;

export const Week = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 10px;
  
  .calendar {
    font-size: 24px;
  }

  .icon-controller {
    font-size: 10px;
    padding: 1px 4px;
    border: 2px solid #a2aab1;
    border-radius: 50%;
    cursor: pointer;

    &:hover {
      color: #36c6d3;
      border-color: #36c6d3;
    }
  }
`;

export const Group = styled.div`
  line-height: 16px;
  text-align: right;
  padding: 10px 20px 0px 20px;

  &.today {
    color: #6fc041;
  }
`;

export const ItemTimeLine = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  z-index: 1;

  .fa {
    font-size: 20px;
    padding: 0 5px;
  }

  .controller {
    display: none;
    position: absolute;
    z-index: 1;
    top: 0;
    width: 100%;
    background: rgba(0, 0, 0, 0.5);
    cursor: pointer;
  }

  .content-controller {
    display: none;
    position: absolute;
    z-index: 1;
    top: calc(100% + 17px);
    left: 50%;
    height: 100px;
    width: 150px;
    background: rgba(0, 0, 0, 0.5);
    cursor: pointer;

    &:before {
      content: '';
      display: block;
      position: absolute;
      top: -17px;
      border-bottom: 0 solid transparent;
      border-left: 17px solid black;
      border-top: 17px solid transparent;
      opacity: 0.5;
    }
  }
`;
