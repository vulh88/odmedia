import React from 'react';
import PropTypes from 'prop-types';
import FileItem from '../FileItem';
import HaveApiComponent from '../../../BaseComponents/HaveApiComponent';
import resourcesApiService from '../../../../services/api/resources';
import FormAlert from '../../../FormAlert';
import PagingComponent from '../../../PagingComponent';
import {utils} from 'helpers/utils/index';
import lodash from 'lodash';
import {resourceTypes} from '../../config';

const count = 10;

export class FileList extends HaveApiComponent {
  static propsTypes = {
    requestParams: PropTypes.object.isRequired,
    resourceType: PropTypes.oneOf([resourceTypes.overlay, resourceTypes.content]).isRequired
  };

  componentWillMount() {
    this.requestFiles(1);
  }

  requestFiles(page = 1, newRequestParams) {
    const {requestParams} = this.props;
    this.callApi(resourcesApiService.getList({
          page,
          count,
          ...(newRequestParams || requestParams),
          ...(this.search ? {keyword: this.search} : null)
        }),
        ({data, paging}) => {
          this.apiState.fileList = {
            data,
            paging
          };
        }
    );
  }

  handlePageChange = (page) => {
    this.requestFiles(page);
  };

  handleSearchChange = (target) => {
    if (this.search !== target.value) {
      this.search = target.value;
      this.requestFiles(1);
    }
  };

  componentWillReceiveProps({requestParams}) {
    if (requestParams.channel_id !== this.props.requestParams.channel_id) {
      this.requestFiles(1, requestParams);
    }
  }

  componentDidUpdate() {
    utils.triggerWindowResized();
  }

  render() {
    const {title, resourceType} = this.props;
    const {fileList, loading, errors} = this.apiState;
    return (
        <div className="portlet mb-05">
          <div className="portlet-title">
            <div className="caption">
              <span className="caption-subject font-dark bold uppercase">{title}</span>
              {
                loading && <i className="fa fa-spin fa-circle-o-notch pull-right"/>
              }
            </div>
          </div>
          <div className="portlet-body">
            <div className="file-list">
              <FormAlert errors={errors}/>
              <div className="form-group">
                <form>
                  <div className="input-group">
                    <span className="input-group-addon">
                      <i className="fa fa-search"/>
                    </span>
                    <input
                        onChange={(event) =>
                            lodash.debounce(this.handleSearchChange, 250)(event.target)}
                        type="text"
                        className="form-control"
                        placeholder={`Search ${resourceType.name.toLowerCase()} files...`}/>
                  </div>
                </form>
              </div>
              {
                fileList &&
                <div className="file-list__inner">
                  {
                    fileList.data.length ?
                        fileList.data.map(
                            (e, idz) => <FileItem resourceType={this.props.resourceType} key={idz} data={e}/>) :
                        <p className="text-center info">There are no resources</p>
                  }
                </div>
              }
            </div>
            {
              fileList &&
              <div className="file-list__footer">
                <div className="text-right gutter">
                  <PagingComponent
                      paging={fileList.paging}
                      onChangePage={this.handlePageChange}
                  />
                </div>
              </div>
            }
          </div>
        </div>
    );
  }
}

export default FileList;
