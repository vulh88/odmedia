import React from 'react';
import PropsTypes from 'prop-types';
import PlaylistForm from 'containers/PlaylistsPage/Form';
import {getResourcesType} from '../../config';
import {formatDuration} from '../../../../helpers/utils';
import {markActivePlaylist, markActiveResources} from '../../utils';
import linkGenerator from '../../../../common/link-generator';
import {Link} from 'react-router-dom';
import iconsReference from '../../../../common/icons';
import ScrollArea from 'react-scrollbar';
import Popup from 'components/Popup';

class PlaylistSlideOut extends React.Component {
  static propsTypes = {
    playlist: PropsTypes.object.isRequired
  };

  state = {
    isOpen: true,
    playlistName: ''
  };


  toggle() {
    this.state.isOpen = !this.state.isOpen;
    this.forceUpdate();
  }

  open() {
    this.state.isOpen = true;
    this.forceUpdate();
  }

  onClose = (event) => {
    event.preventDefault();
    event.stopPropagation();
    this.toggle();
  };

  close() {
    this.state.isOpen = false;
    this.forceUpdate();
  }

  closePopup = (data) => {
    if (data) {
      this.setState({
        playlistName: data.name
      });
    }
    this.forceUpdate();
    this.popup.close();
  };

  renderEditPlaylistPopup() {
    const {playlist} = this.props;
    return (
        <Popup
            title="Quick edit playlist"
            showFooter={false}
            ref={e => this.popup = e}
            render={() => <PlaylistForm
                            key={playlist.id}
                            updatedCallback={this.props.onPlaylistUpdated}
                            updateId={playlist.id}
                            closePopup={this.closePopup}
                        />}
        >
        </Popup>
    );
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      if (nextProps.playlist) {
        this.setState({
          playlistName: nextProps.playlist.name
        });
      }
    }
  }


  render() {
    const {isOpen, playlistName} = this.state;
    const {playlist} = this.props;

    if (!playlist) {
      return null;
    }
    const {resources}  = playlist;
    const scheduleLink = linkGenerator.playlists.scheduling(playlist);

    return (
        <div className={`playlist-slide-out slide-out ${isOpen ? 'slide-out--open' : 'slide-out--close'}`} {...markActivePlaylist(playlist)}>
          <div className="slide-out__overlay"/>
          <div className="slide-out__wrapper">
            <div className="portlet light">
              <div className="portlet-title">
                <div className="caption">
                  <i className="fa fa-play"/>
                  <span className="caption-subject bold font-grey-gallery uppercase"> Playlist overview </span>
                </div>
                <div className="tools">
                  <Link to={scheduleLink} className="fa fa-clock-o mr-0-5" data-original-title="Go to scheduling" title="Go to scheduling"> </Link>
                  <a className="fa fa-close "  onClick={this.onClose} data-original-title="Close" title="Close"> </a>
                </div>
              </div>
              <ScrollArea
                  speed={0.8}
                  className="area"
                  contentClassName="portlet-body"
                  horizontal={false}
              >
                <div className="slide-out-playlist-info">

                  {/****** Render popup edit playlist *****/}
                  {this.renderEditPlaylistPopup()}

                  <div className="playlist__block">
                    <a onClick={e => this.popup.open()} className="playlist__title">{playlistName}</a>
                    <div className="playlist__meta">
                      <span className="mr-1"><i className={iconsReference.start}/> {playlist.start_time}</span>
                      <span><i className={iconsReference.stop}/> {playlist.end_time}</span>
                    </div>
                  </div>
                </div>
                <div className="slide-out-playlist-resource">
                  <h4 className="mb-2">Playlist resources</h4>
                  {
                    resources ? this.renderListResources()
                        : <p className="text-center mt-5">No resource in this playlist</p>
                  }

                </div>
                </ScrollArea>
            </div>
          </div>
        </div>
    );
  }

  renderListResources() {
    const {playlist} = this.props;
    const {resources} = playlist;

    return resources.sort((a, b) => new Date(a.start_time) - new Date(b.start_time)).map((e, idz) => {
      const type = getResourcesType(e);
      const link = linkGenerator.resources.single(e);
      return (
          <div key={idz} {...markActiveResources(e.id)} className="horizontal-video-card resource-video-card">
            <Link to={link} className="video__thumbnail hand">
              {type.icon}
              {/*<div className="video__thumbnail-responsive">*/}
                {/*<div className="minutes">{formatDuration(e.duration,{minOnly: true})}</div>*/}
              {/*</div>*/}
            </Link>
            <div className="video__block">
              <Link to={link} className="video__title">{e.title || '<Untitled>'}</Link>
              <div className="video__meta">
                <span className="mr-1"><i className={iconsReference.start}/> {e.start_time}</span>
                <span><i className={iconsReference.stop}/> {e.end_time}</span>
                <br/>

                <span className="video__category mr-1">
                  <i className={iconsReference.videoFile}></i>
                  {type.name}
                  </span>
                <span><i className="fa fa-clock-o"/> {formatDuration(e.duration,{minOnly: true})}</span>

              </div>
            </div>
          </div>
      );
    })
  }

}

export default PlaylistSlideOut;

