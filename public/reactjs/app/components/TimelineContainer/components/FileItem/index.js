import React from 'react';
import PropTypes from 'prop-types';
import {getMediaType, mediaTypes, resourceTypes} from '../../config';
import moment from 'moment';
import {formatDuration} from '../../../../helpers/utils';

class Index extends React.PureComponent {
  static propsTypes = {
    data: PropTypes.object.isRequired,
    resourceType: PropTypes.oneOf([resourceTypes.overlay, resourceTypes.content]).isRequired
  };

  handleDragStart = (event) => {
    const data = this.props.data;
    data.type = this.props.resourceType.id;
    data.title = data.title || data.filename;
    event.dataTransfer.setData('data', JSON.stringify(this.props.data));
  };

  render() {
    const {data, resourceType} = this.props;
    const duration             = moment.duration(data.duration, 'seconds');
    data.type                  = resourceType.id;

    return (
        <div className="file-item" title={data.title}>
          <div className="file-item__inner" draggable onDragStart={this.handleDragStart}>
            <div className="file-item__thumbnail">
              {resourceType.icon}
            </div>
            <div className="file-item__info">
              <p>{formatDuration(duration.asSeconds())}</p>
              <p className="file-item_name">
                {data.title}</p>
            </div>
          </div>
        </div>
    );
  }
}

export default Index;
