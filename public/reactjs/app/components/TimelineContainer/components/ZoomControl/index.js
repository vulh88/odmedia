import React from 'react';
import PropTypes from 'prop-types'

class ZoomControl extends React.PureComponent {

  static propsTypes = {
    onZoomOut: PropTypes.func,
    onZoomIn: PropTypes.func,
    onResetZoom: PropTypes.func
  };

  handleZoomOut = () => {
    if (this.props.onZoomOut) {
      this.props.onZoomOut();
    }
  };

  handleZoomIn = () => {
    if (this.props.onZoomIn) {
      this.props.onZoomIn();
    }
  };

  handleResetZoom = () => {
    if (this.props.onResetZoom) {
      this.props.onResetZoom();
    }
  };

  render() {
    return (
        <div className="zoom-control">
          <div
              className="zoom-control__button white zoom-control__button--out"
              onClick={this.handleZoomOut}
          >
            <i className="fa fa-search-minus"/>
          </div>
          <div
              className="zoom-control__button white zoom-control__button--out"
              onClick={this.handleResetZoom}
          >
            <i className="fa fa-window-restore"/>
          </div>
          <div
              className="zoom-control__button white btn-outline zoom-control__button--in"
              onClick={this.handleZoomIn}
          >
            <i className="fa fa-search-plus"/>
          </div>
        </div>
    );
  }
}

export default ZoomControl;