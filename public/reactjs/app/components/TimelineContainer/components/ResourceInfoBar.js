import React from 'react';
import {formatsConfig, resourceTypes} from '../config';
import {showDuration} from '../utils';
import lodash from 'lodash';
import {truncateFileName} from '../../../helpers/utils';

export const ResourceInfoBar = ({data, actions}) => {
  const title = !data ? 'No item selected' : data.title;
  const type = data && resourceTypes[data.type];
  const icon = type && type.icon;

  const deleteButton = actions && <span className="hand hover-link" onClick={actions.remove}><i className="fa fa-trash font-red"/> Remove</span>;
  const metaInfos = [
    //file start
    {
      title: 'Time start',
      icon: 'fa fa-play-circle',
      value: data && data.timeStart.format(formatsConfig.datetimeFormat)
    },
    //file end
    {
      title: 'Time end',
      icon: 'fa fa-stop-circle',
      value: data && data.timeEnd.format(formatsConfig.datetimeFormat)
    },
    //play duration
    {
      title: 'Play duration',
      icon: 'fa fa-compress',
      value: data && showDuration((data.timeEnd - data.timeStart)/1000)
    },
    //file duration
    {
      title: 'File duration',
      icon: 'fa fa-clock-o',
      value: data && showDuration(data.duration)
    },

    //file type
    {
      title: 'File type',
      icon: 'fa fa-file',
      value: type && type.name
    },
    // File name
    {
      title: data && data.filename ? data.filename : 'File name',
      icon: 'fa fa-file-video-o',
      value: data  ?  (data.filename ? truncateFileName(data.filename) : 'N/A') : ''
    }
  ];

  return (
      <div className={`resource-info-bar ${!data ? 'resource-info-bar--unselected' : ''}`}>
        <div className="col-title">
          {
            icon ? icon : <i className="fa fa-file-o resource-icon"/>
          }
          <span className="media-title" title={title}>
            {lodash.truncate(title, {length: 100})}
            </span>
        </div>
        <div className="col-right">
          <div className='metaphors'>
            {metaInfos.map((e, idz) => <span title={e.title} key={idz}><i className={e.icon}/>{e.value}</span>)}
            {deleteButton}
          </div>
        </div>
        <div className="clearfix"/>
      </div>
  );
};