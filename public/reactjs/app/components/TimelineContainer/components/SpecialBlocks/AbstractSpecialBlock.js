import React from 'react';
import {resourceTypes} from '../../config';

class AbstractSpecialBlock extends React.PureComponent {

  popup     = null;
  resolver  = null;

  onClosePopup = () => {
    this.resolver(null);
    this.resolver = null;
  };

  open(resolver) {
    this.resolver = resolver;
    this.popup.open();
  }

  /**
   * model of timeline item.
   * @returns {{id: number, duration: number, title: string, filename: string, type: string}}
   */
  // createItemData() {
  //   return {
  //     id: 92,
  //     duration: 800,
  //     title: 'AbstractSpecialBlock',
  //     filename: 'N/A',
  //     type: resourceTypes.overlay.id
  //   };
  // }

  onSave(formData) {
    const itemData = this.createItemData(formData);
    if (this.resolver) {
      this.resolver(itemData);
      this.popup.close();
      this.resolver = null;
    }
  }

  render() {
    throw new Error('Please implement!');
  }
}

export default AbstractSpecialBlock;
