import React from 'react';
import {Popup} from '../../../Popup';
import AbstractSpecialBlock from './AbstractSpecialBlock';
import EventBlockPlaylist from '../../../../containers/PlaylistsPage/EventBlock';
import {resourceTypes} from '../../config';

class EventBlock extends AbstractSpecialBlock {

  createItemData(data) {
    const eventData = this.getDataEvent(data.eventBlocks, data.event_block_id);
    return {
      id: 0,
      title: "Eventblock - " + eventData.title,
      duration: eventData.duration,
      event_block_id: data.event_block_id,
      event_block_data: data.event_block_data,
      type: resourceTypes.content.id
    };
  }

  getDataEvent(eventBlocks, eventId) {
    let eventData = {
      title: '',
      duration: 0
    };
    eventBlocks.filter((el, idz) => {
      if (eventId == el.id) {
        eventData = {
          title: el.name,
          duration: el.default_duration
        };
        return eventData;
      }
    });
    return eventData;
  }

  render() {
    return (
        <Popup
            title="Add Event block"
            showFooter={false}
            onClose={this.onClosePopup}
            ref={e => this.popup = e}
            className="popup-event-block"
            render={() => <EventBlockPlaylist
                playlist={this.props.playlist}
                onUpdated={(data) => {
                  this.onSave(data);
                }}
            />}
        >
        </Popup>
    );
  }
}

export default EventBlock;
