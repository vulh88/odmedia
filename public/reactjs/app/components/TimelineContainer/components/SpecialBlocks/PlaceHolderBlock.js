import React from 'react';
import {Popup} from '../../../Popup';
import PlaceholderBlock from '../../../../containers/PlaylistsPage/PlaceholderBlock';
import AbstractSpecialBlock from './AbstractSpecialBlock';
import {resourceTypes} from '../../config';

class PlaceHolderBlock extends AbstractSpecialBlock {

  createItemData(data) {
    return {
      id: data.id,
      duration: data.duration,
      title: data.title,
      filename: data.filename,
      type: data.resource_type.is_overlay === 1 ? resourceTypes.overlay.id : resourceTypes.content.id
    };
  }

  render() {
    return (
        <Popup
            title="Add Placeholder"
            showFooter={false}
            onClose={this.onClosePopup}
            ref={e => this.popup = e}
            render={() => <PlaceholderBlock playlist={this.props.playlist} onUpdated={(data) => {
              this.onSave(data);
            }}
            />}
            className="popup-placeholder-block"
        >
        </Popup>
    );
  }
}

export default PlaceHolderBlock;
