import React from 'react';
import {utils} from 'helpers/utils';
import PlaceHolderBlock from './PlaceHolderBlock';
import EventBlock from './EventBlock';

export const specialBlocksDictionary = {
  eventBlock: {
    id: 'eventBlock',
    name: 'Event block',
    popup: EventBlock,
    summary: 'Please drag Event block into time line',
  },
  placeHolderBlock: {
    id: 'placeHolderBlock',
    name: 'Placeholder block',
    popup: PlaceHolderBlock,
    summary: 'Please drag Placeholder block into time line',
  }
};
