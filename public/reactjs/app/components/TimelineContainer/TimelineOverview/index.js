import React from 'react';
import {makeSelectChannel} from 'store/app/selectors';
import playlistsApiService, {PlaylistsApiService} from '../../../services/api/playlists';
import HaveApiComponent from '../../BaseComponents/HaveApiComponent';
import {displayDateRange, displayWeekday, isToday, markActivePlaylist, resetTime} from '../utils';
import moment from 'moment';
import {formatsConfig, resourceTypes} from '../config';
import {parseResourceData, parseStoredResources} from '../TimelineViews/parser';
import TimelineDynamicViewOnly from '../TimelineViews/TimelineDynamicViewOnly';
import SnapshotLoading from '../../SnapshotLoading';
import DatePicker from 'react-datepicker';
import {withContext} from '../../../helpers/react/withContext';
import {AppContext} from '../../../containers/App/context';
import {ResourceInfoBar} from '../components/ResourceInfoBar';
import {formatDuration, plural, utils} from '../../../helpers/utils';
import PlaylistSlideOut from '../components/PlaylistSlideOut';
import iconsReference from '../../../common/icons';
import {Link} from 'react-router-dom';
import ReactDOMServer from 'react-dom/server';
import linkGenerator from '../../../common/link-generator';
import lodash from 'lodash';

const {renderToStaticMarkup} = ReactDOMServer;

const dayLimit = 5;

function formatDateMessage(date) {
  return `${date.format(formatsConfig.dateFormat)} ${isToday(date) ? ' (today)' : ''}`;
}

function isPlaylistInDate(date, playlist) {
  const playlistStartTime = moment(playlist.start_time, formatsConfig.datetimeFormat);
  const playlistEndTime = moment(playlist.end_time, formatsConfig.datetimeFormat);
  const dayStart = date;
  const dayEnd = date.clone().add(1, 'days');
  return (
      playlistStartTime >= dayStart && dayEnd >= playlistEndTime)
      || (playlistStartTime < dayStart && playlistEndTime > dayEnd)
      || ( playlistStartTime < dayEnd && playlistEndTime > dayEnd)
}

@withContext(AppContext)
class TimelineOverview extends HaveApiComponent {

  initApiState() {
    return {
      ...super.apiState,
      date: resetTime(moment()),
      currentDatePlaylists: [],
      allPlaylists: [],
      resources: [],
      selectedItem: null
    }
  }

  get channel() {
    return this.props.contextData.currentChannel;
  }

  get timeRange() {
    return {
      startTime: this.apiState.date.clone(),
      endTime: this.apiState.date.clone().add(1, 'day')
    }
  }

  get weekMaps() {
    const arr = [];
    for (let i = 0; i < dayLimit; i++) {
      arr.push(this.apiState.date.clone().add(i, 'day'));
    }
    return arr;
  }

  getallPlaylists = () => {
    this.apiState.selectedItem = null;
    if (this.popup) {
      this.popup.close();
    }
    const {date} = this.apiState;
    this.callApi(playlistsApiService.getList({
          status: PlaylistsApiService.playlistStatus.publish,
          start_time: date.format(formatsConfig.datetimeFormat),
          end_time: date.clone().add(dayLimit, 'days').format(formatsConfig.datetimeFormat),
          page: -1
        }),
        async ({data}) => {
          this.apiState.allPlaylists = data.sort((a, b) => new Date(a.start_time) - new Date(b.start_time));
          await this.getPlaylistsResources();
        }
    );
  };

  async getPlaylistsResources() {
    const {allPlaylists} = this.apiState;
    let resources = [];
    if (Array.isArray(allPlaylists)) {
      this.apiState.currentDatePlaylists = this.apiState.allPlaylists.filter(e => {
        return isPlaylistInDate(this.apiState.date, e);
      });
      this.apiState.gettingResource = true;
      await Promise.all(allPlaylists.map(e => {
        return playlistsApiService.getListResources(e.id)
            .then(resp => {
              if (resp) {
                const listItems = parseStoredResources(resp, e);
                resources = resources.concat(listItems);
              }
              e.resources = resp || [];
            });
      }));
      this.apiState.gettingResource = false;
      this.apiState.resources = resources;
    }

  }

  componentWillMount() {
    this.getallPlaylists();
  }

  handleDateChange = (newDate) => {
    if (newDate) {
      const {date} = this.apiState;
      if (newDate && newDate !== date.format(formatsConfig.dateFormat)) {
        this.apiState.date = moment(newDate, formatsConfig.dateFormat);
        this.getallPlaylists();
      }
    }
  };

  setDate(date) {
    this.apiState.date = date;
    this.getallPlaylists();
  }

  handleNavigateDate = (delta) => {
    this.apiState.date = this.apiState.date.add(delta, 'days');
    this.getallPlaylists();
  };

  renderFormLayout = () => {
    return (
        <div className="d-flex form-group ">
          <div className="input-group input-group--fix-height mr-0-5" style={{width: 235}}>
                <span className="input-group-addon">
                      <i className="fa fa-calendar"/>
                </span>
            <DatePicker
                dateFormat={formatsConfig.dateFormat}
                readOnly
                todayButton="Today"
                selected={this.apiState.date}
                onChange={this.handleDateChange}
                className="form-control bg-white"
            />
          </div>
          <button onClick={() => this.handleNavigateDate(-1)} type="button"
                  className="btn btn-primary btn-square mr-0-5"><i
              className="fa fa-angle-left"/></button>
          <button onClick={() => this.handleNavigateDate(+1)} type="button" className="btn btn-primary btn-square"><i
              className="fa fa-angle-right"/></button>
          <button onClick={this.getallPlaylists} type="button" className="btn btn-primary btn-square ml-auto"><i
              className="fa fa-repeat"/></button>
          <div className="clearfix"/>
        </div>
    );
  };

  renderDatePlaylists() {
    const {currentDatePlaylists, date} = this.apiState;
    return (
        <div className="accordion" id="accordion-playlists">
          <div className="panel panel-default mb-0">
            <div className="panel-heading">
              <h4 className="panel-title">
                <a className="accordion-toggle accordion-toggle-styled collapsed in" data-toggle="collapse"
                   data-parent="#accordion3" href="#accordion-playlists_1" aria-expanded="true">
                  {currentDatePlaylists.length} {currentDatePlaylists.length > 1 ? 'playlists were' : 'playlist was'} scheduled
                  on {formatDateMessage(date)}
                </a>
              </h4>
            </div>
            <div id="accordion-playlists_1" className="panel-collapse collapse in" aria-expanded="true">
              {
                currentDatePlaylists.length ?
                    <div className="panel-body">
                      {
                        this.renderPlaylistItem(currentDatePlaylists)
                      }
                    </div> : null
              }
            </div>
          </div>
        </div>
    );
  }

  renderPlaylistItem(playlists) {
    const numberOfResources = e => e.resources ? e.resources.length : 0;
    const duration = e => formatDuration((new Date(e.end_time) - new Date(e.start_time)) / 1000);
    return (
        <div className="row">
          {
            playlists.map((e, idz) => (
                <div className="col-sm-4 col-md-3 col-lg-2" key={e.id}
                     title={e.name}
                     {...markActivePlaylist(e)}
                >
                  <div className="video-card playlist-video-card hand" onClick={() => this.openPlaylistPopup(e, idz)}>
                    <div className="video__thumbnail thumbnail-smooth-hover">
                      <i className={iconsReference.start}/>
                      <div className="minutes">{duration(e)}</div>
                    </div>
                    <div className="video__block">{e.name}
                      <div className="video__meta">
                        <span className="video__views-number">{plural(numberOfResources(e), 'resource')}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
            ))
          }
        </div>
    );
  }

  openPlaylistPopup(playlist, idz) {
    this.apiState.popupPlaylist = playlist;
    this.popup.open();
  }

  handlePlaylistUpdated = (data) => {
    const {currentDatePlaylists} = this.apiState;
    const idz = currentDatePlaylists.findIndex(e => e.id === data.id);
    if (currentDatePlaylists[idz]) {
      currentDatePlaylists[idz] = {
        ...currentDatePlaylists[idz],
        ...data
      };
      this.forceUpdate();
    }
  };

  componentDidUpdate() {
    utils.triggerWindowResized();
  }

  renderWeekOverView() {
    const {allPlaylists} = this.apiState;
    const weekMaps = this.weekMaps;
    const weekDaysWithData = weekMaps.map((e, idz) => {
      return {
        index: idz,
        date: e,
        playlists: allPlaylists.filter((p) => isPlaylistInDate(e, p))
      }
    });


    return (
        <div className="channel-week-overview portlet light mt-5">
          <div className="portlet-title">
            <div className="caption">
              <i className="fa fa-calendar"/>
              <span className="caption-subject font-primary sbold uppercase">Week overview</span>
            </div>
          </div>
          <div className="portlet-body">
            <WeekCalendar weekDaysWithData={weekDaysWithData} parent={this}/>
          </div>
        </div>
    );
  }

  render() {
    const {currentDatePlaylists, resources, selectedItem, loading, popupPlaylist} = this.apiState;
    const {startTime, endTime} = this.timeRange;
    const channel = this.channel;
    return (
        <div className="channel-overview-page mb-5">
          <PlaylistSlideOut ref={e => this.popup = e} onPlaylistUpdated={this.handlePlaylistUpdated}
                            playlist={popupPlaylist}/>
          <h1 className="page-title">{channel && this.channel.name} Overview</h1>
          <div className="channel-date-overview portlet light mt-3">
            <div className="portlet-title ">
              <div className="caption">
                <i className="fa fa-calendar-check-o"/>
                <span className="caption-subject font-primary sbold uppercase">Date overview</span>
              </div>
            </div>
            <div className="portlet-body">
              <section className="section-overview pb-2 mb-5">
                <div className="overview-timeline pt-2">
                  <div className="overview-form">
                    {this.renderFormLayout()}
                  </div>
                  <div className="clearfix"/>
                  <SnapshotLoading name="overview" loading={loading}>
                    {
                      props => (
                          <div>
                            <ResourceInfoBar data={selectedItem && selectedItem.data}/>
                            <TimelineDynamicViewOnly
                                playlists={currentDatePlaylists}
                                startTime={startTime}
                                endTime={endTime}
                                resources={resources}
                                onSelectedItem={item => this.apiState.selectedItem = item}
                                playlist={{
                                  start_time: startTime,
                                  end_time: endTime,
                                }}
                            />
                          </div>
                      )
                    }
                  </SnapshotLoading>
                </div>
                {this.renderDatePlaylists()}
              </section>
            </div>
          </div>
          {this.renderWeekOverView()}
        </div>
    );

  }
}


export default TimelineOverview;

const WeekCalendar = function ({weekDaysWithData, parent}) {
  let fromTo = null;
  if (weekDaysWithData && weekDaysWithData.length > 2) {
    fromTo = displayDateRange(weekDaysWithData[0].date, weekDaysWithData[weekDaysWithData.length - 1].date);
  }
  return (
      <div className="has-toolbar fc fc-ltr fc-unthemed channel-overview-calendar">
        <div className="fc-toolbar mb-2">
          {
            Array.isArray(weekDaysWithData) ?
                <div className="fc-left"><h3 className="my-0">{fromTo}</h3></div> : null

          }
          <div className="fc-right">
            <div className="fc-button-group">
              <button onClick={() => parent.handleNavigateDate(-1)} type="button"
                      className="btn btn-primary btn-square mr-0-5"><i
                  className="fa fa-angle-left"/></button>
              <button onClick={() => parent.handleNavigateDate(+1)} type="button" className="btn btn-primary btn-square"><i
                  className="fa fa-angle-right"/></button>
            </div>
          </div>
          <div className="fc-center"/>
          <div className="fc-clear"/>
        </div>
        <div className="fc-view-container" style={{}}>
          <div className="fc-view fc-agendaWeek-view fc-agenda-view">
            <table>
              <thead className="fc-head">
              <tr>
                <td className="fc-widget-header">
                  <div className="fc-row fc-widget-header" style={{borderRightWidth: 1}}>
                    <table>
                      <thead>
                      <tr>
                        {
                          weekDaysWithData.map(e => (
                              <th key={e.date} className="fc-day-header fc-widget-header fc-sun">
                                {
                                  displayWeekday(e.date)
                                }
                              </th>
                          ))
                        }
                      </tr>
                      </thead>
                    </table>
                  </div>
                </td>
              </tr>
              </thead>
              <tbody className="fc-body">
              <tr>
                <td className="fc-widget-content">
                  <div className="fc-day-grid">
                    <div className="fc-row fc-week fc-widget-content" style={{borderRightWidth: 1}}>
                      <div className="fc-bg position-relative">
                        <table>
                          <tbody>
                          <tr className="day-track">
                            {
                              weekDaysWithData.map(e => (
                                  <td key={e.date}
                                      className={`fc-day fc-widget-content ${isToday(e.date) ? 'fc-today' : ''}`}>

                                    {e.playlists.length ? e.playlists.map(p => (
                                        <div key={p.id} className="playlist-group">
                                          <Link to={linkGenerator.playlists.scheduling(p)} className="playlist-card__info">
                                            <i className={iconsReference.start}/>
                                            <span className="playlist-name">{p.name}</span>
                                          </Link>
                                          <div className="playlist-card__body">
                                            {
                                              p.resources && p.resources.map(r => {
                                                const data = parseResourceData(r);
                                                return (
                                                    <p key={r.id} className={`playlist-card__resource ${data.type}`} {...resourceToolTipsProps(r)}>
                                                      <span className="time">{data.timeStart.format('HH:mm')} - {data.timeEnd.format('HH:mm')}</span>
                                                      <span className="resource-name">- {r.title || r.filename || 'untitled'}</span>
                                                    </p>
                                                )
                                              })
                                            }
                                          </div>
                                        </div>
                                    )) : null
                                    }
                                  </td>
                              ))
                            }
                          </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
  );
};
const playlistGroupDemo = (
    <div className="playlist-group">
      <Link to="#" className="playlist-card__info">
        <i className={iconsReference.start}/>
        <span className="playlist-name">Hot sexy lonely</span>
      </Link>
      <div className="playlist-card__body">
        <p className="playlist-card__resource" {...resourceToolTipsProps()}>
          <span className="time">9:00 - 10:00</span>
          <span className="resource-name">- Alice come to the giant world</span>
        </p>
        <p className="playlist-card__resource">
          <span className="time">9:00 - 08:00</span>
          <span className="resource-name">- Blah blah blah blah</span>
        </p>
      </div>
    </div>
);

function resourceToolTipsProps (resource) {
  if (resource) {
    const type = lodash.find(resourceTypes, type => type.track === resource.item_track);
    return {
      'data-html': true,
      'data-container': 'body',
      'data-trigger': 'hover',
      'data-delay': JSON.stringify({show: 0, hide: 0}),
      'data-placement': 'auto',
      'data-toggle': 'tooltip',
      'data-original-title': renderToStaticMarkup((<div className="resource-tooltip">
      <span>
        <i className={iconsReference.videoFile}/>
          {type.name}
      </span>
        <span>
        <i className={iconsReference.playDuration}/>
          {formatDuration(resource.duration)}
      </span>
        <span>
        <i className={iconsReference.start}/>
          {resource.start_time}
      </span>
        <span>
        <i className={iconsReference.stop}/>
          {resource.end_time}
      </span>
      </div>))
    }
  }
  return null;
};



