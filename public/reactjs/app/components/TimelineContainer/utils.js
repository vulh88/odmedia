import moment from 'moment';
import {formatDuration} from '../../helpers/utils';

function tryClass(condition, className) {
  return condition ? className : '';
}

// Copy time date2 to date 1
function copyTime(date1, date2) {
  date2 = moment.isMoment(date2) ? date2 : moment(date2);
  const clonned = date1.clone();
  ['hour', 'minute', 'second', 'millisecond']
      .map(e => {
        clonned[e](date2[e]());
      });
  return clonned;
}

export function makeSameDate(date1, date2) {
  const today = moment();
  return [copyTime(today, date1), copyTime(today, date2)];
}

function resetTime(date) {
  ['hour', 'minute', 'second', 'millisecond']
      .map(e => {
        date[e](0);
      });
  return date;
}

function minutesOfDay(m){
  return m.minutes() + m.hours() * 60;
}

function isVisible(item, weekGroups) {
  return weekGroups.find(e => e.id === item.groupId);
}

function isToday(date) {
  return moment().format('YYYY-MM-DD') === (date.format('YYYY-MM-DD'));
}

export function displayDateRange(date1, date2) {
  const year  = date1.isSame(date2, 'year') ? '' : ' YYYY';
  const month = date1.isSame(date2, 'year') && date1.isSame(date2, 'month') ? '' : 'MMM ';
  return `${date1.format(`MMM DD${year}`)} — ${date2.format(`${month}DD, YYYY`)}`
}

export function displayWeekday(date) {
  return date.format('ddd, DD/MM');
}
/**
 *
 * @param duration in seconds
 */
function showDuration(duration) {
  return formatDuration(duration);
}

export {
  isToday,
  showDuration,
  minutesOfDay,
  tryClass,
  resetTime,
  isVisible,
  copyTime
}

/*
ui utils
 */

export function markActivePlaylist(playlist) {
  const getPlaylist = () => $(`.rct-range.playlist-${playlist.id}`);
  const activeClass = 'playlist-active-range--highlight';

  return {
    onMouseEnter: () => {
      const $resource = getPlaylist();
      $resource.addClass(activeClass);
    },
    onMouseLeave: () => {
      const $resource = getPlaylist();
      $resource.removeClass(activeClass);
    }
  }
}

export function markActiveResources(scheduledId) {
  const activeClass = 'marked-active';
  return {
    onMouseEnter: () => {
      const $resource = $(`.rct-item.scheduled-id-${scheduledId}`);
      $resource.addClass(activeClass);
    },
    onMouseLeave: () => {
      const $resource = $(`.rct-item.scheduled-id-${scheduledId}`);
      $resource.removeClass(activeClass);
    }
  }

}