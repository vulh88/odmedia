import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {formatsConfig, resourceTypes} from './config';
import FileList from './components/FileList';
import {makeSelectChannel} from 'store/app/selectors';
import {createStructuredSelector} from 'reselect';
import playlistsApiService from '../../services/api/playlists';
import TimelineWeekView from './TimelineViews/TimelineWeekView';
import HaveApiComponent from '../BaseComponents/HaveApiComponent';
import injectMixins from '../../helpers/react/inject-mixins';
import TimelineDynamicView from './TimelineViews/TimelineDynamicView';
import {parseStoredResources} from './TimelineViews/parser';
import AlertBlock from '../AlertBlock';
import toast from '../../helpers/user-interface/toast';
import {ResourceInfoBar} from './components/ResourceInfoBar';
import lodash from 'lodash';
import ButtonLoading from '../ButtonLoading';
import quickAlert from '../../helpers/user-interface/quick-alert';
import Popup from '../Popup';
import PlaylistForm from '../../containers/PlaylistsPage/Form';
import {disableInteract, enableInteract, makeSameDate} from './utils';
import routePaths from '../../settings/route-paths';
import {withRouter} from 'react-router';
import locationHelper from '../../helpers/location';
import iconsReference from '../../common/icons';
import {specialBlocksDictionary} from './components/SpecialBlocks';
import {convertToArray} from '../../helpers/utils';
import moment from 'moment';
import {utils} from 'helpers/utils';
import {Link} from 'react-router-dom';
import linkGenerator from '../../common/link-generator';

const viewTypes = {
  dynamic: {
    component: TimelineDynamicView,
    name: 'Dynamic',
    icon: <i className="fa fa-calendar-minus-o"/>
  },
  week: {
    component: TimelineWeekView,
    name: 'Weekview',
    icon: <i className="fa fa-calendar"/>
  }
};

@withRouter
class TimelineContainer extends HaveApiComponent {

  uiState = injectMixins.autoRenderOnChange.call(this, {
    viewType: viewTypes.dynamic,
    isOverlapping: false,
    selectedItem: null,
    errors: null
  });

  resources = parseStoredResources(this.props.resources, this.props.playlist);

  specialBlocks = {...specialBlocksDictionary};

  static propTypes = {
    resources: PropTypes.arrayOf(PropTypes.object).isRequired,
    playlist: PropTypes.object.isRequired
  };


  scheduleResource = async (resource) => {
    this.validateResources();
    if (!this.uiState.errors) {
      this.apiState.isSaving = true;
      const resourceType = resourceTypes[resource.data.type];
      const params = {
        playlist_id: this.props.playlist.id,
        resource_id: resource.data.id,
        item_track: resourceType.track,
        start_time: resource.data.timeStart.format(formatsConfig.apiDateTime),
        end_time: resource.data.timeEnd.format(formatsConfig.apiDateTime),
      };
      // If this item was stored on server with an id
      if (resource.data.scheduledId) {
        params.scheduledId = resource.data.scheduledId;
      }
      // or else, it's a newly item
      else {
        resource.data.isAddingNew = true;
      }

      /**
       * Set data when submit by event block
       */
      if (resource.data.hasOwnProperty("event_block_id") && resource.data.hasOwnProperty("event_block_data")) {
        params.event_block_id = resource.data.event_block_id;
        params.event_block_data = resource.data.event_block_data;
      }

      try {
        const data = await playlistsApiService.scheduleResource(params);
        if (!params.scheduledId) {
          resource.data.scheduledId = data.id;
          resource.data.isAddingNew = false;
        }
      } catch (err) {
        console.error(err);
      } finally {
        this.apiState.isSaving = false;
      }
    }
  };

  handleDeleteResource = (resource) => {
    if (this.uiState.selectedItem && this.uiState.selectedItem.id === resource.id) {
      this.uiState.selectedItem = null;
    }
    if (resource.data.scheduledId) {
      this.callApi(playlistsApiService.deleteResource({id: resource.data.scheduledId}), () => {
        toast.success('Deleted resource from timeline');
      });
    }
    setTimeout(this.validateResources);
  };

  handlePublishPlaylist = async () => {
    if (await quickAlert.confirm('Do you want to publish this playlist?')) {
      this.uiState.isPublishing = true;
      await this.callApi(
          playlistsApiService.publish(this.props.playlist.id),
          () => {
            quickAlert.success('All\'s done!', 'Playlist was successfully published!');
          },
          ({errors}) => {
            errors.map(e => {
              toast.error(e, 'Error message', {timeOut: 10000});
            });
          },
          {autoShowError: false}
      );
      this.uiState.isPublishing = false;
    }
  };

  /**
   * when item was selected from timeline, it will be post into this function
   * @param item can be null when unselected
   */
  handleSelectedItem = (item) => {
    this.uiState.selectedItem = item;
  };

  isOverlapping(a, b) {
    // Don't check the same item.
    // Overlay can be overlapped
    if (a.data === b.data || [a.data.type, b.data.type].includes(resourceTypes.overlay.id)) {
      return false;
    }

    return (
            (a.data.timeStart <= b.data.timeStart && a.data.timeEnd >= b.data.timeEnd)
            ||
            (a.data.timeStart <= b.data.timeEnd && a.data.timeStart >= b.data.timeStart)
            ||
            (a.data.timeEnd >= b.data.timeStart && a.data.timeStart <= b.data.timeStart)
        )
  }

  validateResources = () => {
    const errors = [];
    const {resources} = this;
    const overlapMessage    = 'Some resources are overlapping scheduled.';
    // reset error state
    this.uiState.errors = null;
    resources.map(({data}) => data.error = false);

    // Check overlapping
    for (let i = 0; i < resources.length - 1; i++) {
      for (let j = i +1; j < resources.length; j++) {
        if (this.isOverlapping(resources[i], resources[j])) {
          errors.push(overlapMessage);
          // marked as error
          resources[i].data.error = true;
          resources[j].data.error = true;
          break;
        }
      }
    }

    //Check resource time setting
    for (let i = 0; i < resources.length; i++) {
      const resource = resources[i];
      const {data} = resource;
      let isError = false;
      if (data.time_not_before) {
        const [notBefore, startTime] = makeSameDate(moment(data.time_not_before, formatsConfig.timeFormat), data.timeStart);
        if (startTime  <  notBefore) {
          isError = true;
        }
      }
      if (data.time_not_after) {
        const [notAfter, startTime] = makeSameDate(moment(data.time_not_after, formatsConfig.timeFormat), data.timeStart);
        if (startTime  >  notAfter) {
          isError = true;
        }
      }
      if (isError) {
        // marked as error
        resource.data.error = true;
        errors.push(this.getStartTimeErrorMessage(resource));
      }
    }

    // Save error if haved
    if (errors.length) {
      this.uiState.errors = errors;
    }
  };

  getStartTimeErrorMessage(resource) {
    const {data} = resource;
    const shortTitle = <Link to={linkGenerator.resources.single({resource_id: data.id})} target="_blank"><strong>{utils.cutStringByChar(data.title, 80)}</strong></Link>;

    if(data.time_not_after && data.time_not_before) {
      return <span>"{shortTitle}" must start between {data.time_not_before} and {data.time_not_after}</span>;
    }
    else if (data.time_not_after) {
      return <span>"{shortTitle}" must start before {data.time_not_after}</span>;
    }
    else if (data.time_not_before) {
      return <span>"{shortTitle}" must start after {data.time_not_before}</span>;
    }
  }

  switchViewType = (type) => {
    if (this.uiState.viewType !== type) {
      this.uiState.viewType = type;
      this.uiState.selectedItem = null
    }
  };

  handleDuplicate = async () => {
    const {id} = this.props.playlist;
    const {history} = this.props;

    if (await quickAlert.confirm(`Would you like to duplicate this playlist?`)) {
      this.callApi(
        playlistsApiService.duplicatePlaylist({
          id: id
        }),
        async ({data}) => {
          if (await quickAlert.confirm(`All's done!`, `Would you like navigating to the cloned playlist?`, {
            type: 'success',
            yes: `Yes, let's go!`,
            no: `No, thanks!`
          })) {
            history.push(locationHelper.getLink(routePaths.playlists.single,{id: data.id}));
          }
        }
      );
    }
  };

  handleSpecialBlockDrag = (event, id) => {
    const data = {specialBlockId: id};
    event.dataTransfer.setData('data', JSON.stringify(data));
  };

  findSpecialBlockById = (id) => {
    return convertToArray(this.specialBlocks).find(e => id === e.id)
  };

  /**
   * When drop a special block into the timeline.
   * Open a popup and let user fill-in setting then return a new data based on user setting
   * this function return a promise,
   * prefer using async, await
   */
  handleDropSpecialBlock = (data, timeStart) => {
    const specialBlock = this.findSpecialBlockById(data.specialBlockId);
    if (specialBlock && specialBlock.ref) {
      return new Promise((resolver) => {
        specialBlock.ref.open(resolver);
      });
    }
    return null;
  };

  renderSpecialBlocks() {
    const {specialBlocks} = this;
    const specialBlockArray = convertToArray(specialBlocks);
    return (
        <div className="mb-1">
          <div className="special-block-buttons btn-group">
            {
              specialBlockArray.map(e => <a key={e.id} draggable onDragStart={(event) => this.handleSpecialBlockDrag(event, e.id)} title={e.summary} className="btn default btn-circle mr-1">
                <i className={e.id === 'eventBlock' ? 'fa fa-calendar' : 'fa fa-product-hunt'}/>
              </a>)
            }
          </div>
          <div className="special-block-popups-container">
            {specialBlockArray.map(e => <e.popup key={e.id} playlist={this.props.playlist} ref={r => e.ref = r}/>)}
          </div>
        </div>
    );
  }

  updatedCallback = (data) => {
    this.popup.close();
    if (data.status === "pending" && data.change_state_timeline === 1) {
      this.props.onPlaylistChange();
    }
  };

  render() {
    const {currentChannel, playlist} = this.props;
    const {isOverlapping, viewType, errors} = this.uiState;
    const {isSaving, isPublishing} = this.apiState;
    return (
        <div>
          {this.renderPlaylistPopup()}
          <div className="row">
            <div className="col-sm-6">
              <FileList
                  resourceType={resourceTypes.content}
                  requestParams={{only_content_resources: 1, channel_id: currentChannel.id}}
                  title="Content files"/>
            </div>
            <div className="col-sm-6">
              <FileList
                  resourceType={resourceTypes.overlay}
                  requestParams={{only_overlay_resources: 1, channel_id: currentChannel.id}}
                  title="Overlay files"/>
            </div>
          </div>
          <div className="clearfix"/>
          <AlertBlock messages={errors}
                      style={AlertBlock.styles.error}/>
          <div className="form-group mb-1 mt-1">
            <div className="btn-group view-type-buttons pull-left">
              {
                lodash.map(viewTypes, (e, key) =>
                    <label key={key} className={`btn btn-default ${viewType === e ? 'active' : ''}`}
                           onClick={() => this.switchViewType(e)}>{e.icon} {e.name}</label>)
              }
            </div>
            <div className="playlist-time pull-left">
              <span>
                <i className={iconsReference.start}/>
                {playlist.start_time}
              </span>
              <span>
                <i className={iconsReference.stop}/>
                {playlist.end_time}
              </span>
            </div>
            <div className="timeline-button-group pull-right">
              {this.renderSpecialBlocks()}
              <button className="btn default btn-circle no-event mr-1" disabled>
                <i className={`fa ${isSaving ? 'fa-circle-o-notch fa-spin' : 'fa-save'}`}/>
              </button>
              <button
                  title="Duplicate playlist"
                  className="btn default btn-circle mr-1"
                  onClick={() => this.handleDuplicate()}>
                <i className="fa fa-clone"/>
              </button>
              <button
                  title="Edit playlist"
                  className="btn btn-default btn-circle mr-1"
                  onClick={() => this.popup.open()}
              >
                <i className="fa fa-pencil"/>
              </button>
              <ButtonLoading
                  onClick={this.handlePublishPlaylist}
                  loading={isPublishing}
                  disabled={isOverlapping}
                  className="btn green btn-circle"
                  title="Publish playlist">
                <i className="fa fa-check-square-o"/>
              </ButtonLoading>
            </div>
            <div className="clearfix"/>
          </div>
          <ResourceInfoBar data={this.uiState.selectedItem && this.uiState.selectedItem.data}
                           actions={this.uiState.selectedItem && this.uiState.selectedItem.actions}/>
          <viewType.component
              playlist={playlist}
              resources={this.resources}
              onDropSpecialBlock={this.handleDropSpecialBlock}
              onSelectedItem={this.handleSelectedItem}
              onDeleteResource={this.handleDeleteResource}
              scheduleResource={this.scheduleResource}/>
        </div>
    );
  }


  renderPlaylistPopup = () => {
    return (
        <Popup
            title="Quick edit playlist"
            showFooter={false}
            ref={e => this.popup = e}
            render={() => <PlaylistForm updatedCallback={this.updatedCallback}/>}
        >
        </Popup>
    );
  }
}

export default connect(
    createStructuredSelector({
      currentChannel: makeSelectChannel()
    }),
    null
)(TimelineContainer);

