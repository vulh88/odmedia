import React from 'react';

const formatsConfig = {
  monthFormat: 'MMMM - YYYY',
  dateFormat: 'YYYY-MM-DD',
  timeFormat: 'HH:mm:ss',
  apiDateTime: 'YYYY-MM-DD HH:mm:ss',
  get datetimeFormat() {
    return `${this.dateFormat} ${this.timeFormat}`;
  }
};


/**
 * References to resource types, deal with backend
 */
const resourceTypes = {
  overlay: {
    id: 'overlay',
    name: 'Overlay',
    track: 1,
    icon: <img src={require('./assets/overlay-icon.png')} alt="overlay-icon" className="resource-icon"/>,
  },
  content: {
    id: 'content',
    name: 'Content',
    track: 2,
    icon: <i className="fa fa-video-camera resource-icon font-red"/>,
  }
};

export const getResourcesType = (e) => {
  if (e.item_track === resourceTypes.overlay.track) {
    return resourceTypes.overlay;
  }
  else {
    return resourceTypes.content;
  }
};

// Keep it as default,
const keys = {
  groupIdKey: 'id',
  groupTitleKey: 'type',
  groupRightTitleKey: 'weekday',
  itemIdKey: 'id',
  itemTitleKey: 'data',
  itemDivTitleKey: 'title',
  itemGroupKey: 'groupId',
  itemTimeStartKey: 'timeStart',
  itemTimeEndKey: 'timeEnd',
  rangeIdKey: 'id',
  rangeTimeStartKey: 'timeStart',
  rangeTimeEndKey: 'timeEnd'
};

const timelineConfig = {
  keys,
  fullUpdate: false,
  itemsSorted: true,
  stackItems: true,
  itemTouchSendsClick: false,
  sidebarWidth: 130,
  headerLabelHeight: 50,
  itemHeightRatio: 0.8,
  lineHeight: 60,
  minResizeWidth: 5, // Resizable pixel, or having to zoom to resize
  timeSteps: {minute: 15, hour: 1},
  canResize: 'right',
  dragSnap: 1000,
  headerLabelFormats: {
    yearShort: 'YYYY',
    yearLong: 'YYYY',
    monthShort: formatsConfig.monthFormat,
    monthMedium: formatsConfig.monthFormat,
    monthMediumLong: formatsConfig.monthFormat,
    monthLong: formatsConfig.monthFormat,
    dayShort: '[Time]',
    dayLong: '[Time]',
    hourShort: formatsConfig.timeFormat,
    hourMedium: formatsConfig.timeFormat,
    hourMediumLong: formatsConfig.timeFormat,
    hourLong: formatsConfig.timeFormat,
    time: formatsConfig.timeFormat
  }
};

export {
  timelineConfig,
  formatsConfig,
  resourceTypes
};
