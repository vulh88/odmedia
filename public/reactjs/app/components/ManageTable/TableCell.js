import React from 'react';
import PropTypes from 'prop-types';
import { Cell, CellHeader } from './styled-components';

const TableCellView = ({
  children,
  className,
  overflowNone,
  headerResponsive,
  style
}) => {
  const classNamesCell = `
  cell td
  ${overflowNone ? 'overflow-none' : ''}
  ${className || ''}`;

  return (
    <Cell className={classNamesCell} style={style}>
      <CellHeader>{headerResponsive}:</CellHeader>
      <div>{React.Children.toArray(children)}</div>
    </Cell>
  );
};

TableCellView.propTypes = {
  children: PropTypes.node,
  headerResponsive: PropTypes.string.isRequired,
  className: PropTypes.string,
  overflowNone: PropTypes.bool
};

export default TableCellView;
