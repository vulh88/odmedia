import React from 'react';
import PropTypes from 'prop-types';
import { isFunction } from 'lodash';
import { TableHeaderCell } from './styled-components';

class TableHeaderView extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    onClick: PropTypes.func,
    field: PropTypes.string,
    sortingHeader: PropTypes.bool,
    sort: PropTypes.bool,
    style: PropTypes.object
  };

  state = {
    sorting: false
  };

  onSortHeader = () => {
    this.setState({
      sorting: !this.state.sorting
    });
    if (isFunction(this.props.onClick)) {
      this.props.onClick();
    }
  };

  render() {
    const { children, className, sortingHeader, sort, style } = this.props;
    const { sorting } = this.state;
    let classSort = sort ? 'sorting' : '';
    if (sortingHeader) {
      classSort = sorting ? 'sorting_asc' : 'sorting_desc';
    }

    const classNamesHeader = `
    cell th
    ${className || ''}
    ${classSort}`;

    return (
      <TableHeaderCell
        className={classNamesHeader}
        onClick={this.onSortHeader}
        style={style}
      >
        <strong>{React.Children.toArray(children)}</strong>
      </TableHeaderCell>
    );
  }
}

export default TableHeaderView;
