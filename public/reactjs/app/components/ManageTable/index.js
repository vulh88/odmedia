import React from 'react';
import PropTypes from 'prop-types';
import { isFunction, isNumber } from 'lodash';

import {
  Portlet,
  PortletBody,
  WrapperActions,
  NoResult,
  Table
} from './styled-components';
import TableTools from './TableTools';
import TableHeader from './TableHeader';
import TableRow from './TableRow';
import TableCell from './TableCell';
import TableFooter from './TableFooter';
import locationHelper from '../../helpers/location';
import BlockLoading from '../BlockLoading';
import quickAlert from '../../helpers/user-interface/quick-alert';
import FadeIn from '../EffectComponents/FadeIn';
import {debounce} from '../../helpers/javascript/debounce';

class ManageTable extends React.PureComponent {
  static propTypes = {
    data: PropTypes.array.isRequired,
    columns: PropTypes.array.isRequired,
    nameTable: PropTypes.string,
    tools: PropTypes.bool,
    loading: PropTypes.bool,
    showActions: PropTypes.bool,
    paging: PropTypes.any,
    handleChangePage: PropTypes.func,
    handleLimitChange: PropTypes.func,
    handleKeywordChange: PropTypes.func,
    handleSortBy: PropTypes.func,
    actions: PropTypes.arrayOf(
      PropTypes.shape({
        icon: PropTypes.string,
        needConfirm: PropTypes.bool,
        handler: PropTypes.func,
        isShow: PropTypes.oneOfType([PropTypes.bool, PropTypes.func])
      })
    ),
    activeRowId: PropTypes.number
  };

  static defaultProps = {
    nameTable: ' ',
    tools: false,
    paging: {},
    showActions: true,
    styleActions: { width: 140, flexGrow: 0 },
    activeRowId: 0
  };

  query = {};

  state = {
    pagination: true,
    currentPage: this.props.paging.currentPage - 1,
    filteredData: this.props.data,
    totalRow: this.props.paging.totalPage * this.props.paging.limitPage,
    searching: false,
    sorting: false,
    sortingHeader: null,
    idSelected: null
  };

  componentWillReceiveProps(nextProps) {
    this.query = locationHelper.getUrlParams();
    this.setState({
      filteredData: nextProps.data,
      currentPage: nextProps.paging.currentPage - 1,
      totalRow: nextProps.data.length
    });
  }

  onChangeEntry = e => {
    if (isFunction(this.props.handleLimitChange)) {
      this.props.handleLimitChange(parseInt(e.target.value));
    }
  };

  onChangePage = page => {
    this.setState({ currentPage: page });
    this.props.handleChangePage(page + 1);
  };

  onChangeSearch = debounce(e => {
    const value = e.target.value.toLowerCase();
    //Todo: Implement search
    if (this.props.handleKeywordChange) {
      this.props.handleKeywordChange(value);
    }
  }, 100);

  onSortHeader = (columnId) => {
    const fieldName = this.props.columns[columnId].sort;
    if (typeof fieldName === 'string' &&  this.props.handleSortBy) {
      this.props.handleSortBy(fieldName);
    }
    else {
      this.doClientSort(columnId);
    }
    this.setState({
      sortingHeader: columnId
    })
  };

  onCompareBy(columnId) {
    const { sorting } = this.state;
    const column = this.props.columns[columnId];
    const invert = sorting ? 1 : -1;
    return (a, b) => {
      const va = this.getCellValue(column, a);
      const vb = this.getCellValue(column, b);
      if (va < vb) return 1 * invert;
      if (va > vb) return -1 * invert;
      return 0;
    };
  }

  doClientSort = columnId => {
    if (isNumber(columnId)) {
      const { filteredData, sorting } = this.state;
      const arrayCopy = [...filteredData];
      this.setState({
        sorting: !sorting,
        filteredData: arrayCopy.sort(this.onCompareBy(columnId))
      });
    }
  };

  getCellValue(col, row, idz) {
    if (isFunction(col.value)) {
      return col.value(row);
    }
    return col.value;
  }

  handleActionClick = async (row, action) => {
    if (
      !action.needConfirm ||
      (action.needConfirm &&
        await quickAlert.confirm(
          typeof action.needConfirm === 'string'
            ? action.needConfirm
            : 'Do you want to continue this action?'
        ))
    ) {
      if (isFunction(action.handler)) {
        action.handler(row);
      }
    }
  };

  renderActionsOfRow = row => {
    const { actions } = this.props;
    if (actions) {
      return (
        <WrapperActions>
          {actions
            .filter(e => {
              if (e.isShow === undefined) {
                return true;
              }
              if (isFunction(e.isShow)) {
                return e.isShow(row);
              }
              return e.isShow;
            })
            .map((e, idz) => {
              return (
                <li key={idz}>
                  <a
                      className={`row__action-button btn default ${
                      e.backgroundColor || ''
                          }`}
                      {...e.props}
                      onClick={() => this.handleActionClick(row, e)}
                  >
                    <i className={`${e.icon}`} />
                  </a>
                </li>
              );
            })}
        </WrapperActions>
      );
    }
    return null;
  };

  renderTableHeader = () => {
    const { showActions, styleActions } = this.props;

    // Push additional Actions columns
    let headerCols = this.props.columns;
    if (showActions) {
      headerCols = this.props.columns.concat([
        {
          name: 'Actions',
          sort: false,
          style: styleActions
        }
      ]);
    }

    return (
      <TableRow>
        {headerCols.map((col, idz) => {
          return (
              <TableHeader
                  key={col.name}
                  {...(col.sort
                      ? {
                        sort: true,
                        onClick: () => this.onSortHeader(idz)
                      }
                      : null)}
                  sortingHeader={col.sort && idz === this.state.sortingHeader}
                  style={col.style}
              >
                {col.name}
              </TableHeader>
          )
        })}
      </TableRow>
    );
  };

  renderTableCell = row => {
    const { columns, showActions, styleActions } = this.props;
    const cells = columns.map((col, idz) => {
      return (
        <TableCell key={idz} headerResponsive={col.name} style={col.style}>
          {this.getCellValue(col, row)}
        </TableCell>
      );
    });
    if (showActions) {
      cells.push(
        <TableCell
          key="row__action-cell"
          headerResponsive="action-cell"
          style={styleActions}
        >
          {this.renderActionsOfRow(row)}
        </TableCell>
      );
    }

    return cells;
  };

  renderTableRow = () => {
    const { filteredData } = this.state;
    const { activeRowId } = this.props;

    if (filteredData.length) {
      return filteredData.map((item, idz) => item._deleted ? null : (
          <TableRow key={idz} className={item.id === activeRowId ? "active-row" : ""}>{this.renderTableCell(item)}</TableRow>
      ));
    }
    return (
      <NoResult>
        <span>No Result</span>
      </NoResult>
    );
  };

  render() {
    const { columns, tools, paging, loading } = this.props;
    const { currentPage, searching } = this.state;
    return (
      <Portlet>
        <PortletBody>
          {tools && (
            <TableTools
              onChangeEntry={this.onChangeEntry}
              limit={paging.limit}
              onChangeSearch={({target}) => this.onChangeSearch({target})}
            />
          )}

          <Table cols={columns.length + 1}>
            {this.renderTableHeader()}
            {this.renderTableRow()}
            <FadeIn show={loading}>
              <div className="wrapper-loading">
                <BlockLoading />
              </div>
            </FadeIn>
          </Table>
          {paging && paging.totalData ? (
            <TableFooter
              limit={paging.limit}
              totalData={paging.totalData}
              totalPage={paging.totalPage}
              currentPage={currentPage}
              searching={searching}
              onChangePage={this.onChangePage}
            />
          ) : null}
        </PortletBody>
      </Portlet>
    );
  }
}

export default ManageTable;
