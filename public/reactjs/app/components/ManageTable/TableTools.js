import React from 'react';
import PropTypes from 'prop-types';

class TableHeaderView extends React.PureComponent {
  static propTypes = {
    onChangeEntry: PropTypes.func,
    limit: PropTypes.number,
    onChangeSearch: PropTypes.func
  };

  render() {
    const {limit, onChangeEntry} = this.props;
    const options = [10,20,30,50,100];
    if(!options.includes(limit) && limit !== -1) {
      options.push(limit);
    }
    return (
      <div className="row">
        <div className="col-sm-12 col-md-6">
          <label>
            <select
                className="form-control"
                value={limit}
                onChange={onChangeEntry}
            >
              {
                options.map((e, idz) => <option key={idz} value={e}>{e}</option>)
              }
              <option value="-1">All</option>
            </select>
            {limit !== -1 && 'Per page'}
          </label>
        </div>
        <div className="col-sm-12 col-md-6 text-right">
          <label>
            <span>Search&nbsp;</span>
            <input
              type="search"
              className="form-control"
              placeholder="Keyword"
              onChange={this.props.onChangeSearch}
            />
          </label>
        </div>
      </div>
    );
  }
}

export default TableHeaderView;
