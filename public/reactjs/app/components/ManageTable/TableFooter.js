import React from 'react';
import ReactPaginate from 'react-paginate';
import PropTypes from 'prop-types';
import {TableFooter} from './styled-components';


class TableFooterView extends React.PureComponent {
  static propTypes = {
    limit: PropTypes.number,
    totalData: PropTypes.number,
    totalPage: PropTypes.number,
    currentPage: PropTypes.number,
    onChangePage: PropTypes.func,
    searching: PropTypes.bool
  };

  handlePageClick = ({selected}) => {
    const {onChangePage} = this.props;
    onChangePage(selected);
  };

  render() {
    const {
      totalPage,
      limit,
      currentPage,
      totalData
    } = this.props;
    const itemStart = Math.min(currentPage * limit + 1, totalData);
    const itemEnd = Math.min(itemStart + limit - 1, totalData);

    const info = `Showing ${itemStart} to ${itemEnd} of ${totalData} entries`;

    return (
        <TableFooter className="row">
          <div className="col-md-5 col-sm-12">
            {totalData && <div className="info">{info}</div>}
          </div>

          <div className="col-md-7 col-sm-12 text-right">
            <div>
              <ReactPaginate
                  pageCount={totalPage}
                  previousLabel={<i className="fa fa-angle-left"/>}
                  nextLabel={<i className="fa fa-angle-right"/>}
                  breakLabel={<a>...</a>}
                  breakClassName="break-me"
                  containerClassName="pagination"
                  activeClassName="active"
                  marginPagesDisplayed={1}
                  pageRangeDisplayed={2}
                  forcePage={Math.max(currentPage, 0)}
                  onPageChange={this.handlePageClick}
              />
            </div>
          </div>
        </TableFooter>
    );
  }
}

export default TableFooterView;
