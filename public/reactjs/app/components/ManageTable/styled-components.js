import styled from 'styled-components';

import sortBoth from '../../images/datatables/sort_both.png';
import sortAsc from '../../images/datatables/sort_asc.png';
import sortDesc from '../../images/datatables/sort_desc.png';
import sortAscDisabled from '../../images/datatables/sort_asc_disabled.png';
import sortDescDisabled from '../../images/datatables/sort_desc_disabled.png';

export const Portlet = styled.div`
  position: relative;
  padding: 0;
  background-color: #fff;
  // border: 1px solid #e7ecf1;

  .red {
    color: #e7505a;
  }

  .white {
    color: #fff;
  }

  .green {
    color: #27ae60;
  }

  .wrapper-loading {
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    background: rgba(255, 255, 255, 0.5);
    z-index: 3;
    display: flex;
    align-items: center;
  }
`;

export const PortletBody = styled.div`
  padding-top: 8px;

  .text-right {
    text-alight: right;
  }

  input.form-control {
    display: inline-block;
    width: 145px;
    font-weight: normal;
    white-space: nowrap;
    margin-left: 5px;
    margin-bottom: 5px;
  }

  select.form-control {
    display: inline-block;
    width: 80px;
    font-weight: normal;
    white-space: nowrap;
    margin-right: 5px;
    margin-bottom: 5px;
  }
`;

export const Table = styled.div`
  border-bottom: 2px solid #e7ecf1;
  position: relative;
  
  .cell {
    ${props => `width: ${100 / props.cols}%`}};
  }

  @media (max-width: 480px) {
    .cell {
      width: 100%;
    }
  }
`;

export const NoResult = styled.div`
  display: flex;
  border: 1px solid #e7ecf1;
  border-bottom: 0;
  border-right: 2px solid #e7ecf1;
  justify-content: center;
`;

export const WrapperActions = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;

  li {
    display: inline-block;
  }

  a {
    padding: 0 5px;
  }

  .row__action-button {
    margin: 0 5px;
  }
`;

export const Cell = styled.div`
  display: flex;
  align-items: center;
  box-sizing: border-box;
  flex-grow: 1;
  width: 100%;
  padding: 10px 18px;
  border-right: 1px solid #e7ecf1;

  > div {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
  }

  &.td {
    padding: 4px 10px;
  }

  &.overflow-none {
    overflow: initial;
  }

  > strong {
    font-size: 14px;
    font-weight: 600;
    margin: 0;
  }

  @media (max-width: 480px) {
    &.td {
      display: inline-flex;
      padding: 0;
      border-bottom: 1px solid #e7ecf1;
    }

    > div {
      padding: 8px 10px;
    }

    > div:last-child {
      flex: 2;
    }
  }
`;

export const CellHeader = styled.div`
  display: none;

  @media (max-width: 480px) {
    display: inline-flex;
    flex: 1;
    border-right: 1px solid #e7ecf1;
    font-weight: 600;
  }
`;

export const TableFooter = styled.div`
  .info {
    padding-top: 8px;
    white-space: nowrap;
  }

  .pagination > li > a {
    user-select: none;
  }
`;

export const TableHeaderCell = styled.div`
  box-sizing: border-box;
  flex-grow: 1;
  width: 100%;
  overflow: hidden;
  padding: 10px 10px;
  border-right: 1px solid #e7ecf1;
  cursor: pointer;
  user-select: none;

  &.overflow-none {
    overflow: initial;
  }

  > strong {
    font-size: 14px;
    font-weight: 600;
    margin: 0;
  }

  &.sorting {
    background: url(${sortBoth}) center right no-repeat;
  }

  &.sorting_asc {
    background: url(${sortAsc}) center right no-repeat;
  }

  &.sorting_desc {
    background: url(${sortDesc}) center right no-repeat;
  }

  &.sorting_asc_disabled {
    background: url(${sortAscDisabled}) center right no-repeat;
  }

  &.sorting_desc_disabled {
    background: url(${sortDescDisabled}) center right no-repeat;
  }
`;

export const RowTable = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 0;
  border: 1px solid #e7ecf1;
  border-bottom: 0;

  &.hight-light {
    background-color: #fbfcfd;
  }

  &:hover:not(:first-child) {
    background: #f3f4f6 !important;
  }

  @media (max-width: 480px) {
    flex-direction: column;
    border-bottom: 1px solid #e7ecf1;
    margin-top: 20px;

    &:first-child {
      display: none;
    }
  }
`;
