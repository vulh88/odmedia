import React from 'react';
import PropTypes from 'prop-types';
import {RowTable} from './styled-components';


const TableRowView = ({children, hightLight, className, rowRef}) => {
  const classNamesRow = `
  ${hightLight ? 'hight-light' : ''}
  ${className || ''}`;
  return (
      <RowTable ref={rowRef} className={classNamesRow}>
        {React.Children.toArray(children)}
      </RowTable>
  );
};

TableRowView.propTypes = {
  rowRef: PropTypes.func,
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  hightLight: PropTypes.bool
};

export default TableRowView;
