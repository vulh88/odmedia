import React from 'react';
import PropTypes from 'prop-types';
import {isFunction} from 'lodash';
import LaddaButton, { SLIDE_UP } from 'react-ladda';

class ButtonLoading extends React.PureComponent {
  static propTypes = {
    onClick: PropTypes.func,
    children: PropTypes.node.isRequired,
    className: PropTypes.string,
    loading: PropTypes.bool,
    disabled: PropTypes.bool
  };

  static defaultProps = {
    disabled: false
  };

  onClick = e => {
    if (isFunction(this.props.onClick)) {
      this.props.onClick(e);
    }
  };

  render() {
    const { children, className, loading, disabled } = this.props;
    return (
      <LaddaButton
        className={className}
        loading={loading}
        onClick={this.onClick}
        data-style={SLIDE_UP}
        disabled={disabled}
      >
        {React.Children.toArray(children)}
      </LaddaButton>
    );
  }
}

export default ButtonLoading;
