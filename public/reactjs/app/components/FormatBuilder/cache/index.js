class FormatCache {
  storage = {};

  getStorage() {
    return this.storage;
  }

  clearStorage() {
    this.storage = {};
  }
}

export const formatCache = new FormatCache();
