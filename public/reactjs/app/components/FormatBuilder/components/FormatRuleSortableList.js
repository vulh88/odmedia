import {SortableContainer} from 'react-sortable-hoc';
import React from 'react';

@SortableContainer
class FormatRuleSortableList extends React.PureComponent {

  render() {
    const {onActionClick, list} = this.props;
    return (
        <div>
          {
            list.map(({rule: FormatRuleSortable, initSetting, uid}, idz) =>
                <FormatRuleSortable
                    ruleRef={e => list[idz].ruleRef = e}
                    onActionClick={onActionClick}
                    ruleIndex={idz}
                    index={idz}
                    key={uid}
                    initSetting={initSetting}
                />)
          }
        </div>
    );
  }
}

export default FormatRuleSortableList;
