import React from 'react';
import BaseFormatRule from './BaseFormatRule';
import {createStructuredSelector} from "reselect";
import FormatApiService from 'services/api/formats';
import BlockLoading from '../../../BlockLoading';
import {formatCache} from '../../cache';

class FormatRule extends BaseFormatRule {

  cache = formatCache.getStorage();

  ruleInfo = {
    id: 'format-rule',
    serverTypeId: 'format-rule',
    title: 'Format',
    icon: 'fa fa-archive'
  };

  /**
   * Can be override by data turned back from server db storage
   * @type {{formatId: number}}
   */
  initData = {
    formatId: 0
  };

  state = {
    formats: []
  };

  async getFormats() {
    const {currentFormatId} = this.props.contextData;
    const formatsApi = await FormatApiService.getList({
      channel_id: this.props.contextData.currentChannel.id,
      count: 1000,
      sort: 'id'
    });
    const formats = [{
      key: 0,
      value: 0,
      name: "Select Format"
    }];

    formatsApi.data.map((e, idz) => {
      if (e.id != currentFormatId) {
        formats.push({
          key: e.id,
          value: e.id,
          name: e.name
        })
      }
    });

    return formats;
  }

  async getInitData() {
    if (!this.cache.formats) {
      this.cache.formats = this.getFormats();
    }
    this.setState({
      formats: await this.cache.formats
    });
  }

  onChangeFormat = (e) => {
    this.setting.data.formatId = e.target.value;
  };

  reRenderFormats = () => {
    const {currentFormatId} = this.props.contextData;
    const {formats} = this.state;
    if (formats.length > 0) {
      formats.filter((el, idz) => {
        if (el.value == currentFormatId) {
          formats.splice(idz, 1);
        }
      });
      console.log("formats : ", formats);
      this.setState({
        formats: formats
      });
    }
  };

  renderCanvasInner() {
    const {formats} = this.state;
    const {formatId} = this.initData;

    return (
      <div className="element-builder">
        <p className="title">Insert Format</p>
        <p className="child-title">The following format is played:</p>
        <div>
          {!this.ready && <BlockLoading/>}
          <select
              key={Math.random()}
              className="form-control"
              name={"format_id"}
              onChange={e => this.onChangeFormat(e)}
              defaultValue={formatId}>
            {formats.map((o,i) =>  <option key={i} {...o}>{o.name}</option>)}
          </select>
        </div>
      </div>
    );
  }

}

export default FormatRule;
