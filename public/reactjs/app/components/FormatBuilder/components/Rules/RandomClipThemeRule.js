import React from 'react';
import BaseFormatRule from './BaseFormatRule';
import {createStructuredSelector} from "reselect";
import ThemesApiService from 'services/api/themes';
import BlockLoading from '../../../BlockLoading';
import {formatCache} from '../../cache';

class RandomClipThemeRule extends BaseFormatRule {

  cache = formatCache.getStorage();

  ruleInfo = {
    id: 'random-clips-theme',
    serverTypeId: 'random-clips-theme',
    title: 'Clips of a theme',
    icon: 'fa fa-file-video-o'
  };

  /**
   * Can be override by data turned back from server db storage
   * @type {{themeId: number, reverse: number}}
   */
  initData = {
    themeId: 0,
    reverse: 0
  };

  state = {
    themes: []
  };

  componentDidMount() {
    const {themeId} = this.initData;
    this.setting.data.themeId = this.setting.data.themeId > 0 ? this.setting.data.themeId : themeId;
  }

  async getListThemes() {
    const themesDataApi = await ThemesApiService.getList({
      channel_id: this.props.contextData.currentChannel.id,
      count: 1000,
      sort: 'id'
    });
    const themes = [{
      key: 0,
      value: 0,
      name: "Select Theme"
    }];
    themesDataApi.data.map(e => (themes.push({
      key: e.id,
      value: e.id,
      name: e.name
    })));
    return themes;
  }

  async getInitData() {
    if (!this.cache.themes) {
      this.cache.themes = this.getListThemes();
    }
    this.setState({
      themes: await this.cache.themes
    });
  }

  onThemeChange = (e) => {
    this.setting.data.themeId = e.target.value;
  };

  onTurnedChange = (e) => {
    this.setting.data.reverse = e.target.checked ? 1 : 0;
  };

  renderCanvasInner() {
    const {themes} = this.state;
    const {themeId, reverse} = this.initData;

    return (
      <div className="element-builder">
        <p className="title">{this.ruleInfo.title}</p>
        <div>
          {!this.ready && <BlockLoading/>}
          <select
              key={Math.random()}
              className="form-control"
              name={"theme_id"}
              onChange={e => this.onThemeChange(e)}
              defaultValue={themeId}>

            { themes.map((o,i) =>  <option key={i} {...o}>{o.name}</option>)}

          </select>
        </div>
        <label className="mt-checkbox  mt-checkbox-outline">
          <input
              onChange={e => this.onTurnedChange(e)}
              type="checkbox"
              name="turned-rule"
              id="turned-rule"
              defaultChecked={reverse} />
          Reverse
          <span/>
        </label>
      </div>
    );
  }

}

export default RandomClipThemeRule;
