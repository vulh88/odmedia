import React from 'react';
import BaseFormatRule from './BaseFormatRule';
import FormatBuilder from '../../index';

export const containerRuleInfo = {
  id: 'container',
  serverTypeId: 'container',
  title: 'Format container',
  icon: 'fa fa-align-justify'
};

class ContainerRule extends BaseFormatRule {

  ruleInfo = containerRuleInfo;

  relationshipTypes = [
    {
      id: 'and',
      name: 'And',
      description: 'All the added rules will be added to the condition'
    },
    {
      id: 'or',
      name: 'Or',
      description: 'One of the added rules will be added to the condition'
    }
  ];

  previousRulesData = null;

  initData = {
    relationshipType: this.relationshipTypes[0].id
  };

  mapInitSetting() {
    super.mapInitSetting();
    if (this.setting.data.rules) {
      this.previousRulesData = this.setting.data.rules.map(e => ({
        priority: e.index,
        id: e.id,
        rule_type: e.type,
        data: e.data,
      }));
    }
  }


  getSetting() {
    return {
      ...super.getSetting(),
      data: {
        relationshipType: this.setting.data.relationshipType,
        rules: this.formatBuilder.getData()
      }
    }
  }

  onChangeType = (type) => {
    this.setting.data.relationshipType = type.id;
    this.forceUpdate();
  };

  renderCanvasInner() {
    const {relationshipTypes, setting, previousRulesData} = this;
    return (
        <div style={{width: '100%'}} className="pb-4 pt-2 container-rule">
          <p className="title">Container rule</p>
          <div className="button-group my-1">
            {
              relationshipTypes.map(e => <button key={e.id} className={`btn btn-default ${e.id === setting.data.relationshipType ? 'active' : ''}`} onClick={() => this.onChangeType(e)}>{e.name}</button>)
            }
          </div>
          <FormatBuilder rulesData={previousRulesData} ref={e => this.formatBuilder = e} isInContainer/>
        </div>
    );
  }

}

export default ContainerRule;
