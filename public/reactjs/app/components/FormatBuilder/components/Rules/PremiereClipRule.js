import React from 'react';
import BaseFormatRule from './BaseFormatRule';

class PremiereClipRule extends BaseFormatRule {

  ruleInfo = {
    id: 'premiere-clip',
    serverTypeId: 'premiere-clip',
    title: 'Premiere clip',
    icon: 'fa fa-trophy'
  };

  renderCanvasInner() {
    return (
      <div className="element-builder">
        <p className="title">{this.ruleInfo.title}</p>
        <span className="child-title">A clip with the 'premiere' feature is selected.</span>
      </div>
    );
  }

}

export default PremiereClipRule;
