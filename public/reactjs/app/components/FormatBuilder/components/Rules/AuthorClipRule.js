import React from 'react';
import BaseFormatRule from './BaseFormatRule';
import BlockLoading from '../../../BlockLoading';
import ResourceApiService from 'services/api/resources';
import {formatCache} from '../../cache';

class AuthorClipRule extends BaseFormatRule {

  cache = formatCache.getStorage();

  ruleInfo = {
    id: 'author-clip',
    serverTypeId: 'author-clip',
    title: 'Clip based on author',
    icon: 'fa fa-users'
  };

  /**
   * Can be override by data turned back from server db storage
   * @type {{author: number, reverse: number}}
   */
  initData = {
    author: 0,
    reverse: 0
  };

  state = {
    authors: []
  };

  async getAuthors() {
    const authorsApi = await ResourceApiService.getAuthors({
      channel_id: this.props.contextData.currentChannel.id,
      count: 1000,
      sort: 'id'
    });
    const authors = [{
      key: 0,
      value: 0,
      name: "Select Author"
    }];
    authorsApi.data.map((el, idz) => (authors.push({
      key: el.id,
      value: el.id,
      name: el.full_name
    })));
    return authors;
  }

  async getInitData() {
    if (!this.cache.authors) {
      this.cache.authors = this.getAuthors();
    }
    this.setState({
      authors: await this.cache.authors
    });
    this.forceUpdate();
  }

  onChangeAuthor = (e) => {
    this.setting.data.author = e.target.value;
  };

  onTurnedChange = (e) => {
    this.setting.data.reverse = e.target.checked ? 1 : 0;
  };

  renderCanvasInner() {
    const {authors} = this.state;
    const {author, reverse} = this.initData;

    return (
      <div className="element-builder">
        <p className="title">Random clip from author</p>
        <p className="child-title">This line selects a random clip from the following author:</p>
        <div>
          {!this.ready && <BlockLoading/>}
          <select
              key={Math.random()}
              className="form-control"
              name={"author"}
              onChange={e => this.onChangeAuthor(e)}
              defaultValue={author}
          >
            {authors.map((o,i) =>  <option key={i} {...o}>{o.name}</option>)}
          </select>
        </div>
        <label className="mt-checkbox  mt-checkbox-outline">
          <input
              onChange={e => this.onTurnedChange(e)}
              type="checkbox"
              name="turned-rule"
              id="turned-rule"
              defaultChecked={reverse} />
          Reverse
          <span/>
        </label>
      </div>
    );
  }

}

export default AuthorClipRule;
