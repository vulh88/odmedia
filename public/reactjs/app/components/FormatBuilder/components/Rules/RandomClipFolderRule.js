import React from 'react';
import BaseFormatRule from './BaseFormatRule';
import {createStructuredSelector} from "reselect";
import Popup from 'components/Popup';
import TreeFolder from 'components/Tree';
import ResourceFoldersApiService from 'services/api/resource-folders';
import BlockLoading from '../../../BlockLoading';
import {formatCache} from '../../cache';


class RandomClipFolderRule extends BaseFormatRule {

  cache = formatCache.getStorage();

  ruleInfo = {
    id: 'random-clips-folder',
    serverTypeId: 'random-clips-folder',
    title: 'Random clips from a folder',
    icon: 'fa fa-folder'
  };

  state = {
    showPopup: false,
    selected: '',
    resourceFolders: []
  };

  /**
   * Can be override by data turned back from server db storage
   * @type {{resourceId: number, reverse: number}}
   */
  initData = {
    resourceId: 0,
    reverse: 0
  };

  componentDidMount() {
    const {resourceId} = this.initData;
    const {resourceFolders} = this.state;
    const {data} = this.setting;
    let resourceID = resourceId;

    if (Object.keys(data).length > 0) {
      resourceID = data.resourceId;
    }
    const title = this.checkSelected(resourceFolders, resourceID);
    this.setSelected(title);

  }

  async getResourceFolders() {
    const resourceFolders = await ResourceFoldersApiService.getTreeFolders({
      channel_id: this.props.contextData.currentChannel.id,
      count: 1000,
      sort: 'id'
    });

    return resourceFolders;
  }

  async getInitData() {
    if (!this.cache.resourceFolders) {
      this.cache.resourceFolders = this.getResourceFolders();
    }
    this.setState({
      resourceFolders: await this.cache.resourceFolders
    });
  }

  addResourceFolder = ({value, title}) => {
    this.setting.data.resourceId = value;
    this.setSelected(title);
  };

  onTurnedChange = (e) => {
    this.setting.data.reverse = e.target.checked ? 1 : 0;
  };

  setSelected(title) {
    this.setState({
      selected: title
    });
  }

  checkSelected(resourceFolders, value) {
    let selected = '';
    resourceFolders.map((el, idz) => {
      if (el.key == value) {
        selected = el.name;
      } else {
        if (el.children) {
          selected = this.checkSelected(el.children, value);
        }
      }
    });
    return selected;
  }

  renderCanvasInner() {
    const {resourceFolders} = this.state;
    const {resourceId, reverse} = this.initData;
    const {selected} = this.state;

    return (
      <div>
        <div className="element-builder">
          <p className="title">{this.ruleInfo.title}</p>
          {this.ready ? (
           <div>
              <p>
                <button type="button" className="btn dark" onClick={() => this.popup.open()}>Select folder</button>
                <span className="selected">{selected}</span>
              </p>
              <label className="mt-checkbox  mt-checkbox-outline">
                <input
                    type="checkbox"
                    name="turned-rule"
                    id="turned-rule"
                    onChange={e => this.onTurnedChange(e)}
                    defaultChecked={reverse} />
                Reverse
                <span></span>
              </label>
              <Popup title={"Resource Explorer"} buttonSaveTitle={"Apply"} ref={e => this.popup = e}>
                <TreeFolder data={resourceFolders} func={{addRule: this.addResourceFolder }} keys={[resourceId]} />
              </Popup>
            </div>
              )
            : <BlockLoading/>
          }

        </div>
      </div>
    );
  }
}

export default RandomClipFolderRule;
