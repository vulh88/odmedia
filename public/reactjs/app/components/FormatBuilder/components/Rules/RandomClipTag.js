import React from 'react';
import BaseFormatRule from './BaseFormatRule';
import Popup from 'components/Popup';
import ResourceTagApiService from 'services/api/resource-tags';
import ManageTable from 'components/ManageTable';
import LoadingIndicator from 'components/LoadingIndicator';
import ListingPage from 'components/BaseComponents/ListingPage';
import { createStructuredSelector } from 'reselect';
import {connect} from 'react-redux';
import { makeSelectChannel } from 'store/app/selectors';
import {withRouter} from 'react-router';

class RandomClipTag extends BaseFormatRule {

  ruleInfo = {
    id: 'random-clip-tag',
    serverTypeId: 'random-clip-tag',
    title: 'Random clip with tag (s)',
    icon: 'fa fa-tags'
  };

  /**
   * Can be override by data turned back from server db storage
   * @type {{tag: string, reverse: number}}
   */
  initData = {
    tag: null,
    reverse: 0
  };

  state = {
    tagsData: null,
    checkValidated: false
  };

  componentDidMount() {
    this.setting.validated = 1;
  }

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      this.onCheckValidated();
    }
  }

  onInputTag = (e) => {
    this.setting.data.tag = e.target.value.trim();
    this.setState({
      checkValidated: false
    });
  };

  onTurnedChange = (e) => {
    this.setting.data.reverse = e.target.checked ? 1 : 0;
  };

  onGetTags() {
    const {tag} = this.setting.data;
    /**
     * Get Resource Tags Data
     * @type {Array}
     */
    if (tag) {
      this.setState({
        tagsData: tag
      });

      this.popup.open();
    }
  };

  onCheckValidated = () => {
    const { data } = this.setting;
    const { contextData,  ruleIndex} = this.props;
    const { validationItems } = contextData;
    let validated = false;
    validationItems.filter((el, idz) => {
      if (el.index == ruleIndex && !data.tag) {
        validated = true;
      }
    });
    this.setState({
      checkValidated: validated
    });

    this.forceUpdate();
  };

  renderCanvasInner() {
    const { contextData } = this.props;
    const {tag, reverse} = this.initData;
    const {tagsData, checkValidated} = this.state;

    return (
      <div className="element-builder">
        <div>
          <p className="title">{this.ruleInfo.title}</p>
          <p className="child-title">This line selects a random clip with the following tag (s):</p>
          <div className="row">
            <div className="col-md-10">
              <input type={"text"} name={"tag"} defaultValue={tag} onChange={e => this.onInputTag(e)} className={"input " + (checkValidated ? "error" : "")} />
              <span className="error-msg">{checkValidated ? "The field is a required field" : ""}</span>
            </div>
            <div className="col-md-2">
              <button type="button" className="btn" onClick={() => this.onGetTags()}>View Resources</button>
            </div>
          </div>
          <label className="mt-checkbox  mt-checkbox-outline">
            <input
                onChange={e => this.onTurnedChange(e)}
                type="checkbox"
                name="turned-rule"
                id="turned-rule"
                defaultChecked={reverse} />
            Reverse
            <span/>
          </label>
        </div>

        <Popup title={"Tag Explorer- " + contextData.currentChannel.name} buttonSaveTitle={"Apply"} showFooter={false} className={"popup-custom"} ref={e => this.popup = e}>
          {
            <TagExplorer tagsData={tagsData} />
          }
        </Popup>
      </div>
    );
  }

}

@withRouter
@connect(
    createStructuredSelector({
      currentChannel: makeSelectChannel()
    })
)
export class TagExplorer extends ListingPage {
  restApiService = ResourceTagApiService;
  ignoreQuery = true;

  requestData(params = {}) {
    if (this.props.tagsData) {
      params.keyword = this.props.tagsData;
    }
    params.channel_id = this.props.currentChannel.id;
    params.sort = "name";
    super.requestData(params);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.tagsData !== this.props.tagsData) {
      this.requestData();
    }
  }

  render() {
    const { data, paging, loading } = this.apiState;
    const columns = [
      {
        name: 'Tag Name',
        value: e => e.name,
        sort: true
      },
      {
        name: 'Filename',
        value: e => e.resource.filename,
        sort: true
      },
      {
        name: 'Title',
        value: e => e.resource.title,
        sort: true
      },
      {
        name: 'Author',
        value: e => e.resource.author,
        sort: true
      },
      {
        name: 'Last Modified',
        value: e => e.resource.updated_at,
        sort: true
      },
      {
        name: 'Length',
        value: e => e.resource.duration,
        sort: true
      }
    ];
    return (
        <div>
          <ManageTable
              loading={loading}
              data={data || []}
              paging={paging || {}}
              handleChangePage={this.handlePageChange}
              handleLimitChange={this.handleLimitChange}
              handleSortBy={this.handleSortBy}
              columns={columns}
              showActions={false}
              tools={false}
          />

        </div>
    );
  }

}

export default RandomClipTag;
