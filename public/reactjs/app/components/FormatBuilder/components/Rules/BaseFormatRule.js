import React from 'react';
import {SortableElement} from 'react-sortable-hoc';
import quickAlert from '../../../../helpers/user-interface/quick-alert';
import {FormatBuilderContext} from '../context';
import {withContext} from '../../../../helpers/react/withContext';

class BaseFormatRule extends React.PureComponent {

  ready = false;

  ruleInfo = {
    id: 'base',
    serverTypeId: 'base',
    title: 'Format base',
    icon: 'fa fa-align-justify'
  };

  /**
   * This data will be sent to server, see function getSetting()
   * @type {{id: number, value: number}}
   */
  setting = {
    id: 0,
    data: null,
    validated: 0
  };

  /**
   * The default data of a rule.
   * Can be overridden by data turned back from server db storage
   * @type {{}}
   */
  initData = {};

  /**
   * Get the setting of rule for saving on server
   * @returns {{id: number, value: number, index: *, type: string}}
   */
  getSetting() {
    return {
      id: this.setting.id,
      data: this.setting.data,
      index: this.props.ruleIndex,
      type: this.ruleInfo.serverTypeId,
      validated: this.setting.validated,
    };
  }

  /**
   * Constructor
   * @param props
   */
  constructor(props) {
    super(props);
    if (props) {
      /**
       * Important point, that export the current rule instance out for parent ruleRef callback (which was passed by the props)
       */
      if (props.ruleRef) {
        props.ruleRef(this);
      }
    }
    else {
      // Fake props, for render in canvas
      this.props = {
        inToolbar: true
      };
    }
  }

  /**
   * Use to map the init data to the setting object.
   */
  mapInitSetting() {
    const {props} = this;
    /**
     * If have init data, (ex: update format builder rules.)
     */
    if (props.initSetting) {
      const {id, data} = props.initSetting;
      this.setting.id =  id > 0 ? id : 0;
      if (data) {
        //add previous data
        this.initData = this.setting.data = data;
      }
      else {
        this.setting.data = this.initData;
      }
    }
  }

  async componentWillMount() {
    this.mapInitSetting();
    await this.getInitData();
    this.ready = true;
    this.forceUpdate();
  }

  /**
   * Child have to implement this
   */
  getInitData() {
    return Promise.resolve(true);
  }

  /**
   * Used to trigger action callback to the function onActionClick handler which was passed from props
   * @param type
   * @returns {*}
   */
  triggerActionClick(type) {
    const {onActionClick, ruleIndex} = this.props;
    if (onActionClick) {
      return onActionClick(type, ruleIndex);
    }
    return null;
  }

  /**
   * Handle drag event started
   * @param event
   */
  handleDragStart = (event) => {
    event.dataTransfer.setData('data', this.ruleInfo.id);
  };


  /**
   * A rule have two render mode:
   * - In canvas
   * - In toolbar
   * This function used for rendering in toolbar (like icon and title)
   * @returns {*}
   */
  renderInToolbar = () => {
    const {title, icon} = this.ruleInfo;
    return (
        <div className="format-tool-item" draggable onDragStart={this.handleDragStart}>
        <span>
          <i className={icon}/>
        </span>
          <p>{title}</p>
        </div>
    );
  };

  /**
   * A rule will have two render mode:
   * - In canvas
   * - In toolbar
   * This function is used in canvas mode
   * @returns {*}
   */
  renderCanvasInner() {
    return (
        <div>
          <p>This is a base format rule</p>
        </div>
    );
  }

  /**
   * Clone a rule, rule component be mounted in the canvas, so the rule must be a SortableElement
   * @param setting
   * @returns {{rule: React.Component<*>, uid: number, dataHolder: {}, initSetting}}
   */
  cloneRule(setting = {}) {
    return {
      rule: SortableElement(withContext(FormatBuilderContext)(this.constructor)),
      uid: Math.random(),
      dataHolder: {},
      initSetting: setting
    }
  }


  /**
   * Function to check if any type representing the current format rule type string.
   * @param type
   * @returns {boolean}
   */
  isType = (type) => {
    return type === this.ruleInfo.serverTypeId;
  };

  /**
   * This function's used to determine whether the rule is in canvas or in toolbar
   * @returns {{inToolbar: boolean}|boolean}
   */
  isCanvasMode() {
    return this.props && !this.props.inToolbar;
  }

  /**
   * Handle remove rule,
   * Just trigger an action to parent. The remove sequence will be handled by the FormatBuilder.
   * @returns no
   */
  onRemoveClick = async () => {
    await quickAlert.confirm('Do you want to remove this rule?') && this.triggerActionClick('remove');
  };

  /**
   * The main render template of a rule on canvas
   * @returns {*}
   */
  render() {
    return (
        <div className="format-rule">
          {this.renderCanvasInner()}
          <div className="format-rule__actions">
            <button
                className="fa fa-remove font-red"
                onClick={this.onRemoveClick}
            />
          </div>
        </div>
    )
  }
}

export default BaseFormatRule;

