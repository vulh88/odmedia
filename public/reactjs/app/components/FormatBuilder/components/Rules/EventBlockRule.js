import React from 'react';
import BaseFormatRule from './BaseFormatRule';
import {createStructuredSelector} from "reselect";
import EventBlocksApiService from 'services/api/event-blocks';
import BlockLoading from '../../../BlockLoading';
import {formatCache} from '../../cache';

class EventBlockRule extends BaseFormatRule {

  cache = formatCache.getStorage();

  ruleInfo = {
    id: 'event-block',
    serverTypeId: 'event-block',
    title: 'Event Block',
    icon: 'fa fa-eye'
  };

  state = {
    stateEventBlockTime: 0,
    eventBlocks: []
  };

  /**
   * Can be override by data turned back from server db storage
   * @type {{eventBlockId: number, eventBlockTime: number}}
   */
  initData = {
    eventBlockId: 0,
    eventBlockTime: 0
  };

  componentDidMount() {
    const {eventBlockId, eventBlockTime} = this.initData;
    this.setState({
      stateEventBlockTime: eventBlockTime
    })
  }

  async getEventBlocks() {
    const eventBlocksApi = await EventBlocksApiService.getList({
      channel_id: this.props.contextData.currentChannel.id,
      count: 1000,
      sort: 'id'
    });
    const eventBlocks = [{
      key: 0,
      value: 0,
      name: "Select Event Block"
    }];
    eventBlocksApi.data.map(e => (eventBlocks.push({
      key: e.id,
      value: e.id,
      name: e.name
    })));
    return eventBlocks;
  }

  async getInitData() {
    if (!this.cache.eventBlocks) {
      this.cache.eventBlocks = this.getEventBlocks();
    }
    this.setState({
      eventBlocks: await this.cache.eventBlocks
    });
  }

  onChangeEventBlock = (e) => {
    this.setting.data.eventBlockId = e.target.value;

  };

  onChangeEventBlockTime = (e) => {
    const eventBlockTime = parseInt(e.target.value) <= 0 ? 0 : parseInt(e.target.value);
    this.setting.data.eventBlockTime = eventBlockTime;
    this.setState({
      stateEventBlockTime: eventBlockTime
    });
  };


  renderCanvasInner() {
    const {eventBlocks} = this.state;
    const {eventBlockId, eventBlockTime} = this.initData;
    const {stateEventBlockTime} = this.state;

    return (
      <div className="element-builder">
        <p className="title">{this.ruleInfo.title}</p>
        <p className="child-title"> An <strong>event block</strong> is placed on this location.</p>
        <div>
          {!this.ready && <BlockLoading/>}
          <select
              key={Math.random()}
              className="form-control"
              name="event-block-id"
              onChange={e => this.onChangeEventBlock(e)}
              defaultValue={eventBlockId}
          >
            {eventBlocks.map((o,i) => <option key={i} {...o}>{o.name}</option> )}
          </select>
        </div>
        <br />
        <p className="child-title">If desired, adjusted time in seconds:</p>
        <input
            type="number"
            name="event-block-time"
            min={0}
            className="eventblock-time"
            value={Number(stateEventBlockTime).toString()}
            onChange={e => this.onChangeEventBlockTime(e)} />
      </div>
    );
  }
}

export default EventBlockRule;
