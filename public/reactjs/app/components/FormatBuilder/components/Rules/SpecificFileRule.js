import React from 'react';
import BaseFormatRule from './BaseFormatRule';
import Popup from 'components/Popup';
import ResourcesApiService from 'services/api/resources';
import { createStructuredSelector } from 'reselect';
import { makeSelectChannel } from 'store/app/selectors';
import ResourcesTableSpecificRule from 'containers/ResourcesPage/components/Table/ResourcesTableSpecificRule';


class SpecificFileRule extends BaseFormatRule {

  ruleInfo = {
    id: 'specific-file',
    serverTypeId: 'specific-file',
    title: 'Specific File',
    icon: 'fa fa-file-video-o'
  };

  /**
   * Can be override by data turned back from server db storage
   * @type {{resource: number, reverse: number}}
   */
  initData = {
    resource: null
  };

  state = {
    resourceSelected: '',
    resourcePopupSelected: '',
    paging: 1,
    pagingActive: 1,
    resource: {
      title: '',
      id: 0
    }
  };

  async componentDidMount() {
    if (this.setting.data.resource) {
      const resource = await ResourcesApiService.getOne(this.setting.data.resource);
      if (resource.title) {
        this.setState({
          resourceSelected: resource.title,
          resourcePopupSelected: resource.title,
        });
        this.forceUpdate();
      }
    }
  }

  onShowResourcesTable = () => {
    this.setState({
      change: !this.state.change,
    });

    if (this.listTable) {
      this.listTable.handlePageChange(this.state.pagingActive);
    }
    this.popup.open();
  };

  onGetResource = ({id, title, paging}) => {
    this.state.resource.id = id;
    this.state.resource.title = title;
    this.setState({
      resource: this.state.resource,
      resourcePopupSelected: title,
      paging: paging
    });
  };

  onClose = (e) => {
    this.setState({
      resourcePopupSelected: this.state.resourceSelected,
    });
    this.popup.close();
  };

  onSave = (e) => {
    this.onClose();
    this.setState({
      resourceSelected: this.state.resource.title,
      resourcePopupSelected: this.state.resource.title,
      pagingActive: this.state.paging
    });
    this.setting.data.resource = this.state.resource.id;
  };



  renderCanvasInner() {
    const{resourceSelected, resourcePopupSelected} = this.state;
    const { history, contextData } = this.props;
    const { data } = this.setting;

    return (
      <div>
        <div className="element-builder">
          <p className="title">{this.ruleInfo.title}</p>
          <p className="child-title">
            The following clip will be played :
            <strong className="selected"> {resourceSelected} </strong>
          </p>
          <div>
            <button type="button" className="btn dark" onClick={() => this.onShowResourcesTable()}>Choose</button>
          </div>
        </div>

        <Popup
            title={"Resource Explorer - " + contextData.currentChannel.name}
            className={"popup-custom"}
            ref={e => this.popup = e}
            showFooter={false}
        >
          <PopupResource
              history={history}
              onGetResource={this.onGetResource}
              activeId={data ? data.resource : 0}
              descFooter={resourcePopupSelected}
              onClose={this.onClose}
              tableRef={e => this.listTable = e}
              onSave={(e) => this.onSave(e)}
          />

        </Popup>

      </div>
    );
  }
}

const PopupResource = (props) => {
  return (
      <div>
        <ResourcesTableSpecificRule
           // change={Math.random()}
            tableRef={props.tableRef}
            history={props.history}
            onGetResource={props.onGetResource}
            activeId={props.activeId}
            specificRule={true}
        />
        <div className="modal-footer">
          <p className="pull-left child-title"><strong>Current selection:</strong> {props.descFooter}</p>
          <button type="button" className="btn dark btn-outline" data-dismiss="modal"
                  onClick={e => props.onClose(e)}>Close
          </button>
          <button type="button" className="btn green"
                  onClick={e => props.onSave(e)}>Apply</button>
        </div>
      </div>
  );
};

export default SpecificFileRule;
