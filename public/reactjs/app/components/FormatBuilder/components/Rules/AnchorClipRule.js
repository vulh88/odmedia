import React from 'react';
import BaseFormatRule from './BaseFormatRule';

class AnchorClipRule extends BaseFormatRule {

  ruleInfo = {
    id: 'anchor-clip',
    serverTypeId: 'anchor-clip',
    title: 'Anchor clip',
    icon: 'fa fa-anchor'
  };

  /**
   * Can be override by data turned back from server db storage
   *
   */
  initData = {
    time: null
  };

  onChangeTime = (e) => {
    this.setting.data.time = e.target.value;
  };

  renderCanvasInner() {
    const defaultTime = this.setting.data.time ? this.setting.data.time : this.initData.time;

    return (
      <div className="element-builder">
        <p className="title">{this.ruleInfo.title}</p>
        <div>
          <p className="child-title">Place an anchor at a point on the timeline. Everything playing before the anchor will be cut off</p>
        </div>
        <div>
            <p className="child-title">
              <strong>Time of day : </strong>
              <input
                  type="time"
                  name="time"
                  defaultValue={defaultTime}
                  onChange={e => this.onChangeTime(e)} />
            </p>
        </div>
      </div>
    );
  }

}

export default AnchorClipRule;
