import React from 'react';
import injectMixins from '../../helpers/react/inject-mixins';
import {arrayMove} from 'react-sortable-hoc';
import RandomClipThemeRule from './components/Rules/RandomClipThemeRule';
import RandomClipFolderRule from './components/Rules/RandomClipFolderRule';
import FormatRuleSortableList from './components/FormatRuleSortableList';
import PropTypes from 'prop-types';
import PremiereClipRule from './components/Rules/PremiereClipRule';
import AnchorClipRule from './components/Rules/AnchorClipRule';
import ContainerRule, {containerRuleInfo} from './components/Rules/ContainerRule';
import EventBlockRule from './components/Rules/EventBlockRule';
import AuthorClipRule from './components/Rules/AuthorClipRule';
import FormatRule from './components/Rules/FormatRule';
import toast from '../../helpers/user-interface/toast';
import RandomClipTag from './components/Rules/RandomClipTag';
import SpecificFileRule from './components/Rules/SpecificFileRule';
import {StickyContainer, Sticky} from 'react-sticky';
import {formatCache} from './cache';
import ScrollArea from 'react-scrollbar';

class FormatBuilder extends React.PureComponent {
  static propsTypes = {
    rulesData: PropTypes.array,
    isInContainer: PropTypes.bool,
  };

  rulesPrototypes = [
    new ContainerRule(),
    new RandomClipThemeRule(),
    new RandomClipFolderRule(),
    new PremiereClipRule(),
    new AnchorClipRule(),
    new EventBlockRule(),
    new AuthorClipRule(),
    new FormatRule(),
    new RandomClipTag(),
    new SpecificFileRule(),
  ];

  uiState = injectMixins.autoRenderOnChange.call(this, {
    isDragOver: false,
    addedRules: []
  });

  constructor(props) {
    super(props);
    if (!this.props.isInContainer) {
      formatCache.clearStorage();
    }
  }

  handleDragOver = (e) => {
    e.preventDefault();
    e.stopPropagation();
    this.uiState.isDragOver = true;
  };

  handleDragLeave = (e) => {
    this.uiState.isDragOver = false;
  };

  parseTransferData(e) {
    try {
      return this.rulesPrototypes.find(r => r.ruleInfo.id === e.dataTransfer.getData('data'));
    } catch (err) {
      console.error(err);
      return null;
    }
  }

  handleDrop = (e) => {
    e.stopPropagation();
    this.uiState.isDragOver = false;
    const rule = this.parseTransferData(e);
    if (rule) {
      if (this.props.isInContainer && rule.ruleInfo.id === containerRuleInfo.id) {
        return toast.error(`Container rule's not allowed here!`);
      }
      this.uiState.addedRules = [...this.uiState.addedRules, rule.cloneRule()];
    }
  };

  onSortEnd = ({oldIndex, newIndex}) => {
    this.uiState.addedRules = arrayMove(this.uiState.addedRules, oldIndex, newIndex);
  };

  onActionClick = (type, index) => {
    if (type === 'remove') {
      const {addedRules} = this.uiState;
      if (addedRules[index] !== -1) {
        addedRules.splice(index, 1);
      }
      this.uiState.addedRules = [...addedRules];
    }
  };

  componentDidMount() {
    this.parseDataUpdate();
  }

  parseDataUpdate() {
    const {rulesData} = this.props;
    if (rulesData && rulesData.length) {
      const storedRules = [...rulesData];
      this.uiState.addedRules = storedRules
          .sortBy('priority')
          .map(e => {
            const r = this.rulesPrototypes.find((r) => r.isType(e.rule_type));
            if (r) {
              return r.cloneRule({
                index: e.priority,
                id: e.id,
                type: e.rule_type,
                data: e.data,
              });
            }
            else {
              console.error(`Unknown rule types: ${e.rule_type}`);
            }
          })
          .filter(e => e);
    }
  }

  getData = () => {
    const res = this.uiState.addedRules.map(e => {
      return e.ruleRef.getSetting();
    });
    console.log('rules', res);
    return res;
  };

  renderBuilderCanvas() {
    const {isDragOver, addedRules} = this.uiState;
    return (
        <div
            className="format-builder__canvas"
            onDragOver={this.handleDragOver}
            onDragLeave={this.handleDragLeave}
            onDrop={this.handleDrop}>
          <FormatRuleSortableList
              onActionClick={this.onActionClick}
              onSortEnd={this.onSortEnd}
              list={addedRules}/>
          {isDragOver && <div className="format-rule-placeholder"/>}
        </div>
    )
  }

  renderToolsBar() {
    return (
        <StickyContainer className="sticky-wrapper">
          <Sticky topOffset={-50} bottomOffset={33}>
            {({isSticky, style}) =>
                (
                    <div className={`format-builder__controls-bar portlet light ${ isSticky ? 'is--fixed' : ''}`} style={style}>
                      <div className="portlet-title text-center">
                        <span className="caption">
                          Tools and Conditions
                        </span>
                      </div>
                      <ScrollArea
                          speed={0.8}
                          className="area"
                          contentClassName="portlet-body"
                          horizontal={false}
                      >
                        <div className="row haft-gutters">
                          {
                            this.rulesPrototypes.map((e, idz) => (
                                <div
                                    key={idz}
                                    className="col-sm-3 col-md-6 col-lg-4">
                                  {e.renderInToolbar()}
                                </div>
                            ))
                          }
                        </div>
                      </ScrollArea>
                      </div>
                )
            }
          </Sticky>
        </StickyContainer>
    )
  }

  render() {
    const {isDragOver} = this.uiState;
    const {isInContainer} = this.props;

    // Render in container
    if (isInContainer) {
      return this.renderBuilderCanvas();
    }

    //Render no container
    return (
        <div className={`format-builder ${isDragOver ? 'is--drag-over' : ''}`}>
          <h2 className="page-title">Format builder</h2>
          {/*<button className="btn btn-success" onClick={this.getData}>Gather data</button>*/}
          <div className="format-builder__inner row">
            <div className="col-md-4 col-md-push-8">
              {this.renderToolsBar()}
            </div>
            <div className="col-md-8 col-md-pull-4">
              {this.renderBuilderCanvas()}
            </div>
          </div>
        </div>
    );
  }
}

export default FormatBuilder;
