import React from 'react';
import PropTypes from 'prop-types';
import CountUp from 'react-countup';

import './assets/styles/styles.css';

class CounterBox extends React.PureComponent {
  static propTypes = {
    start: PropTypes.number,
    end: PropTypes.number,
    duration: PropTypes.number,
    useEasing: PropTypes.bool,
    useGrouping: PropTypes.bool,
    separator: PropTypes.string,
    decimals: PropTypes.any,
    decimal: PropTypes.string,
    prefix: PropTypes.string,
    suffix: PropTypes.string,
    onComplete: PropTypes.func,
    onStart: PropTypes.func,
    colorClass: PropTypes.string,
    text: PropTypes.string
  };

  render() {
    const {
      start,
      end,
      duration,
      useEasing,
      useGrouping,
      separator,
      decimals,
      decimal,
      prefix,
      suffix,
      onComplete,
      onStart,
      colorClass,
      text
    } = this.props;

    return (
      <a className={`dashboard-stat dashboard-stat-v2 ${colorClass}`}>
        <div className="visual">
          <i className="fa fa-comments" />
        </div>
        <div className="details">
          <div className="number">
            <CountUp
              start={start}
              end={end}
              duration={duration || 1}
              useEasing={useEasing || false}
              useGrouping={useGrouping || false}
              separator={separator}
              decimals={decimals}
              decimal={decimal}
              prefix={prefix}
              suffix={suffix}
              onComplete={onComplete}
              onStart={onStart}
            />
          </div>
          <div className="desc"> {text} </div>
        </div>
      </a>
    );
  }
}

export default CounterBox;
