import { defineMessages } from 'react-intl';
import routePaths from 'settings/route-paths';

export default defineMessages({
  scheduling: {
    id: 'odmedia.components.Sidebar.SchedulingPage',
    defaultMessage: 'Scheduling',
    link: ''
  },
  formats: {
    id: 'odmedia.components.Sidebar.FormatsPage',
    defaultMessage: 'Formats',
    link: routePaths.formats.list
  },
  themes: {
    id: 'odmedia.components.Sidebar.ThemesPage',
    defaultMessage: 'Themes',
    link: routePaths.themes.list
  },
  resources: {
    id: 'odmedia.components.Sidebar.ResourcesPage',
    defaultMessage: 'Resources',
    link: routePaths.resources.list
  },
  import: {
    id: 'odmedia.components.Sidebar.ImportPage',
    defaultMessage: 'Import',
    link: routePaths.resources.import
  },
  system: {
    id: 'odmedia.components.Sidebar.SystemRolesPage',
    defaultMessage: 'System Roles',
    link: routePaths.roles.assignments
  },
  overview: {
    id: 'odmedia.components.Sidebar.Overview',
    defaultMessage: 'Overview',
    link: ''
  },
  support: {
    id: 'odmedia.components.Sidebar.SupportPage',
    defaultMessage: 'Support',
    link: ''
  },
  channels: {
    id: 'odmedia.components.Sidebar.ChannelsPage',
    defaultMessage: 'Channels',
    link: routePaths.channels.list
  },
  table: {
    id: 'odmedia.components.Sidebar.Table',
    defaultMessage: 'List',
    link: 'odmedia.components.Sidebar.Table',
  },
  playlists: {
    id: 'odmedia.components.Sidebar.Playlist',
    defaultMessage: 'Playlist',
    link: routePaths.playlists.list
  },
  users: {
    id: 'odmedia.components.Sidebar.UserSystem',
    defaultMessage: 'User',
    link: routePaths.users.list
  },
  settings: {
    id: 'odmedia.components.Sidebar.SettingsPage',
    defaultMessage: 'System Settings',
    link: routePaths.pages.system_settings
  },
  'create-new': {
    id: 'odmedia.components.Sidebar.CreateNew',
    defaultMessage: 'Add new',
    link: 'odmedia.components.Sidebar.CreateNew'
  },
  'update': {
    id: 'odmedia.components.Sidebar.Update',
    defaultMessage: 'Update',
    link: 'odmedia.components.Sidebar.Update'
  },
  'content-types': {
    id: 'odmedia.components.Sidebar.ContentTypesPage',
    defaultMessage: 'Content types',
    link: routePaths.contentTypes.list
  },
  'view': {
    id: 'odmedia.components.Sidebar.ViewPage',
    defaultMessage: 'View',
    link: 'odmedia.components.Sidebar.ViewPage'
  },
  'system-roles': {
    id: 'odmedia.components.Sidebar.SystemRolesPage',
    defaultMessage: 'System Roles',
    link: routePaths.roles.assignments
  },
  'event-blocks': {
    id: 'odmedia.components.Sidebar.EventBlocksPage',
    defaultMessage: 'Event Blocks',
    link: routePaths.eventBlocks.list
  },
  'scheduler': {
    id: 'odmedia.components.Sidebar.SettingsPage',
    defaultMessage: 'Scheduler settings',
    link: routePaths.pages.settings
  },
  'verify-resources': {
    id: 'odmedia.components.Sidebar.VerifyResources',
    defaultMessage: 'Download Resources',
    link: 'odmedia.components.Sidebar.VerifyResources'
  },
});
