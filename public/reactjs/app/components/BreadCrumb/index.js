import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Link } from 'react-router-dom';
import { injectIntl, intlShape } from 'react-intl';

import messages from './messages';

import {
  makeSelectChannel,
  makeSelectLoadingChannel
} from 'store/app/selectors';

const generateBreadCrumbName = (path) => {
  let breadcrumbType = '';

  console.log("check  : ", isNaN(Math.floor(path[2])));
  if ( !isNaN(Math.floor(path[2]))) {
    breadcrumbType = path[3] ? path[3] : 'update';
  } else {
    breadcrumbType = path[2];
  }
  console.log("breadcrumbType  : ", breadcrumbType);
  return breadcrumbType;
};

const BreadCrumb = props => {

  const getPath = props.location.pathname.split('/');
  const intl = props.intl;
  const isSystemPage = getPath[1] === 'system';
  const renderChild = () => {
    const namePage = generateBreadCrumbName(getPath);
    console.log("pathname : ", props.location.pathname);
    if (namePage) {
      return (
        messages[namePage] && (
          <li>
            <i className="fa fa-circle" />
            <span>{intl.formatMessage(messages[namePage])}</span>
          </li>
        )
      );
    }

    return null;
  };

  const renderParent = () => {
    const namePage = getPath[1];
    const pageUrl = messages[namePage] ? messages[namePage].link : '#';

    if (namePage) {
      return (
        <li>
          {isSystemPage ? null : <i className="fa fa-circle" />}
          <Link to={`${pageUrl}`}>
            {messages[namePage] && intl.formatMessage(messages[namePage])}
          </Link>
        </li>
      );
    }

    return (
      <li>
        <i className="fa fa-circle" />
        <span>{'Dashboard'}</span>
      </li>
    );
  };

  const renderChannel = () => {
    if (isSystemPage) {
      return null;
    }
    if (props.currentChannel) {
      return (
        <li>
          <Link to={'/'}>
            {props.loadingChannel ? (
              <i className="fa fa-spin fa-circle-o-notch" />
            ) : (
              props.currentChannel.name
            )}
          </Link>
        </li>
      );
    }
    return (
      <li>
        <span>{'Channel'}</span>
      </li>
    );
  };

  return (
    <div className="page-bar">
      <ul className="page-breadcrumb">
        {renderChannel()}
        {renderParent()}
        {renderChild()}
      </ul>
    </div>
  );
};

BreadCrumb.propTypes = {
  intl: intlShape.isRequired,
  location: PropTypes.object,
  allRoute: PropTypes.array,
  currentChannel: PropTypes.object,
  loadingChannel: PropTypes.bool
};

const mapStateToProps = createStructuredSelector({
  currentChannel: makeSelectChannel(),
  loadingChannel: makeSelectLoadingChannel()
});

export default injectIntl(
  connect(
    mapStateToProps,
    null
  )(BreadCrumb)
);
