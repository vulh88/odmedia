import React from 'react';
import lodash from 'lodash';
import Alert from 'react-s-alert';

const FormAlert = ({errors, prefix = '', success}) => {
  if (errors && errors.__showed !== '') {
    // @todo fix this ugly hot fix
    try {
      errors.__showed = '';
    } catch (e) {
    }
    Alert.error(
        <div>
          <h4>Error!</h4>
          <span>{lodash.map(errors, item => prefix + item)}</span>
        </div>,
        {position: 'bottom-right'}
    );
  }
  if (success) {
    Alert.success(
        <div>
          <h4>Successfully!</h4>
          <span>{prefix + success}</span>
        </div>,
        {position: 'bottom-right'}
    );
  }
  return null;
};

export default FormAlert;
