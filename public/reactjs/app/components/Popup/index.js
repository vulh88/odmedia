import React from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';

const appRoot = document.getElementById('app');

export class Popup extends React.PureComponent {

  constructor(props) {
    super(props);
    // STEP 1: create a container <div>
    const id = '#popup-container';
    let el = document.getElementById(id);
    if (!el) {
      el = document.createElement('div');
      el.id = id;
    }
    this.containerEl = el;
  }

  static propTypes = {
    title: PropTypes.string,
    onOpen: PropTypes.func,
    showFooter: PropTypes.bool,
    render: PropTypes.func,
    buttonSaveTitle: PropTypes.string,
    className: PropTypes.string,
    descFooter: PropTypes.string
  };

  static defaultProps = {
    title: '',
    buttonSaveTitle: 'Save',
    showFooter: true,
    className: '',
    descFooter: ''
  };

  state = {
    show: this.props.show
  };

  onClose = () => {
    this.setState({
      show: false
    });
    if (this.props.onClose) {
      this.props.onClose();
    }
  };

  open() {
    this.setState({
      show: true
    });
    if (this.props.onOpen) {
      this.props.onOpen();
    }
  }

  close = () => {
    this.onClose();
  };

  onBackDropClick = (e) => {
    const {target} = e;
    if (!$(target).is('.modal-dialog, .modal-dialog *, .rc-time-picker-panel, .rc-time-picker-panel *')) {
      this.onClose()
    }
  };

  onSaveClick = (e) => {
    this.onClose(e);
  };

  escFunction = (event) => {
    if (event.keyCode === 27 && this.state.show) {
      this.onClose();
    }
  };

  componentDidMount() {
    appRoot.appendChild(this.containerEl);
    document.addEventListener("keydown", this.escFunction, false);
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.escFunction, false);
  }

  render() {
    const {title, children, buttonSaveTitle, render, showFooter, className, descFooter} = this.props;
    const {show} = this.state;
    const showPopup = show ? 'in' : '';
    return ReactDOM.createPortal(
        <div>
          {show && <div className="modal-backdrop fade in"/>}
          <div className={"modal fade " + showPopup} id="basic" tabIndex="-1" role="basic" aria-hidden="true"
               onClick={this.onBackDropClick}>
            <div className={"modal-dialog " + className}>
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal" aria-hidden="true"
                          onClick={e => this.onClose(e)}/>
                  <h4 className="modal-title"><strong>{title}</strong></h4>
                </div>
                <div className="modal-body">{
                  render && show ? render() : null
                } {children}
                </div>

                {
                  showFooter ? <div className="modal-footer">
                    <span className="pull-left"><strong>{descFooter}</strong></span>
                    <button type="button" className="btn dark btn-outline" data-dismiss="modal"
                            onClick={e => this.onClose(e)}>Close
                    </button>
                    <button type="button" className="btn green"
                            onClick={e => this.onSaveClick(e)}>{buttonSaveTitle}</button>
                    </div>
                   : null
                }
              </div>
            </div>
          </div>
        </div>,

        this.containerEl
    );
  }
}

export default Popup;