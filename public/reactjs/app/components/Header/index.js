import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {createStructuredSelector} from 'reselect';

import ButtonLoading from 'components/ButtonLoading';

import {requestLogout, setCurrentChannel} from 'store/app/actions';
import {
  makeSelectLoading,
  makeSelectLoadingChannel,
  makeSelectToken,
  makeSelectListChannel,
  makeSelectChannel
} from 'store/app/selectors';

import Logo from './assets/images/logo.png';
import toast from '../../helpers/user-interface/toast';

class Header extends React.PureComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    accessToken: PropTypes.string,
    onToggleSidebar: PropTypes.func,
    loading: PropTypes.bool,
    loadingChannel: PropTypes.bool,
    listChannel: PropTypes.array,
    currentChannel: PropTypes.object,
    location: PropTypes.object
  };

  onLogout = () => {
    const {accessToken, dispatch} = this.props;
    const result                  = {accessToken};
    dispatch(requestLogout(result));
  };

  onChangeChannel = channel => {
    const {dispatch} = this.props;
    dispatch(
        setCurrentChannel({channel, callback: () => toast.success('Switched channel successfully')})
    );
  };

  renderRightHeader = () => {
    const {loading, loadingChannel, listChannel, currentChannel} = this.props;
    let channelList                                              = [];
    if (!loadingChannel && listChannel) {
      channelList =
          listChannel.map &&
          listChannel.map(item => (
              <li
                  key={item.id}
                  className={
                    currentChannel && currentChannel.id === item.id ? 'active' : null
                  }
              >
                <a onClick={() => this.onChangeChannel(item)}>
                  <i className="fa fa-play"/>
                  <span>{item.name}</span>
                </a>
              </li>
          ));
    }

    return (
        <div className="top-menu">
          <ul className="nav navbar-nav pull-right">
            {/* <li className="dropdown dropdown-language">
            <a
              className="dropdown-toggle"
              data-toggle="dropdown"
              data-hover="dropdown"
              data-close-others="true"
            >
              <span className="langname">EN </span>
              <i className="fa fa-angle-down" />
            </a>

            <ul className="dropdown-menu dropdown-menu-default">
              <li className="">
                <a>DE</a>
              </li>

              <li className="active">
                <a>EN</a>
              </li>
            </ul>
          </li> */}
            <li className="dropdown dropdown-user">
              <a
                  className="dropdown-toggle"
                  data-toggle="dropdown"
                  data-hover="dropdown"
                  data-close-others="true"
              >
              <span className="username username-hide-on-mobile">
                {loadingChannel && <i className="fa fa-spin fa-circle-o-notch"/>}
                {currentChannel && currentChannel.name}
              </span>
                <i className="fa fa-angle-down"/>
              </a>

              <ul className="dropdown-menu dropdown-menu-default">

                {loadingChannel ? (
                    <li className="text-center">
                      <a>
                        <i className="fa fa-spin fa-circle-o-notch"/>
                      </a>
                    </li>
                ) : (
                    channelList
                )}
              </ul>
            </li>

            <li className="dropdown dropdown-quick-sidebar-toggler">
              <ButtonLoading
                  loading={loading}
                  className="dropdown-toggle"
                  onClick={this.onLogout}
              >
                <i className="icon-logout"/>
              </ButtonLoading>
            </li>
          </ul>
        </div>
    );
  };

  render() {
    return (
        <div className="page-header navbar navbar-fixed-top">
          <div className="page-header-inner">
            <div className="page-logo">
              <Link to="/">
                <img src={Logo} alt="logo" className="logo-default"/>
              </Link>
              <div
                  className="menu-toggler sidebar-toggler"
                  onClick={this.props.onToggleSidebar}
              >
                <span/>
              </div>
            </div>

            <a
                className="menu-toggler responsive-toggler"
                data-toggle="collapse"
                data-target=".navbar-collapse"
            >
              <span/>
            </a>

            {this.renderRightHeader()}
          </div>
        </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  accessToken: makeSelectToken(),
  loading: makeSelectLoading(),
  loadingChannel: makeSelectLoadingChannel(),
  listChannel: makeSelectListChannel(),
  currentChannel: makeSelectChannel()
});

export default connect(
    mapStateToProps,
    null
)(Header);
