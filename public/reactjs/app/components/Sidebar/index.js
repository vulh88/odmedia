import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { injectIntl, intlShape } from 'react-intl';

import { SidebarApp } from './assets/js/sidebar';
import getNavLinks from 'components/Sidebar/nav-links';
import locationHelper from 'helpers/location';
import {linksHidden} from 'common/link-generator/disable-links';
import {connect} from 'react-redux';
import {createStructuredSelector} from "reselect";
import {makeSelectChannelSetting} from 'store/app/selectors';

@connect(createStructuredSelector({channelSetting: makeSelectChannelSetting()}))
class Sidebar extends React.PureComponent {
  static propTypes = {
    location: PropTypes.object,
    isOpenSidebar: PropTypes.bool,
    isShowOverview: PropTypes.bool,
    allRoute: PropTypes.array,
    match: PropTypes.object,
    intl: intlShape.isRequired
  };

  state = {
    load: false
  };

  componentDidMount() {
    SidebarApp.init();
  }

  isActiveMenu(menu) {
    const opt = { exact: true };
    const { pathname } = this.props.location;
    const isActive = locationHelper.isPathActive(pathname, menu.link, opt);

    const manualActive =
      menu.activePath &&
      locationHelper.isPathActive(pathname, menu.activePath, { exact: false });

    const isChildActive =
      menu.children && !!menu.children.find(e => this.isActiveMenu(e));

    return isActive || isChildActive || manualActive;
  }

  renderMenuItem = (menu, idz) => {
    const isActive = this.isActiveMenu(menu);
    const LinkComponent = menu.link
      ? props => <NavLink {...props} className="nav-link" to={menu.link} />
      : props => <a {...props} className="nav-link nav-toggle" />;
    let isShowMenu = linksHidden(this.props.channelSetting, menu.link);
    if (isShowMenu) {
      return (
          <li key={idz} className={`nav-item ${isActive ? 'active open' : ''}`}>
            <LinkComponent>
              {menu.icon && <i className={menu.icon} />}

              <span className="title">{menu.title}</span>

              {menu.children && (
                  <span className={`arrow ${isActive ? 'active open' : ''}`} />
              )}
            </LinkComponent>

            {menu.children && (
                <ul className="sub-menu">{menu.children.map(this.renderMenuItem)}</ul>
            )}
          </li>
      );
    }

  };

  render() {
    const { intl, isOpenSidebar } = this.props;
    const navLinks = getNavLinks({ intl });

    return (
      <div className="page-sidebar-wrapper">
        <div className="page-sidebar navbar-collapse collapse">
          <ul
            className={`page-sidebar-menu  page-header-fixed  ${
                isOpenSidebar ? '' : 'page-sidebar-menu-closed'
            }`}
            data-keep-expanded="false"
            data-auto-scroll="true"
            data-slide-speed="200"
          >
            {navLinks.map(this.renderMenuItem)}
          </ul>
        </div>
      </div>
    );
  }
}

export default injectIntl(Sidebar);
