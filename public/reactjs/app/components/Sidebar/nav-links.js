import messages from './messages';
import routePaths from 'settings/route-paths';

const getNavLinks = ({ intl }) => {
  return [
    // Resources
    {
      title: intl.formatMessage(messages.ResourcesPage),
      icon: 'icon-docs',
      children: [
        { title: 'Overview', link: routePaths.resources.list },
        {
          title: intl.formatMessage(messages.ImportPage),
          link: routePaths.resources.import
        }
      ]
    },
    {
      title: intl.formatMessage(messages.SchedulingPage),
      icon: 'fa fa-calendar',
      activePath: routePaths.playlists.list,
      children: [
          { title: 'Overview', link: routePaths.playlists.overview },
          { title: 'Playlist', link: routePaths.playlists.list },
          {
            title: intl.formatMessage(messages.EventBlocksPage), link: routePaths.eventBlocks.list,
            activePath: routePaths.eventBlocks.list,
            children: [
              { title: 'List', link: routePaths.eventBlocks.list },
              { title: 'Add new', link: routePaths.eventBlocks.createNew }
            ]
          },
      ]
    },
    {
      title: intl.formatMessage(messages.ContentTypesPage),
      icon: 'icon-hourglass',
      activePath: routePaths.contentTypes.list,
      children: [
        { title: 'List', link: routePaths.contentTypes.list },
        { title: 'Add new', link: routePaths.contentTypes.createNew }
      ]
    },
    {
      title: intl.formatMessage(messages.ThemesPage),
      icon: 'fa fa-film',
      activePath: routePaths.themes.list,
      children: [
        {
          title: intl.formatMessage(messages.ThemesTablePage),
          link: routePaths.themes.list
        },
        {
          title: intl.formatMessage(messages.CreateThemePage),
          link: routePaths.themes.createNew
        }
      ]
    },
    {
      title: intl.formatMessage(messages.FormatsPage),
      icon: 'icon-film',
      link: routePaths.formats.list,
      activePath: routePaths.formats.list,
      children: [
        {
          title: intl.formatMessage(messages.FormatsListPage),
          link: routePaths.formats.list
        },
        {
          title: intl.formatMessage(messages.CreateFormatPage),
          link: routePaths.formats.createNew
        }
      ]
    },
    {
      title: 'Scheduler settings',
      icon: 'icon-settings',
      link: routePaths.pages.settings
    },
    {
      title: intl.formatMessage(messages.SystemRolesPage),
      icon: 'icon-wrench',
      link: routePaths.users.list,
      activePath: routePaths.users.list,
      children: [
        // User
        {
          title: 'User',
          link: routePaths.users.list,
          activePath: routePaths.users.list,
          children: [
            {
              title: 'Overview',
              link: routePaths.users.list
            },
            {
              title: 'System Roles',
              link: routePaths.roles.assignments,
              activePath: routePaths.roles.assignments
            }
          ]
        },
        // Channels
        {
          title: 'Channels',
          activePath: routePaths.channels.list,
          children: [
            { title: 'List', link: routePaths.channels.list },
            { title: 'Add new', link: routePaths.channels.createNew }
          ]
        },
        //System settings
        {
          title: 'Settings',
          link: routePaths.pages.system_settings
        }
      ]
    }
  ]; // Scheduling // Content types // Themes // Scheduler settings // System
};

export default getNavLinks;
