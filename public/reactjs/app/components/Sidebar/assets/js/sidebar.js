import $ from 'jquery';

/**
Core script to handle the entire theme and core functions
* */

const App = (function() {
  return {
    scrollTo(el, offeset) {
      let pos = el && el.length > 0 ? el.offset().top : 0;

      if (el) {
        if ($('body').hasClass('page-header-fixed')) {
          pos -= $('.page-header').height();
        } else if ($('body').hasClass('page-header-top-fixed')) {
          pos -= $('.page-header-top').height();
        } else if ($('body').hasClass('page-header-menu-fixed')) {
          pos -= $('.page-header-menu').height();
        }
        pos += offeset || -1 * el.height();
      }

      $('html,body').animate({ scrollTop: pos }, 'slow');
    },

    getViewPort() {
      let e = window;
      let a = 'inner';
      if (!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
      }

      return { width: e[`${a}Width`], height: e[`${a}Height`] };
    },
    getResponsiveBreakpoint(size) {
      // bootstrap responsive breakpoints
      const sizes = { xs: 480, sm: 768, md: 992, lg: 1200 };
      // extra small // small // medium // large

      return sizes[size] ? sizes[size] : 0;
    }
  };
})();

const Layout = (function() {
  const resBreakpointMd = App.getResponsiveBreakpoint('md'); // 992px
  // handle go to top button
  const handleGo2Top = function() {
    const Go2TopOperation = function() {
      const CurrentWindowPosition = $(window).scrollTop(); // current vertical position
      if (CurrentWindowPosition > 100) {
        $('.go2top').show();
      } else {
        $('.go2top').hide();
      }
    };

    Go2TopOperation(); // call headerFix() when the page was loaded
    if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
      $(window).bind('touchend touchcancel touchleave', () => {
        Go2TopOperation();
      });
    } else {
      $(window).scroll(() => {
        Go2TopOperation();
      });
    }

    $('.go2top').click(e => {
      e.preventDefault();
      $('html, body').animate({ scrollTop: 0 }, 600);
    });
  };

  // Handle sidebar menu
  const handleSidebarMenu = function() {
    $('.page-sidebar').on('click', 'li > a', function(e) {
      if (
        $(this)
          .next()
          .hasClass('sub-menu') === false
      ) {
        if (
          App.getViewPort().width < resBreakpointMd &&
          $('.page-sidebar').hasClass('in')
        ) {
          // close the menu on mobile view while laoding a page
          $('.page-header .responsive-toggler').click();
        }
        return;
      }

      const parent = $(this)
        .parent()
        .parent();
      const the = $(this);
      const menu = $('.page-sidebar-menu');
      const sub = $(this).next();

      const autoScroll = menu.data('auto-scroll');
      const slideSpeed = parseInt(menu.data('slide-speed'));
      const keepExpand = menu.data('keep-expanded');

      if (keepExpand !== true) {
        parent
          .children('li.open')
          .children('a')
          .children('.arrow')
          .removeClass('open');
        parent
          .children('li.open')
          .children('.sub-menu:not(.always-open)')
          .slideUp(slideSpeed);
        parent.children('li.open').removeClass('open');
      }

      const slideOffeset = -200;

      if (sub.is(':visible')) {
        $('.arrow', $(this)).removeClass('open');
        $(this)
          .parent()
          .removeClass('open');
        sub.slideUp(slideSpeed, () => {
          if (
            autoScroll === true &&
            $('body').hasClass('page-sidebar-closed') === false
          ) {
            if ($('body').hasClass('page-sidebar-fixed')) {
              menu.slimScroll({
                scrollTo: the.position().top
              });
            } else {
              App.scrollTo(the, slideOffeset);
            }
          }
        });
      } else {
        $('.arrow', $(this)).addClass('open');
        $(this)
          .parent()
          .addClass('open');
        sub.slideDown(slideSpeed, () => {
          if (
            autoScroll === true &&
            $('body').hasClass('page-sidebar-closed') === false
          ) {
            if ($('body').hasClass('page-sidebar-fixed')) {
              menu.slimScroll({
                scrollTo: the.position().top
              });
            } else {
              App.scrollTo(the, slideOffeset);
            }
          }
        });
      }

      e.preventDefault();
    });

    // handle scrolling to top on responsive menu toggler click when header is fixed for mobile view
    $(document).on(
      'click',
      '.page-header-fixed-mobile .responsive-toggler',
      () => {
        App.scrollTop();
      }
    );
  };

  return {
    // Main init methods to initialize the layout
    // IMPORTANT!!!: Do not modify the core handlers call order.

    init() {
      handleGo2Top();
      handleSidebarMenu();
    }
  };
})();

const SidebarApp = Layout;
export { SidebarApp };
