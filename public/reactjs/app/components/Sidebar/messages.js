import { defineMessages } from 'react-intl';

export default defineMessages({
  DashboardPage: {
    id: 'odmedia.components.Sidebar.DashboardPage',
    defaultMessage: 'Dashboard & Notifications'
  },
  SchedulingPage: {
    id: 'odmedia.components.Sidebar.SchedulingPage',
    defaultMessage: 'Scheduling'
  },
  ContentTypesPage: {
    id: 'odmedia.components.Sidebar.ContentTypesPage',
    defaultMessage: 'Content types'
  },
  ThemesPage: {
    id: 'odmedia.components.Sidebar.ThemesPage',
    defaultMessage: 'Themes'
  },
  ThemesTablePage: {
    id: 'odmedia.components.Sidebar.ThemesTablePage',
    defaultMessage: 'List'
  },
  ResourcesPage: {
    id: 'odmedia.components.Sidebar.ResourcesPage',
    defaultMessage: 'Resources'
  },
  SystemRolesPage: {
    id: 'odmedia.components.Sidebar.SystemRolesPage',
    defaultMessage: 'System'
  },
  SettingsPage: {
    id: 'odmedia.components.Sidebar.SettingsPage',
    defaultMessage: 'Scheduler Settings'
  },
  SettingsChannelPage: {
    id: 'odmedia.components.Sidebar.SettingsChannelPage',
    defaultMessage: 'General'
  },
  SupportPage: {
    id: 'odmedia.components.Sidebar.SupportPage',
    defaultMessage: 'Support'
  },
  CreateThemePage: {
    id: 'odmedia.components.Sidebar.CreateThemePage',
    defaultMessage: 'Add new'
  },
  ImportPage: {
    id: 'odmedia.components.Sidebar.ImportPage',
    defaultMessage: 'Import'
  },
  UploadPage: {
    id: 'odmedia.components.Sidebar.UploadPage',
    defaultMessage: 'Upload'
  },
  FormatsPage: {
    id: 'odmedia.components.Sidebar.FormatsPage',
    defaultMessage: 'Formats'
  },
  FormatsListPage: {
    id: 'odmedia.components.Sidebar.FormatsListPage',
    defaultMessage: 'List'
  },
  CreateFormatPage: {
    id: 'odmedia.components.Sidebar.CreateFormatPage',
    defaultMessage: 'Add new'
  },
  EventBlocksPage: {
    id: 'odmedia.components.Sidebar.EventBlocksPage',
    defaultMessage: 'Event Blocks'
  },
  EventBlocksListPage: {
    id: 'odmedia.components.Sidebar.EventBlocksListPage',
    defaultMessage: 'List'
  },
  CreateEventBlockPage: {
    id: 'odmedia.components.Sidebar.CreateEventBlockPage',
    defaultMessage: 'Add new'
  },
});
