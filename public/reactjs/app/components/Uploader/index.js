import Dropzone from 'react-dropzone';
import {ItemUploader} from './ItemUploader';
import PropTypes from 'prop-types';
import React from 'react';
import toast from '../../helpers/user-interface/toast';
import withAsync from '../../helpers/react/withAsync/withAsync';
import systemSettingsSchema from '../../containers/SettingsPage/components/SystemForm/schema';
import apiSettings from 'services/api/settings';

const byteToMb = 1024 * 1024;

@withAsync()
export class Uploader extends React.PureComponent {
  static propsTypes = {
    accept: PropTypes.string,
    maxSize: PropTypes.number, // Max file size in MB,
    multiple: PropTypes.bool,
    name: PropTypes.string,
    params: PropTypes.object
  };
  static async preload(props) {
    const settingsData = await apiSettings.getSystemSettings({});

    return {
      settingsData
    };
  }

  static defaultProps = {
    maxSize: 10,
    name: 'file',
    accept: 'video/mp4',
    multiple: true,
  };

  state = {
    accepted: []
  };

  onDrop = (fileList) => {
    const {accepted} = this.state;
    if (accepted.length && !this.props.multiple) {
      toast.error('Single file only');
      return;
    }
    fileList.map(file => {
      if (this.validateMediaFile(file)) {
        file.uid = Math.random();
        accepted.push(file);
      }
    });
    this.forceUpdate();
  };

  validateMediaFile(file) {
    // If have any special requirements for files put it here
    return true;
  }

  onDropRejected = (fileList) => {
    const {maxSize} = this.props;
    fileList.forEach(file => {
      if (file.size > maxSize * byteToMb) {
        toast.error(`Please select file not exceed ${maxSize} MB`);
      } else {
        toast.error('File not supported');
      }
    });
  };

  onRemoveClick(id) {
    this.state.accepted.splice(id, 1);
    this.forceUpdate();
  }

  render() {
    const {accept, maxSize, multiple, name, preloadData, params} = this.props;
    const {settingsData} = preloadData;
    const {accepted} = this.state;
    return (
        <div className="uploader-box">
          {
            accepted.length !== 0 ?
                <div className="uploading-list row">
                  <div className="uploader-box__inner col-lg-offset-3 col-lg-6">
                    <span>File list ({accepted.length})</span>
                    <div className="clearfix"/>
                    <ul>
                      {
                        accepted.map((f, id) =>
                            <li key={f.uid}>
                              <ItemUploader
                                  name={name}
                                  file={f}
                                  uploadInstantly
                                  onRemoveClick={() => this.onRemoveClick(id)}
                                  params={params}
                              />
                            </li>
                        )
                      }
                    </ul>
                  </div>
                </div> : null
          }
          <div className="uploader__drop-zone">
            <Dropzone
                className="drop-zone"
                ref={el => this.dropZone = el}
                accept={accept}
                maxSize={maxSize * byteToMb}
                multiple={multiple}
                name={name}
                onDrop={this.onDrop}
                onDropRejected={this.onDropRejected}
            >
              <p>Drag and drop your files here</p>
              <span>Or</span>
              <button
                  type="button"
                  className="btn green uploader__btn"
                  onClick={(event) => {
                    event.stopPropagation();
                    this.dropZone.open();
                  }}>Click to add files
              </button>
              <div className="notices-upload">
                <span>Maximum upload file size: {settingsData.maximum_upload}MB</span>
                <span>Content extensions: {settingsData.content_type_extensions}</span>
                <span>Overlay extensions: {settingsData.overlay_type_extensions}</span>
              </div>
            </Dropzone>
          </div>
        </div>
    );
  }
}

export default Uploader;
