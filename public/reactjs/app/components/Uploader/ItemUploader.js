import PropTypes from 'prop-types';
import {Line} from 'rc-progress';
import {utils} from 'helpers/utils/index';
import React from 'react';
import resourcesApiService from '../../services/api/resources';
import {makeSelectChannel} from 'store/app/selectors';
import {createStructuredSelector} from 'reselect';
import {connect} from 'react-redux';
import toast from '../../helpers/user-interface/toast';
import lodash from 'lodash';

@connect(createStructuredSelector({
  currentChannel: makeSelectChannel()
}))
export class ItemUploader extends React.PureComponent {

  static propsTypes = {
    file: PropTypes.object.isRequired,
    uploadInstantly: PropTypes.bool,
    params: PropTypes.object.isRequired
  };

  state = {
    loaded: 0,
    savedData: null,
    error: false,
    total: 0,
    fileSize: 0,
    percent: 0
  };

  get type() {
    const file = this.props.file;
    if (file.type.indexOf('video') !== -1) {
      return 'video';
    } else if (file.type.indexOf('image') !== -1) {
      return 'image';
    }
    return null;
  }

  get isComplete() {
    return this.state.percent === 100 && this.state.savedData;
  }

  componentDidMount() {
    this.fileInfo();
  }

  componentWillUpdate(nextProps, nextState)
  {
    if (nextProps.file !== this.props.file) {
      this.props = nextProps;
      this.fileInfo();
    }
  }

  fileInfo() {
    const {file, uploadInstantly} = this.props;
    if (file.size) {
      this.setState({
        fileSize: Math.round(file.size / 1000),
        error: null
      });
    }
    if (uploadInstantly) {
      this.uploadFile(file);
    }
  }

  async doUpload(file) {
    const body = {};
    body.file = file;
    body.channel_id = this.props.currentChannel.id;
    body.only_overlay_resources = this.props.params.only_overlay_resources;
    body.only_content_resources = this.props.params.only_content_resources;
    body.resource_folder_id = this.props.params.resource_folder_id;
    body.upload_type = this.props.params.upload_type;
    return resourcesApiService.upload(body, this.onUploadProgress);
  }

  async uploadFile(file) {
    try {
      const data = await this.doUpload(file);
    } catch (err) {
      lodash.map(err.errors, m => toast.error(m));
      this.setState({
        error: err.errors
      });
    }
  }

  onUploadProgress = (event, cancelSource) => {
    this.cancelSource = cancelSource;
    const percentage = event.loaded / event.total * 100;
    this.setState({
      loaded: Math.floor(event.loaded / 1024),
      total: Math.floor(event.total / 1024),
      percent: Math.floor(percentage)
    });
  };

  render() {
    const file = this.props.file;
    const {loaded, total, percent, fileSize} = this.state;
    return (
        <div className={this.state.error ? 'uploader--error' : ''}>
          <div className="uploader__img">
            <img src={file.preview} alt=""/>
          </div>
          <div className="uploader__info">
            <div>{file.name}</div>
            <Line percent={percent} strokeWidth="4" strokeColor={this.state.error ? '#f00' : '#D3D3D3'}/>
            <div>
              <span>{utils.pointNumber(loaded, ',')} kb of {utils.pointNumber(total || fileSize, ',')} kb </span>
              {
                this.state.error ?
                    <span>(Upload error)</span> : this.isComplete ?
                    <span>(Completed)</span> :
                    <span>({percent} % Completed)</span>
              }
            </div>
            {
              this.state.savedData ? <input type="hidden" name="uploadFiles" value={this.state.savedData}/> : null
            }
            {
              this.isComplete ?
                  <span className="remove-btn hand" onClick={this.props.onRemoveClick}>
                    <i className="fa fa-trash"/>
                  </span> :
                  <span className="remove-btn hand" onClick={this.cancelUploadAndRemove}>
                    <i className="fa fa-remove"/>
                  </span>
            }
          </div>
        </div>

    );
  }

  cancelUpload() {
    if (this.cancelSource) {
      this.cancelSource.cancel('Canceled upload');
    }
  }

  // Abort upload if component was unmmounted
  componentWillUnmount() {
    this.cancelUpload();
  }

  cancelUploadAndRemove = () => {
    this.cancelUpload();
    this.props.onRemoveClick();
  }
}

