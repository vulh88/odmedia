import React from 'react';
import PropTypes from 'prop-types';
import { timeLeft } from 'helpers/utils/index';
import {formatDuration} from '../../helpers/utils';

class ElementBox extends React.PureComponent {
  static propTypes = {
    className: PropTypes.string,
    name: PropTypes.string,
    time: PropTypes.number,
    img: PropTypes.string,
    onClickEdit: PropTypes.func,
    onClickTrash: PropTypes.func
  };

  render() {
    const {
      className,
      name,
      img,
      time,
      onClickEdit,
      onClickTrash
    } = this.props;

    return (
      <div className={`element-box ${className || ''}`}>
        <div className="element-background">
          <img src={img} alt="" />
        </div>

        <div className="element-control">
          <a onClick={onClickEdit}>
            <i className="fa fa-pencil" />
          </a>
          <a onClick={onClickTrash}>
            <i className="fa fa-trash" />
          </a>
        </div>

        <div className="element-info">
          <div>{formatDuration(time)}</div>
          <div className="element-info--tag btn red">ACTIVE</div>
        </div>
        <div className="element-name">{name}</div>
      </div>
    );
  }
}

export default ElementBox;
