import React from 'react';
import { FormattedMessage } from 'react-intl';

import LocaleToggle from 'containers/LocaleToggle';
import messages from './messages';

class Footer extends React.Component {
  render() {
    return (
      <div className="page-footer">
        {/*<LocaleToggle className="inline-block" />{' '}*/}
        <div className="page-footer-inner">
          2018 ©
          <FormattedMessage
            {...messages.authorMessage}
            values={{ author: 'ODArrange' }}
          />
        </div>
      </div>
    );
  }
}

export default Footer;
