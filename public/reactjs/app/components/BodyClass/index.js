import React from 'react';
import PropTypes from 'prop-types';

class BodyClass extends React.PureComponent {
  static propsTypes = {
    classList: PropTypes.string
  };

  classList = null;

  componentDidUpdate() {
    this.updateClassList();
  }

  componentDidMount(){
    this.updateClassList();
  }

  updateClassList() {
    if (this.classList) {
      document.body.classList.remove(this.classList);
    }
    document.body.classList.add(this.props.classList);
    this.classList = this.props.classList;
  }

  render() {
    return null;
  }
}

export default BodyClass;
