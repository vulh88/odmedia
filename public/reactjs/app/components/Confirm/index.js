import React from 'react';

export class ConfirmModal extends React.PureComponent {


  confirm(message, options = {}) {
    return (new Promise((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
    }));
  }

  open() {

  }

  render() {
    return (
        <div id="static" className="modal fade in" tabIndex="-1" data-backdrop="static" data-keyboard="false"
             style="display: block; padding-right: 15px;">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true"/>
                <h4 className="modal-title">Confirmation</h4>
              </div>
              <div className="modal-body">
                <p> Would you like to continue with some arbitrary task? </p>
              </div>
              <div className="modal-footer">
                <button type="button" data-dismiss="modal" className="btn dark btn-outline">Cancel</button>
                <button type="button" data-dismiss="modal" className="btn green">Continue Task</button>
              </div>
            </div>
          </div>
        </div>
    );
  }
}