import React from 'react';
import PropTypes from 'prop-types';

class TableHeaderView extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    onClick: PropTypes.func,
    field: PropTypes.string,
    sortingHeader: PropTypes.string,
    sort: PropTypes.bool
  };

  state = {
    sorting: false
  };

  onSortHeader = () => {
    this.setState({
      sorting: !this.state.sorting
    });

    this.props.onClick();
  };

  render() {
    const {
      children,
      className,
      field,
      sortingHeader,
      sort,
      styles
    } = this.props;
    const { sorting } = this.state;
    let classSort = field && sort ? 'sorting' : '';
    if (field && sortingHeader === field) {
      const sortType = sorting ? 'sorting_asc' : 'sorting_desc';
      classSort = sortType;
    }

    const classNamesHeader = `
    cell th
    ${className || ''}
    ${classSort}`;

    return (
      <div
        className={classNamesHeader}
        onClick={this.onSortHeader}
        style={styles}
      >
        <strong>{React.Children.toArray(children)}</strong>
      </div>
    );
  }
}

export default TableHeaderView;
