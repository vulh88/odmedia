import React from 'react';
import ReactPaginate from 'react-paginate';
import PropTypes from 'prop-types';

class TableFooterView extends React.PureComponent {
  static propTypes = {
    entries: PropTypes.number,
    totalPage: PropTypes.number,
    totalUsers: PropTypes.number,
    currentPage: PropTypes.number,
    onChangePage: PropTypes.func,
    searching: PropTypes.bool
  };

  handlePageClick = ({ selected }) => {
    const { onChangePage } = this.props;
    onChangePage(selected);
  };

  render() {
    const {
      entries,
      totalUsers,
      currentPage,
      searching,
      totalPage
    } = this.props;

    const itemStart = currentPage * entries + 1;
    const itemEnd = currentPage * entries + totalUsers;
    const itemEndResult = searching || entries === -1 ? totalUsers : itemEnd;

    const info = `Showing ${itemStart} to ${itemEnd} of ${itemEndResult} entries`;

    return (
      <div className="row table-footer">
        <div className="col-md-5 col-sm-12">
          <div className="info">{info}</div>
        </div>

        <div className="col-md-7 col-sm-12 text-right">
          <div>
            <ReactPaginate
              pageCount={totalPage}
              previousLabel={<i className="fa fa-angle-left" />}
              nextLabel={<i className="fa fa-angle-right" />}
              breakLabel={<a>...</a>}
              breakClassName="break-me"
              containerClassName="pagination"
              activeClassName="active"
              marginPagesDisplayed={1}
              pageRangeDisplayed={2}
              forcePage={Math.max(currentPage, 0)}
              onPageChange={this.handlePageClick}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default TableFooterView;
