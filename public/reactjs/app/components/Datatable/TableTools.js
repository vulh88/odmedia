import React from 'react';
import PropTypes from 'prop-types';

class TableHeaderView extends React.PureComponent {
  static propTypes = {
    onChangeEntry: PropTypes.func,
    onChangeSearch: PropTypes.func
  };

  render() {
    return (
      <div className="row">
        <div className="col-sm-12 col-md-6">
          <div className="dataTables_length">
            <label>
              <select
                className="form-control"
                onChange={this.props.onChangeEntry}
              >
                <option value="10">10</option>
                <option value="-1">All</option>
              </select>
              entries
            </label>
          </div>
        </div>
        <div className="col-sm-12 col-md-6">
          <div className="dataTables_filter text-right">
            <label>
              <span>Search:</span>
              <input
                type="search"
                className="form-control input-sm input-small input-inline"
                placeholder=""
                onChange={this.props.onChangeSearch}
              />
            </label>
          </div>
        </div>
      </div>
    );
  }
}

export default TableHeaderView;
