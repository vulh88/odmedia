import React from 'react';
import PropTypes from 'prop-types';

const TableCellView = ({
  children,
  className,
  overflowNone,
  headerResponsive,
  styles
}) => {
  const classNamesCell = `
  cell td
  ${overflowNone ? 'overflow-none' : ''}
  ${className || ''}`;

  return (
    <div className={classNamesCell} style={styles}>
      <div className="cell-header">{headerResponsive}:</div>
      <div>{React.Children.toArray(children)}</div>
    </div>
  );
};

TableCellView.propTypes = {
  children: PropTypes.node,
  headerResponsive: PropTypes.string.isRequired,
  className: PropTypes.string,
  overflowNone: PropTypes.bool,
  style: PropTypes.object
};

export default TableCellView;
