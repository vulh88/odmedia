import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import LoadingIndicator from 'components/LoadingIndicator';

import TableTools from './TableTools';
import TableHeader from './TableHeader';
import TableRow from './TableRow';
import TableCell from './TableCell';
import TableFooter from './TableFooter';

class Datatable extends React.PureComponent {
  static propTypes = {
    cols: PropTypes.number.isRequired,
    data: PropTypes.array.isRequired,
    cells: PropTypes.array.isRequired,
    actionsTable: PropTypes.object,
    nameTable: PropTypes.string,
    tools: PropTypes.bool,
    loading: PropTypes.bool,
    totalPage: PropTypes.number,
    totalData: PropTypes.number,
    currentPage: PropTypes.number,
    limitPage: PropTypes.number,
    onClickEdit: PropTypes.func,
    onClickTrash: PropTypes.func,
    onClickDelete: PropTypes.func,
    onClickRestore: PropTypes.func,
    onClickAssign: PropTypes.func,
    onClickChannel: PropTypes.func,
    handleChangePage: PropTypes.func
  };

  state = {
    pagination: true,
    entries: this.props.limitPage,
    currentPage: this.props.currentPage - 1,
    filteredUsers: this.props.data,
    totalUsers: this.props.data.length,
    searching: false,
    sorting: false,
    sortingHeader: null,
    idSelected: null
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      filteredUsers: nextProps.data,
      entries: nextProps.limitPage,
      currentPage: nextProps.currentPage - 1,
      totalUsers: nextProps.data.length
    });
  }

  onChangeEntry = e => {
    const entries = parseInt(e.target.value);
    this.setState({ entries });
  };

  onChangePage = page => {
    this.setState({ currentPage: page });
    this.props.handleChangePage(page + 1);
  };

  onChangeSearch = e => {
    const { data } = this.props;
    const { entries } = this.state;
    const value = e.target.value.toLowerCase();

    const users = data.filter(user => {
      if (value && value.length > 2) {
        if (user.first_name || user.last_name || user.email) {
          const type = user.first_name + user.last_name + user.email;
          return type.toLowerCase().indexOf(value) !== -1;
        }
      }
      return data;
    });

    this.setState({
      searching: value.length > 2,
      filteredUsers: users,
      totalUsers: users.length,
      pagination: users.length > 0 && entries !== -1
    });
  };

  onCompareBy(key) {
    const { sorting } = this.state;

    if (sorting) {
      return function(a, b) {
        if (a[key] < b[key]) return 1;
        if (a[key] > b[key]) return -1;
        return 0;
      };
    }

    return function(a, b) {
      if (a[key] < b[key]) return -1;
      if (a[key] > b[key]) return 1;
      return 0;
    };
  }

  onSortHeader = (keyHeader, sort) => {
    const { filteredUsers, sorting } = this.state;
    const arrayCopy = [...filteredUsers];
    const result = arrayCopy.sort(this.onCompareBy(keyHeader));

    if (keyHeader && sort) {
      this.setState({
        filteredUsers: result,
        sorting: !sorting,
        sortingHeader: keyHeader
      });
    }
  };

  handleConfirmModal = typeModal => {
    const { onClickTrash, onClickDelete } = this.props;
    const { filteredUsers, idSelected } = this.state;
    _.remove(filteredUsers, user => user.id === idSelected);
    this.setState({ filteredUsers });

    switch (typeModal) {
      case 'trash':
        onClickTrash(idSelected);
        break;

      case 'delete':
        onClickDelete(idSelected);
        break;
    }
  };

  renderAction = id => {
    const {
      actionsTable,
      onClickEdit,
      onClickRestore,
      onClickAssign,
      onClickChannel
    } = this.props;

    return (
      <ul className="wrapper-action">
        {actionsTable.refresh && (
          <li>
            <a className="btn default" onClick={() => onClickRestore(id)}>
              <i className="fa fa-repeat" />
            </a>
          </li>
        )}
        {actionsTable.edit && (
          <li>
            <a className="btn default" onClick={() => onClickEdit(id)}>
              <i className="fa fa-pencil" />
            </a>
          </li>
        )}
        {actionsTable.trash && (
          <li>
            <a
              className="btn default red"
              onClick={() =>
                this.setState({
                  idSelected: id
                })
              }
              data-toggle="modal"
              href="#confirmMoveTrash"
            >
              <i className="fa fa-trash" />
            </a>
          </li>
        )}
        {actionsTable.delete && (
          <li>
            <a
              className="btn default red"
              onClick={() =>
                this.setState({
                  idSelected: id
                })
              }
              data-toggle="modal"
              href="#confirmDelete"
            >
              <i className="fa fa-remove" />
            </a>
          </li>
        )}
        {actionsTable.assign && (
          <li>
            <a className="btn default" onClick={() => onClickAssign(id)}>
              <i className="fa fa-shield" />
            </a>
          </li>
        )}
        {actionsTable.channel && (
          <li>
            <a className="btn default" onClick={() => onClickChannel(id)}>
              Assign channel
            </a>
          </li>
        )}
      </ul>
    );
  };

  renderTableHeader = () => {
    const { cells } = this.props;
    const { sortingHeader } = this.state;

    return (
      <TableRow>
        {cells.map(cell => (
          <TableHeader
            key={cell.name}
            field={cell.field}
            sortingHeader={sortingHeader}
            sort={cell.sort}
            styles={cell.style}
            onClick={() => this.onSortHeader(cell.field, cell.sort)}
          >
            {cell.name}
          </TableHeader>
        ))}
      </TableRow>
    );
  };

  renderTableCell = item => {
    const { cells } = this.props;

    return cells.map(cell => {
      if (cell.field === 'actions') {
        return (
          <TableCell
            key={Math.random()}
            headerResponsive={cell.name}
            styles={cell.style}
          >
            {this.renderAction(item.id)}
          </TableCell>
        );
      }

      return (
        <TableCell
          key={Math.random()}
          headerResponsive={cell.name}
          styles={cell.style}
        >
          {item[cell.field]}
        </TableCell>
      );
    });
  };

  renderTableRow = () => {
    const { filteredUsers } = this.state;

    if (filteredUsers.length) {
      return filteredUsers.map(item => (
        <TableRow key={Math.random()}>{this.renderTableCell(item)}</TableRow>
      ));
    }

    return (
      <div className="no-result">
        <span>No Result</span>
      </div>
    );
  };

  render() {
    const { cols, tools, totalPage, nameTable, loading } = this.props;
    const {
      pagination,
      entries,
      currentPage,
      totalUsers,
      searching
    } = this.state;

    if (loading) {
      return <LoadingIndicator />;
    }

    return (
      <div className="portlet light bordered table-custom">
        <div className="portlet-body">
          {tools && (
            <TableTools
              onChangeEntry={this.onChangeEntry}
              onChangeSearch={this.onChangeSearch}
            />
          )}

          <div className={`table col-${cols}`}>
            {this.renderTableHeader()}
            {this.renderTableRow()}
          </div>

          {pagination && (
            <TableFooter
              entries={entries}
              totalPage={totalPage}
              totalUsers={totalUsers}
              currentPage={currentPage}
              onChangePage={this.onChangePage}
              searching={searching}
            />
          )}
        </div>

        <div
          className="modal fade"
          id="confirmMoveTrash"
          tabIndex="-1"
          role="confirmMoveTrash"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-hidden="true"
                />
                <h4 className="modal-title">Move {nameTable} to trash</h4>
              </div>
              <div className="modal-body">
                Are you sure you want to move trash {nameTable}?
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn dark btn-outline"
                  data-dismiss="modal"
                >
                  No
                </button>
                <button
                  type="button"
                  className="btn green"
                  onClick={() => this.handleConfirmModal('trash')}
                  data-dismiss="modal"
                >
                  Yes
                </button>
              </div>
            </div>
          </div>
        </div>

        <div
          className="modal fade"
          id="confirmDelete"
          tabIndex="-1"
          role="confirmDelete"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-hidden="true"
                />
                <h4 className="modal-title">Delete {nameTable}</h4>
              </div>
              <div className="modal-body">
                Are you sure you want to delete {nameTable}?
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn dark btn-outline"
                  data-dismiss="modal"
                >
                  No
                </button>
                <button
                  type="button"
                  className="btn green"
                  onClick={() => this.handleConfirmModal('delete')}
                  data-dismiss="modal"
                >
                  Yes
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Datatable;
