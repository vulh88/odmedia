import React from 'react';
import PropTypes from 'prop-types';

const TableRowView = ({ children, hightLight, className, rowRef }) => {
  const classNamesRow = `
  table-row
  ${hightLight ? 'hight-light' : ''}
  ${className || ''}`;
  return (
    <div ref={rowRef} className={classNamesRow}>
      {React.Children.toArray(children)}
    </div>
  );
};

TableRowView.propTypes = {
  rowRef: PropTypes.func,
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  hightLight: PropTypes.bool
};

export default TableRowView;
