import React from 'react';

function BlockLoading({message, isCover}) {
  return (
      <div className={`block-loading ${isCover ? 'wrapper-loading' : ''}`}>
        <div className={`m-blockui ${!message ? 'bg-transparent' : ''}`}>
          {message && <span>{message}</span>}
          <span><div className="m-loader  m-loader--success m-loader--lg"/></span>
        </div>
      </div>
  )
}

export default BlockLoading;
