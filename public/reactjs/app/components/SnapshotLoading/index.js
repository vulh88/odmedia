import React from 'react';
import BlockLoading from '../BlockLoading';

class SnapshotLoading extends React.PureComponent {

  static cache = {};

  snapshot = this.props.name ? SnapshotLoading.cache[this.props.name] : null;

  componentWillReceiveProps(nextProps) {
    if (nextProps.loading === true && this.props.loading === false) {
      const html = this.root.innerHTML;
      this.snapshot = () => (
          <div style={{position: 'relative'}}>
            <div className="wrapper-loading">
              <BlockLoading/>
            </div>
            <div dangerouslySetInnerHTML={{__html: html}}/>
          </div>
      );
      if (this.props.name) {
        SnapshotLoading.cache[this.props.name] = this.snapshot;
      }
    }
  }

  render() {
    const {loading, children} = this.props;

    if (loading) return this.snapshot ? React.createElement(this.snapshot) :  <BlockLoading/>;

    return (
        <div ref={e => this.root = e}>
          {
            children(this.props)
          }
        </div>
    )
  }
}
export default SnapshotLoading;
