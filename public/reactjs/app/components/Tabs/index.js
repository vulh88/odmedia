import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import routePaths from '../../settings/route-paths';

export const Tabs = ({ labelList, indexActive, className }) => (
  <div className={`portlet box light ${className || ''}`}>
    <div className="portlet-body">
      <div className="tabbable-line">
        <ul className="nav nav-tabs">
          {labelList.map((item, index) => (
            <li
              key={item.label}
              className={`${
                index === indexActive ? 'active' : ''
              } ${item.classNameTab || ''}
              `}
            >
              <a
                href={`#${item.linkTo}`}
                data-toggle="tab"
                aria-expanded="false"
              >
                {item.label}
              </a>
            </li>
          ))}
        </ul>
        <div className="tab-content">
          {labelList.map((item, index) => (
            <div
              key={item.label}
              id={item.linkTo}
              className={`tab-pane ${
                index === indexActive ? 'active' : ''
              } ${item.classNameTabContent || ''}
              `}
            >
              {item.content}
            </div>
          ))}
        </div>
      </div>
    </div>
  </div>
);

Tabs.propTypes = {
  labelList: PropTypes.array.isRequired,
  indexActive: PropTypes.number.isRequired
};

export const TabsLink = ({ labelList, indexActive, className }) => {
  return (
    <div className={`portlet box light ${className || ''}`}>
      <div className="portlet-body">
        <div className="tabbable-line">
          <ul className="nav nav-tabs">
            {labelList.map((item, index) => {
              if (item.hidden) {
                return null;
              }
              return (
                <li
                  key={item.label}
                  className={`${
                    index === indexActive ? 'active' : ''
                  } ${item.classNameTab || ''}
              `}
                >
                  <Link to={{ pathname: item.linkTo, state: item.state }}>
                    {item.label}
                  </Link>
                </li>
              );
            })}
          </ul>
          <div className="tab-content">
            {labelList.map((item, index) => (
              <div
                key={item.label}
                className={`tab-pane ${
                  index === indexActive ? 'active' : ''
                } ${item.classNameTabContent || ''}
              `}
              >
                {index === indexActive && item.content}
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

TabsLink.propTypes = {
  labelList: PropTypes.array.isRequired,
  indexActive: PropTypes.number.isRequired
};


export const TabsLayout = ({ labelList, className, children }) => {
  return (
      <div className={`portlet box light ${className || ''}`}>
        <div className="portlet-body">
          <div className="tabbable-line">
            <ul className="nav nav-tabs">
              {labelList.map((item, index) => {
                return (
                    <li
                        key={index}
                        className={item.isActive ? 'active' : ''}
                    >
                      <Link to={item.link}>
                        {item.name}
                      </Link>
                    </li>
                );
              })}
            </ul>
            <div className="tab-content">
              {children}
            </div>
          </div>
        </div>
      </div>
  );
};

TabsLayout.propTypes = {
  labelList: PropTypes.array.isRequired,
  children: PropTypes.any.isRequired
};
