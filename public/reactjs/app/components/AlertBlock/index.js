import React from 'react';
import PropTypes from 'prop-types';
import lodash from 'lodash';

const alertStyles = {
  error: 'error',
  success: 'success',
  warning: 'warning',
  info: 'info'
};

const alertStylesData = {
  [alertStyles.error]: {
    icon: 'fa-exclamation-triangle',
    prefix: 'Error!',
    className: 'alert-danger'
  },
  [alertStyles.success]: {
    icon: 'fa-check-circle',
    prefix: 'Well done!',
    className: 'alert-success'
  },
  [alertStyles.warning]: {
    icon: 'fa-exclamation-triangle',
    prefix: 'Warning!',
    className: 'alert-warning'
  },
  [alertStyles.info]: {
    icon: 'fa-info-circle',
    prefix: 'Info!',
    className: 'alert-info'
  }
};

class AlertBlock extends React.PureComponent {

  static propsTypes = {
    prefix: PropTypes.bool,
    canClose: PropTypes.bool,
    messages: PropTypes.any,
    style: PropTypes.oneOf([lodash.map(alertStyles, e => e)])
  };

  static defaultProps = {
    prefix: false,
    canClose: true
  };

  static styles = alertStyles;


  state = {
    userClosed: false
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.messages !== this.props.messages) {
      this.setState({
        userClosed: false
      });
    }
  }

  handleUserClickClose = (e) => {
    e.stopPropagation();
    this.setState({
      userClosed: true
    });
  };

  render() {
    const {messages, style, canClose, prefix} = this.props;
    const {userClosed} = this.state;
    const styleData = alertStylesData[style];
    if (Array.isArray(messages) && messages.length) {
      return (
          <div
              className={`${styleData.className} alert m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert-dismissible ${userClosed ? 'hidden' : ''}`}>
            <div className="m-alert__icon">
              <i className={`fa font-white ${styleData.icon}`}/>
              <span/>
            </div>
            <div className="m-alert__text">
              {prefix &&
              <strong>{styleData.prefix} </strong>}
              <span>{lodash.map(messages, (e, idz) => <span style={{display: 'block'}} key={idz}>{e}</span>)}</span>
            </div>
            {
              canClose && <div className="m-alert__close">
                <button type="button" className="close" aria-label="Close" onClick={this.handleUserClickClose}>
                </button>
              </div>
            }
          </div>
      )
    }
    return null;
  }
}

export default AlertBlock;

