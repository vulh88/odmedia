import React from 'react';
import PropTypes from 'prop-types';
import Tree, { TreeNode } from 'rc-tree';
import 'rc-tree/assets/index.css';

class TreeFolder extends React.PureComponent {

  static propTypes = {
    keys: PropTypes.array,
    data: PropTypes.array,
  };
  static defaultProps = {
    keys: [0],
    data: [],
  };

  constructor(props) {
    super(props);
    const keys = props.keys;
    this.state = {
      checkedKeys: keys,
      selectedKeys: [],
      autoExpandParent: true,
      defaultExpandedKeys: keys,
      //defaultSelectedKeys: keys,
      defaultCheckedKeys: keys,
      //realKey: parseInt(keys[0])
    };
  }
  onExpand = (expandedKeys) => {

  };
  onSelect = (selectedKeys, info) => {
    //console.log('selected', selectedKeys, info);

  };
  onCheck = (checkedKeys, info) => {
    //console.log('onCheck', checkedKeys, info);

    const resourceFolderID = parseInt(info.node.props.eventKey);
    const data = {
      value : resourceFolderID,
      title : info.node.props.title,
    };
    this.setState(this.state);
    this.props.func.addRule(data);
    this.triggerClick([resourceFolderID]);

  };

  triggerClick = (arrKeys) => {
    this.setState({
      checkedKeys: arrKeys,
    });
  };

  render() {
    const {data} = this.props;
    const {...state} = this.state;
    return (
        <div>
          <Tree
              className={"tree-folder"}
              checkable
              defaultExpandAll
              onExpand={this.onExpand}
             // defaultSelectedKeys={this.state.defaultSelectedKeys}
            //  defaultCheckedKeys={this.state.defaultCheckedKeys}
            //  onSelect={this.onSelect}
              onCheck={this.onCheck}
              checkedKeys={state.checkedKeys}
              checkStrictly
          >

            {loopTree(data)}

          </Tree>
        </div>
    );
  }
}

const loopTree = data => {
  return data.map((item) => {
    if (item.children) {
      return (
          <TreeNode
              key={item.key} title={item.title}
          >
            {loopTree(item.children)}
          </TreeNode>
      );
    }
    return <TreeNode key={item.key} title={item.title} />;
  });
};

export default TreeFolder;