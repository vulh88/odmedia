<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::set('upload_extensions', 'jpg, gif, jpeg, png, mp3, mp4');
        Setting::set('maximum_upload', '10');
        Setting::set('content_type_extensions', 'mov');
        Setting::set('overlay_type_upload', 'mp4');
        Setting::save();
    }
}
