<?php

use Illuminate\Database\Eloquent\Collection;


trait AppHelpers
{
    /**
     * Convert To Bytes
     * @param $from
     * @return float|int
     */
    public static function convertToBytes($from){
        $number=substr($from,0,-2);
        switch(strtoupper(substr($from,-2))){
            case "KB":
                return $number*1024;
            case "MB":
                return $number*pow(1024,2);
            case "GB":
                return $number*pow(1024,3);
            case "TB":
                return $number*pow(1024,4);
            case "PB":
                return $number*pow(1024,5);
            default:
                return $from;
        }
    }

    /**
     * Convert MB To KB
     * @param $size
     * @return float|int
     */
    public static function convertMBtoKB($size )
    {
        return $size * 1000;
    }

    /**
     * Build tree folders
     * @param $data
     * @param int $parentId
     * @return array
     */
    public static function buildTreeFolders($data, $parentId = 0) {
        $branch = array();
        foreach ($data as $element) {
            if ($element->parent_id == $parentId) {
                $children = self::buildTreeFolders($data, $element->id);
                $element->key = $element->id;
                $element->title = $element->name;
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

    /**
     * Format length
     * @param $length
     * @return float|int
     */
    public static function formatLength($length)
    {
        $format = explode(':', $length);
        $formatDay = intval(str_replace('_', '',$format[0]));
        $formatHour = intval(str_replace('_', '',$format[1]));
        $formatMinute = intval(str_replace('_', '',$format[2]));
        $formatSecond = intval(str_replace('_', '',$format[3]));
        $seconds = ($formatDay * 86400) + ($formatHour * 3600) + ($formatMinute * 60) + $formatSecond;
        return $seconds;
    }

    /**
     * Format length from H:i:s
     * @param $length
     * @return float|int
     */
    public static function formatLengthHMS($length)
    {
        $format = explode(':', $length);
        $formatHour = intval(str_replace('_', '',$format[0]));
        $formatMinute = intval(str_replace('_', '',$format[1]));
        $formatSecond = intval(str_replace('_', '',$format[2]));
        $seconds =  ($formatHour * 3600) + ($formatMinute * 60) + $formatSecond;
        return $seconds;
    }

    /**
     * Parse Format length
     * length = second
     * @param $length
     * @return float|int
     */
    public static function parseFormatLength($length)
    {
        $d = floor($length / 86400);
        $H = floor($length / 3600 % 24 );
        $i = floor($length / 60 % 60);
        $s = floor($length % 60);
        return sprintf("%02d:%02d:%02d:%02d", $d, $H, $i, $s);
    }

    /**
     * Parse end time from format start time + length
     * @param $startTime
     * @param $seconds
     * @return string
     */
    public static function parseEndTimeFormat($startTime, $seconds)
    {
        $carbonDate = \Carbon\Carbon::parse($startTime)->addSeconds($seconds)->format('Y-m-d H:i:s');
        return $carbonDate;
    }

    /**
     * Get array extensions for image
     * @return array
     */
    public static function imageExtensions()
    {
        return [
            'png',
            'jpg',
            'gif',
            'jpeg'
        ];
    }

    /**
     * Rename file name
     * @param $file
     * @param $directory
     * @return string
     */
    public static function fileExists($file, $directory)
    {
        $fileName = strstr($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension(), true);
        $fileExtension = $file->getClientOriginalExtension();
        $i = 0;
        while( File::exists( $directory. '/' . $fileName . ($i == 0 ? '': '-' . $i) . '.' . $fileExtension ) )
        {
            $i++;
        }
        return $fileName . ($i == 0 ? '': '-' . $i) . '.' . $fileExtension;
    }

    /**
     * Create thumbnail for image
     * @param $realPath
     * @param $savePath
     * @return mixed
     */
    public static function generateThumbnail($realPath, $savePath)
    {
        $img = Image::make($realPath);
        $img->resize(config('thumbnail.resize.width'), config('thumbnail.resize.height'));
        $img->save($savePath);
        return $img;
    }

    /**
     * Create name for file
     * @param string $type
     * @return string
     */
    public static function generateFileName($type)
    {
        $name = sha1(str_random(30) . time());
        return  $type ? $name . '.' . $type : $name;
    }

    /**
     * Rename File
     * @param $fileName
     * @return string
     */
    public static function renameFile($fileName)
    {
        return strstr($fileName, '.', true);
    }

    /**
     * Use library ffmpeg to generated thumbnail for video
     * @param $fetchThumbFrame
     * @param $filePathName
     * @param $fullThumbPath
     */
    public function generateThumbByffmpeg($fetchThumbFrame, $filePathName, $fullThumbPath)
    {
        $width = config('thumbnail.resize.width');
        $height = config('thumbnail.resize.height');
        $generateThumb = "ffmpeg -ss " . $fetchThumbFrame . " -i " . $filePathName . " -vframes 1 -filter:v scale=" . $width . ":" . $height . " -vcodec png -an -y " . $fullThumbPath;
        exec($generateThumb);
    }

    /**
     * Get author name from current user
     * @return string
     */
    public static function getAuthorOfUser() {
        $user = Auth::user();
        return $user->first_name . ' ' . $user->last_name;
    }

    /**
     * Rename label for label permission
     * @param $label
     * @return string
     */
    public static function renameLabelPermission($label) {
        return  ucfirst(str_replace(['-', '_'], ' ', $label));
    }

    /**
     * Parse date time
     * @param $value
     * @param string $format
     * @return string
     */
    public static function parseDateTime($value, $format = 'Y-m-d H:i:s') {
        return \Carbon\Carbon::parse($value)->format($format);
    }
}