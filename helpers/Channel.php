<?php
use Modules\User\Repositories\Contracts\ChannelUsersRepositoryInterface;
use Illuminate\Support\Facades\Auth;

if (!function_exists('getDefaultOverlayTypeId')) {
    /**
     * Default overlay type ID if a user channel
     * @param $channelId
     * @param $userId
     * @return int
     */
    function getDefaultOverlayTypeId($channelId, $userId = '') {

        if (!$userId) {
            $userId = Auth::user()->id;
        }

        $repo = app(ChannelUsersRepositoryInterface::class);
        $channelUser = $repo->findWhere([
            'channel_id'    => $channelId,
            'user_id'   => $userId,
        ])->first();
        return $channelUser->default_overlay_type;
    }
}

if (!function_exists('getDefaultContentTypeId')) {
    /**
     * Default content type ID if a user channel
     * @param $channelId
     * @param $userId
     * @return int
     */
    function getDefaultContentTypeId($channelId, $userId = '') {

        if (!$userId) {
            $userId = Auth::user()->id;
        }

        $repo = app(ChannelUsersRepositoryInterface::class);
        $channelUser = $repo->findWhere([
            'channel_id'    => $channelId,
            'user_id'   => $userId,
        ])->first();
        return $channelUser->default_content_type;
    }
}

if (!function_exists('channelUserInfo')) {
    /**
     * Channel user info
     * @param $channelId
     * @param $userId
     * @return array
     */
    function channelUserInfo($channelId, $userId = '') {

        if (!$userId) {
            $userId = Auth::user()->id;
        }

        $repo = app(ChannelUsersRepositoryInterface::class);
        $channelUser = $repo->findWhere([
            'channel_id'    => $channelId,
            'user_id'   => $userId,
        ])->first();

        return $channelUser ? $channelUser->toArray() : [];
    }
}

if (!function_exists('createChannelStorage')) {
    /**
     * Create Channel Storage
     * @param $channelId
     * @param $channelName
     * @return array
     */
    function createChannelStorage($channelId, $channelName) {
        $channelStorage = str_slug($channelName) .'-'. $channelId;
        $resourcePath = config('filesystems.odmedia.resources') . '/' . $channelStorage;
        $thumbnailStorage = config('filesystems.odmedia.resources_thumbnail') . '/' . $channelStorage;
        if (!File::isDirectory($resourcePath)) {
            File::makeDirectory($resourcePath, 0777, true);
        }
        if (!File::isDirectory($thumbnailStorage)) {
            File::makeDirectory($thumbnailStorage, 0777, true);
        }
        return [
            'resource_path' => $channelStorage,
            'thumbnail_storage' => $channelStorage
        ];
    }
}

if (!function_exists('createChannelUserStorage')) {
    /**
     * Create Channel Storage
     * @param $resourcePath
     * @param $thumbnailPath
     * @param $userEmail
     * @param $userId
     * @return array
     */
    function createChannelUserStorage($resourcePath, $thumbnailPath, $userEmail, $userId) {
        $userStorage = generateUserPathStorage($userEmail, $userId);
        $resourceStorage = config('filesystems.odmedia.resources') . '/' . $resourcePath. '/' . $userStorage;
        $thumbnailStorage = config('filesystems.odmedia.resources_thumbnail') . '/' . $thumbnailPath. '/' . $userStorage;
        if (!File::isDirectory($resourceStorage)) {
            File::makeDirectory($resourceStorage, 0777, true);
        }
        if (!File::isDirectory($thumbnailStorage)) {
            File::makeDirectory($thumbnailStorage, 0777, true);
        }
        return [
            'resource_path' => $resourceStorage,
            'thumbnail_storage' => $thumbnailStorage
        ];
    }
}

if (!function_exists('generateUserPathStorage')) {
    /**
     * Create Channel Storage
     * @param $userEmail
     * @param $userId
     * @return string
     */
    function generateUserPathStorage($userEmail, $userId) {
        return strstr($userEmail, '@', true) .'-'. $userId;
    }
}
