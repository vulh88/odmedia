# Table of Contents

* [Resources](#resources)
* [Installation](#installation)
* [Packages](#packages)
* [Deployment](#deployment)
* [Errors](#errors)
* [API Codes](#api-codes)



# Resources

* ERD Diagram: 
* Requirements Specification:  
* Summary Logic: 
* Diagram: 
* Admin API Documentation: 
    
# Installation <a id="installation"></a>

- The **storage** and the **bootstrap/cache** directories should be writable by your web server.  

- Taking into account the Laravel requirements https://laravel.com/docs/5.6/installation 

- Create .env file, then enter database credential info

- Run the following command below:


```
composer update
php artisan key:generate
php artisan migrate
php artisan db:seed
php artisan module:seed

``` 

- Install passport for API auth


```
php artisan passport:install

```


- Copy client_ID and client_secret of the Password grant client from the generated terminal


```
PASSWORD_CLIENT_ID={ID}
PASSWORD_CLIENT_SECRET={secret}

``` 

- Generate API doc:


```
php artisan l5-swagger:generate

```

API doc URI: {your-domain}/api/documentation


- Front-end UI (Reactjs - Optional)

- Create file .env from .env.example

From the 'public/reactjs' run the following command, yarn requires node and npm:


```
yarn build

```

OR 

```
yarn start

```
to quick run

- [Important] FFmpeg: http://www.codebind.com/linux-tutorials/install-ffmpeg-ubuntu-16-04/

# API Codes

* 1002: You are not authorized on this channel