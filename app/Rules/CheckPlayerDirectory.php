<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckPlayerDirectory implements Rule
{

    private $value;

    private $repository;

    /**
     * Create a new rule instance.
     *
     * @param $repository
     */
    public function __construct($repository)
    {
        $this->repository = $repository;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $valid = false;
        $record = $this->repository->find($value);
        if ($record) {
            if ($record->player_directory) {
                $valid = true;
            }
        }
        return $valid;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Player resource directory is a required. Please recheck information in Scheduler settings';
    }
}
