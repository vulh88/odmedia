<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckChannelStorage implements Rule
{

    private $repository;

    /**
     * Create a new rule instance.
     *
     * @param $repository
     */
    public function __construct($repository)
    {
        $this->repository = $repository;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $valid = false;
        $record = $this->repository->find($value);
        if ($record->resource_path
            && $record->thumbnail_storage
            && ($record->resource_path != 'string' && $record->thumbnail_storage != 'string')) {
            $valid = true;
        }
        return $valid;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Please update data of current channel again';
    }
}
