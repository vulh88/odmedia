<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Setting;

class CheckExtensionFile implements Rule
{

    private $type;
    private $uploadExts;

    function __construct($type)
    {
        $this->type = $type;
        if ($this->type == 1) {
            $this->uploadExts = Setting::get('overlay_type_upload', 'mov');
        } else {
            $this->uploadExts = Setting::get('content_type_extensions', 'mp4');
        }
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!$this->filterMineType($value, $this->uploadExts)) {
            return false;
        }
        return true;
    }


    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $title = $this->type == 1 ? 'The overlay resource' : 'The content resource';
        return $title . ' file must be a file of type:' . $this->uploadExts;
    }

    /**
     * @param $file
     * @param $extensions
     * @return bool
     */
    private function filterMineType($file, $extensions)
    {
        $exExtensions = explode(',', $extensions);
        $extensionArr = [];
        foreach ($exExtensions as $v) {
            $type = trim($v);
            $extensionArr[] = $type;
        }
        if (!in_array($file->getClientOriginalExtension(), $extensionArr)) {
            return false;
        }
        return true;
    }
}
