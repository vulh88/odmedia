<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\User\Repositories\Contracts\ChannelUsersRepositoryInterface;

class CheckExistsChannelUser implements Rule
{
    /**
     * @var $user_id
     */
    private $userId;

    /**
     * @var $unAsisgn
     */
    private $unAsisgn;

    /**
     * Create a new rule instance.
     *
     * @param $userId
     * @param int $unAssign
     */
    public function __construct($userId, $unAssign = 0)
    {
        $this->userId = $userId;
        $this->unAsisgn = $unAssign;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $repository = app()->make(ChannelUsersRepositoryInterface::class);
        $count = $repository->findWhere([
                "channel_id" => $value,
                "user_id" => $this->userId
            ])->count();

        if ($this->unAsisgn) {
            return $count > 0;
        }

        return $count <= 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->unAsisgn ? __("This channel has not assigned to this user yet") : __("This channel is already assigned to this user");
    }
}
