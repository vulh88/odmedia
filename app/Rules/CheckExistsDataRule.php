<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckExistsDataRule implements Rule
{

    private $value;

    private $repository;

    /**
     * Create a new rule instance.
     *
     * @param $repository
     */
    public function __construct($repository)
    {
        $this->repository = $repository;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($value === 0) {
            return false;
        }

        if (!$this->repository->find($value)) {
            $this->value = $value;
            return false;
        }
        return true;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Can not find data with id ' . $this->value;
    }
}
