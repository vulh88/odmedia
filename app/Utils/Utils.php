<?php

namespace App\Utils;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\File;
use DateTime;

class Utils
{
    public static function renderSelectWithFoundRows($columns = array())
    {
        if (!empty($columns)) {
            $selectQuery = " SQL_CALC_FOUND_ROWS " . implode(',', $columns);
        } else {
            $selectQuery = " SQL_CALC_FOUND_ROWS * ";
        }
        return $selectQuery;
    }

    /**
     * Check all elms of an array are existed in another
     *
     * @param array @child
     * @param array $parent
     * @return boolean
     */
    public static function allElmsArrayExistedInArray($child, $parent)
    {
        if (count($child) > count($parent) || !is_array($child) || !is_array($parent)) return false;

        foreach($child as $c) {
            if (!in_array($c, $parent)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Combine array columns and alias
     *
     * @param array $columns
     * @param string $alias
     * @return array select
    */
    public static function combineArrayColumnsAndAlias($columns, $alias)
    {
        if (!$columns || is_string($columns)) return $alias. '.*';

        $select = [];
        foreach($columns as $column) {
            $select[] = $alias.".". $column;
        }
        return $select;
    }

    /**
     * Format Log data
     * @param array $input
     * @param string $line
     * @param string $function
     * @param string $class
     *
     * @return array
     */
    public static function formatLog($input, $line = '', $function = '', $class = '')
    {
        return array_merge($input, [
            'user_id' => Auth::user() ? Auth::user()->id : 'Testing',
            'ip' => Request::ip(),
            'line'  => $line,
            'function'  => $function,
            'class' => $class,
            'userAgent' => Request::header('User-Agent')
        ]);
    }

    /**
     * Check is base encode 64 image
     *
     * @param string $data
     * @return boolean
     */
    public static function isBase64EncodeImage($data)
    {
        $imageContents = base64_decode($data);

        // If its not base64 end processing and return false
        if ($imageContents === false) {
            return false;
        }

        $validExtensions = ['png', 'jpeg', 'jpg', 'gif'];

        $tempFile = tmpfile();

        fwrite($tempFile, $imageContents);

        $contentType = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $tempFile);

        fclose($tempFile);

        if (substr($contentType, 0, 5) !== 'image') {
            return false;
        }

        $extension = ltrim($contentType, 'image/');

        if (!in_array(strtolower($extension), $validExtensions)) {
            return false;
        }

        return true;
    }

    /**
     * Get country name based on country code
     *
     * @param collection $countries
     * @param string $countryCode
     *
     * @return string
     */
    public static function getCountryName($countries, $countryCode)
    {
        if ( $countries->isEmpty() || empty($countryCode) ) {
            return '';
        }

        $country = $countries->filter(function($value, $key) use ($countryCode) {
            return strtolower($value['code']) == strtolower($countryCode);
        })->first();

        return is_null($country) ? $countryCode : $country['name'];
    }

    /**
     * Get language name based on language code
     *
     * @param string $languageCode
     *
     * @return string
     */
    public static function getLanguageName($languageCode)
    {
        $languages = collect(json_decode(File::get(public_path() . '/languages.json'), true));
        if ( $languages->isEmpty() || empty($languageCode) ) {
            return '';
        }

        $language = $languages->filter(function($value, $key) use ($languageCode) {
            return strtolower($value['value']) == strtolower($languageCode);
        })->first();

        return is_null($language) ? $languageCode : $language['name'];
    }

    /**
     * Determine which currency need to multiply 100ths
     *
     * For example:
     * If you use USD currency and want to charge customer for $10, you must send to Stripe server the amount = 1000 cents.
     * Because the Stripe server get the amount in cents.
     * Refer: https://stripe.com/docs/api#charge_object-amount
     *
     * But for some currency, you don't need to multiply by 100ths because their smallest currency unit is 1.
     *
     * @param string $currency
     *
     * @refer: https://support.stripe.com/questions/which-zero-decimal-currencies-does-stripe-support
     * @return int
     */
    public static function getStripeCurrencyMultiplier($currency = '')
    {
        // default
        if ( empty($currency) ) {
            return 100;
        }

        // these currencies no need to multiply by 100ths
        $zeroDecimalCurrencies = [
            'BIF', 'CLP', 'DJF',
            'GNF', 'JPY', 'KMF',
            'KRW', 'MGA', 'PYG',
            'RWF', 'VND', 'VUV',
            'XAF', 'XOF', 'XPF'
        ];

        return in_array(strtoupper($currency), $zeroDecimalCurrencies) ? 1 : 100;
    }

    /**
     * Insert an element to array
     *
     * @param array $array
     * @param $element
     * @param integer $position
     * @return array
    */
    public static function arrayInsert($array, $element, $position)
    {
        // if the array is empty just add the element to it
        if(empty($array)) {
            $array[] = $element;
            // if the position is a negative number
        } elseif(is_numeric($position) && $position < 0) {
            // if negative position after count
            if(count($array) + $position < 0) {
                $position = 0;
            } else {
                $position = count($array) + $position;
            }
            // try again with a positive position
            $array = array_insert($array, $element, $position);
            // if array position already set
        } elseif(isset($array[$position])) {
            // split array into two parts
            $split1 = array_slice($array, 0, $position, true);
            $split2 = array_slice($array, $position, null, true);
            // add new array element at between two parts
            $array = array_merge($split1, array($position => $element), $split2);
            // if position not set add to end of array
        } elseif(is_null($position)) {
            $array[] = $element;
            // if the position is not set
        } elseif(!isset($array[$position])) {
            $array[$position] = $element;
        }
        // clean up indexes
        $array = array_values($array);
        return $array;
    }

    /**
     * Get points by key
     *
     * @param string $key
     * @author vulh
     * @return integer points
     */
    public static function getPointsByKey($key)
    {
        $pointRepo = app(PointSCDType2RepositoryInterface::class);
        return $pointRepo->getPointByKey($key);
    }

    /**
     * Cut string
     *
     * @param $text
     * @param integer $length
     * @param string $ending
     * @param bool $exact
     * @param bool $considerHtml
     * @return array
     */
    public static function truncation($text, $length = 180, $ending = '...', $exact = false, $considerHtml = true)
    {
        if ($considerHtml) {
            // if the plain text is shorter than the maximum length, return the whole text
            if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
                return [
                    'text'    => $text,
                    'hasMore'   => false
                ];
            }
            // splits all html-tags to scanable lines
            preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
            $total_length = strlen($ending);
            $open_tags = array();
            $truncate = '';
            foreach ($lines as $line_matchings) {
                // if there is any html-tag in this line, handle it and add it (uncounted) to the output
                if (!empty($line_matchings[1])) {
                    // if it's an "empty element" with or without xhtml-conform closing slash
                    if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
                        // do nothing
                        // if tag is a closing tag
                    } else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
                        // delete tag from $open_tags list
                        $pos = array_search($tag_matchings[1], $open_tags);
                        if ($pos !== false) {
                            unset($open_tags[$pos]);
                        }
                        // if tag is an opening tag
                    } else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
                        // add tag to the beginning of $open_tags list
                        array_unshift($open_tags, strtolower($tag_matchings[1]));
                    }
                    // add html-tag to $truncate'd text
                    $truncate .= $line_matchings[1];
                }
                // calculate the length of the plain text part of the line; handle entities as one character
                $content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
                if ($total_length+$content_length> $length) {
                    // the number of characters which are left
                    $left = $length - $total_length;
                    $entities_length = 0;
                    // search for html entities
                    if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
                        // calculate the real length of all entities in the legal range
                        foreach ($entities[0] as $entity) {
                            if ($entity[1]+1-$entities_length <= $left) {
                                $left--;
                                $entities_length += strlen($entity[0]);
                            } else {
                                // no more characters left
                                break;
                            }
                        }
                    }
                    $truncate .= substr($line_matchings[2], 0, $left+$entities_length);
                    // maximum lenght is reached, so get off the loop
                    break;
                } else {
                    $truncate .= $line_matchings[2];
                    $total_length += $content_length;
                }
                // if the maximum length is reached, get off the loop
                if($total_length>= $length) {
                    break;
                }
            }
        } else {
            if (strlen($text) <= $length) {
                return [
                    'text'    => $text,
                    'hasMore'   => false
                ];
            } else {
                $truncate = substr($text, 0, $length - strlen($ending));
            }
        }
        // if the words shouldn't be cut in the middle...
        if (!$exact) {
            // ...search the last occurance of a space...
            $spacepos = strrpos($truncate, ' ');
            if (isset($spacepos)) {
                // ...and cut the text in this position
                $truncate = substr($truncate, 0, $spacepos);
            }
        }
        // add the defined ending to the text
        $truncate .= $ending;
        if($considerHtml) {
            // close all unclosed html-tags
            foreach ($open_tags as $tag) {
                $truncate .= '</' . $tag . '>';
            }
        }
        return [
            'text'    => $truncate,
            'hasMore'   => strlen($text) > $length
        ];
    }

    /**
     * format number
     *
     * @param $number
     * @return string
     */
    public static function formatNumber($number)
    {
        return number_format($number, 0, '.', ',');
    }

    /**
     * Format current user name to abbreviation style
     * Example: Kate Johnson -> KJ
     *
     * @param string $username
     *
     * @ticket #12444
     * @return string
     */
    public static function formatUserNameToAbbreviationStyle($username = '')
    {
        if ( empty($username) ) {
            return $username;
        }

        $abbreviation = '';
        $words = explode(' ', $username);
        foreach($words as $word) {
            $abbreviation .= strtoupper($word[0]);
        }

        return $abbreviation;
    }

    /**
     * Is date time format
     * @param $date
     * @param string $format
     * @return bool
     */
    public static function validateDateTime($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }
}
