<?php

namespace App\Utils;


class MyDateTime
{
    /**
     * Correct date time formatting for MySQL
     * @param $time
     * @return false|string
     */
    public static function formatDateTimeForMySQL($time) {
        return date(config('api.mysql_format_datetime'), $time);
    }
}