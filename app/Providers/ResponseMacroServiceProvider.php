<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        // No content
        Response::macro('noContent', function ($status = 204) {
            return Response::json([], $status);
        });

        Response::macro('error', function ($message = '', $code = '', $status = 400) {
            return Response::json([
                'message' => $message,
                'code'    => $code,
            ], $status);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
