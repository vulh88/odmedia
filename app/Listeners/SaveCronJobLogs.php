<?php

namespace App\Listeners;

use App\Events\Cronjob;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use DB;
use Carbon\Carbon;

class SaveCronJobLogs
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Cronjob  $event
     * @return void
     */
    public function handle(Cronjob $event)
    {
        if (!empty($event->data)) {
            DB::table('cronjob_logs')->insert(
                [
                    'type' => $event->data['type'],
                    'description' => $event->data['description'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]
            );
        }
    }
}
