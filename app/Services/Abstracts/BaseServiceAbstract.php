<?php

namespace App\Services\Abstracts;

abstract class BaseServiceAbstract
{
    /**
     * Execute service
     * @param array $data
     * @return mixed
     */
    abstract public function execute($data = []);

    /**
     * After execute service
     * @return mixed
     */
    abstract public function afterExecuted();

    /**
     * Destruct
     *
     */
    public function __destruct()
    {
        $this->afterExecuted();
    }
}
