<?php

namespace App\Services\ApiTrait;


trait ResourceCollectionTrait
{
    private $total;
    private $currentPage;
    private $lastPage;

    /**
     * @return mixed
     */
    public function getLastPage()
    {
        return $this->lastPage;
    }

    /**
     * @param mixed $lastPage
     */
    public function setLastPage($lastPage)
    {
        $this->lastPage = $lastPage;
    }

    /**
     * @return mixed
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    /**
     * @param mixed $currentPage
     */
    public function setCurrentPage($currentPage)
    {
        $this->currentPage = $currentPage;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    public function __construct($resource, $total = 0, $currentPage = 1, $lastPage = 1)
    {
        $this->setTotal($total);
        $this->setCurrentPage($currentPage);
        $this->setLastPage($lastPage);
        parent::__construct($resource);
    }

}