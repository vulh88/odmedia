<?php

$defaultActions = ['read', 'create', 'edit', 'trash', 'destroy'];
$simpleActions = ['read', 'create', 'edit', 'destroy'];
$mediaPackageActions = ['read', 'create', 'edit', 'trash', 'delete'];

return [

    'roles' => ['Administrator', 'Scheduler'],

    'cache_file'  => 'role_permissions.json',
    'warning_form_message' => "You do not have permission to update some fields in this form",
    'permission_mapped_label'  => [
        'read'    => "Read",
        'create'    => "Create",
        'edit'  => 'Edit',
        'trash'  => 'Trash',
        'destroy'  => 'Destroy',
        'delete'  => 'Delete',
        'publish'  => 'publish',
    ],

    'simple_permissions'   => [
        'users' => [
            'label'   => 'User',
            'actions'   => array_merge($defaultActions, ['channel-settings', 'channels', 'update-channel'])
        ],
        'roles' => [
            'label'   => 'Role',
            'actions'   => $simpleActions
        ],
        'permissions' => [
            'label'   => 'Permission',
            'actions'   => ['read']
        ],
        'assignments' => [
            'label'   => 'Assignment',
            'actions'   => ['read', 'edit', 'getAssignedChannel']
        ],
        'channels' => [
            'label'   => 'Channel',
            'actions'   => $simpleActions
        ],
        'resources' => [
            'label'   => 'Resource',
            'actions'   => array_merge($simpleActions, ['import-API', 'preview-import-API', 'upload', 'authors', 'download'])
        ],
        'resource-types' => [
            'label'   => 'Resource type',
            'actions'   => $simpleActions
        ],
        'resource-folders' => [
            'label'   => 'Resource folder',
            'actions'   => array_merge($simpleActions, ['tree_folders'])
        ],
        'resource-tags' => [
            'label'   => 'Resource tag',
            'actions'   => $simpleActions
        ],
        'themes' => [
            'label'   => 'Theme',
            'actions'   => $simpleActions
        ],
        'formats' => [
            'label'   => 'Format',
            'actions'   => array_merge($defaultActions, ['duplicate'])
        ],
        'event-blocks' => [
            'label'   => 'Event blocks',
            'actions'   => array_merge($defaultActions, ['types'])
        ],
        'playlists' => [
            'label'   => 'Playlist',
            'actions'   => array_merge($defaultActions, ['publish', 'generate', 'duplicate', 'add-placeholder', 'resources', 'resources-for-downloading'])
        ],
    ],

    // Controller action name - Permission name
    'map_actions'   => [
        'index' => 'read',
        'more' => 'read',
        'show'  => 'read',
        'search'    => 'read',
        'update'    => 'edit',

        'store' => 'create',

        'edit-assign'   => 'edit',
        'update-assign' => 'edit',
        'restore'   => 'trash',

        // Media package
        'popup' => 'read',
        'list'  => 'read',
        'quota' => 'read',
        'download'  => 'read',
        'upload'    => 'create',
        'rename'    => 'edit',
        'delete'    => 'destroy',
    ],

    // Group with full permission
    'group_with_full_permissions'   => 'Administrator',
    'subscriber_role'   => 'Scheduler',

    // Actions that allows all users
    'free_permissions'  => [
        'admin.dashboard',
        'admin.logout',
    ],

    'map_mods'   => [
        'media'   => 'files'
    ],

    // Config parent template
    'root_view'   => 'admin.layout.admin',

    // Config route prefix
    'route_prefix'  => 'admin',

    // Publish assets to
    'publish_asset_to' => 'admin',

    // additional middleware here
    'middleware'    => '',

    // setting keys
    'settings' => [
        'super_admin_group' => 'super_admin_group',
    ],
    'ignore_user_repo'  => true,
    'default_limit' => 15,
    'default_guard_name'    => 'api',
];
