<?php

return [
    'default_limit' => 10,
    'format_datetime' => 'Y-m-d H:i:s',
    'overlay_track' => 1,
    'content_track' => 2,
    'mysql_format_datetime' => 'Y-m-d H:i:s',

];